<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Registers extends Model
{
    protected $table = 'registers';


    public function facebooks()
    {
        return $this->hasMany('App\Model\Facebooks',"reg_id");
    }


    //search keyword
    public static function scopeSearchKeyword($query, $search)
    {
    	$query = $query->where(function ($query2) use ($search) {
		                $query2->where('fullname', 'like', '%'.$search.'%')
		                    ->orWhere('email', 'like', '%'.$search.'%')
		                    ->orWhere('lineid', 'like', '%'.$search.'%');
	            	});
        return $query;
    }
    //end search keyword

    //Default Fix order by
    public static function scopeFilterOrderBy($query,$field="order_by",$ascdesc="ASC")
    {
        return $query->orderBy($field,$ascdesc);
        //return $query->where('title_th', 'like', '%'.$search.'%');
    }
}
