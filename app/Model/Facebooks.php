<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Facebooks extends Model
{
    protected $table = 'facebooks';


    public function registers()
    {
        return $this->belongsTo('App\Model\Registers');
    }
}
