<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Users extends Model
{

    protected $table = 'users';

    public $timestamps = true;

    public static function storeUsers($request, $arr_data)
    {

        $ip_address = $request->ip();

        if ($request->ip() == "::1") {
            $ip_address = "127.0.0.1";
        }

        $status = (isset($request->status)) ? $request->status : "N";

        $group = (isset($request->group)) ? $request->group : 2;

        if (isset($arr_data["id"]) && $arr_data["id"] != "") {

            DB::beginTransaction();

            try {

                if (isset($request->password) && $request->password != "") {

                    DB::table("users")->where('id', $arr_data["id"])->update([

                        'username' => $request->username,

                        'name' => $request->name,

                        'email' => $request->email,

                        'password' => bcrypt($request->password),

                        'status' => $status,

                        'updated_at' => date("Y-m-d H:i:s"),

                    ]);
                } else {

                    DB::table("users")->where('id', $arr_data["id"])->update([

                        'username' => $request->username,

                        'name' => $request->name,

                        'email' => $request->email,

                        'status' => $status,

                        'updated_at' => date("Y-m-d H:i:s"),

                    ]);
                }

                DB::table("role_user")->where('user_id', $arr_data["id"])->update([

                    'role_id' => $group,

                    'user_type' => 'App\User',

                ]);

                DB::commit();

                return "Y";

                // all good

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;

                return "N";

                // something went wrong

            } catch (\Throwable $e) {

                DB::rollback();

                throw $e;

                return "N";
            }
        } else {

            DB::beginTransaction();

            try {

                $user_id = DB::table("users")->insertGetId([

                    'username' => $request->username,

                    'name' => $request->name,

                    'email' => $request->email,

                    'password' => bcrypt($request->password),

                    'status' => $status,

                    'created_at' => date("Y-m-d H:i:s"),

                    'updated_at' => date("Y-m-d H:i:s"),

                ]);

                $insert_id = DB::table("role_user")->insertGetId([

                    'role_id' => $group,

                    'user_id' => $user_id,

                    'user_type' => 'App\User',

                ]);

                DB::commit();

                return "Y";

                // all good

            } catch (\Exception $e) {

                DB::rollback();

                throw $e;

                return "N";

                // something went wrong

            } catch (\Throwable $e) {

                DB::rollback();

                throw $e;

                return "N";
            }
        }
    }

    //get admin

    public static function getRoles()
    {

        $arr_data = array();

        $results = DB::table("role_user")->orderBy('user_id', 'ASC')->get();

        foreach ($results as $result) {

            $arr_data[$result->user_id] = $result->role_id;
        }

        return $arr_data;
    }

    //end get admin

    //get roles group

    public static function getRolesGroup()
    {

        $role = DB::table("role_user")->where('user_id', Auth::user()->id)->first();

        if ($role->role_id == 1) {
            $results = DB::table("roles")->orderBy('id', 'ASC')->get();
        } else {
            $results = DB::table("roles")->where('id', '!=', 1)->orderBy('id', 'ASC')->get();
        }

        return $results;
    }

    //end get roles group

}
