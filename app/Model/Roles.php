<?php



namespace App\Model;



use Illuminate\Database\Eloquent\Model;

use Carbon\Carbon;

use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;



class Roles extends Model

{

    protected $table = 'roles';

    public $timestamps = true;



    public static function storeRoles($request,$arr_data)

    {

        $ip_address = $request->ip();

        if ($request->ip()=="::1")

            $ip_address = "127.0.0.1";



        $status = (isset($request->status))?$request->status:"N";

        



        if (isset($arr_data["id"]) && $arr_data["id"]!="")

        {

            DB::beginTransaction();

              try {

                DB::table("roles")->where('id', $arr_data["id"])->update([

                    'name' => $request->name,

                    'display_name' => $request->display_name,

                    'updated_at' => date("Y-m-d H:i:s")

                ]);



                $role_id = $arr_data["id"];



                DB::commit();

                //return "Y";

                    // all good

              } catch (\Exception $e) {

                DB::rollback();

                //throw $e;

                //return "N";

                    // something went wrong

              } catch (\Throwable $e) {

                DB::rollback();

                //throw $e;

                //return "N";

              }

        }

        else

        {

            DB::beginTransaction();

            try {

                $role_id = DB::table("roles")->insertGetId([

                    'name' => $request->name,

                    'display_name' => $request->display_name,

                    'created_at' => date("Y-m-d H:i:s"),

                    'updated_at' => date("Y-m-d H:i:s")

                ]);



                DB::commit();

                //return "Y";

                    // all good

            } catch (\Exception $e) {

                DB::rollback();

                //throw $e;

                //return "N";

                    // something went wrong

            } catch (\Throwable $e) {

                DB::rollback();

                //throw $e;

                //return "N";

            }

        }



        DB::table("permission_role")->where('role_id',$role_id)->delete();

        DB::beginTransaction();

        try {

            $results = DB::table("permissions")->get();



            if (isset($results))

            {

                foreach ($results as $result)

                {

                    if (isset($request->chk_permission[$result->id]) && $request->chk_permission[$result->id]!="")

                    {

                        $result_permission = DB::table("permission_role")->insertGetId([

                            'permission_id' => $result->id,

                            'role_id' => $role_id

                        ]);

                    }

                }

            }



            DB::commit();

            return "Y";

                    // all good

        } catch (\Exception $e) {

            DB::rollback();

                //throw $e;

            return "N";

                    // something went wrong

        } catch (\Throwable $e) {

            DB::rollback();

                //throw $e;

            return "N";

        }

    }





    //get admin

    public static function getRoles()

    {

    	$arr_data = array();

        $results = DB::table("role_user")->orderBy('user_id','ASC')->get();

        foreach ($results as $result)

        {

        	$arr_data[$result->user_id] = $result->role_id;

        }



        return $arr_data;

    }

    //end get admin



    //get permission name

    public static function getPermissionName()

    {

        $arr_data = array();

        $results = DB::table("permissions")->orderBy('order_by','ASC')->get();

        if (isset($results) && sizeof($results)>0)

        {

            foreach ($results as $result)

            {

                $arr_data[$result->groups][$result->id] = $result->display_name;

            }

        }

        return $arr_data;

    }

    //end get permission name



    //get permission role

    public static function getPermissionByUser($id)

    {

        $arr_data = array();

        //get permission role

        $results = DB::table("permission_role")->where('role_id',$id)->get();

        if (isset($results) && sizeof($results)>0)

        {

            foreach ($results as $result)

            {

                $arr_data[$result->permission_id] = true;

            }

        }



        return $arr_data;

    }

    //end get permission role

}

