<?php

namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Home extends Model
{
    protected $fillable = ['title_th','title_en','detail_th','detail_en','content_th','content_en','show','status','order_by'];
    protected $table = 'rooms_type';
    public $timestamps = true;


    public static function getLang($lang)
    {
        if (isset($lang) && $lang=="th")
        {
            session(['locale' => "th"]);
            //Session::put('locale',"th");
            $locale = Session::get('locale'); // ถ้ามีอยู่ก็ดึงค่าออกมาเก็บไว้
            App::setLocale($locale); // ตั้งค่าที่ได้รับมาให้เป็นภาษาหลัก
        }
        else
        {
            session(['locale' => "en"]);
            //Session::put('locale',"en");
            $locale = Session::get('locale'); // ถ้ามีอยู่ก็ดึงค่าออกมาเก็บไว้
            App::setLocale($locale); // ตั้งค่าที่ได้รับมาให้เป็นภาษาหลัก
        }
    }

    public static function getOptionRoomsType($lang)
    {
      $rooms_type = self::all();
      foreach ($rooms_type as $type)
      {
          if (isset($lang) && $lang=="th")
          {
            $type_array[$type->id] = $type->title_th;
          }
          else
          {
            $type_array[$type->id] = $type->title_en;
          }
      }
      return $type_array;
    }
}
