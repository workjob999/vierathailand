<?php
namespace App\Model\Front;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Functions\Functionphp;

class Homes extends Model
{
    protected $table = 'homes';
    public $timestamps = true;


    public static function getHomes()
    {
        $lang = Session::get('locale');
        $result = DB::table("homes")->where([
            ['status','Y'],
            ['id',1]
        ])->orderBy('order_by','ASC')->orderBy("updated_at","DESC")->first();
                
        $exp_created_date = explode(" ",$result->created_at);
        $exp_created_at = explode("-",$exp_created_date[0]);
        $pathDate = $exp_created_at[0]."/".$exp_created_at[1];
        $result->pathDate = $pathDate;
        return $result;
    }


    public static function getHomeItems()
    {
        $lang = Session::get('locale');
        $results = DB::table("home_items")->where([
            ['status','Y'],
            ['home_id',1]
        ])->orderBy('order_by','ASC')->orderBy("updated_at","DESC")->get();
                
        if (isset($results) && sizeof($results)>0)
        {
            foreach ($results as $result)
            {
                $exp_created_date = explode(" ",$result->created_at);
                $exp_created_at = explode("-",$exp_created_date[0]);
                $pathDate = $exp_created_at[0]."/".$exp_created_at[1];
                $result->pathDate = $pathDate;
            }
        }
        
        return $results;
    }
}