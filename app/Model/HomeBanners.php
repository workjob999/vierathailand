<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class HomeBanners extends Model
{
    protected $table = 'home_banners';


    //search keyword
    public static function scopeSearchKeyword($query, $search)
    {
    	$query = $query->where(function ($query2) use ($search) {
                        $query2->where('title_th', 'like', '%'.$search.'%')
		                    ->orWhere('title_en', 'like', '%'.$search.'%')
		                    ->orWhere('title_ch', 'like', '%'.$search.'%');
	            	});
        return $query;
    }
    //end search keyword

    //Default Fix order by
    public static function scopeFilterOrderBy($query,$field="order_by",$ascdesc="ASC")
    {
        return $query->orderBy($field,$ascdesc);
        //return $query->where('title_th', 'like', '%'.$search.'%');
    }
}
