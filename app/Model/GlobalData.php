<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class GlobalData extends Model
{
    protected $table = 'users';
    public $timestamps = true;

    //get user backend
    public static function getUsersData()
    {
    	$arr_data = array();
        $results = DB::table("users")->orderBy('id','ASC')->get();
        foreach ($results as $result)
        {
            $arr_data[$result->id]["username"] = $result->username;
            $arr_data[$result->id]["name"] = $result->name;
            $arr_data[$result->id]["email"] = $result->email;
        }

        return $arr_data;
    }
    //end get user backend


    //get provinces
    public static function getProvinces()
    {
        $arr_data = array();
        $results = DB::table("provinces")->orderBy('name','ASC')->get();
        foreach ($results as $result)
        {
            $arr_data[$result->id] = $result->name;
        }

        return $arr_data;
    }
    //end get provinces


    //get Amphur
    public static function getAmphurs($id)
    {
        $arr_data = array();
        $results = DB::table("amphures")->where('province_id',$id)->orderBy('name','ASC')->get();
        foreach ($results as $result)
        {
            $arr_data[$result->id] = $result->name;
        }

        return $arr_data;
    }
    //end get Amphur


    //get Tambon
    public static function getTambons($id)
    {
        $arr_data = array();
        $results = DB::table("tambons")->where('amphur_id',$id)->orderBy('name','ASC')->get();
        foreach ($results as $result)
        {
            $arr_data[$result->code] = $result->name;
        }

        return $arr_data;
    }
    //end get Tambon


    //get Tambon
    public static function getPostcodes($id)
    {
        $result = DB::table("postcodes")->where('tambon_code',$id)->orderBy('code','ASC')->first();

        return $result->code;
    }
    //end get Tambon
}
