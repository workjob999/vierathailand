<?php

namespace App\Model\Functions;

use Illuminate\Database\Eloquent\Model;
use App\Model\Webadmin\Vendors;
use Excel;
use PHPExcel_Worksheet_Drawing;
use PHPExcel_Style_Alignment;
use PHPExcel_Style_NumberFormat;
use PHPExcel_Style_Fill;
use PHPExcel_Style_Border;
use PHPExcel_Worksheet_PageSetup;

class ExportFile extends Model
{
    public static function export_po($purchaseOrder,$type = "xlsx")
    {
        Excel::create('PO'.$purchaseOrder['id'], function($excel) use($purchaseOrder){

          $excel->sheet('Sheet1', function($sheet) use($purchaseOrder){

            // $sheet->fromArray($data, null, 'A1', true, false);
            $objDrawing = new PHPExcel_Worksheet_Drawing;
            $objDrawing->setPath('assets/img/purchase_order/image1.png'); 
            $objDrawing->setCoordinates('A1');          
            $objDrawing->setOffsetX(50); 
            $objDrawing->setOffsetY(5); 
            $objDrawing->setWidth(180); 
            $objDrawing->setHeight(50); 
            $objDrawing->setWorksheet($sheet);
            $sheet->getRowDimension(1)->setRowHeight(45);

            // 
            // PO Title
            // 
            $sheet->mergeCells('D1:H1')->setCellValue('D1', "Mercular CO., LTD\rPurchase Order (PO)");

            $styleArray = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                ),
                'font'  => array(
                    'bold'  => true,
                    'size'  => 16
                ),
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'c6deb6')
                )
            );
            $sheet->getStyle('D1')->applyFromArray($styleArray);

            // 
            // PO Number/Location/Date/Time
            // 
            $sheet
                ->setCellValue('B3', 'P/O Number:')
                ->setCellValue('C3', $purchaseOrder['id'])
                ->setCellValue('B4', 'P/O Location :')
                ->setCellValue('B5', 'Date :')
                ->setCellValue('C5', date("d/m/y"))
                ->setCellValue('F3', 'Time :')
                ->setCellValue('G3', date("H:i").' น.');

            $sheet
                ->getStyle('C3')
                ->getNumberFormat()
                ->setFormatCode( PHPExcel_Style_NumberFormat::FORMAT_TEXT );

            // 
            // Add Reciept Address
            // 
            $sheet
                ->setCellValue('B6', 'ที่อยู่ออกใบเสร็จ :')
                ->setCellValue('C6', 'บริษัท เมอร์คูลาร์ จำกัด (สาขาที่ 00001)')
                ->setCellValue('C7', 'เลขที่ 138 อาคารบุญมิตร ชั้น 12 ถนนสีลม แขวงสุริยวงศ์')
                ->setCellValue('C8', 'เขตบางรัก กรุงเทพมหานคร 10500');

            // 
            // Add Company Address
            // 
            $sheet
                ->setCellValue('B12', 'ที่อยู่จัดส่งสินค้า :')
                ->setCellValue('C12', 'บริษัท เมอร์คูลาร์ จำกัด')
                ->setCellValue('C13', 'เลขที่ 138 อาคารบุญมิตร ชั้น 12 ห้อง บี 6')
                ->setCellValue('C14', 'ถนนสีลม แขวงสุริยวงศ์ เขตบางรัก ')
                ->setCellValue('C15', 'กรุงเทพฯ 10500')
                ->setCellValue('F10', 'Tel No: ')
                ->setCellValue('G10', '0983698553')
                ->setCellValue('F10', 'Fax No: ')
                ->setCellValue('F13', 'Ordering Person : ภูมิ พืชไพจิตร');
            
            // 
            // Add Header
            // 
            $sheet
                ->setCellValue('A18', 'รายการ')
                ->setCellValue('B18', 'SKU')
                ->setCellValue('C18', 'ชื่อยี่ห้อ')
                ->setCellValue('D18', 'ชื่อรายการสินค้า')
                ->setCellValue('E18', 'สี')
                ->setCellValue('F18', 'จำนวนชิ้น')
                ->setCellValue('G18', "ราคาทุนต่อหน่วย\r(รวม Vat)")
                ->setCellValue('H18', "ราคาทุนสุทธิต่อหน่วย\r(ไม่รวม Vat)")
                ->setCellValue('I18', 'ส่วนลดพิเศษ (%)')
                ->setCellValue('J18', "จำนวนเงิน สุทธิ\rไม่รวม  Vat");

            // 
            // Vendor detail
            // 
            $vendor = Vendors::find($purchaseOrder['vendor']);

            $sheet
                ->setCellValue('F5', 'Vendor Code')
                ->setCellValue('G5', $vendor['id'])
                ->setCellValue('F6', 'Vendor Name')
                ->setCellValue('G6', $vendor['name'])
                ->setCellValue('I13', 'Payment Terms:')
                ->setCellValue('J13', $vendor['payment_terms']);

            $styleArray = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_LEFT
                )
            );

            $sheet->getStyle('G5')->applyFromArray($styleArray);

            $styleArray = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                ),
                'font'  => array(
                    'bold'  => true
                )
            );

            $sheet->getStyle('J13')->applyFromArray($styleArray);

            $styleArray = array(
                'fill' => array(
                    'type' => PHPExcel_Style_Fill::FILL_SOLID,
                    'color' => array('rgb' => 'DAE5F2')
                )
            );

            $sheet->getStyle('G5:G6')->applyFromArray($styleArray);
            $sheet->getStyle('J13')->applyFromArray($styleArray);

            //
            // entryDate,shipDate,Etc.
            //
            $sheet
                ->setCellValue('I6', 'Entry Date:')
                ->setCellValue('I7', 'Ship Date:')
                ->setCellValue('I8', 'Received Date:')
                ->setCellValue('I9', 'Receiver#:')
                ->setCellValue('J6', date("d/m/y"));


            // 
            // Style :: Table header color
            // 
            $sheet
            ->getStyle('A18:J18')
            ->getFill()
            ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
            ->getStartColor()
            ->setARGB('FFbbbbbb');

            $styleArray = array(
              'alignment' => array(
                      'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                      'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
                  )
            );

            $sheet->getStyle('A18:J18')->applyFromArray($styleArray);
            $sheet->getStyle('A18:J18')->getAlignment()->setWrapText(true);

            // 
            // Style :: Border Cell
            // 
            $styleArray = array(
              'borders' => array(
                'allborders' => array(
                  'style' => PHPExcel_Style_Border::BORDER_THIN
                )
              )
            );

            $sheet->getStyle('A18:J18')->applyFromArray($styleArray);

            // 
            // Style :: Border Cell
            // 
            $row = 18;
            $sheet->setHeight(array(
              $row     =>  60
            ));

            $sheet->setWidth(array(
              'A'     =>  8,
              'B'     =>  15,
              'C'     =>  15,
              'D'     =>  50,
              'E'     =>  10,
              'F'     =>  12,
              'G'     =>  18,
              'H'     =>  15,
              'I'     =>  15,
              'J'     =>  15
            ));

            $i = 1;
            foreach ($purchaseOrder['detail'] as &$order) {

                $sheet
                    ->setCellValue('A'.($row+$i), $i)
                    ->setCellValue('B'.($row+$i), $order['item']['SKU_FR'])
                    ->setCellValue('C'.($row+$i), $order['item']['B_NAME'])
                    ->setCellValue('D'.($row+$i), $order['item']['SKU_DESCR'])
                    ->setCellValue('E'.($row+$i), $order['item']['COLOR'])
                    ->setCellValue('F'.($row+$i), $order['qty'])
                    ->setCellValue('G'.($row+$i), $order['item']['REG_COST_VAT'])
                    ->setCellValue('H'.($row+$i), "=ROUND(G".($row+$i)."/1.07,2)")
                    ->setCellValue('I'.($row+$i), $order['item']['discount']/100)
                    ->setCellValue('J'.($row+$i), "=ROUND(G".($row+$i)."/1.07*(1-I".($row+$i).")*F".($row+$i)." ,2)");

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                // Comma SEPARATED
                $sheet->getStyle('G'.($row+$i))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle('H'.($row+$i))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
                $sheet->getStyle('J'.($row+$i))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);


                $sheet->getStyle('A'.($row+$i).':J'.($row+$i))->applyFromArray($styleArray);

                $styleArray = array(
                    'alignment' => array(
                        'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                    )
                );
                $sheet->getStyle('A'.($row+$i))->applyFromArray($styleArray);
                $sheet->getStyle('B'.($row+$i))->applyFromArray($styleArray);
                $sheet->getStyle('C'.($row+$i))->applyFromArray($styleArray);
                $sheet->getStyle('E'.($row+$i))->applyFromArray($styleArray);
                $sheet->getStyle('F'.($row+$i))->applyFromArray($styleArray);

                $i++;
            }

            //
            // Delimiter
            //
            for ($j=0; $j < 3; $j++) { 

                $styleArray = array(
                    'borders' => array(
                        'allborders' => array(
                            'style' => PHPExcel_Style_Border::BORDER_THIN
                        )
                    )
                );

                $sheet->getStyle('A'.($row+$i+$j).':J'.($row+$i+$j))->applyFromArray($styleArray);
            }

            //
            // Summary
            //

            $firstRow = $row+1;
            $LastRow = $row+$i+$j-1;


            $sheet
              ->setCellValue('I'.($row+$i+$j+2), 'ยอดรวมทั้งสิ้น')
              ->setCellValue('J'.($row+$i+$j+2), "=ROUND(SUMPRODUCT(F".$firstRow.":F".$LastRow.",G".$firstRow.":G".$LastRow."),2)");

            $sheet
              ->setCellValue('I'.($row+$i+$j), 'มูลค่าสินค้า')
              ->setCellValue('I'.($row+$i+$j+1), 'ภาษีมูลค่าเพิ่ม')
              ->setCellValue('J'.($row+$i+$j), "=ROUND(J".($row+$i+$j+2)."/1.07,2)")
              ->setCellValue('J'.($row+$i+$j+1), "=J".($row+$i+$j+2)."-J".($row+$i+$j));


            $sheet->getStyle('J'.($row+$i+$j))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->getStyle('J'.($row+$i+$j+1))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);
            $sheet->getStyle('J'.($row+$i+$j+2))->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_NUMBER_COMMA_SEPARATED1);


            $styleArray = array(
                'borders' => array(
                    'allborders' => array(
                        'style' => PHPExcel_Style_Border::BORDER_THIN
                    )
                )
            );

            $sheet->getStyle('I'.($row+$i+$j).':J'.($row+$i+$j+2))->applyFromArray($styleArray);

            //
            // Sign
            //
            $sheet->mergeCells('E'.($row+$i+$j+6).':F'.($row+$i+$j+6));  
            $sheet->setCellValue('E'.($row+$i+$j+6), "...............................");
            $sheet->mergeCells('E'.($row+$i+$j+7).':F'.($row+$i+$j+7));  
            $sheet->setCellValue('E'.($row+$i+$j+7), "ผู้สั่งซื้อ");

            $styleArray = array(
                'alignment' => array(
                    'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER
                )
            );

            $sheet->getStyle('E'.($row+$i+$j+7))->applyFromArray($styleArray);

            // Sign Image
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing->setPath('assets/img/purchase_order/image2.png'); 
            $objDrawing->setCoordinates('E'.($row+$i+$j+3));          
            $objDrawing->setOffsetX(50); 
            $objDrawing->setOffsetY(0); 
            $objDrawing->setWidth(250); 
            $objDrawing->setHeight(65); 
            $objDrawing->setWorksheet($sheet);

            //
            // Page setup
            //
            $sheet->getPageSetup()->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A4);
            $sheet->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
            $sheet->getPageSetup()->setFitToPage(true);
            $sheet->getPageSetup()->setFitToWidth(1);
            $sheet->getPageSetup()->setFitToHeight(0);

          });

        })->download($type);
    }
}
