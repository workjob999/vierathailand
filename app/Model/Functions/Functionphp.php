<?php

namespace App\Model\Functions;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class Functionphp extends Model
{
    public static function php_listFolder($path)
    {
      $file_names = @scandir($path);
      $array_dir = array();

      if ($file_names && sizeof($file_names)>0)
      {
        foreach($file_names as $value)
        {
          if ($value != ".." && $value != ".")
          {
            $array_dir[$value] = strtr($value,"_"," ");
          }
        }
      }
      
      return $array_dir;
    }


    public static function php_listImageInFolder($path)
    {
      $file_names = @scandir($path);
      $array_dir = array();
      $indexLoop = 0;

      if ($file_names && sizeof($file_names)>0)
      {
        foreach($file_names as $value)
        {
          if ($value != ".." && $value != ".")
          {
            $indexLoop++;

            $size = filesize($path."/".$value); // หาขนาดของไฟล์ได้มาเป็น Bytes
            $array_dir[$indexLoop]["file"] = $value;
            $array_dir[$indexLoop]["name"] = strtr($value,"_"," ");
            $array_dir[$indexLoop]["size"] = $size;
          }
        }
      }
      
      return $array_dir;
    }


    public static function php_showDateTime($datetime,$lang="th",$type="short")
    {
      $config_lang["en"]["long"] = array("January","February","March","April","May","June","July","August","September","October","November","December");
      $config_lang["en"]["short"] = array("Jan.","Feb.","Mar.","Apr.","May.","Jun.","Jul.","Aug.","Sep.","Oct.","Nov.","Dec.");
      $config_lang["th"]["long"] = array("มกราคม","กุมภาพันธ์","มีนาคม","เมษายน","พฤษภาคม","มิถุนายน","กรกฎาคม","สิงหาคม","กันยายน","ตุลาคม","พฤศจิกายน","ธันวาคม");
      $config_lang["th"]["short"] = array("ม.ค.","ก.พ.","มี.ค.","เม.ย.","พ.ค.","มิ.ย.","ก.ค.","ส.ค.","ก.ย.","ต.ค.","พ.ย.","ธ.ค.");
      
      $return = array();
      
      //format Day
      $return["d"] = date("d",$datetime);
      $return["D"] = date("D",$datetime);
      $return["j"] = date("j",$datetime);
      $return["l"] = date("l",$datetime);
      $return["S"] = date("S",$datetime);
      //format Day
      
      //format month
      $return["m"] = date("m",$datetime);
      $return["n"] = date("n",$datetime);
      $return["month"] = $config_lang[$lang][$type][((int)(date("n",$datetime))-1)];
      //format month
      
      //format year
      if ($lang=="en")
      {
        $return["Y"] = date("Y",$datetime);
        $return["y"] = date("y",$datetime);
      }
      else if ($lang=="th")
      {
        $return["Y"] = (int)(date("Y",$datetime))+543;
        $return["y"] = ((int)(date("Y",$datetime))+543)%100;
      }
      //format year
      
      //format time
      $return["g"] = date("g",$datetime);
      $return["G"] = date("G",$datetime);
      $return["h"] = date("h",$datetime);
      $return["H"] = date("H",$datetime);
      $return["i"] = date("i",$datetime);
      $return["s"] = date("s",$datetime);
      //format time

      return $return;
    }


    public static function php_convertTimeDBToShow($time)
    {
      $new_time = "";
      if ($time!="" && $time!==NULL)
      {
        $exp_time = explode(":",$time);
        $new_time = $exp_time[0].":".$exp_time[1];
      }
      
      return $new_time;
    }


    //taken from wordpress
    public static function php_utf8_uri_encode( $utf8_string, $length = 0 ) {
        $unicode = '';
        $values = array();
        $num_octets = 1;
        $unicode_length = 0;

        $string_length = strlen( $utf8_string );
        for ($i = 0; $i < $string_length; $i++ ) {

            $value = ord( $utf8_string[ $i ] );

            if ( $value < 128 ) {
                if ( $length && ( $unicode_length >= $length ) )
                    break;
                $unicode .= chr($value);
                $unicode_length++;
            } else {
                if ( count( $values ) == 0 ) $num_octets = ( $value < 224 ) ? 2 : 3;

                $values[] = $value;

                if ( $length && ( $unicode_length + ($num_octets * 3) ) > $length )
                    break;
                if ( count( $values ) == $num_octets ) {
                    if ($num_octets == 3) {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]) . '%' . dechex($values[2]);
                        $unicode_length += 9;
                    } else {
                        $unicode .= '%' . dechex($values[0]) . '%' . dechex($values[1]);
                        $unicode_length += 6;
                    }

                    $values = array();
                    $num_octets = 1;
                }
            }
        }

        return $unicode;
    }

    //taken from wordpress
    public static function php_seems_utf8($str) {
        $length = strlen($str);
        for ($i=0; $i < $length; $i++) {
            $c = ord($str[$i]);
            if ($c < 0x80) $n = 0; # 0bbbbbbb
            elseif (($c & 0xE0) == 0xC0) $n=1; # 110bbbbb
            elseif (($c & 0xF0) == 0xE0) $n=2; # 1110bbbb
            elseif (($c & 0xF8) == 0xF0) $n=3; # 11110bbb
            elseif (($c & 0xFC) == 0xF8) $n=4; # 111110bb
            elseif (($c & 0xFE) == 0xFC) $n=5; # 1111110b
            else return false; # Does not match any model
            for ($j=0; $j<$n; $j++) { # n bytes matching 10bbbbbb follow ?
                if ((++$i == $length) || ((ord($str[$i]) & 0xC0) != 0x80))
                    return false;
            }
        }
        return true;
    }

    //function sanitize_title_with_dashes taken from wordpress
    public static function php_sanitize($title) {
        $title = strip_tags($title);
        // Preserve escaped octets.
        $title = preg_replace('|%([a-fA-F0-9][a-fA-F0-9])|', '---$1---', $title);
        // Remove percent signs that are not part of an octet.
        $title = str_replace('%', '', $title);
        // Restore octets.
        $title = preg_replace('|---([a-fA-F0-9][a-fA-F0-9])---|', '%$1', $title);

        if ($this->php_seems_utf8($title)) {
            if (function_exists('mb_strtolower')) {
                $title = mb_strtolower($title, 'UTF-8');
            }
            $title = $this->php_utf8_uri_encode($title, 200);
        }

        $title = strtolower($title);
        $title = preg_replace('/&.+?;/', '', $title); // kill entities
        $title = str_replace('.', '-', $title);
        $title = preg_replace('/[^%a-z0-9 _-]/', '', $title);
        $title = preg_replace('/\s+/', '-', $title);
        $title = preg_replace('|-+|', '-', $title);
        $title = trim($title, '-');

        return $title;
    }


    public static function getSplitSKU($sku)
    {
        $arr_data = array();
        $arr_data["brands"] = intval(substr($sku,0,4));
        $arr_data["models"] = intval(substr($sku,4,3));
        $arr_data["attributes"] = intval(substr($sku,7,3));
        $arr_data["colours"] = intval(substr($sku,10,3));
        return $arr_data;
    }


    public static function convertMoneyToNumber($money)
    {
        $imp_money = $money;
        $exp_money = explode(",",$money);
        if (sizeof($money)>0)
        {
            $imp_money = implode("",$exp_money);
        }

        return $imp_money;
    }


    public static function php_gen_key_order($key_len)
    {
        $key = "";

        $letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $len = strlen($letters);
        $letter = $letters[rand(0, $len-1)];
        for ($i = 0; $i< $key_len;$i++) {
          $letter = $letters[rand(0, $len-1)];
          $key.=$letter;
        }

        return $key;
    }


    public static function php_convertDateDB($date)
    {
      $new_date = "";
      if ($date=="0000-00-00")
        $new_date = $date;
      else if ($date!="" && $date!==NULL)
      {
        $exp_date = explode("/",$date);
        $new_date = $exp_date[2]."-".$exp_date[1]."-".$exp_date[0];
      }
      
      return $new_date;
    }


    public static function php_convertDBToShow($date)
    {
      $new_date = "";
      if ($date!="")
      {
        $exp_date = explode("-",$date);
        $new_date = $exp_date[2]."/".$exp_date[1]."/".$exp_date[0];
      }
      
      return $new_date;
    }


    public static function php_convertDateToPathFile($date)
    {
      $pathFolder = "";
      if ($date!==null && $date!=="")
      {
        $exp_date = explode("-",$date);
        $pathFolder = $exp_date[0]."/".$exp_date[1];
      }
      return $pathFolder;
    }
}
