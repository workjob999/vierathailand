<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Api\PromotionsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    //index
    public function index()
    {
        // $apiPromotions = new PromotionsApiController;
        // $promotions = $apiPromotions->getPromotionsFront($request, 0);

        $data = array();

        return view('front.promotion')->with($data);
    }

    public function details()
    {
        $data = array();

        return view('front.promotion-details')->with($data);
    }

    public function index_viera(Request $request, $lang)
    {
        //$homePromotions = new HomePromotionsApiController;
        //$promotions = $homePromotions->getHomePromotionsFront($request, 0);
        $homePromotions = new PromotionsApiController;
        $promotions = $homePromotions->getPromotionsFront($request, 0);

        if (isset($promotions) && sizeof($promotions) > 0) {
            foreach ($promotions as $promotion) {
                $promotion->pathFile = Functionphp::php_convertDateToPathFile($promotion->created_at);

                if ($lang == "en" && $promotion->status_en == "Y") {
                    $promotion->thumb = $promotion->thumb_en;
                    $promotion->title = $promotion->title_en;
                    $promotion->detail = $promotion->detail_en;
                } else if ($lang === "ch" && $promotion->status_ch == "Y") {
                    $promotion->thumb = $promotion->thumb_ch;
                    $promotion->title = $promotion->title_ch;
                    $promotion->detail = $promotion->detail_ch;
                } else {
                    $promotion->thumb = $promotion->thumb_th;
                    $promotion->title = $promotion->title_th;
                    $promotion->detail = $promotion->detail_th;
                }

                $promotion->thumb = $promotion->thumb==""?$promotion->thumb_th:$promotion->thumb;
                $promotion->title = $promotion->title==""?$promotion->title_th:$promotion->title;
                $promotion->detail = $promotion->detail==""?$promotion->detail_th:$promotion->detail;

                // $promotion->thumb = $promotion->thumb===""?$promotion->thumb_th:$promotion->thumb;
                // $promotion->title = $promotion->title===""?$promotion->title_th:$promotion->title;
            }
            //$promotion = $getPromotion[0];

        }
        $data = array(
            "promotions" => $promotions,
        );

        return view('front/viera.promotion')->with($data);
    }

    public function viera_details(Request $request,$lang, $id)
    {
        $apiPromotions = new PromotionsApiController;
        $promotion = $apiPromotions->getPromotionById($request, $id);

        if (isset($promotion) && sizeof($promotion) > 0) {
            $promotion->pathFile = Functionphp::php_convertDateToPathFile($promotion->created_at);
            
            if ($lang == "en" && $promotion->status_en == "Y") {
                $promotion->thumb = $promotion->thumb_en;
                $promotion->title = $promotion->title_en;
                $promotion->detail = $promotion->detail_en;
                $promotion->content = $promotion->content_en;
            } else if ($lang === "ch" && $promotion->status_ch == "Y") {
                $promotion->thumb = $promotion->thumb_ch;
                $promotion->title = $promotion->title_ch;
                $promotion->detail = $promotion->detail_ch;
                $promotion->content = $promotion->content_ch;
            } else {
                $promotion->thumb = $promotion->thumb_th;
                $promotion->title = $promotion->title_th;
                $promotion->detail = $promotion->detail_th;
                $promotion->content = $promotion->content_th;
            }
        }

        $data = array(
            "promotion" => $promotion,
        );

        return view('front/viera.promotion-details')->with($data);
    }
}
