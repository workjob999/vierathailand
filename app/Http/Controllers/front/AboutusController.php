<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class AboutusController extends Controller
{
    //index
    public function index()
    {
        $data = array();

        return view('front.aboutus')->with($data);
    }
}
