<?php
namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\DealerApiController;

class RegisterController extends Controller
{
    //index
    public function index_viera(Request $request,$lang)
    {
        //dd("test");
        $data = array();

        return view('front.viera.register')->with($data);
    }

    public function register(Request $request,$lang)
    {
        $dealer = new DealerApiController();
        $response = $dealer->store($request);

        return $response;
        // $result_array = json_decode($response->content(), true);
        // dd($result_array["status"]);
    }
}
