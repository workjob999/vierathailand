<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Api\HomeBannersApiController;
use App\Http\Controllers\Api\HomePromotionsApiController;
use App\Http\Controllers\Api\ReviewsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    //index
    public function index()
    {

        $data = array();

        return view('front.home')->with($data);
    }

    public function index_viera(Request $request)
    {
        $lang = $request->lang;
        $homeBanners = new HomeBannersApiController;
        $banners = $homeBanners->getHomeBannersFront($request, 0);
        //$banners = array();
        if (isset($banners) && sizeof($banners) > 0) {
            //$banner = $getBanner[0];
            foreach ($banners as $banner)
            {
                $banner->pathFile = Functionphp::php_convertDateToPathFile($banner->created_at);

                if ($lang=="en" && $banner->status_en=="Y")
                {
                    $banner->thumb = $banner->thumb_en;
                    $banner->url = $banner->url_en;
                    $banner->title = $banner->title_en;
                }
                else if ($lang==="ch" && $banner->status_ch=="Y")
                {
                    $banner->thumb = $banner->thumb_ch;
                    $banner->url = $banner->url_ch;
                    $banner->title = $banner->title_ch;
                }
                else {
                    $banner->thumb = $banner->thumb_th;
                    $banner->url = $banner->url_th;
                    $banner->title = $banner->title_th;
                }
            }
            
            // $banner->thumb = $banner->thumb===""?$banner->thumb_th:$banner->thumb;
            // $banner->title = $banner->title===""?$banner->title_th:$banner->title;
        }

        $homePromotions = new HomePromotionsApiController;
        $promotions = $homePromotions->getHomePromotionsFront($request, 0);
        //$promotion = array();
        if (isset($promotions) && sizeof($promotions) > 0) {
            foreach ($promotions as $promotion)
            {
                $promotion->pathFile = Functionphp::php_convertDateToPathFile($promotion->created_at);

                if ($lang=="en" && $promotion->status_en=="Y")
                {
                    $promotion->thumb = $promotion->thumb_en;
                    $promotion->url = $promotion->url_en;
                    $promotion->title = $promotion->title_en;
                }
                else if ($lang==="ch" && $promotion->status_ch=="Y")
                {
                    $promotion->thumb = $promotion->thumb_ch;
                    $promotion->url = $promotion->url_ch;
                    $promotion->title = $promotion->title_ch;
                }
                else {
                    $promotion->thumb = $promotion->thumb_th;
                    $promotion->url = $promotion->url_th;
                    $promotion->title = $promotion->title_th;
                }

                // $promotion->thumb = $promotion->thumb===""?$promotion->thumb_th:$promotion->thumb;
                // $promotion->title = $promotion->title===""?$promotion->title_th:$promotion->title;
            }
            //$promotion = $getPromotion[0];
            
        }


        $reviewsApi = new ReviewsApiController;
        $reviews = $reviewsApi->getReviewsFront($request, 0);
        //$banners = array();
        if (isset($reviews) && sizeof($reviews) > 0) {
            //$banner = $getBanner[0];
            foreach ($reviews as $review)
            {
                $review->pathFile = Functionphp::php_convertDateToPathFile($review->created_at);

                if ($lang=="en" && $review->status_en=="Y")
                {
                    $review->thumb = $review->thumb_en;
                    $review->url = $review->url_en;
                    $review->title = $review->title_en;
                }
                else if ($lang==="ch" && $review->status_ch=="Y")
                {
                    $review->thumb = $review->thumb_ch;
                    $review->url = $review->url_ch;
                    $review->title = $review->title_ch;
                }
                else {
                    $review->thumb = $review->thumb_th;
                    $review->url = $review->url_th;
                    $review->title = $review->title_th;
                }
            }
        }

        // Log::info($getPromotion);
        // Log::info($getBanner);

        $data = array(
            "banners" => $banners,
            "promotions" => $promotions,
            "reviews" => $reviews
        );
        //$data = array();

        return view('front.viera.home')->with($data);
    }
}
