<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class LifestyleController extends Controller
{
    //index
    public function index()
    {
        $data = array();

        return view('front.lifestyle')->with($data);
    }

    public function details1()
    {
        $data = array();

        return view('front.lifestyle-details-1')->with($data);
    }

    public function details2()
    {
        $data = array();

        return view('front.lifestyle-details-2')->with($data);
    }

    public function details3()
    {
        $data = array();

        return view('front.lifestyle-details-3')->with($data);
    }
}
