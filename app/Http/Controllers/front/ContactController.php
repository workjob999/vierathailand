<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use App\Http\Classes\Helper;
use App\Model\Functions\Functionphp;
use Carbon\Carbon;
use Mail;

class ContactController extends Controller
{
    //index
    public function index()
    {
        $data = array();

        return view('front.contact')->with($data);
    }

    public function index_viera()
    {
        $data = array();

        return view('front/viera.contact')->with($data);
    }

    public function sendmail(Request $request)
    {
        $fullname = $request->input("fullname");
        $phone = $request->input("phone");
        $email = $request->input("email");
        $subject = $request->input("subject");
        $msg = $request->input("msg");
        $result = Mail::send('emails.contact_template', compact('fullname','email',"phone","subject","msg"), function($message) use ($fullname, $email) {
            $message->to("nodkrab@hotmail.com","Nod");
            $message->to("workjob999@gmail.com","workjob999");
            $message->to(config('config.config_arr_mailadmin')["email"],config('config.config_arr_mailadmin')["name"])->subject('Contact');
            $message->from($email,$fullname);
            //$message->from(config('config.config_arr_mailadmin')["email"],config('config.config_arr_mailadmin')["name"]);
        });
        
        return response()->json([
            "success"=>true,
        ], 200);
    }
}
