<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Api\DealerApiController;
use App\Http\Classes\Helper;
use App\Model\GlobalData;
use App\Model\Functions\Functionphp;
use Illuminate\Support\Facades\Storage;

class DealerController extends Controller
{
    //index
    public function index_viera(Request $request,$lang)
    {
        //dd($lang);
        $data = array();

        return view('front.viera.register')->with($data);
    }

    public function register(Request $request,$lang)
    {
        //dd($lang);
        //$data = array();
        $dealer = new DealerApiController;
        $request->request->add(['status' => "N"]);
        $request->request->add(['front' => true]);
        $response = $dealer->store($request);

        return $response;

        //return view('front/viera.dealer')->with($data);
    }

    public function dealer(Request $request,$lang)
    {
        $search = $request->input("s")===NULL?"":$request->input("s");
        $dealer = "";
        $agent = "";
        $contentSearch = "";
        $request->request->add(['status' => "Y"]);

        //dealer
        $searchType = $request->input("t")===NULL?"":$request->input("t");
        if ($searchType=="")
        {
            $request->request->add(['type' => 1]);
            $api_dealer = new DealerApiController;
            $response = $api_dealer->getRegisterList($request,1,$orderBy='created_by','DESC',0,$search);
            $data_dealer = json_decode($response->content());
            if ($data_dealer->status!==NULL && $data_dealer->status=="success")
            {
                $dealer = $data_dealer->data;
                if (!empty($dealer) && sizeof($dealer)>0)
                {
                    foreach ($dealer as $deal)
                    {
                        $deal->pathFile = Functionphp::php_convertDateToPathFile($deal->created_at);
                        $deal->arr_facebook = explode("\n",$deal->facebook);
                    }
                }
            }
            //dealer
    
            //agent
            $request->merge(['type' => 2]);
            $response = $api_dealer->getRegisterList($request,1,$orderBy='code','DESC',0,$search);
            $data_agent = json_decode($response->content());
    
            if ($data_agent->status!==NULL && $data_agent->status=="success")
            {
                $agent = $data_agent->data;
                if (!empty($agent) && sizeof($agent)>0)
                {
                    foreach ($agent as $deal)
                    {
                        $deal->pathFile = Functionphp::php_convertDateToPathFile($deal->created_at);
                        $deal->arr_facebook = explode("\n",$deal->facebook);
                    }
                }
            }
            //agent
        }
        else {
            $request->request->add(['type' => $searchType]);
            $api_dealer = new DealerApiController;
            $response = $api_dealer->getRegisterList($request,1,$orderBy='created_by','DESC',0,$search);
            $data_dealer = json_decode($response->content());
            if ($data_dealer->status!==NULL && $data_dealer->status=="success")
            {
                $contentSearch = $data_dealer->data;
                if (!empty($contentSearch) && sizeof($contentSearch)>0)
                {
                    foreach ($contentSearch as $deal)
                    {
                        $deal->pathFile = Functionphp::php_convertDateToPathFile($deal->created_at);
                        $deal->arr_facebook = explode("\n",$deal->facebook);
                    }
                }
            }
        }
        

        $data = array(
            "dealer"=>$dealer,
            "agent"=>$agent,
            "contentSearch"=>$contentSearch
        );

        return view('front/viera.dealer')->with($data);
    }

    public function checkDuplicate(Request $request,$lang)
    {
        $dealer = new DealerApiController;
        $response = $dealer->checkDuplicate($request);

        return $response;
    }
}
