<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;

class ProductsController extends Controller
{
    //index
    public function index()
    {
        $data = array();

        return view('front.products')->with($data);
    }

    public function index_viera()
    {
        $data = array();

        return view('front/viera.products')->with($data);
    }
}
