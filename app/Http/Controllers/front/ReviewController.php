<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Api\ReviewsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    //index
    public function index()
    {
        // $apiReviews = new ReviewsApiController;
        // $reviews = $apiReviews->getReviewsFront($request, 0);

        $data = array();

        return view('front.review')->with($data);
    }

    public function details()
    {
        $data = array();

        return view('front.review-details')->with($data);
    }

    public function index_viera(Request $request, $lang)
    {
        //$homeReviews = new HomeReviewsApiController;
        //$reviews = $homeReviews->getHomeReviewsFront($request, 0);
        $homeReviews = new ReviewsApiController;
        $reviews = $homeReviews->getReviewsFront($request, 0);

        if (isset($reviews) && sizeof($reviews) > 0) {
            foreach ($reviews as $review) {
                $review->pathFile = Functionphp::php_convertDateToPathFile($review->created_at);

                if ($lang == "en" && $review->status_en == "Y") {
                    $review->thumb = $review->thumb_en;
                    $review->title = $review->title_en;
                    $review->detail = $review->detail_en;
                    $review->content = $review->content_en;
                } else if ($lang === "ch" && $review->status_ch == "Y") {
                    $review->thumb = $review->thumb_ch;
                    $review->title = $review->title_ch;
                    $review->detail = $review->detail_ch;
                    $review->content = $review->content_ch;
                } else {
                    $review->thumb = $review->thumb_th;
                    $review->title = $review->title_th;
                    $review->detail = $review->detail_th;
                    $review->content = $review->content_th;
                }

                // $review->thumb = $review->thumb===""?$review->thumb_th:$review->thumb;
                // $review->title = $review->title===""?$review->title_th:$review->title;
            }
            //$review = $getReview[0];

        }
        $data = array(
            "reviews" => $reviews,
        );

        return view('front/viera.review')->with($data);
    }

    public function viera_details(Request $request,$lang, $id)
    {
        $apiReviews = new ReviewsApiController;
        $review = $apiReviews->getReviewById($request, $id);

        if (isset($review) && sizeof($review) > 0) {
            $review->pathFile = Functionphp::php_convertDateToPathFile($review->created_at);
            
            if ($lang == "en" && $review->status_en == "Y") {
                $review->thumb = $review->thumb_en;
                $review->title = $review->title_en;
                $review->detail = $review->detail_en;
                $review->content = $review->content_en;
            } else if ($lang === "ch" && $review->status_ch == "Y") {
                $review->thumb = $review->thumb_ch;
                $review->title = $review->title_ch;
                $review->detail = $review->detail_ch;
                $review->content = $review->content_ch;
            } else {
                $review->thumb = $review->thumb_th;
                $review->title = $review->title_th;
                $review->detail = $review->detail_th;
                $review->content = $review->content_th;
            }
        }

        $data = array(
            "review" => $review,
        );

        return view('front/viera.review-details')->with($data);
    }
}
