<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Model\Registers;
use App\Http\Controllers\Api\DealerApiController;
use App\Http\Requests\Webadmin\RegistersRequest;
use App\PermissionRole;
use App\Model\GlobalData;
use App\Model\Functions\Functionphp;
use File;
use Illuminate\Support\Facades\Storage;
use Excel;

class RegistersController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.registers')) {
            if (Session::get('limit_page.registers') == 'All') {
                $this->limit_page = Registers::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.registers');
            }
        } else {
            Session::put('limit_page.registers', $this->limit_page);
        }
    }

    public function index($orderBy = '', $ascdesc = '', $search = '')
    {
        $query = Registers::query();

        if (Session::has('limit_page.registers')) {
            if (Session::get('limit_page.registers') == 'All') {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.registers');
            }
        } else {
            Session::put('limit_page.registers', $this->limit_page);
        }

        $arr_data['orderBy'] = 'order_by';
        $arr_data['ascdesc'] = 'DESC';
        $arr_data['orderByUrl'] = 'order_by';
        $arr_data['urlSearch'] = '';
        $arr_data['status_icon_sort'] = '';

        $search = (isset($search) && $search != '') ? trim($search) : '';
        $orderByUrl = (isset($orderBy) && $orderBy != '') ? $orderBy : 'order_by';
        $orderBy = ($orderBy == 'order_by' || $orderBy == '') ? 'order_by' : $orderBy;
        $orderBy = ($orderBy == 'fullname') ? 'fullname' : $orderBy;
        $orderBy = ($orderBy == 'modify') ? 'updated_at' : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != '') ? $ascdesc : 'DESC';

        //$arr_data["page"] = $page;
        $arr_data['search'] = $search;
        $arr_data['orderBy'] = $orderBy;
        $arr_data['ascdesc'] = $ascdesc;
        $arr_data['orderByUrl'] = $orderByUrl;
        $arr_data['urlSearch'] = '/'.$search;

        $arr_data['status_icon_sort'] = '';

        $registers = new DealerApiController();
        $data_registers = $registers->getRegisterList('', 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_registers = json_decode($data_registers->content(), true);

        $arr_data['start_sort'] = $data_registers->firstItem() - 1;

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            'hasPermission' => $hasPermission,
            'registers' => $data_registers,
            'arr_data' => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission['view-registers-list']) {
            return view('web-admin.registers.index')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    public function create()
    {
        $registers = Registers::select('order_by')->latest('order_by')->first();
        $startDate = '';
        $endDate = '';

        $arr_data = array();
        if (empty($registers)) {
            $order = 0;
            $arr_data['order_by'] = 1;
        } else {
            $arr_data['order_by'] = $registers->order_by + 1;
        }

        $arr_data['disabled_data'] = false;

        $pathDate = date('Y').'/'.date('m');
        $html_facebook = '';

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            'hasPermission' => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            'html_facebook' => $html_facebook,
            'action_page' => 'create',
        );

        if ($hasPermission['create-registers']) {
            return view('web-admin.registers.create')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission['create-registers']) {
            $register = new DealerApiController();
            $response = $register->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array['status'];
        } else {
            $result = 'fail';
        }

        if ($result == 'success') {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $registers = Registers::find($id);
        if (empty($registers)) {
            abort(404);
        }

        $pathDate = Functionphp::php_convertDateToPathFile($registers->created_at);

        $facebook = $registers->facebook;
        $html_facebook = '';
        // if (isset($facebook) && sizeof($facebook)>0)
        // {
        //     foreach ($facebook as $face)
        //     {
        //         if ($html_facebook!="")
        //             $html_facebook .= "\n";
        //         $html_facebook .= $face->url;
        //     }
        // }

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        $arr_data['disabled_data'] = true;

        $data = array(
            'hasPermission' => $hasPermission,
            'arr_data' => $arr_data,
            'registers' => $registers,
            'pathDate' => $pathDate,
            'html_facebook' => $html_facebook,
            'action_page' => 'view',
        );

        if ($hasPermission['view-registers-list']) {
            return view('web-admin.registers.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $registers = Registers::find($id);
        if (empty($registers)) {
            $order = 0;
            $arr_data['order_by'] = 1;
            abort(404);
        } else {
            $arr_data['order_by'] = $registers->order_by;
        }

        $arr_data['disabled_data'] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($registers->created_at);

        $facebook = $registers->facebook;
        $html_facebook = '';
        // if (isset($facebook) && sizeof($facebook)>0)
        // {
        //     foreach ($facebook as $face)
        //     {
        //         if ($html_facebook!="")
        //             $html_facebook .= "\n";
        //         $html_facebook .= $face->url;
        //     }
        // }

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            'hasPermission' => $hasPermission,
            'registers' => $registers,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            'html_facebook' => $html_facebook,
            'action_page' => 'edit',
        );

        if ($hasPermission['edit-registers']) {
            return view('web-admin.registers.edit')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/'));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(RegistersRequest $request, $id)
    {
        $registers = Registers::find($id);
        if (empty($registers)) {
            abort(404);
        }

        // $pathDate = Functionphp::php_convertDateToPathFile($registers->created_at);
        // $pathDate = ($pathDate=="")?date("Y/m"):$pathDate;
        // $pathThumb = config('config.config_pathUpload')."/registers/".$pathDate;
        // File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission['edit-registers']) {
            $register = new DealerApiController();
            $response = $register->store($request, 2, $id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array['status'];
        } else {
            $result = 'fail';
        }

        if ($result == 'success') {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms').'/dealer/registers/?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $registers = Registers::find($id);

        $permission = new PermissionRole();
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission['edit-registers']) {
            $pathDate = Functionphp::php_convertDateToPathFile($registers->created_at);
            $pathThumb = config('config.config_pathUpload').'/registers/'.$pathDate;

            Helper::storageProcess('delete', $pathThumb, $registers->thumb);
            // Helper::storageProcess('delete',$pathThumb,$registers->banner_th);

            $result_delete = $registers->delete();
            echo 'Y';
        } else {
            echo 'N';
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode('|$|', $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $registers = Registers::find($id);

                $permission = new PermissionRole();
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission['edit-registers']) {
                    $pathDate = Functionphp::php_convertDateToPathFile($registers->created_at);
                    $pathThumb = config('config.config_pathUpload').'/registers/'.$pathDate;

                    Helper::storageProcess('delete', $pathThumb, $registers->thumb);
                    // Helper::storageProcess('delete',$pathThumb,$registers->banner_th);

                    $result_delete = $registers->delete();
                }
            }
            echo 'Y';
        } else {
            echo 'N';
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $registers = Registers::orderBy("created_at","DESC")->get();
        if(empty($registers))
          abort(404);

        if (isset($registers))
          {
              foreach ($registers as $register)
              {
                  $exp_facebook = explode("\n",$register->facebook);
                  $register->facebook = implode(",",$exp_facebook);
                  $register->pathFile = Functionphp::php_convertDateToPathFile($register->created_at);
              }   
          }

        
        $data = array(
            "registers" => $registers
        );

        $filename = date("Ymd-His")."_export";

        Excel::create($filename, function($excel) use ($data) {
            $excel->sheet('Registers', function($sheet) use ($data){
                $sheet->loadView('excel.registers')->with($data);
            });
        })->export("xls");
    }
}
