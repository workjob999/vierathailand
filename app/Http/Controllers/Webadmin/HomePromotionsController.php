<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use App\Http\Controllers\Api\HomePromotionsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\GlobalData;
use App\Model\HomePromotions;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class HomePromotionsController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.homePromotions')) {
            if (Session::get('limit_page.homePromotions') == "All") {
                $this->limit_page = HomePromotions::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homePromotions');
            }
        } else {
            Session::put('limit_page.homePromotions', $this->limit_page);
        }
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        $query = HomePromotions::query();
        if (Session::has('limit_page.homePromotions')) {
            if (Session::get('limit_page.homePromotions') == "All") {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homePromotions');
            }
        } else {
            Session::put('limit_page.homePromotions', $this->limit_page);
        }

        $arr_data["orderBy"] = "order_by";
        $arr_data["ascdesc"] = "ASC";
        $arr_data["orderByUrl"] = "order_by";
        $arr_data["urlSearch"] = "";
        $arr_data["status_icon_sort"] = "";

        $search = (isset($search) && $search != "") ? trim($search) : "";
        $orderByUrl = (isset($orderBy) && $orderBy != "") ? $orderBy : "order_by";
        $orderBy = ($orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "order_by" || $orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "title") ? "title" : $orderBy;
        $orderBy = ($orderBy == "modify") ? "updated_at" : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != "") ? $ascdesc : "ASC";

        //$arr_data["page"] = $page;
        $arr_data["search"] = $search;
        $arr_data["orderBy"] = $orderBy;
        $arr_data["ascdesc"] = $ascdesc;
        $arr_data["orderByUrl"] = $orderByUrl;
        $arr_data["urlSearch"] = "/" . $search;

        $arr_data["status_icon_sort"] = "";

        $homePromotions = new HomePromotionsApiController;
        $dataHomePromotions = $homePromotions->getHomePromotionsList("", 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_homePromotions = json_decode($dataHomePromotions->content(), true);

        $arr_data["start_sort"] = $dataHomePromotions->firstItem() - 1;

        if (isset($dataHomePromotions) && sizeof($dataHomePromotions)>0)
        {
            foreach ($dataHomePromotions as $data)
            {
                $data->pathFile = Functionphp::php_convertDateToPathFile($data->created_at);
            }
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            "hasPermission" => $hasPermission,
            "homePromotions" => $dataHomePromotions,
            "arr_data" => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission["view-home-promotions-list"]) {
            return view("web-admin.homePromotions.index")->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    public function create()
    {
        $homePromotions = HomePromotions::select('order_by')->latest('order_by')->first();

        $arr_data = array();
        if (empty($homePromotions)) {
            $arr_data["order_by"] = 1;
        } else {
            $arr_data["order_by"] = $homePromotions->order_by + 1;
        }

        $arr_data["disabled_data"] = false;

        $pathDate = date("Y") . "/" . date("m");

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            "action_page" => "create"
        );

        if ($hasPermission["create-home-promotions"]) {
            return view("web-admin.homePromotions.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/promotions/'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["create-home-promotions"]) {
            $homePromotion = new HomePromotionsApiController;
            $response = $homePromotion->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/promotions/?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/promotions?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homePromotions = HomePromotions::find($id);
        if (empty($homePromotions)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homePromotions->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homePromotions->startDate!="")
            $startDate = $now->parse($homePromotions->startDate)->format("d/m/Y");
        if ($homePromotions->endDate!="")
            $endDate = $now->parse($homePromotions->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = true;
        $pathDate = Functionphp::php_convertDateToPathFile($homePromotions->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homePromotions" => $homePromotions,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "view"
        );

        if ($hasPermission["view-home-promotions-list"]) {
            return view('web-admin.homePromotions.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homePromotions = HomePromotions::find($id);
        if (empty($homePromotions)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homePromotions->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homePromotions->startDate!="")
            $startDate = $now->parse($homePromotions->startDate)->format("d/m/Y");
        if ($homePromotions->endDate!="")
            $endDate = $now->parse($homePromotions->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($homePromotions->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homePromotions" => $homePromotions,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "edit"
        );

        if ($hasPermission["edit-home-promotions"]) {
            return view('web-admin.homePromotions.edit')->with($data);
        } else {
            return redirect(Route("cms-home-promotions-index"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $homePromotions = HomePromotions::find($id);
        if (empty($homePromotions)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-home-promotions"]) {
            $homePromotion = new HomePromotionsApiController;
            //dd($request,$homePromotions);
            $response = $homePromotion->update($request,$id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        // if ($hasPermission["edit-home-promotions"]) {
        //     $data = array(
        //         "firstname" => $request->input("firstname"),
        //         "lastname" => $request->input("lastname"),
        //         "email" => $request->input("email"),
        //         "phone" => $request->input("phone"),
        //         "budget" => $request->input("budget"),
        //         "user_id" => Auth::user()->id,
        //     );
        //     $url = env("APP_URL") . "/api/homePromotions/" . $id;
        //     $json_data = json_encode($data);
        //     $header = [
        //         'Content-Type: application/json',
        //     ];
        //     $response = Helper::curl_customRequest($url, $json_data, "PATCH", "", 20, $header);
        //     $data_response = json_decode($response);
        //     $response_success = $data_response->success;
        // }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/promotions?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/promotions?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $homePromotions = HomePromotions::find($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-home-promotions"]) {
            // $pathDate = Functionphp::php_convertDateToPathFile($homePromotions->created_at);
            // $pathThumb = config('config.config_pathUpload')."/homePromotions/".$pathDate;

            // Helper::storageProcess('delete',$pathThumb,$homePromotions->thumb_th);
            // Helper::storageProcess('delete',$pathThumb,$homePromotions->promotion_th);

            $result_delete = $homePromotions->delete();
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode("|$|", $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $homePromotions = HomePromotions::find($id);

                $permission = new PermissionRole;
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission["edit-home-promotions"]) {
                    // $pathDate = Functionphp::php_convertDateToPathFile($homePromotions->created_at);
                    // $pathThumb = config('config.config_pathUpload')."/homePromotions/".$pathDate;

                    // Helper::storageProcess('delete',$pathThumb,$homePromotions->thumb_th);
                    // Helper::storageProcess('delete',$pathThumb,$homePromotions->promotion_th);

                    $result_delete = $homePromotions->delete();
                }
            }
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $homePromotions = HomePromotions::orderBy("created_at", "DESC")->get();
        if (empty($homePromotions)) {
            abort(404);
        }

        if (isset($homePromotions)) {
            foreach ($homePromotions as $homePromotion) {
                $exp_phone = explode("-", $homePromotion->phone);
                $homePromotion->phone = implode("", $exp_phone);
            }
        }

        //dd($homePromotions);
        $data = array(
            "homePromotions" => $homePromotions,
            "project" => "kara",
        );

        $filename = date("Ymd-His") . "_export_homePromotion_kara-ari-rama6";

        //dd($data);
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('Registers', function ($sheet) use ($data) {
                $sheet->loadView('excel.homePromotions')->with($data);
            });
        })->export("xls");
    }
}
