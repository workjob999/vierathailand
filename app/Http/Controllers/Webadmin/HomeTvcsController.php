<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use App\Http\Controllers\Api\HomeTvcsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\GlobalData;
use App\Model\HomeTvcs;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class HomeTvcsController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.homeTvcs')) {
            if (Session::get('limit_page.homeTvcs') == "All") {
                $this->limit_page = HomeTvcs::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homeTvcs');
            }
        } else {
            Session::put('limit_page.homeTvcs', $this->limit_page);
        }
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        $query = HomeTvcs::query();
        if (Session::has('limit_page.homeTvcs')) {
            if (Session::get('limit_page.homeTvcs') == "All") {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homeTvcs');
            }
        } else {
            Session::put('limit_page.homeTvcs', $this->limit_page);
        }

        $arr_data["orderBy"] = "order_by";
        $arr_data["ascdesc"] = "ASC";
        $arr_data["orderByUrl"] = "order_by";
        $arr_data["urlSearch"] = "";
        $arr_data["status_icon_sort"] = "";

        $search = (isset($search) && $search != "") ? trim($search) : "";
        $orderByUrl = (isset($orderBy) && $orderBy != "") ? $orderBy : "modify";
        $orderBy = ($orderBy == "") ? "updated_at" : $orderBy;
        $orderBy = ($orderBy == "order_by" || $orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "title") ? "title" : $orderBy;
        $orderBy = ($orderBy == "modify") ? "updated_at" : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != "") ? $ascdesc : "DESC";

        //$arr_data["page"] = $page;
        $arr_data["search"] = $search;
        $arr_data["orderBy"] = $orderBy;
        $arr_data["ascdesc"] = $ascdesc;
        $arr_data["orderByUrl"] = $orderByUrl;
        $arr_data["urlSearch"] = "/" . $search;

        $arr_data["status_icon_sort"] = "";

        $homeTvcs = new HomeTvcsApiController;
        $dataHomeTvcs = $homeTvcs->getHomeTvcsList("", 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_homeTvcs = json_decode($dataHomeTvcs->content(), true);

        $arr_data["start_sort"] = $dataHomeTvcs->firstItem() - 1;

        if (isset($dataHomeTvcs) && sizeof($dataHomeTvcs)>0)
        {
            foreach ($dataHomeTvcs as $data)
            {
                $data->pathFile = Functionphp::php_convertDateToPathFile($data->created_at);
            }
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            "hasPermission" => $hasPermission,
            "homeTvcs" => $dataHomeTvcs,
            "arr_data" => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission["view-home-tvc-list"]) {
            return view("web-admin.homeTvcs.index")->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    public function create()
    {
        $homeTvcs = HomeTvcs::select('order_by')->latest('order_by')->first();

        $arr_data = array();
        if (empty($homeTvcs)) {
            $arr_data["order_by"] = 1;
        } else {
            $arr_data["order_by"] = $homeTvcs->order_by + 1;
        }

        $arr_data["disabled_data"] = false;

        $pathDate = date("Y") . "/" . date("m");

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
        );

        if ($hasPermission["create-home-tvc"]) {
            return view("web-admin.homeTvcs.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/tvc/'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["create-home-tvc"]) {
            $homeTvc = new HomeTvcsApiController;
            $response = $homeTvc->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/tvc/?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/tvcs?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homeTvcs = HomeTvcs::find($id);
        if (empty($homeTvcs)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homeTvcs->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homeTvcs->startDate!="")
            $startDate = $now->parse($homeTvcs->startDate)->format("d/m/Y");
        if ($homeTvcs->endDate!="")
            $endDate = $now->parse($homeTvcs->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = true;
        $pathDate = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homeTvcs" => $homeTvcs,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate
        );

        if ($hasPermission["view-home-tvc-list"]) {
            return view('web-admin.homeTvcs.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homeTvcs = HomeTvcs::find($id);
        if (empty($homeTvcs)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homeTvcs->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homeTvcs->startDate!="")
            $startDate = $now->parse($homeTvcs->startDate)->format("d/m/Y");
        if ($homeTvcs->endDate!="")
            $endDate = $now->parse($homeTvcs->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homeTvcs" => $homeTvcs,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate
        );

        if ($hasPermission["edit-home-tvc"]) {
            return view('web-admin.homeTvcs.edit')->with($data);
        } else {
            return redirect(Route("cms-home-tvc-index"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $homeTvcs = HomeTvcs::find($id);
        if (empty($homeTvcs)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-home-tvc"]) {
            $homeTvc = new HomeTvcsApiController;
            //dd($request,$homeTvcs);
            $response = $homeTvc->update($request,$id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/tvc?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/tvc?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $homeTvcs = HomeTvcs::find($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-home-tvc"]) {
            // $pathDate = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);
            // $pathThumb = config('config.config_pathUpload')."/homeTvcs/".$pathDate;

            // Helper::storageProcess('delete',$pathThumb,$homeTvcs->thumb_th);
            // Helper::storageProcess('delete',$pathThumb,$homeTvcs->tvc_th);

            $result_delete = $homeTvcs->delete();
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode("|$|", $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $homeTvcs = HomeTvcs::find($id);

                $permission = new PermissionRole;
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission["edit-home-tvc"]) {
                    // $pathDate = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);
                    // $pathThumb = config('config.config_pathUpload')."/homeTvcs/".$pathDate;

                    // Helper::storageProcess('delete',$pathThumb,$homeTvcs->thumb_th);
                    // Helper::storageProcess('delete',$pathThumb,$homeTvcs->tvc_th);

                    $result_delete = $homeTvcs->delete();
                }
            }
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $homeTvcs = HomeTvcs::orderBy("created_at", "DESC")->get();
        if (empty($homeTvcs)) {
            abort(404);
        }

        if (isset($homeTvcs)) {
            foreach ($homeTvcs as $homeTvc) {
                $exp_phone = explode("-", $homeTvc->phone);
                $homeTvc->phone = implode("", $exp_phone);
            }
        }

        //dd($homeTvcs);
        $data = array(
            "homeTvcs" => $homeTvcs,
            "project" => "kara",
        );

        $filename = date("Ymd-His") . "_export_homeTvc_kara-ari-rama6";

        //dd($data);
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('Registers', function ($sheet) use ($data) {
                $sheet->loadView('excel.homeTvcs')->with($data);
            });
        })->export("xls");
    }
}
