<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use App\Http\Controllers\Api\HomeBannersApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\GlobalData;
use App\Model\HomeBanners;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class HomeBannersController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.homeBanners')) {
            if (Session::get('limit_page.homeBanners') == "All") {
                $this->limit_page = HomeBanners::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homeBanners');
            }
        } else {
            Session::put('limit_page.homeBanners', $this->limit_page);
        }
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        $query = HomeBanners::query();
        if (Session::has('limit_page.homeBanners')) {
            if (Session::get('limit_page.homeBanners') == "All") {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.homeBanners');
            }
        } else {
            Session::put('limit_page.homeBanners', $this->limit_page);
        }

        $arr_data["orderBy"] = "order_by";
        $arr_data["ascdesc"] = "ASC";
        $arr_data["orderByUrl"] = "order_by";
        $arr_data["urlSearch"] = "";
        $arr_data["status_icon_sort"] = "";

        $search = (isset($search) && $search != "") ? trim($search) : "";
        $orderByUrl = (isset($orderBy) && $orderBy != "") ? $orderBy : "order_by";
        $orderBy = ($orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "order_by" || $orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "title") ? "title" : $orderBy;
        $orderBy = ($orderBy == "modify") ? "updated_at" : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != "") ? $ascdesc : "ASC";

        //$arr_data["page"] = $page;
        $arr_data["search"] = $search;
        $arr_data["orderBy"] = $orderBy;
        $arr_data["ascdesc"] = $ascdesc;
        $arr_data["orderByUrl"] = $orderByUrl;
        $arr_data["urlSearch"] = "/" . $search;

        $arr_data["status_icon_sort"] = "";

        $homeBanners = new HomeBannersApiController;
        $dataHomeBanners = $homeBanners->getHomeBannersList("", 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_homeBanners = json_decode($dataHomeBanners->content(), true);

        $arr_data["start_sort"] = $dataHomeBanners->firstItem() - 1;

        if (isset($dataHomeBanners) && sizeof($dataHomeBanners)>0)
        {
            foreach ($dataHomeBanners as $data)
            {
                $data->pathFile = Functionphp::php_convertDateToPathFile($data->created_at);
            }
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            "hasPermission" => $hasPermission,
            "homeBanners" => $dataHomeBanners,
            "arr_data" => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission["view-home-banners-list"]) {
            return view("web-admin.homeBanners.index")->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    public function create()
    {
        $homeBanners = HomeBanners::select('order_by')->latest('order_by')->first();

        $arr_data = array();
        if (empty($homeBanners)) {
            $arr_data["order_by"] = 1;
        } else {
            $arr_data["order_by"] = $homeBanners->order_by + 1;
        }

        $arr_data["disabled_data"] = false;

        $pathDate = date("Y") . "/" . date("m");

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            "action_page" => "create"
        );

        if ($hasPermission["create-home-banners"]) {
            return view("web-admin.homeBanners.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/banners/'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["create-home-banners"]) {
            $homeBanner = new HomeBannersApiController;
            $response = $homeBanner->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/banners/?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/banners?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $homeBanners = HomeBanners::find($id);
        if (empty($homeBanners)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homeBanners->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homeBanners->startDate!="")
            $startDate = $now->parse($homeBanners->startDate)->format("d/m/Y");
        if ($homeBanners->endDate!="")
            $endDate = $now->parse($homeBanners->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = true;
        $pathDate = Functionphp::php_convertDateToPathFile($homeBanners->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homeBanners" => $homeBanners,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "view"
        );

        if ($hasPermission["view-home-banners-list"]) {
            return view('web-admin.homeBanners.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $homeBanners = HomeBanners::find($id);
        if (empty($homeBanners)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $homeBanners->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($homeBanners->startDate!="")
            $startDate = $now->parse($homeBanners->startDate)->format("d/m/Y");
        if ($homeBanners->endDate!="")
            $endDate = $now->parse($homeBanners->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($homeBanners->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "homeBanners" => $homeBanners,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "edit"
        );

        if ($hasPermission["edit-home-banners"]) {
            return view('web-admin.homeBanners.edit')->with($data);
        } else {
            return redirect(Route("cms-home-banners-index"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $homeBanners = HomeBanners::find($id);
        if (empty($homeBanners)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-home-banners"]) {
            $homeBanner = new HomeBannersApiController;
            //dd($request,$homeBanners);
            $response = $homeBanner->update($request,$id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        // if ($hasPermission["edit-home-banners"]) {
        //     $data = array(
        //         "firstname" => $request->input("firstname"),
        //         "lastname" => $request->input("lastname"),
        //         "email" => $request->input("email"),
        //         "phone" => $request->input("phone"),
        //         "budget" => $request->input("budget"),
        //         "user_id" => Auth::user()->id,
        //     );
        //     $url = env("APP_URL") . "/api/homeBanners/" . $id;
        //     $json_data = json_encode($data);
        //     $header = [
        //         'Content-Type: application/json',
        //     ];
        //     $response = Helper::curl_customRequest($url, $json_data, "PATCH", "", 20, $header);
        //     $data_response = json_decode($response);
        //     $response_success = $data_response->success;
        // }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/home/banners?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/home/banners?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $homeBanners = HomeBanners::find($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-home-banners"]) {
            // $pathDate = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
            // $pathThumb = config('config.config_pathUpload')."/homeBanners/".$pathDate;

            // Helper::storageProcess('delete',$pathThumb,$homeBanners->thumb_th);
            // Helper::storageProcess('delete',$pathThumb,$homeBanners->banner_th);

            $result_delete = $homeBanners->delete();
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode("|$|", $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $homeBanners = HomeBanners::find($id);

                $permission = new PermissionRole;
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission["edit-home-banners"]) {
                    // $pathDate = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
                    // $pathThumb = config('config.config_pathUpload')."/homeBanners/".$pathDate;

                    // Helper::storageProcess('delete',$pathThumb,$homeBanners->thumb_th);
                    // Helper::storageProcess('delete',$pathThumb,$homeBanners->banner_th);

                    $result_delete = $homeBanners->delete();
                }
            }
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $homeBanners = HomeBanners::orderBy("created_at", "DESC")->get();
        if (empty($homeBanners)) {
            abort(404);
        }

        if (isset($homeBanners)) {
            foreach ($homeBanners as $homeBanner) {
                $exp_phone = explode("-", $homeBanner->phone);
                $homeBanner->phone = implode("", $exp_phone);
            }
        }

        //dd($homeBanners);
        $data = array(
            "homeBanners" => $homeBanners,
            "project" => "kara",
        );

        $filename = date("Ymd-His") . "_export_homeBanner_kara-ari-rama6";

        //dd($data);
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('Registers', function ($sheet) use ($data) {
                $sheet->loadView('excel.homeBanners')->with($data);
            });
        })->export("xls");
    }
}
