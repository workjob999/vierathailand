<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use App\Http\Controllers\Api\ReviewsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\GlobalData;
use App\Model\Reviews;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class ReviewsController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.reviews')) {
            if (Session::get('limit_page.reviews') == "All") {
                $this->limit_page = Reviews::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.reviews');
            }
        } else {
            Session::put('limit_page.reviews', $this->limit_page);
        }
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        $query = Reviews::query();
        if (Session::has('limit_page.reviews')) {
            if (Session::get('limit_page.reviews') == "All") {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.reviews');
            }
        } else {
            Session::put('limit_page.reviews', $this->limit_page);
        }

        $arr_data["orderBy"] = "order_by";
        $arr_data["ascdesc"] = "ASC";
        $arr_data["orderByUrl"] = "order_by";
        $arr_data["urlSearch"] = "";
        $arr_data["status_icon_sort"] = "";

        $search = (isset($search) && $search != "") ? trim($search) : "";
        $orderByUrl = (isset($orderBy) && $orderBy != "") ? $orderBy : "order_by";
        $orderBy = ($orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "order_by" || $orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "title") ? "title" : $orderBy;
        $orderBy = ($orderBy == "modify") ? "updated_at" : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != "") ? $ascdesc : "ASC";

        //$arr_data["page"] = $page;
        $arr_data["search"] = $search;
        $arr_data["orderBy"] = $orderBy;
        $arr_data["ascdesc"] = $ascdesc;
        $arr_data["orderByUrl"] = $orderByUrl;
        $arr_data["urlSearch"] = "/" . $search;

        $arr_data["status_icon_sort"] = "";

        $reviews = new ReviewsApiController;
        $dataReviews = $reviews->getReviewsList("", 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_reviews = json_decode($dataReviews->content(), true);

        $arr_data["start_sort"] = $dataReviews->firstItem() - 1;

        if (isset($dataReviews) && sizeof($dataReviews)>0)
        {
            foreach ($dataReviews as $data)
            {
                $data->pathFile = Functionphp::php_convertDateToPathFile($data->created_at);
            }
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            "hasPermission" => $hasPermission,
            "reviews" => $dataReviews,
            "arr_data" => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission["view-reviews-list"]) {
            return view("web-admin.reviews.index")->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    public function create()
    {
        $reviews = Reviews::select('order_by')->latest('order_by')->first();

        $arr_data = array();
        if (empty($reviews)) {
            $arr_data["order_by"] = 1;
        } else {
            $arr_data["order_by"] = $reviews->order_by + 1;
        }

        $arr_data["disabled_data"] = false;

        $pathDate = date("Y") . "/" . date("m");

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            "action_page" => "create"
        );

        if ($hasPermission["create-reviews"]) {
            return view("web-admin.reviews.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/reviews'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["create-reviews"]) {
            $review = new ReviewsApiController;
            $response = $review->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/reviews?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/reviews?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $reviews = Reviews::find($id);
        if (empty($reviews)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $reviews->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($reviews->startDate!="")
            $startDate = $now->parse($reviews->startDate)->format("d/m/Y");
        if ($reviews->endDate!="")
            $endDate = $now->parse($reviews->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = true;
        $pathDate = Functionphp::php_convertDateToPathFile($reviews->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "reviews" => $reviews,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "view"
        );

        if ($hasPermission["view-reviews-list"]) {
            return view('web-admin.reviews.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $reviews = Reviews::find($id);
        if (empty($reviews)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $reviews->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($reviews->startDate!="")
            $startDate = $now->parse($reviews->startDate)->format("d/m/Y");
        if ($reviews->endDate!="")
            $endDate = $now->parse($reviews->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($reviews->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "reviews" => $reviews,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "edit"
        );

        if ($hasPermission["edit-reviews"]) {
            return view('web-admin.reviews.edit')->with($data);
        } else {
            return redirect(Route("cms-reviews-index"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $reviews = Reviews::find($id);
        if (empty($reviews)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-reviews"]) {
            $review = new ReviewsApiController;
            //dd($request,$reviews);
            $response = $review->update($request,$id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        // if ($hasPermission["edit-reviews"]) {
        //     $data = array(
        //         "firstname" => $request->input("firstname"),
        //         "lastname" => $request->input("lastname"),
        //         "email" => $request->input("email"),
        //         "phone" => $request->input("phone"),
        //         "budget" => $request->input("budget"),
        //         "user_id" => Auth::user()->id,
        //     );
        //     $url = env("APP_URL") . "/api/reviews/" . $id;
        //     $json_data = json_encode($data);
        //     $header = [
        //         'Content-Type: application/json',
        //     ];
        //     $response = Helper::curl_customRequest($url, $json_data, "PATCH", "", 20, $header);
        //     $data_response = json_decode($response);
        //     $response_success = $data_response->success;
        // }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/reviews?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/reviews?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $reviews = Reviews::find($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-reviews"]) {
            // $pathDate = Functionphp::php_convertDateToPathFile($reviews->created_at);
            // $pathThumb = config('config.config_pathUpload')."/reviews/".$pathDate;

            // Helper::storageProcess('delete',$pathThumb,$reviews->thumb_th);
            // Helper::storageProcess('delete',$pathThumb,$reviews->banner_th);

            $result_delete = $reviews->delete();
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode("|$|", $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $reviews = Reviews::find($id);

                $permission = new PermissionRole;
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission["edit-reviews"]) {
                    // $pathDate = Functionphp::php_convertDateToPathFile($reviews->created_at);
                    // $pathThumb = config('config.config_pathUpload')."/reviews/".$pathDate;

                    // Helper::storageProcess('delete',$pathThumb,$reviews->thumb_th);
                    // Helper::storageProcess('delete',$pathThumb,$reviews->banner_th);

                    $result_delete = $reviews->delete();
                }
            }
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $reviews = Reviews::orderBy("created_at", "DESC")->get();
        if (empty($reviews)) {
            abort(404);
        }

        if (isset($reviews)) {
            foreach ($reviews as $review) {
                $exp_phone = explode("-", $review->phone);
                $review->phone = implode("", $exp_phone);
            }
        }

        //dd($reviews);
        $data = array(
            "reviews" => $reviews,
            "project" => "kara",
        );

        $filename = date("Ymd-His") . "_export_review_kara-ari-rama6";

        //dd($data);
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('Registers', function ($sheet) use ($data) {
                $sheet->loadView('excel.reviews')->with($data);
            });
        })->export("xls");
    }
}
