<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProcessdataController extends Controller
{

    /**

     * Display a listing of the resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function __construct()
    {

        $this->middleware('auth');

    }

    public function index()
    {

        //

    }

    /**

     * Show the form for creating a new resource.

     *

     * @return \Illuminate\Http\Response

     */

    public function create()
    {

        //

    }

    /**

     * Store a newly created resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @return \Illuminate\Http\Response

     */

    public function store(Request $request)
    {

        //

    }

    /**

     * Display the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function show($id)
    {

        //

    }

    /**

     * Show the form for editing the specified resource.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function edit($id)
    {

        //

    }

    /**

     * Update the specified resource in storage.

     *

     * @param  \Illuminate\Http\Request  $request

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function update(Request $request, $id)
    {

        //

    }

    /**

     * Remove the specified resource from storage.

     *

     * @param  int  $id

     * @return \Illuminate\Http\Response

     */

    public function destroy($id)
    {

        //

    }

    /*----------------------Process Other---------------------------------*/

    public function changeLimitPage(Request $request)
    {

        $post = $_POST;

        if (isset($post["limit_page"]) && $post["limit_page"] != "") {

            /*if ($post["limit_page"]=="All")

            $resultCount = DB::table($post['table'])->count()+1;

            else*/

            $resultCount = $post["limit_page"];

            Session::forget('limit_page.' . $post["table"]);

            Session::put('limit_page.' . $post["table"], $resultCount);

        }

    }

    public function changeStatus(Request $request)
    {

        $post = $_POST;

        $result_query = DB::table($post['table'])->select('status')->where('id', $post['id'])->first();

        $result_status = $result_query->status;

        $status = "N";

        if ($result_status == "N") {
            $status = "Y";
        }

        $result = DB::table($post['table'])->where('id', $post['id'])->update(['status' => $status]);

        if ($result) {
            echo $status;
        } else {
            echo "F";
        }

        //DB::update("update ".$post['table']." set status = '".$status."' where id = ?",[$post["id"]]);

        /*$sql = "update ".$post['table']." set status = '".$status."' where id = '".$post["id"]."'";

    $result = DB::update($sql);*/

    }

    /*Delete File*/

    public function deleteFileData(Request $request)
    {

        //print_r($_POST);

        $table = $request->table;
        if ($request->table === "homeBanners") {
          $table = "home_banners";
        } else if ($request->table === "homePromotions") {
          $table = "home_promotions";
        } else if ($request->table === "homeTvcs") {
          $table = "home_promotions";
        } 

        $result_query = DB::table($table)->select($request->field_data, 'created_at')->where([
          ['id', $request->id],
      ])->first();

        $exp_create_date = explode(" ", $result_query->created_at);

        $exp_created_at = explode("-", $exp_create_date[0]);

        $pathDate = $exp_created_at[0] . "/" . $exp_created_at[1];

        $filename = "";

        if ($request->field_data == "thumb_th") {
            $filename = $result_query->thumb_th;
        } else if ($request->field_data == "thumb_en") {
            $filename = $result_query->thumb_en;
        } else if ($request->field_data == "banner_th") {
            $filename = $result_query->banner_th;
        } else if ($request->field_data == "banner_en") {
            $filename = $result_query->banner_en;
        } else if ($request->field_data == "icon") {
            $filename = $result_query->icon;
        } else if ($request->field_data == "logo") {
            $filename = $result_query->logo;
        }

        @unlink(config('config.config_pathUpload') . "/" . $request->table . "/" . $pathDate . "/" . $filename);

        $result = DB::table($table)->where('id', $request->id)->update([$request->field_data => ""]);

        if ($result) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*Delete File*/

    public function deleteThumb(Request $request)
    {

        $post = $_POST;

        //Storage::disk('upload')->delete('abouts/'.$post['thumb']);

        if (isset($post['lang']) && $post['lang'] == "th") {

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $post['thumb']);

            $result = DB::table($post['table'])->where('id', $post['id'])->update(['thumb_th' => ""]);

        } else if (isset($post['lang']) && $post['lang'] == "en") {

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $post['thumb']);

            $result = DB::table($post['table'])->where('id', $post['id'])->update(['thumb_en' => ""]);

        } else {

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $post['thumb']);

            $result = DB::table($post['table'])->where('id', $post['id'])->update(['thumb' => ""]);

        }

        if ($result) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Other---------------------------------*/

    /*----------------------Process Album---------------------------------*/

    public function uploadFileAlbum(Request $request)
    {
        $post = $_POST;
        $files = $request->file('images');
        $html = "";
        foreach ($files as $file) {
            if ($file->isValid()) {
                $extension = $file->getClientOriginalName();
                $exp_exten = explode(" ", $extension);
                $extension = implode("_", $exp_exten);
                $size = $file->getClientSize();
                $mineType = $file->getMimeType();
                $filename = time() . "_" . $extension;

                $items = DB::table($post["table"])->where($post["field_refer"], $post["item_id"])->orderBy('order_by', 'desc')->first();
                if (isset($items->order_by)) {
                    $order_by = ($items->order_by + 1);
                    $exp_created_date = explode(" ", $items->created_at);
                    $exp_created_at = explode("-", $exp_created_date[0]);
                    $pathDate = $exp_created_at[0] . "/" . $exp_created_at[1];
                } else {
                    $order_by = 1;
                    $pathDate = date("Y") . "/" . date("m");
                }

                $pathThumb = config('config.config_pathUpload') . "/" . $post["thumb"] . "/" . $pathDate;
                File::makeDirectory($pathThumb, 0777, true, true);
                $file = $file->move($pathThumb, $filename);

                $command_insert = "";
                if ($post["item_id"] != "") {
                    $result_id = DB::table($post["table"])->insertGetId([
                        'thumb' => $filename,
                        $post["field_refer"] => $post["item_id"],
                        'size' => $size,
                        'type' => $mineType,
                        'visitor' => $request->ip(),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id,
                        'order_by' => $order_by,
                    ]);
                } else {
                    $result_id = DB::table($post["table"])->insertGetId([
                        'thumb' => $filename,
                        'size' => $size,
                        'type' => $mineType,
                        'visitor' => $request->ip(),
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s"),
                        'created_by' => Auth::user()->id,
                        'updated_by' => Auth::user()->id,
                        'order_by' => $order_by,
                    ]);
                }

                if ($result_id != "") {
                    $html_result = $result_id . "|$|" . $order_by;
                    $output = array(
                        'uploaded' => 'OK',
                        'html_result' => $html_result,
                    );
                } else {
                    $output = array('uploaded' => 'ERROR');
                }
            }
        }
        echo json_encode($output);
    }

    public function deleteFileAlbum(Request $request)
    {
        $post = $_POST;
        $result_query = DB::table($post['table'])->select('thumb', 'created_at')->where([
            [$post["field_refer"], $post['item_id']],
            ['order_by', $post['key']],
        ])->first();

        $exp_created_date = explode(" ", $result_query->created_at);
        $exp_created_at = explode("-", $exp_created_date[0]);
        $pathDate = $exp_created_at[0] . "/" . $exp_created_at[1];

        @unlink(config('config.config_pathUpload') . "/" . $post['thumb'] . "/" . $pathDate . "/" . $result_query->thumb);

        $result = DB::table($post["table"])->where([
            [$post["field_refer"], $post['item_id']],
            ['order_by', $post['key']],
        ])->delete();

        if ($result) {
            $output = array('uploaded' => 'OK');
        } else {
            $output = array('uploaded' => 'ERROR');
        }
        echo json_encode($output);

    }

    public function changeOrderAlbum(Request $request)
    {

        $post = $_POST;

        $exp_data = explode("|$|", $post["data"]);

        $arr_item = array();

        foreach ($exp_data as $key => $value) {

            $result = DB::table($post['table'])->select('id')->where([

                [$post["field_refer"], $post['item_id']],

                ['order_by', ($value + 1)],

            ])->first();

            $arr_item[$key] = $result->id;

        }

        // foreach ($arr_item as $key=>$value)

        // {

        //     $result = DB::table($post['table'])->where('id', $value)->update(['order_by' => ($key+1)]);

        // }

        if ($result) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Album---------------------------------*/

    /*----------------------Process Delete All-------------------------------*/

    public function deleteAllData(Request $request)
    {

        $post = $_POST;

        $exp_html_value = explode("|$|", $post["html_value"]);

        $bool = false;

        $table = $post['table'];

        $exp_table = explode("/", $table);

        if (isset($exp_table) && sizeof($exp_table) > 1) {

            $table = $exp_table[sizeof($exp_table) - 1];

        }

        foreach ($exp_html_value as $value) {

            $result = DB::table($table)->select('banner', 'thumb')->where("id", $value)->first();

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $result->banner);

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $result->thumb);

            $result_delete = DB::table($table)->where("id", $value)->delete();

            if (!$result_delete) {
                $bool = true;
            }

        }

        if (!$bool) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Delete All---------------------------*/

    /*----------------------Process Delete Album All-------------------------------*/

    public function deleteAlbumAllData(Request $request)
    {

        $post = $_POST;

        $exp_html_value = explode("|$|", $post["html_value"]);

        $sub_table = substr($post['table'], 0, -1);

        $table_item = $sub_table . "_items";

        $table_detail = $sub_table . "_details";

        $table_id = $sub_table . "_id";

        $bool = false;

        foreach ($exp_html_value as $value) {

            /*delete item*/

            $result_item = DB::table($table_item)->select('id', 'thumb')->where($table_id, $value)->get();

            if (sizeof($result_item) > 0) {

                foreach ($result_item as $value_item) {

                    @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $value_item->thumb);

                    $result_item_delete = DB::table($table_item)->where("id", $value_item->id)->delete();

                }

            }

            /*delete item*/

            /*delete detail*/

            $result_detail = DB::table($table_detail)->where($table_id, $value)->get();

            if (sizeof($result_detail) > 0) {

                foreach ($result_detail as $value_detail) {

                    $result_detail_delete = DB::table($table_detail)->where("id", $value_detail->id)->delete();

                }

            }

            /*delete detail*/

            /*delete*/

            $result = DB::table($post['table'])->select('banner', 'thumb')->where("id", $value)->first();

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $result->banner);

            @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $result->thumb);

            $result_delete = DB::table($post["table"])->where("id", $value)->delete();

            /*delete*/

            if (!$result_delete) {
                $bool = true;
            }

        }

        if (!$bool) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Delete All---------------------------*/

    /*----------------------Process Delete Banner-------------------------------*/

    public function deleteBanner(Request $request)
    {

        $post = $_POST;

        $result = DB::table($post['table'])->select('banner')->where("id", $post["id"])->first();

        @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $result->banner);

        $result_update = DB::table($post['table'])->where('id', $post["id"])->update(['banner' => '']);

        if ($result_update) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Delete Banner---------------------------*/

    /*----------------------Process Delete Banner Multi-------------------------------*/

    public function deleteBannerMulti(Request $request)
    {

        $post = $_POST;

        @unlink(config('config.config_pathUpload') . "/" . $post['table'] . "/" . $post["filename"]);

        $result_update = DB::table($post['table'])->where('id', $post["id"])->update([$post["banner_field"] => '']);

        if ($result_update) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process Delete Banner Multi---------------------------*/

    /*----------------------Process sortable data-------------------------------*/

    public function sortableData(Request $request)
    {

        $post = $_POST;

        $exp_html_sort = explode("|$|", $post["html_sort"]);

        $start_sort = 0;

        if (isset($post["start_sort"])) {

            if ($post["start_sort"] != "") {
                $start_sort = $post["start_sort"];
            }

        }

        $indexLoop = $start_sort;

        $bool = false;

        foreach ($exp_html_sort as $value) {

            $indexLoop++;

            $exp_value = explode("_", $value);

            $result_update = DB::table($post['table'])->where('id', $exp_value[1])->update(['order_by' => $indexLoop]);

        }

        echo "Y";

    }

    public function sortableSlideData(Request $request)
    {

        $post = $_POST;

        $exp_html_sort = explode("|$|", $post["html_sort"]);

        $start_sort = 0;

        if (isset($post["start_sort"])) {

            if ($post["start_sort"] != "") {
                $start_sort = $post["start_sort"];
            }

        }

        $indexLoop = $start_sort;

        $bool = false;

        foreach ($exp_html_sort as $value) {

            $indexLoop++;

            $exp_value = explode("_", $value);

            $result_update = DB::table($post['table'])->where('id', $exp_value[1])->update(['order_slide' => $indexLoop]);

        }

        echo "Y";

    }

    /*----------------------End Process sortable data---------------------------*/

    /*----------------------Process change order detail-------------------------------*/

    public function changeOrderDetail(Request $request)
    {

        $post = $_POST;

        $exp_html_content = explode("|$|", $post["data"]);

        $indexLoop = 0;

        foreach ($exp_html_content as $value) {

            $indexLoop++;

            $result_update = DB::table($post['table'])->where('id', $value)->update(['order_by' => $indexLoop]);

            // $exp_value = explode("_",$value);

            // $result_update = DB::table($post['table'])->where('id', $exp_value[3])->update(['order_by' => $indexLoop]);

        }

        echo "Y";

    }

    /*----------------------End Process change order detail---------------------------*/

    /*----------------------Process check active data-------------------------------*/

    public function checkHasActiveData(Request $request)
    {

        $post = $_POST;

        $result = DB::table($post["table"])->where([

            [$post["refer_field"], $post["refer_id"]],

        ])->first();

        if ($result) {
            if ($result->id != "") {
                echo "N";
            } else {
                echo "Y";
            }
        } else {
            echo "Y";
        }

    }

    /*----------------------End Process delete product detail-------------------------------*/

    /*----------------------Process check active data-------------------------------*/

    public function checkHasActiveAllData(Request $request)
    {

        $post = $_POST;

        $bool = false;

        $exp_refer_id = explode("|$|", $post["refer_id"]);

        if (sizeof($exp_refer_id) > 0) {

            foreach ($exp_refer_id as $value) {

                $result = DB::table($post["table"])->where([

                    [$post["refer_field"], $value],

                ])->first();

                if ($result) {
                    if ($result->id != "") {
                        $bool = true;
                    }
                }

            }

        }

        if (!$bool) {
            echo "Y";
        } else {
            echo "N";
        }

    }

    /*----------------------End Process delete product detail-------------------------------*/

}
