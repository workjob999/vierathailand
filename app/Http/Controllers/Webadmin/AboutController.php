<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Controllers\Api\AboutApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\About;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AboutController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        
    }

    public function create()
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $id = 1;
        $about = About::find($id);

        $pathDate = Functionphp::php_convertDateToPathFile($about->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "about" => $about,
            'pathDate' => $pathDate
        );

        if ($hasPermission["edit-about"]) {
            return view('web-admin.about.edit')->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $about = About::find($id);
        if (empty($about)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-about"]) {
            $homeBanner = new AboutApiController;
            $response = $homeBanner->update($request,$id);
            dd($request,$response);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/about?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/about?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        
    }

    public function deleteAllSubContent(Request $request)
    {
        
    }

    public function exportAllSubContent(Request $request)
    {
        
    }
}
