<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Webadmin\UserRequest;
use App\Model\Users;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    protected $limit_page = 30;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.users')) {
            if (Session::get('limit_page.users') == "All") {
                $this->limit_page = DB::table("users")->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.users');
            }

        } else {
            Session::put('limit_page.users', $this->limit_page);
        }

    }

    public function index()
    {
        if (Session::has('limit_page.users')) {
            if (Session::get('limit_page.users') == "All") {
                $this->limit_page = DB::table("users")->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.users');
            }

        } else {
            Session::put('limit_page.users', $this->limit_page);
        }

        $result_role = DB::table("role_user")->select('role_id')->where('user_id', Auth::user()->id)->first();

        $users = new Users;
        if ($result_role->role_id == 1) {
            $results = $users->paginate($this->limit_page);
        } else {
            $results = $users->join('role_user', function ($join) {
                $join->on('users.id', '=', 'role_user.user_id')
                    ->where('role_user.role_id', '<>', 1);
            })->paginate($this->limit_page);
        }

        $roles = Users::getRoles();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'users_db' => $results,
            "roles" => $roles,
        );

        if ($hasPermission["view-user-list"]) {
            return view("web-admin.users.index")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $arr_data = array();

        $disabled_data = false;
        $groups = Users::getRolesGroup();
        $role_user = Users::getRoles();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'disabled_data' => $disabled_data,
            'groups' => $groups,
            'role_user' => $role_user,
        );

        if ($hasPermission["create-user"]) {
            return view("web-admin.users.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/users/user'));
        }

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        $filename = "";
        $users = new Users;
        $arr_data["id"] = "";
        $arr_data["filename"] = "";

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["create-user"]) {
            $result = $users->storeUsers($request, $arr_data);
        } else {
            $result = "N";
        }

        //print_r($result);
        if ($result == "Y") {
            return redirect(url(config('config.config_pathCms') . '/users/user?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/users/user?rs=error'));
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $users = Users::find($id);
        if (empty($users)) {
            abort(404);
        }

        $groups = Users::getRolesGroup();
        $role_user = Users::getRoles();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "users" => $users,
            'groups' => $groups,
            'role_user' => $role_user,
        );

        if ($hasPermission["view-user-list"]) {
            return view("web-admin.users.show")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $users = Users::find($id);
        if (empty($users)) {
            $order = 0;
            abort(404);
        }

        $arr_data = array();

        $disabled_data = false;
        $groups = Users::getRolesGroup();
        $role_user = Users::getRoles();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "users" => $users,
            "arr_data" => $arr_data,
            "disabled_data" => $disabled_data,
            'groups' => $groups,
            'role_user' => $role_user,
        );

        if ($hasPermission["edit-user"]) {
            return view('web-admin.users.edit')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/users/user'));
        }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        $users = Users::find($id);
        if (empty($users)) {
            abort(404);
        }

        $filename = "";
        $arr_data["id"] = $id;
        $arr_data["filename"] = "";

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-user"]) {
            $result = $users->storeUsers($request, $arr_data);
        } else {
            $result = "N";
        }

        if ($result == "Y") {
            return redirect(url(config('config.config_pathCms') . '/users/user?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/users/user?rs=error'));
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $users = Users::find($id);
        if (empty($users)) {
            abort(404);
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-user"]) {
            $result = $users->delete();
            $roles = DB::table("role_user")->where('user_id', $id)->delete();
        } else {
            $result = false;
        }

        if ($result) {
            echo "Y";
        } else {
            echo "N";
        }

    }
}
