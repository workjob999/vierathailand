<?php

namespace App\Http\Controllers\Webadmin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Model\Roles;
use App\Http\Requests\Webadmin\RoleRequest;
use App\PermissionRole;

class RoleController extends Controller
{
    protected $limit_page=30;
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.users'))
        {
            if (Session::get('limit_page.users')=="All")
                $this->limit_page = DB::table("users")->count()+1;
            else
                $this->limit_page = Session::get('limit_page.users');
        }
        else
          Session::put('limit_page.users',$this->limit_page);
    }


    public function index()
    {
        if (Session::has('limit_page.users'))
        {
            if (Session::get('limit_page.users')=="All")
                $this->limit_page = DB::table("users")->count()+1;
            else
                $this->limit_page = Session::get('limit_page.users');
        }
        else
            Session::put('limit_page.users',$this->limit_page);

        $roles = new Roles;
        $results = $roles->paginate($this->limit_page);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'roles'=>$results
        );

        if ($hasPermission["view-role-list"])
            return view("web-admin.roles.index")->with($data);
        else
            return redirect(url(config('config.config_pathCms')));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $disabled_data = false;
        $permissionName = Roles::getPermissionName();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'disabled_data' => $disabled_data,
            'permissionName' => $permissionName
        );

        if ($hasPermission["create-role"])
            return view("web-admin.roles.create")->with($data);
        else
            return redirect(url(config('config.config_pathCms').'/users/role'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $roles = new Roles;
        $arr_data = array();

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["create-role"])
            $result = $roles->storeRoles($request,$arr_data);
        else
            $result = "N";

        //dd($result);
        if ($result=="Y")
          return redirect(url(config('config.config_pathCms').'/users/role?rs=success'));
        else
          return redirect(url(config('config.config_pathCms').'/users/role?rs=error'));
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $roles = Roles::find($id);
        if(empty($roles))
          abort(404);

        $permissionName = Roles::getPermissionName();
        $permissionRole = Roles::getPermissionByUser($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "roles" => $roles,
            'permissionName' => $permissionName,
            'permissionRole' => $permissionRole
        );

        if ($hasPermission["view-role-list"])
            return view("web-admin.roles.show")->with($data);
        else
            return redirect(url(config('config.config_pathCms')));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $roles = Roles::find($id);
        if(empty($roles))
        {
            $order = 0;
            abort(404);
        }

        $disabled_data = false;
        $permissionName = Roles::getPermissionName();
        $permissionRole = Roles::getPermissionByUser($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "roles" => $roles,
            "disabled_data" => $disabled_data,
            'permissionName' => $permissionName,
            'permissionRole' => $permissionRole
        );
        //dd($permissionName);

        if ($hasPermission["edit-role"])
            return view('web-admin.roles.edit')->with($data);
        else
            return redirect(url(config('config.config_pathCms').'/users/role'));
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        $roles = Roles::find($id);
        if(empty($roles))
          abort(404);
      
        $arr_data["id"] = $id;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-role"])
            $result = $roles->storeRoles($request,$arr_data);
        else
            $result = "N";

        if ($result=="Y")
          return redirect(url(config('config.config_pathCms').'/users/role?rs=success'));
        else
          return redirect(url(config('config.config_pathCms').'/users/role?rs=error'));
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $roles = Roles::find($id);
        if(empty($roles))
            abort(404);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-user"])
        {
            $permissionRole = DB::table("permission_role")->where('role_id',$id)->delete();
            $result = $roles->delete();
        }
        else
            $result = false;

        if ($result)
          echo "Y";
        else
          echo "N";
    }
}
