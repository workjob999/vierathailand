<?php

namespace App\Http\Controllers\Webadmin;

use App\Http\Classes\Helper;
use App\Http\Controllers\Api\PromotionsApiController;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\GlobalData;
use App\Model\Promotions;
use App\PermissionRole;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Carbon\Carbon;

class PromotionsController extends Controller
{
    protected $limit_page = 30;

    public function __construct()
    {
        $this->middleware('auth');
        if (Session::has('limit_page.promotions')) {
            if (Session::get('limit_page.promotions') == "All") {
                $this->limit_page = Promotions::count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.promotions');
            }
        } else {
            Session::put('limit_page.promotions', $this->limit_page);
        }
    }

    public function index($orderBy = "", $ascdesc = "", $search = "")
    {
        $query = Promotions::query();
        if (Session::has('limit_page.promotions')) {
            if (Session::get('limit_page.promotions') == "All") {
                $this->limit_page = $query->count() + 1;
            } else {
                $this->limit_page = Session::get('limit_page.promotions');
            }
        } else {
            Session::put('limit_page.promotions', $this->limit_page);
        }

        $arr_data["orderBy"] = "order_by";
        $arr_data["ascdesc"] = "ASC";
        $arr_data["orderByUrl"] = "order_by";
        $arr_data["urlSearch"] = "";
        $arr_data["status_icon_sort"] = "";

        $search = (isset($search) && $search != "") ? trim($search) : "";
        $orderByUrl = (isset($orderBy) && $orderBy != "") ? $orderBy : "order_by";
        $orderBy = ($orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "order_by" || $orderBy == "") ? "order_by" : $orderBy;
        $orderBy = ($orderBy == "title") ? "title" : $orderBy;
        $orderBy = ($orderBy == "modify") ? "updated_at" : $orderBy;
        $ascdesc = (isset($ascdesc) && $ascdesc != "") ? $ascdesc : "ASC";

        //$arr_data["page"] = $page;
        $arr_data["search"] = $search;
        $arr_data["orderBy"] = $orderBy;
        $arr_data["ascdesc"] = $ascdesc;
        $arr_data["orderByUrl"] = $orderByUrl;
        $arr_data["urlSearch"] = "/" . $search;

        $arr_data["status_icon_sort"] = "";

        $promotions = new PromotionsApiController;
        $dataPromotions = $promotions->getPromotionsList("", 2, $orderBy, $ascdesc, $this->limit_page, $search);

        //$arr_promotions = json_decode($dataPromotions->content(), true);

        $arr_data["start_sort"] = $dataPromotions->firstItem() - 1;

        if (isset($dataPromotions) && sizeof($dataPromotions)>0)
        {
            foreach ($dataPromotions as $data)
            {
                $data->pathFile = Functionphp::php_convertDateToPathFile($data->created_at);
            }
        }

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $getUsersData = GlobalData::getUsersData();

        $data = array(
            "hasPermission" => $hasPermission,
            "promotions" => $dataPromotions,
            "arr_data" => $arr_data,
            'getUsersData' => $getUsersData,
        );

        if ($hasPermission["view-promotions-list"]) {
            return view("web-admin.promotions.index")->with($data);
        } else {
            return redirect(Route("cms-dashboard"));
        }
    }

    public function create()
    {
        $promotions = Promotions::select('order_by')->latest('order_by')->first();

        $arr_data = array();
        if (empty($promotions)) {
            $arr_data["order_by"] = 1;
        } else {
            $arr_data["order_by"] = $promotions->order_by + 1;
        }

        $arr_data["disabled_data"] = false;

        $pathDate = date("Y") . "/" . date("m");

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            'arr_data' => $arr_data,
            'pathDate' => $pathDate,
            "action_page" => "create"
        );

        if ($hasPermission["create-promotions"]) {
            return view("web-admin.promotions.create")->with($data);
        } else {
            return redirect(url(config('config.config_pathCms') . '/promotions'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    //public function store(Request $request)
    {
        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["create-promotions"]) {
            $promotion = new PromotionsApiController;
            $response = $promotion->store($request, 2, 0);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/promotions?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/promotions?rs=error'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $promotions = Promotions::find($id);
        if (empty($promotions)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $promotions->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($promotions->startDate!="")
            $startDate = $now->parse($promotions->startDate)->format("d/m/Y");
        if ($promotions->endDate!="")
            $endDate = $now->parse($promotions->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = true;
        $pathDate = Functionphp::php_convertDateToPathFile($promotions->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "promotions" => $promotions,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "view"
        );

        if ($hasPermission["view-promotions-list"]) {
            return view('web-admin.promotions.show')->with($data);
        } else {
            return redirect(url(config('config.config_pathCms')));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promotions = Promotions::find($id);
        if (empty($promotions)) {
            $order = 0;
            $arr_data["order_by"] = 1;
            abort(404);
        } else {
            $arr_data["order_by"] = $promotions->order_by;
        }

        $now = Carbon::now();
        $startDate = "";
        $endDate = "";
        if ($promotions->startDate!="")
            $startDate = $now->parse($promotions->startDate)->format("d/m/Y");
        if ($promotions->endDate!="")
            $endDate = $now->parse($promotions->endDate)->format("d/m/Y");

        $arr_data["disabled_data"] = false;
        $pathDate = Functionphp::php_convertDateToPathFile($promotions->created_at);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        $data = array(
            "hasPermission" => $hasPermission,
            "promotions" => $promotions,
            "arr_data" => $arr_data,
            'pathDate' => $pathDate,
            "startDate" => $startDate,
            "endDate" => $endDate,
            "action_page" => "edit"
        );

        if ($hasPermission["edit-promotions"]) {
            return view('web-admin.promotions.edit')->with($data);
        } else {
            return redirect(Route("cms-promotions-index"));
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $promotions = Promotions::find($id);
        if (empty($promotions)) {
            abort(404);
        }

        $response_success = false;

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);

        if ($hasPermission["edit-promotions"]) {
            $promotion = new PromotionsApiController;
            //dd($request,$promotions);
            $response = $promotion->update($request,$id);
            $result_array = json_decode($response->content(), true);
            $result = $result_array["success"];
        } else {
            $result = "fail";
        }

        // if ($hasPermission["edit-promotions"]) {
        //     $data = array(
        //         "firstname" => $request->input("firstname"),
        //         "lastname" => $request->input("lastname"),
        //         "email" => $request->input("email"),
        //         "phone" => $request->input("phone"),
        //         "budget" => $request->input("budget"),
        //         "user_id" => Auth::user()->id,
        //     );
        //     $url = env("APP_URL") . "/api/promotions/" . $id;
        //     $json_data = json_encode($data);
        //     $header = [
        //         'Content-Type: application/json',
        //     ];
        //     $response = Helper::curl_customRequest($url, $json_data, "PATCH", "", 20, $header);
        //     $data_response = json_decode($response);
        //     $response_success = $data_response->success;
        // }

        if ($result) {
            return redirect(url(config('config.config_pathCms') . '/promotions?rs=success'));
        } else {
            return redirect(url(config('config.config_pathCms') . '/promotions?rs=error'));
        }
    }

    public function deleteSubContent($id)
    {
        $promotions = Promotions::find($id);

        $permission = new PermissionRole;
        $hasPermission = $permission->all_check_permission(Auth::user()->id);
        if ($hasPermission["edit-promotions"]) {
            // $pathDate = Functionphp::php_convertDateToPathFile($promotions->created_at);
            // $pathThumb = config('config.config_pathUpload')."/promotions/".$pathDate;

            // Helper::storageProcess('delete',$pathThumb,$promotions->thumb_th);
            // Helper::storageProcess('delete',$pathThumb,$promotions->banner_th);

            $result_delete = $promotions->delete();
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function deleteAllSubContent(Request $request)
    {
        $refer_id = $request->refer_id;
        $exp_refer_id = explode("|$|", $refer_id);
        if (isset($exp_refer_id) && sizeof($exp_refer_id) > 0) {
            foreach ($exp_refer_id as $id) {
                $promotions = Promotions::find($id);

                $permission = new PermissionRole;
                $hasPermission = $permission->all_check_permission(Auth::user()->id);
                if ($hasPermission["edit-promotions"]) {
                    // $pathDate = Functionphp::php_convertDateToPathFile($promotions->created_at);
                    // $pathThumb = config('config.config_pathUpload')."/promotions/".$pathDate;

                    // Helper::storageProcess('delete',$pathThumb,$promotions->thumb_th);
                    // Helper::storageProcess('delete',$pathThumb,$promotions->banner_th);

                    $result_delete = $promotions->delete();
                }
            }
            echo "Y";
        } else {
            echo "N";
        }
    }

    public function exportAllSubContent(Request $request)
    {
        $promotions = Promotions::orderBy("created_at", "DESC")->get();
        if (empty($promotions)) {
            abort(404);
        }

        if (isset($promotions)) {
            foreach ($promotions as $promotion) {
                $exp_phone = explode("-", $promotion->phone);
                $promotion->phone = implode("", $exp_phone);
            }
        }

        //dd($promotions);
        $data = array(
            "promotions" => $promotions,
            "project" => "kara",
        );

        $filename = date("Ymd-His") . "_export_promotion_kara-ari-rama6";

        //dd($data);
        Excel::create($filename, function ($excel) use ($data) {
            $excel->sheet('Registers', function ($sheet) use ($data) {
                $sheet->loadView('excel.promotions')->with($data);
            });
        })->export("xls");
    }
}
