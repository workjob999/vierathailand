<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Model\Registers;
use App\Http\Classes\Helper;
use App\Model\Functions\Functionphp;
use Carbon\Carbon;
use File;

class DealerApiController extends Controller
{
    public function store($request, $type = 1, $register_id = 0)
    {
        //dd($request);
        $ip_address = $request->ip();
        if ($request->ip() == '::1') {
            $ip_address = '127.0.0.1';
        }

        //$current_datetime = date('Y-m-d H:i:s');
        $now = Carbon::now();
        $current_datetime = $now->toDateTimeString();

        $status = '';
        if ($type == 2) {
            $status = isset($request->status) ? $request->status : 'N';
        }
        if (isset($request->front) && $request->front)
            $status = isset($request->status) ? $request->status : 'N';

        $register = Registers::find($register_id);
        if (empty($register)) {
            $register = new Registers();
            $register->created_at = $current_datetime;
            if ($type == 2) {
                $register->created_by = Auth::user()->id;
            }

            $pathFile = $now->format('Y/m');
            $bool_addnew = true;
        } else {
            $pathFile = Functionphp::php_convertDateToPathFile($register->created_at);
        }

        if ($request->file('thumb') !== null) {
            $filename_thumb = '';
            if ($request->input('old_thumb') !== null && $request->input('old_thumb') != '') {
                $filename_thumb = $request->input('old_thumb');
            }

            $pathThumb = config('config.config_pathUpload').'/registers/'.$pathFile;
            File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

            if ($request->hasFile('thumb')) {
                $extension = $request->file('thumb')->getClientOriginalName();

                Helper::storageProcess('delete', $pathThumb, $filename_thumb);
                $filename_thumb = 'thumb-'.time().'.'.$extension;
                Helper::storageProcess('save', $pathThumb, $filename_thumb, 'thumb', $request);
            }

            $register->thumb = $filename_thumb;
        }

        if ($request->input('code')!==null)
            $register->code = $request->input('code');
        $register->type = $request->input('type');
        $register->fullname = $request->input('fullname');
        $register->phone = $request->input('phone');
        $register->email = $request->input('email');
        $register->lineid = $request->input('lineid');
        $register->refer = $request->input('refer');
        $register->sale_place = $request->input('sale_place');
        if ($type == 1) {
            $imp_facebook = implode("\n", $request->input('facebook_page'));
            $register->facebook = $imp_facebook;

            $filename = '';
            if ($request->baseimage != '') {
                $pathThumb = config('config.config_pathUpload').'/registers/'.$pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);
                //$filename = 'profile-'.time().'.jpg';
                // $base64_image_string = $request->baseimage;
                // @copy($base64_image_string, $pathThumb.'/'.$filename);

                $data = $request->baseimage;
                list($img_type, $data) = explode(';', $data);

                $exp_img_type = explode('/', $img_type);
                $filename = 'profile-'.time().'.'.$exp_img_type[1];
                list(, $data) = explode(',', $data);
                $data = base64_decode($data);

                @file_put_contents($pathThumb.'/'.$filename, $data);

                //dd('test');
            }

            $register->thumb = $filename;
        } else {
            $register->facebook = $request->input('facebook');
        }
        $register->updated_at = $current_datetime;

        if ($type == 2) {
            $register->updated_by = Auth::user()->id;
        }

        $register->visitor = $ip_address;

        if ($status != '') {
            $register->status = $status;
        }

        if ($request->input('order_by') !== null) {
            $register->order_by = $request->input('order_by');
        } else {
            $result_order = Registers::select('order_by')->orderBy('order_by', 'DESC')->first();
            if (empty($result_order)) {
                $order_by = 1;
            } else {
                $order_by = $result_order->order_by + 1;
            }

            $register->order_by = $order_by;
        }

        $register->save();

        $register_id = $register->id;
        if ($register_id === null || $register_id == 0) {
            return response()->json(array('status' => 'fail', 'register_id' => $register_id, 'message' => 'Register success'), 404);
        } else {
            return response()->json(array('status' => 'success', 'register_id' => $register_id), 200);
        }
    }

    public function getRegisterList($request, $type = 1, $orderBy = 'order_by', $ascdesc = 'asc', $limit_page = 0, $keyword = '')
    {
        $query = Registers::query();
        $query2 = Registers::query();

        if ($keyword != '') {
            $query = $query->searchKeyword($keyword);
        }

        $status = isset($request->status)?$request->status:"";
        if ($status!=="")
            $query = $query->where("status",$status);

        $typeRegister = isset($request->type)?$request->type:"";
        if ($typeRegister!=="")
            $query = $query->whereType($typeRegister);

        
        if ($limit_page == 0) {
            $query2 = clone $query;
            $query2 = $query2->whereNull("code");
            $query = $query->whereNotNull("code");

            //dd($query->get(),$query2->get());

            $query = $query->orderBy("code", "ASC")->orderBy("created_at","DESC");
            $query2 = $query2->orderBy("created_at", "DESC");

            $resultFirst = $query->get();
            $resultSecond = $query2->get();

            $collection = collect($resultFirst);
            $merged     = $collection->merge($resultSecond);
            $result   = $merged->all();
            
            //$result = $resultFirst->merge($resultSecond);
            //$query = $query->filterOrderBy($orderBy, $ascdesc);
            // $query = $query->orderBy("code", "ASC");
            // $result = $query->get();
        } else {
            $result = $query->paginate($limit_page);
        }

        if ($type == 1) {
            return response()->json(array('status' => 'success', 'data' => $result), 200);
        } else {
            return $result;
        }
    }

    public function checkDuplicate($request)
    {
        $countFullname = Registers::whereFullname($request->input('fullname'))->count();
        if ($countFullname>0)
            return response()->json(array('status' => 'success', 'duplicate' => "fullname"), 200);

        $countPhone = Registers::wherePhone($request->input('phone'))->count();
        if ($countPhone>0)
            return response()->json(array('status' => 'success', 'duplicate' => "phone"), 200);

        return response()->json(array('status' => 'success', 'duplicate' => ""), 200);
    }
}
