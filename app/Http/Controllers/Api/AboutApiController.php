<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\About;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AboutApiController extends Controller
{

    public function getAboutFront($request,$limit)
    {
        $now = Carbon::now();
        $current_date = $now->format("Y-m-d");

        $query = About::query();
        $query = $query->whereStatus("Y");
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('startDate', '<=', $current_date)
                            ->orWhere('startDate', '0000-00-00')
                            ->orWhere('startDate', null);
                        });
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('endDate', '>=', $current_date)
                            ->orWhere('endDate', '0000-00-00')
                            ->orWhere('endDate', null);
                        });
        return $query->skip(0)->take($limit)->get();
    }

    public function getAboutList($request, $type = 1, $orderBy = 'order_by', $ascdesc = 'asc', $limit_page = 0, $keyword = '')
    {
        $query = About::query();

        if ($keyword != '') {
            $query = $query->searchKeyword($keyword);
        }

        if ($orderBy == "title") {
            $query = $query->filterOrderBy("title_th", $ascdesc);
            $query = $query->filterOrderBy("title_en", $ascdesc);
            $query = $query->filterOrderBy("title_ch", $ascdesc);
        } else {
            $query = $query->filterOrderBy($orderBy, $ascdesc);
        }

        if ($limit_page == 0) {
            $result = $query->get();
        } else {
            $result = $query->paginate($limit_page);
        }

        if ($type == 1) {
            return response()->json(array('status' => 'success', 'data' => $result), 200);
        } else {
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = About::query();

        //return $request->input();

        if ($request->input('where') !== null) {
            $where = $request->input('where');
            foreach ($where as $key => $value) {
                $field = $key;
                $cond = key($value);
                $var = pos($value);
                if ($cond == 7) {
                    $var = "%" . $var . "%";
                }

                $query = $query->where($field, config("config.config_arr_condition")[$cond], $var);
            }
        }

        if ($request->input('order') !== null) {
            $order = $request->input('order');
            foreach ($order as $key => $value) {
                $query = $query->orderBy($key, $value);
            }
        }

        if ($request->input('filter') !== null) {
            $filter = $request->input('filter');

            if ($filter["limit"] !== null && $filter["limit"] != 0) {
                $query = $query->take($filter["limit"]);
            }
            if ($filter["offset"] !== null && $filter["offset"] != 0) {
                $query = $query->offset(($filter["offset"] - 1));
            }
            if ($filter["skip"] !== null && $filter["skip"] != 0) {
                $query = $query->skip($filter["skip"]);
            }
        }

        $data = $query->get();

        return response()->json([
            "success" => true,
            "total" => sizeof($data),
            'data' => $data,
            "msg" => "Get data success",
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = About::find($id);
        if (empty($data)) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "",
            ], 402);
        } else {
            return response()->json([
                "success" => true,
                "total" => sizeof($data),
                'data' => $data,
                "msg" => "Get data by id success",
            ], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $validation = Validator::make(
            array(
                'title_th' => $request->input('title_th'),
                //'thumb' => $request->input('thumb')
            ),
            array(
                'title_th' => array('required'),
                //'thumb' => array('required')
                //'email' => array( 'required', 'email' ),
            )
        );

        if ($validation->fails()) {
            //return $validation->errors();
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => $validation->errors(),
            ], 422);
        } else {
            // $duplicateEmail = About::whereEmail($request->input('email'))->count();
            // if ($duplicateEmail)
            // {
            //     return response()->json([
            //         "success"=>false,
            //         'data' => "",
            //         "msg" => "Duplicate"
            //     ], 200);
            // }

            $ip_address = $request->ip();
            if ($request->ip() == "::1") {
                $ip_address = "127.0.0.1";
            }

            $now = Carbon::now();
            $current_datetime = $now->toDateTimeString();
            $pathFile = $now->format('Y/m');

            $fileThumb = "";
            if ($request->file('thumb') !== null) {
                $filename_thumb = '';
                if ($request->input('old_thumb') !== null && $request->input('old_thumb') != '') {
                    $filename_thumb = $request->input('old_thumb');
                }

                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb')) {
                    $extension = $request->file('thumb')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb);
                    $filename_thumb = 'thumb-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb, 'thumb', $request);
                }

                $fileThumb = $filename_thumb;
            }

            $status = ($request->input("status") !== null) ? $request->input("status") : "Y";
            $status_th = "Y";
            $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
            $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
            $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
            $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;
            $order_by = ($request->input("order_by") !== null) ? $request->input("order_by") : 0;

            $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
            $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

            try {
                $homeBanners = new About;
                $homeBanners->thumb = $fileThumb;
                $homeBanners->url = $request->input("url");
                $homeBanners->title_th = $request->input("title_th");
                $homeBanners->detail_th = $request->input("detail_th");
                $homeBanners->content_th = $request->input("content_th");
                $homeBanners->status_th = $status_th;
                $homeBanners->title_en = $request->input("title_en");
                $homeBanners->detail_en = $request->input("detail_en");
                $homeBanners->content_en = $request->input("content_en");
                $homeBanners->status_en = $status_en;
                $homeBanners->title_ch = $request->input("title_ch");
                $homeBanners->detail_ch = $request->input("detail_ch");
                $homeBanners->content_ch = $request->input("content_ch");
                $homeBanners->startDate = $startDate;
                $homeBanners->endDate = $endDate;
                $homeBanners->status_ch = $status_ch;
                $homeBanners->visitor = $ip_address;
                $homeBanners->status = $status;
                $homeBanners->created_at = $current_datetime;
                $homeBanners->updated_at = $current_datetime;
                $homeBanners->created_by = $user_id;
                $homeBanners->updated_by = $user_id;
                $homeBanners->order_by = $order_by;
                $homeBanners->save();

                $data = array(
                    "id" => $homeBanners->id,
                );

                if (!empty($homeBanners->id) && $homeBanners->id != 0) {
                    return response()->json([
                        "success" => true,
                        'data' => $data,
                        "msg" => "Register - insert data success",
                    ], 200);
                } else {
                    return response()->json([
                        "success" => false,
                        'data' => "",
                        "msg" => "Register - error",
                    ], 502);
                }

            } catch (\Exception $e) {
                dd($e);
                return response()->json([
                    "success" => false,
                    'data' => "",
                    "msg" => "Register - error",
                ], 502);
                // something went wrong
            }
        }
        //$request->input("firstname");
        //return $validatedData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->input("firstname");
        $ip_address = $request->ip();
        if ($request->ip() == "::1") {
            $ip_address = "127.0.0.1";
        }

        $current_datetime = Carbon::now();

        $status = ($request->input("status") !== null) ? $request->input("status") : "N";
        $status_th = "Y";
        $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
        $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
        $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
        $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;

        // $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
        // $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

        try {
            $homeBanners = About::find($id);

            $fileThumb = $homeBanners->thumb;
            if ($request->file('thumb') !== null) {
                $filename_thumb = $homeBanners->thumb;

                $pathFile = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb')) {
                    $extension = $request->file('thumb')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb);
                    $filename_thumb = 'thumb-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb, 'thumb', $request);
                }

                $fileThumb = $filename_thumb;
            }

            $homeBanners->thumb = $fileThumb;
            //$homeBanners->url = $request->input("url");
            $homeBanners->title_th = $request->input("title_th");
            $homeBanners->detail_th = $request->input("detail_th");
            $homeBanners->content_th = $request->input("content_th");
            $homeBanners->status_th = $status_th;
            $homeBanners->title_en = $request->input("title_en");
            $homeBanners->detail_en = $request->input("detail_en");
            $homeBanners->content_en = $request->input("content_en");
            $homeBanners->status_en = $status_en;
            $homeBanners->title_ch = $request->input("title_ch");
            $homeBanners->detail_ch = $request->input("detail_ch");
            $homeBanners->content_ch = $request->input("content_ch");
            $homeBanners->status_ch = $status_ch;
            // $homeBanners->startDate = $startDate;
            // $homeBanners->endDate = $endDate;
            $homeBanners->visitor = $ip_address;
            $homeBanners->status = $status;
            $homeBanners->updated_at = $current_datetime;
            $homeBanners->updated_by = $user_id;
            //$homeBanners->order_by = $request->input("order_by");
            $homeBanners->save();

            $data = array(
                "id" => $homeBanners->id,
            );

            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - update data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                //"msg" => "Register - error",
                "msg" => $e
            ], 502);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $delete = About::find($id)->delete();

            $data = array(
                "id" => $id,
            );
            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - delete data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }
}
