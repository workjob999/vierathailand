<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\HomeTvcs;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeTvcsApiController extends Controller
{

    public function getHomeTvcsList($request, $type = 1, $orderBy = 'order_by', $ascdesc = 'asc', $limit_page = 0, $keyword = '')
    {
        $query = HomeTvcs::query();

        if ($keyword != '') {
            $query = $query->searchKeyword($keyword);
        }

        if ($orderBy == "title") {
            $query = $query->filterOrderBy("title_th", $ascdesc);
            $query = $query->filterOrderBy("title_en", $ascdesc);
            $query = $query->filterOrderBy("title_ch", $ascdesc);
        } else {
            $query = $query->filterOrderBy($orderBy, $ascdesc);
        }

        if ($limit_page == 0) {
            $result = $query->get();
        } else {
            $result = $query->paginate($limit_page);
        }

        if ($type == 1) {
            return response()->json(array('status' => 'success', 'data' => $result), 200);
        } else {
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = HomeTvcs::query();

        //return $request->input();

        if ($request->input('where') !== null) {
            $where = $request->input('where');
            foreach ($where as $key => $value) {
                $field = $key;
                $cond = key($value);
                $var = pos($value);
                if ($cond == 7) {
                    $var = "%" . $var . "%";
                }

                $query = $query->where($field, config("config.config_arr_condition")[$cond], $var);
            }
        }

        if ($request->input('order') !== null) {
            $order = $request->input('order');
            foreach ($order as $key => $value) {
                $query = $query->orderBy($key, $value);
            }
        }

        if ($request->input('filter') !== null) {
            $filter = $request->input('filter');

            if ($filter["limit"] !== null && $filter["limit"] != 0) {
                $query = $query->take($filter["limit"]);
            }
            if ($filter["offset"] !== null && $filter["offset"] != 0) {
                $query = $query->offset(($filter["offset"] - 1));
            }
            if ($filter["skip"] !== null && $filter["skip"] != 0) {
                $query = $query->skip($filter["skip"]);
            }
        }

        $data = $query->get();

        return response()->json([
            "success" => true,
            "total" => sizeof($data),
            'data' => $data,
            "msg" => "Get data success",
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = HomeTvcs::find($id);
        if (empty($data)) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "",
            ], 402);
        } else {
            return response()->json([
                "success" => true,
                "total" => sizeof($data),
                'data' => $data,
                "msg" => "Get data by id success",
            ], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $validation = Validator::make(
            array(
                'title_th' => $request->input('title_th'),
                //'thumb' => $request->input('thumb')
            ),
            array(
                'title_th' => array('required'),
                //'thumb' => array('required')
                //'email' => array( 'required', 'email' ),
            )
        );

        if ($validation->fails()) {
            //return $validation->errors();
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => $validation->errors(),
            ], 422);
        } else {
            // $duplicateEmail = HomeTvcs::whereEmail($request->input('email'))->count();
            // if ($duplicateEmail)
            // {
            //     return response()->json([
            //         "success"=>false,
            //         'data' => "",
            //         "msg" => "Duplicate"
            //     ], 200);
            // }

            $ip_address = $request->ip();
            if ($request->ip() == "::1") {
                $ip_address = "127.0.0.1";
            }

            $now = Carbon::now();
            $current_datetime = $now->toDateTimeString();
            $pathFile = $now->format('Y/m');

            //thumb th
            $fileThumbTh = "";
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = '';

                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }


            //thumb en
            $fileThumbEn = "";
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = '';

                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }


            //thumb ch
            $fileThumbCh = "";
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = '';

                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }

            $status = ($request->input("status") !== null) ? $request->input("status") : "Y";
            $status_th = "Y";
            $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
            $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
            $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
            $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;
            $order_by = ($request->input("order_by") !== null) ? $request->input("order_by") : 0;

            $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
            $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

            try {
                $homeTvcs = new HomeTvcs;
                $homeTvcs->url = $request->input("url");
                $homeTvcs->youtube = $request->input("youtube");
                $homeTvcs->thumb_th = $fileThumbTh;
                $homeTvcs->title_th = $request->input("title_th");
                $homeTvcs->detail_th = $request->input("detail_th");
                $homeTvcs->status_th = $status_th;
                $homeTvcs->thumb_en = $fileThumbEn;
                $homeTvcs->title_en = $request->input("title_en");
                $homeTvcs->detail_en = $request->input("detail_en");
                $homeTvcs->status_en = $status_en;
                $homeTvcs->thumb_ch = $fileThumbCh;
                $homeTvcs->title_ch = $request->input("title_ch");
                $homeTvcs->detail_ch = $request->input("detail_ch");
                $homeTvcs->startDate = $startDate;
                $homeTvcs->endDate = $endDate;
                $homeTvcs->status_ch = $status_ch;
                $homeTvcs->visitor = $ip_address;
                $homeTvcs->status = $status;
                $homeTvcs->created_at = $current_datetime;
                $homeTvcs->updated_at = $current_datetime;
                $homeTvcs->created_by = $user_id;
                $homeTvcs->updated_by = $user_id;
                $homeTvcs->order_by = $order_by;
                $homeTvcs->save();

                $data = array(
                    "id" => $homeTvcs->id,
                );

                if (!empty($homeTvcs->id) && $homeTvcs->id != 0) {
                    return response()->json([
                        "success" => true,
                        'data' => $data,
                        "msg" => "Register - insert data success",
                    ], 200);
                } else {
                    return response()->json([
                        "success" => false,
                        'data' => "",
                        "msg" => "Register - error",
                    ], 502);
                }

            } catch (\Exception $e) {
                dd($e);
                return response()->json([
                    "success" => false,
                    'data' => "",
                    "msg" => "Register - error",
                ], 502);
                // something went wrong
            }
        }
        //$request->input("firstname");
        //return $validatedData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->input("firstname");
        $ip_address = $request->ip();
        if ($request->ip() == "::1") {
            $ip_address = "127.0.0.1";
        }

        $current_datetime = Carbon::now();

        $status = ($request->input("status") !== null) ? $request->input("status") : "N";
        $status_th = "Y";
        $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
        $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
        $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
        $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;

        $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
        $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

        try {
            $homeTvcs = HomeTvcs::find($id);

            //thumb th
            $fileThumbTh = $homeTvcs->thumb_th;
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = $homeTvcs->thumb_th;

                $pathFile = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }


            //thumb en
            $fileThumbEn = $homeTvcs->thumb_en;
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = $homeTvcs->thumb_en;

                $pathFile = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }


            //thumb ch
            $fileThumbCh = $homeTvcs->thumb_ch;
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = $homeTvcs->thumb_ch;

                $pathFile = Functionphp::php_convertDateToPathFile($homeTvcs->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }

            $homeTvcs->url = $request->input("url");
            $homeTvcs->youtube = $request->input("youtube");
            $homeTvcs->thumb_th = $fileThumbTh;
            $homeTvcs->title_th = $request->input("title_th");
            $homeTvcs->detail_th = $request->input("detail_th");
            $homeTvcs->status_th = $status_th;
            $homeTvcs->thumb_en = $fileThumbEn;
            $homeTvcs->title_en = $request->input("title_en");
            $homeTvcs->detail_en = $request->input("detail_en");
            $homeTvcs->status_en = $status_en;
            $homeTvcs->thumb_ch = $fileThumbCh;
            $homeTvcs->title_ch = $request->input("title_ch");
            $homeTvcs->detail_ch = $request->input("detail_ch");
            $homeTvcs->status_ch = $status_ch;
            $homeTvcs->startDate = $startDate;
            $homeTvcs->endDate = $endDate;
            $homeTvcs->visitor = $ip_address;
            $homeTvcs->status = $status;
            $homeTvcs->updated_at = $current_datetime;
            $homeTvcs->updated_by = $user_id;
            $homeTvcs->order_by = $request->input("order_by");
            $homeTvcs->save();

            $data = array(
                "id" => $homeTvcs->id,
            );

            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - update data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $homeTvc = HomeTvcs::find($id);
            if (empty($homeTvc))
            {
                about(404);
            }
            $pathFile = Functionphp::php_convertDateToPathFile($homeTvc->created_at);
            $pathThumb = config('config.config_pathUpload') . '/homeTvcs/' . $pathFile;
            Helper::storageProcess('delete', $pathThumb, $homeTvc->thumb_th);
            Helper::storageProcess('delete', $pathThumb, $homeTvc->thumb_en);
            Helper::storageProcess('delete', $pathThumb, $homeTvc->thumb_ch);

            $delete = $homeTvc->delete();

            $data = array(
                "id" => $id,
            );
            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - delete data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }
}
