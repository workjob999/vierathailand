<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\HomeBanners;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class HomeBannersApiController extends Controller
{

    public function getHomeBannersFront($request, $limit)
    {
        $now = Carbon::now();
        $current_date = $now->format("Y-m-d");

        $query = HomeBanners::query();
        $query = $query->whereStatus("Y");
        $query = $query->where(function ($query2) use ($current_date) {
            $query2->whereDate('startDate', '<=', $current_date)
                ->orWhere('startDate', '0000-00-00')
                ->orWhere('startDate', null);
        });
        $query = $query->where(function ($query2) use ($current_date) {
            $query2->whereDate('endDate', '>=', $current_date)
                ->orWhere('endDate', '0000-00-00')
                ->orWhere('endDate', null);
        });
        if ($limit == 0) {
            return $query->orderBy("order_by", "ASC")->orderBy("updated_at", "DESC")->get();
        } else {
            return $query->orderBy("order_by", "ASC")->orderBy("updated_at", "DESC")->skip(0)->take($limit)->get();
        }
    }

    public function getHomeBannersList($request, $type = 1, $orderBy = 'order_by', $ascdesc = 'asc', $limit_page = 0, $keyword = '')
    {
        $query = HomeBanners::query();

        if ($keyword != '') {
            $query = $query->searchKeyword($keyword);
        }

        if ($orderBy == "title") {
            $query = $query->filterOrderBy("title_th", $ascdesc);
            $query = $query->filterOrderBy("title_en", $ascdesc);
            $query = $query->filterOrderBy("title_ch", $ascdesc);
        } else {
            $query = $query->filterOrderBy($orderBy, $ascdesc);
        }

        if ($limit_page == 0) {
            $result = $query->get();
        } else {
            $result = $query->paginate($limit_page);
        }

        if ($type == 1) {
            return response()->json(array('status' => 'success', 'data' => $result), 200);
        } else {
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = HomeBanners::query();

        //return $request->input();

        if ($request->input('where') !== null) {
            $where = $request->input('where');
            foreach ($where as $key => $value) {
                $field = $key;
                $cond = key($value);
                $var = pos($value);
                if ($cond == 7) {
                    $var = "%" . $var . "%";
                }

                $query = $query->where($field, config("config.config_arr_condition")[$cond], $var);
            }
        }

        if ($request->input('order') !== null) {
            $order = $request->input('order');
            foreach ($order as $key => $value) {
                $query = $query->orderBy($key, $value);
            }
        }

        if ($request->input('filter') !== null) {
            $filter = $request->input('filter');

            if ($filter["limit"] !== null && $filter["limit"] != 0) {
                $query = $query->take($filter["limit"]);
            }
            if ($filter["offset"] !== null && $filter["offset"] != 0) {
                $query = $query->offset(($filter["offset"] - 1));
            }
            if ($filter["skip"] !== null && $filter["skip"] != 0) {
                $query = $query->skip($filter["skip"]);
            }
        }

        $data = $query->get();

        return response()->json([
            "success" => true,
            "total" => sizeof($data),
            'data' => $data,
            "msg" => "Get data success",
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = HomeBanners::find($id);
        if (empty($data)) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "",
            ], 402);
        } else {
            return response()->json([
                "success" => true,
                "total" => sizeof($data),
                'data' => $data,
                "msg" => "Get data by id success",
            ], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $validation = Validator::make(
            array(
                'title_th' => $request->input('title_th'),
                //'thumb' => $request->input('thumb')
            ),
            array(
                'title_th' => array('required'),
                //'thumb' => array('required')
                //'email' => array( 'required', 'email' ),
            )
        );

        if ($validation->fails()) {
            //return $validation->errors();
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => $validation->errors(),
            ], 422);
        } else {

            $ip_address = $request->ip();
            if ($request->ip() == "::1") {
                $ip_address = "127.0.0.1";
            }

            $now = Carbon::now();
            $current_datetime = $now->toDateTimeString();
            $pathFile = $now->format('Y/m');

            //thumb th
            $fileThumbTh = "";
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = '';

                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }

            //thumb en
            $fileThumbEn = "";
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = '';

                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }

            //thumb ch
            $fileThumbCh = "";
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = '';

                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }

            $status = ($request->input("status") !== null) ? $request->input("status") : "Y";
            $status_th = "Y";
            $status_en = ($request->input("status_en") !== null && $request->input("status_en") != "") ? $request->input("status_en") : "N";
            $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch") != "") ? $request->input("status_ch") : "N";
            $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
            $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;
            $order_by = ($request->input("order_by") !== null) ? $request->input("order_by") : 0;

            $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
            $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

            try {
                $homeBanners = new HomeBanners;
                $homeBanners->thumb_th = $fileThumbTh;
                $homeBanners->url_th = $request->input("url_th");
                $homeBanners->title_th = $request->input("title_th");
                $homeBanners->detail_th = $request->input("detail_th");
                $homeBanners->status_th = $status_th;
                $homeBanners->thumb_en = $fileThumbEn;
                $homeBanners->url_en = $request->input("url_en");
                $homeBanners->title_en = $request->input("title_en");
                $homeBanners->detail_en = $request->input("detail_en");
                $homeBanners->status_en = $status_en;
                $homeBanners->thumb_ch = $fileThumbCh;
                $homeBanners->url_ch = $request->input("url_ch");
                $homeBanners->title_ch = $request->input("title_ch");
                $homeBanners->detail_ch = $request->input("detail_ch");
                $homeBanners->startDate = $startDate;
                $homeBanners->endDate = $endDate;
                $homeBanners->status_ch = $status_ch;
                $homeBanners->visitor = $ip_address;
                $homeBanners->status = $status;
                $homeBanners->created_at = $current_datetime;
                $homeBanners->updated_at = $current_datetime;
                $homeBanners->created_by = $user_id;
                $homeBanners->updated_by = $user_id;
                $homeBanners->order_by = $order_by;
                $homeBanners->save();

                $data = array(
                    "id" => $homeBanners->id,
                );

                if (!empty($homeBanners->id) && $homeBanners->id != 0) {
                    return response()->json([
                        "success" => true,
                        'data' => $data,
                        "msg" => "Register - insert data success",
                    ], 200);
                } else {
                    return response()->json([
                        "success" => false,
                        'data' => "",
                        "msg" => "Register - error",
                    ], 502);
                }

            } catch (\Exception $e) {
                dd($e);
                return response()->json([
                    "success" => false,
                    'data' => "",
                    "msg" => "Register - error",
                ], 502);
                // something went wrong
            }
        }
        //$request->input("firstname");
        //return $validatedData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->input("firstname");
        $ip_address = $request->ip();
        if ($request->ip() == "::1") {
            $ip_address = "127.0.0.1";
        }

        $current_datetime = Carbon::now();

        $status = ($request->input("status") !== null) ? $request->input("status") : "N";
        $status_th = "Y";
        $status_en = ($request->input("status_en") !== null && $request->input("status_en") != "") ? $request->input("status_en") : "N";
        $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch") != "") ? $request->input("status_ch") : "N";
        $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
        $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;

        $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
        $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

        try {
            $homeBanners = HomeBanners::find($id);

            //thumb th
            $fileThumbTh = $homeBanners->thumb_th;
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = $homeBanners->thumb_th;

                $pathFile = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }

            //thumb en
            $fileThumbEn = $homeBanners->thumb_en;
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = $homeBanners->thumb_en;

                $pathFile = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }

            //thumb ch
            $fileThumbCh = $homeBanners->thumb_ch;
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = $homeBanners->thumb_ch;

                $pathFile = Functionphp::php_convertDateToPathFile($homeBanners->created_at);
                $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }

            $homeBanners->thumb_th = $fileThumbTh;
            $homeBanners->url_th = $request->input("url_th");
            $homeBanners->title_th = $request->input("title_th");
            $homeBanners->detail_th = $request->input("detail_th");
            $homeBanners->status_th = $status_th;
            $homeBanners->thumb_en = $fileThumbEn;
            $homeBanners->url_en = $request->input("url_en");
            $homeBanners->title_en = $request->input("title_en");
            $homeBanners->detail_en = $request->input("detail_en");
            $homeBanners->status_en = $status_en;
            $homeBanners->thumb_ch = $fileThumbCh;
            $homeBanners->url_ch = $request->input("url_ch");
            $homeBanners->title_ch = $request->input("title_ch");
            $homeBanners->detail_ch = $request->input("detail_ch");
            $homeBanners->status_ch = $status_ch;
            $homeBanners->startDate = $startDate;
            $homeBanners->endDate = $endDate;
            $homeBanners->visitor = $ip_address;
            $homeBanners->status = $status;
            $homeBanners->updated_at = $current_datetime;
            $homeBanners->updated_by = $user_id;
            $homeBanners->order_by = $request->input("order_by");
            $homeBanners->save();

            $data = array(
                "id" => $homeBanners->id,
            );

            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - update data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $homeBanner = HomeBanners::find($id);
            if (empty($homeBanner)) {
                about(404);
            }
            $pathFile = Functionphp::php_convertDateToPathFile($homeBanner->created_at);
            $pathThumb = config('config.config_pathUpload') . '/homeBanners/' . $pathFile;
            Helper::storageProcess('delete', $pathThumb, $homeBanner->thumb_th);
            Helper::storageProcess('delete', $pathThumb, $homeBanner->thumb_en);
            Helper::storageProcess('delete', $pathThumb, $homeBanner->thumb_ch);

            $delete = $homeBanner->delete();

            $data = array(
                "id" => $id,
            );
            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - delete data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }
}
