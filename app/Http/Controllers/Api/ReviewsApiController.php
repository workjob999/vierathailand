<?php

namespace App\Http\Controllers\Api;

use App\Http\Classes\Helper;
use App\Http\Controllers\Controller;
use App\Model\Functions\Functionphp;
use App\Model\Reviews;
use Carbon\Carbon;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class ReviewsApiController extends Controller
{

    public function getReviewsFront($request,$limit)
    {
        $now = Carbon::now();
        $current_date = $now->format("Y-m-d");

        $query = Reviews::query();
        $query = $query->whereStatus("Y");
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('startDate', '<=', $current_date)
                            ->orWhere('startDate', '0000-00-00')
                            ->orWhere('startDate', null);
                        });
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('endDate', '>=', $current_date)
                            ->orWhere('endDate', '0000-00-00')
                            ->orWhere('endDate', null);
                        });
        //return $query->skip(0)->take($limit)->get();
        if ($limit == 0) {
            return $query->orderBy("order_by", "ASC")->orderBy("updated_at", "DESC")->get();
        } else {
            return $query->orderBy("order_by", "ASC")->orderBy("updated_at", "DESC")->skip(0)->take($limit)->get();
        }
    }

    public function getReviewById($request,$id)
    {
        $now = Carbon::now();
        $current_date = $now->format("Y-m-d");

        $query = Reviews::query();
        $query = $query->whereStatus("Y");
        $query = $query->whereId($id);
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('startDate', '<=', $current_date)
                            ->orWhere('startDate', '0000-00-00')
                            ->orWhere('startDate', null);
                        });
        $query = $query->where(function ($query2) use ($current_date) {
                            $query2->whereDate('endDate', '>=', $current_date)
                            ->orWhere('endDate', '0000-00-00')
                            ->orWhere('endDate', null);
                        });
        return $query->first();
    }

    public function getReviewsList($request, $type = 1, $orderBy = 'order_by', $ascdesc = 'asc', $limit_page = 0, $keyword = '')
    {
        $query = Reviews::query();

        if ($keyword != '') {
            $query = $query->searchKeyword($keyword);
        }

        if ($orderBy == "title") {
            $query = $query->filterOrderBy("title_th", $ascdesc);
            $query = $query->filterOrderBy("title_en", $ascdesc);
            $query = $query->filterOrderBy("title_ch", $ascdesc);
        } else {
            $query = $query->filterOrderBy($orderBy, $ascdesc);
        }

        if ($limit_page == 0) {
            $result = $query->get();
        } else {
            $result = $query->paginate($limit_page);
        }

        if ($type == 1) {
            return response()->json(array('status' => 'success', 'data' => $result), 200);
        } else {
            return $result;
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $query = Reviews::query();

        //return $request->input();

        if ($request->input('where') !== null) {
            $where = $request->input('where');
            foreach ($where as $key => $value) {
                $field = $key;
                $cond = key($value);
                $var = pos($value);
                if ($cond == 7) {
                    $var = "%" . $var . "%";
                }

                $query = $query->where($field, config("config.config_arr_condition")[$cond], $var);
            }
        }

        if ($request->input('order') !== null) {
            $order = $request->input('order');
            foreach ($order as $key => $value) {
                $query = $query->orderBy($key, $value);
            }
        }

        if ($request->input('filter') !== null) {
            $filter = $request->input('filter');

            if ($filter["limit"] !== null && $filter["limit"] != 0) {
                $query = $query->take($filter["limit"]);
            }
            if ($filter["offset"] !== null && $filter["offset"] != 0) {
                $query = $query->offset(($filter["offset"] - 1));
            }
            if ($filter["skip"] !== null && $filter["skip"] != 0) {
                $query = $query->skip($filter["skip"]);
            }
        }

        $data = $query->get();

        return response()->json([
            "success" => true,
            "total" => sizeof($data),
            'data' => $data,
            "msg" => "Get data success",
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Reviews::find($id);
        if (empty($data)) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "",
            ], 402);
        } else {
            return response()->json([
                "success" => true,
                "total" => sizeof($data),
                'data' => $data,
                "msg" => "Get data by id success",
            ], 200);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($request)
    {
        $validation = Validator::make(
            array(
                'title_th' => $request->input('title_th'),
                //'thumb' => $request->input('thumb')
            ),
            array(
                'title_th' => array('required'),
                //'thumb' => array('required')
                //'email' => array( 'required', 'email' ),
            )
        );

        if ($validation->fails()) {
            //return $validation->errors();
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => $validation->errors(),
            ], 422);
        } else {

            $ip_address = $request->ip();
            if ($request->ip() == "::1") {
                $ip_address = "127.0.0.1";
            }

            $now = Carbon::now();
            $current_datetime = $now->toDateTimeString();
            $pathFile = $now->format('Y/m');

            //thumb th
            $fileThumbTh = "";
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = '';

                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }


            //thumb en
            $fileThumbEn = "";
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = '';

                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }


            //thumb ch
            $fileThumbCh = "";
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = '';

                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }


            //banner th
            $fileBannerTh = "";
            if ($request->file('banner_th') !== null) {
                $filename_banner_th = '';

                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_th')) {
                    $extension = $request->file('banner_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_th);
                    $filename_banner_th = 'banner_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_th, 'banner_th', $request);
                }

                $fileBannerTh = $filename_banner_th;
            }


            //banner en
            $fileBannerEn = "";
            if ($request->file('banner_en') !== null) {
                $filename_banner_en = '';

                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_en')) {
                    $extension = $request->file('banner_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_en);
                    $filename_banner_en = 'banner_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_en, 'banner_en', $request);
                }

                $fileBannerEn = $filename_banner_en;
            }


            //banner ch
            $fileBannerCh = "";
            if ($request->file('banner_ch') !== null) {
                $filename_banner_ch = '';

                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_ch')) {
                    $extension = $request->file('banner_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_ch);
                    $filename_banner_ch = 'banner_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_ch, 'banner_ch', $request);
                }

                $fileBannerCh = $filename_banner_ch;
            }

            $status = ($request->input("status") !== null) ? $request->input("status") : "Y";
            $status_th = "Y";
            $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
            $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
            $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
            $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;
            $order_by = ($request->input("order_by") !== null) ? $request->input("order_by") : 0;

            $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
            $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

            try {
                $reviews = new Reviews;
                $reviews->thumb_th = $fileThumbTh;
                $reviews->banner_th = $fileBannerTh;
                $reviews->title_th = $request->input("title_th");
                $reviews->detail_th = $request->input("detail_th");
                $reviews->content_th = $request->input("content_th");
                $reviews->status_th = $status_th;
                $reviews->thumb_en = $fileThumbEn;
                $reviews->banner_en = $fileBannerEn;
                $reviews->title_en = $request->input("title_en");
                $reviews->detail_en = $request->input("detail_en");
                $reviews->content_en = $request->input("content_en");
                $reviews->status_en = $status_en;
                $reviews->thumb_ch = $fileThumbCh;
                $reviews->banner_ch = $fileBannerCh;
                $reviews->title_ch = $request->input("title_ch");
                $reviews->detail_ch = $request->input("detail_ch");
                $reviews->content_ch = $request->input("content_ch");
                $reviews->startDate = $startDate;
                $reviews->endDate = $endDate;
                $reviews->status_ch = $status_ch;
                $reviews->visitor = $ip_address;
                $reviews->status = $status;
                $reviews->created_at = $current_datetime;
                $reviews->updated_at = $current_datetime;
                $reviews->created_by = $user_id;
                $reviews->updated_by = $user_id;
                $reviews->order_by = $order_by;
                $reviews->save();

                $data = array(
                    "id" => $reviews->id,
                );

                if (!empty($reviews->id) && $reviews->id != 0) {
                    return response()->json([
                        "success" => true,
                        'data' => $data,
                        "msg" => "Register - insert data success",
                    ], 200);
                } else {
                    return response()->json([
                        "success" => false,
                        'data' => "",
                        "msg" => "Register - error",
                    ], 502);
                }

            } catch (\Exception $e) {
                dd($e);
                return response()->json([
                    "success" => false,
                    'data' => "",
                    "msg" => "Register - error",
                ], 502);
                // something went wrong
            }
        }
        //$request->input("firstname");
        //return $validatedData;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return $request->input("firstname");
        $ip_address = $request->ip();
        if ($request->ip() == "::1") {
            $ip_address = "127.0.0.1";
        }

        $current_datetime = Carbon::now();

        $status = ($request->input("status") !== null) ? $request->input("status") : "N";
        $status_th = "Y";
        $status_en = ($request->input("status_en") !== null && $request->input("status_en")!="") ? $request->input("status_en") : "N";
        $status_ch = ($request->input("status_ch") !== null && $request->input("status_ch")!="") ? $request->input("status_ch") : "N";
        $user_id = (Auth::user() !== null) ? Auth::user()->id : 0;
        $user_id = ($request->input("user_id") !== null) ? $request->input("user_id") : $user_id;

        $startDate = ($request->input("startDate") !== null) ? Functionphp::php_convertDateDB($request->input("startDate")) : null;
        $endDate = ($request->input("endDate") !== null) ? Functionphp::php_convertDateDB($request->input("endDate")) : null;

        try {
            $reviews = Reviews::find($id);

            //thumb th
            $fileThumbTh = $reviews->thumb_th;
            if ($request->file('thumb_th') !== null) {
                $filename_thumb_th = $reviews->thumb_th;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_th')) {
                    $extension = $request->file('thumb_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_th);
                    $filename_thumb_th = 'thumb_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_th, 'thumb_th', $request);
                }

                $fileThumbTh = $filename_thumb_th;
            }

            //thumb en
            $fileThumbEn = $reviews->thumb_en;
            if ($request->file('thumb_en') !== null) {
                $filename_thumb_en = $reviews->thumb_en;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_en')) {
                    $extension = $request->file('thumb_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_en);
                    $filename_thumb_en = 'thumb_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_en, 'thumb_en', $request);
                }

                $fileThumbEn = $filename_thumb_en;
            }

            //thumb ch
            $fileThumbCh = $reviews->thumb_ch;
            if ($request->file('thumb_ch') !== null) {
                $filename_thumb_ch = $reviews->thumb_ch;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathThumb) or File::makeDirectory($pathThumb, 0777, true, true);

                if ($request->hasFile('thumb_ch')) {
                    $extension = $request->file('thumb_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathThumb, $filename_thumb_ch);
                    $filename_thumb_ch = 'thumb_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathThumb, $filename_thumb_ch, 'thumb_ch', $request);
                }

                $fileThumbCh = $filename_thumb_ch;
            }


            //banner th
            $fileBannerTh = $reviews->banner_th;
            if ($request->file('banner_th') !== null) {
                $filename_banner_th = $reviews->banner_th;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_th')) {
                    $extension = $request->file('banner_th')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_th);
                    $filename_banner_th = 'banner_th-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_th, 'banner_th', $request);
                }

                $fileBannerTh = $filename_banner_th;
            }

            //banner en
            $fileBannerEn = $reviews->banner_en;
            if ($request->file('banner_en') !== null) {
                $filename_banner_en = $reviews->banner_en;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_en')) {
                    $extension = $request->file('banner_en')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_en);
                    $filename_banner_en = 'banner_en-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_en, 'banner_en', $request);
                }

                $fileBannerEn = $filename_banner_en;
            }

            //banner ch
            $fileBannerCh = $reviews->banner_ch;
            if ($request->file('banner_ch') !== null) {
                $filename_banner_ch = $reviews->banner_ch;

                $pathFile = Functionphp::php_convertDateToPathFile($reviews->created_at);
                $pathBanner = config('config.config_pathUpload') . '/reviews/' . $pathFile;
                File::isDirectory($pathBanner) or File::makeDirectory($pathBanner, 0777, true, true);

                if ($request->hasFile('banner_ch')) {
                    $extension = $request->file('banner_ch')->getClientOriginalName();

                    Helper::storageProcess('delete', $pathBanner, $filename_banner_ch);
                    $filename_banner_ch = 'banner_ch-' . time() . '.' . $extension;
                    Helper::storageProcess('save', $pathBanner, $filename_banner_ch, 'banner_ch', $request);
                }

                $fileBannerCh = $filename_banner_ch;
            }

            $reviews->thumb_th = $fileThumbTh;
            $reviews->banner_th = $fileBannerTh;
            $reviews->title_th = $request->input("title_th");
            $reviews->detail_th = $request->input("detail_th");
            $reviews->content_th = $request->input("content_th");
            $reviews->status_th = $status_th;
            $reviews->thumb_en = $fileThumbEn;
            $reviews->banner_en = $fileBannerEn;
            $reviews->title_en = $request->input("title_en");
            $reviews->detail_en = $request->input("detail_en");
            $reviews->content_en = $request->input("content_en");
            $reviews->status_en = $status_en;
            $reviews->thumb_ch = $fileThumbCh;
            $reviews->banner_ch = $fileBannerCh;
            $reviews->title_ch = $request->input("title_ch");
            $reviews->detail_ch = $request->input("detail_ch");
            $reviews->content_ch = $request->input("content_ch");
            $reviews->status_ch = $status_ch;
            $reviews->startDate = $startDate;
            $reviews->endDate = $endDate;
            $reviews->visitor = $ip_address;
            $reviews->status = $status;
            $reviews->updated_at = $current_datetime;
            $reviews->updated_by = $user_id;
            $reviews->order_by = $request->input("order_by");
            $reviews->save();

            $data = array(
                "id" => $reviews->id,
            );

            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - update data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        try {
            $review = Reviews::find($id);
            if (empty($review))
            {
                about(404);
            }
            $pathFile = Functionphp::php_convertDateToPathFile($review->created_at);
            $pathThumb = config('config.config_pathUpload') . '/reviews/' . $pathFile;
            Helper::storageProcess('delete', $pathThumb, $review->thumb_th);
            Helper::storageProcess('delete', $pathThumb, $review->thumb_en);
            Helper::storageProcess('delete', $pathThumb, $review->thumb_ch);

            $delete = $review->delete();

            $data = array(
                "id" => $id,
            );
            return response()->json([
                "success" => true,
                'data' => $data,
                "msg" => "Register - delete data success",
            ], 200);
        } catch (\Exception $e) {
            return response()->json([
                "success" => false,
                'data' => "",
                "msg" => "Register - error",
            ], 502);
        }
    }
}
