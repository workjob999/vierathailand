<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

class Language
{
    public function handle($request, Closure $next) // รับคำสั่งและลำดับการทำงาน
    {
    	//echo $request->lang;
    	if (isset($request->lang))
    	{
    		if ($request->lang=="en" || $request->lang=="th" || $request->lang=="ch")
    		{
    			session(['locale' => $request->lang]);
    		}
    	}

        if(Session::has('locale')) // ตรวจดูว่ามีตัวแปร locale ใน session หรือไหม่
        {
            $locale = Session::get('locale'); // ถ้ามีอยู่ก็ดึงค่าออกมาเก็บไว้
            App::setLocale($locale); // ตั้งค่าที่ได้รับมาให้เป็นภาษาหลัก
        }
        else
        {
            $locale = "th"; // ถ้ามีอยู่ก็ดึงค่าออกมาเก็บไว้
            session(['locale' => $locale]);
            App::setLocale($locale); // ตั้งค่าที่ได้รับมาให้เป็นภาษาหลัก
        }

        $urlFull = url()->current();
        $expUrl = explode("/",$urlFull);
        $lenUrl = sizeof($expUrl)-1;
        $urlCurrent = "";
        if ($lenUrl>3)
        {
            $urlCurrent = $expUrl[sizeof($expUrl)-1];
        }
        session(['urlCurrent'=>$urlCurrent]);



        return $next($request); // ทำตามคำสั่งในลำดับต่อไป

    }

}

