<?php

namespace App\Http\Classes;

use App\Model\Webadmin\CouponMinMaxs;
use App\Model\Webadmin\Customers;
use App\Model\Webadmin\ProductOptions;
use App\Model\Webadmin\TemporaryReceipt;
use Carbon\Carbon;
use CodeItNow\BarcodeBundle\Utils\BarcodeGenerator;
use Illuminate\Support\Facades\Redis;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Polyfill\Util\TestListener;

Class Helper
{
    public static function getCouponValue($code, $totalProductPrice)
    {
        $coupon = CouponMinMaxs::whereCouponCode($code)->first();
        $discount = 0;
        if ($coupon) {
            if ($coupon->coupon_type == 2) {
                $discount = $coupon->maximum_value != 0 ? $coupon->maximum_value : $coupon->coupon_value;
            } else {
                $discount = ($totalProductPrice * $coupon->coupon_value) / 100;
                if ($discount > $coupon->maximum_value && $coupon->maximum_value <> 0) {
                    $discount = $coupon->maximum_value;
                }
            }
        }
        return $discount;
    }

    public static function calculateCouponDiscount($type = null, $discountCoupon = 0, $totalPrice = 0, $dataPrice = 0)
    {
        if ($type === null || $discountCoupon == 0 || $totalPrice == 0 || $dataPrice == 0) {
            return false;
        }
        $discount = 0;

        switch ($type) {
            case 'discount_per_product':
                if ($totalPrice != $dataPrice) {
                    $productWeight = ($totalPrice - $dataPrice) / ($totalPrice / 100);
                    $discount = ($discountCoupon * $productWeight) / 100;
                } else {
                    $discount = $discountCoupon;
                }
                break;
        }
        return $discount;
    }

    public function redisCheck($couponCode, $masterDataId)
    {
        $redis = Redis::connection('default');

        return $redis->hget($couponCode, $masterDataId);

    }

    public static function genCouponCode($code = null)
    {
        if ($code !== null && CouponMinMaxs::whereCouponCode($code)->first()) {
            return array('status' => 'fail', 'results' => 'code is duplicate');
        }

        $loopKey = true;
        $code = null;
        while ($loopKey) {
            $code = str_random(9);
            if (!CouponMinMaxs::whereCouponCode($code)->first()) {
                $loopKey = false;
            } else {
                $loopKey = true;
            }
        };

        if ($code !== null) {
            return array('status' => 'success', 'results' => Str::upper($code));
        }

        return array('status' => 'fail');

    }

    public static function updateReceiptNo($date = null)
    {
        $date = $date !== null ? Carbon::parse($date) : Carbon::now();
        $year = $date->year;
        $month = $date->month;

        $lastNo = TemporaryReceipt::where('year', $year)->where('month', $month)->pluck('running_number')->first();

        if ($lastNo) {
            $newNumber = $lastNo + 1;
            TemporaryReceipt::where('year', $year)->where('month', $month)->update(['running_number' => $newNumber]);

        } else {
            TemporaryReceipt::insert(['year' => $year, 'month' => $month, 'running_number' => 1]);
        }
    }

    public static function genBarcode($code = null)
    {
        if ($code === null) {
            return null;
        }
        $barcode = new BarcodeGenerator();
        $barcode->setText($code);
        $barcode->setType(BarcodeGenerator::Code128);
        $barcode->setScale(10);
        $barcode->setThickness(80);
        $barcode->setFontSize(0);
        $genCode = $barcode->generate();

        return '<img src="data:image/png;base64,' . $genCode . '" width=300 height="70"/>';
    }

    public static function getLastMonthReceiptNo($date = null)
    {
        $date = $date !== null ? Carbon::parse($date) : Carbon::now();
        $year = $date->year;
        $month = $date->month;

        $lastNo = TemporaryReceipt::where('year', $year)->where('month', $month)->first();

        if (!$lastNo) {
            $lastNo = $date->format('y') . $date->format('m') . '00000';
        } else {

            $lastNoYear = substr($lastNo->year, 2);
            $lastNoMonth = str_pad($lastNo->month, '2', '0', STR_PAD_LEFT);
            $lastNoRunning = str_pad($lastNo->running_number, '5', '0', STR_PAD_LEFT);

            $lastNo = $lastNoYear . $lastNoMonth . $lastNoRunning;
        }

        return $lastNo;
    }

    public static function showThaiDate($date, $type = 'long', $lang = "")
    {
        if (empty($date))
            return null;
        try {

            if ($lang == "")
                $lang = \App::getLocale();

            if ($lang == 'th') {
                setlocale(LC_TIME, 'th_TH.utf8');
                if ($type == 'short') {
                    return Carbon::parse($date)->formatLocalized('%d %b %Ey');
                } else {
                    return Carbon::parse($date)->formatLocalized('%d %B %Ey');
                }

            } else {
                setlocale(LC_TIME, 'en_US.utf8');
                if ($type == 'short') {
                    return Carbon::parse($date)->formatLocalized('%d %b %Y');
                } else {
                    return Carbon::parse($date)->formatLocalized('%d %B %Y');
                }
            }
        } catch (\Exception $e) {
            return $date;
        }

    }

    public static function vatCalculate($number)
    {
        return ($number / 1.07) * 0.07;
    }

    public static function moneyFormat($number, $dec = 0)
    {
        if (!isset($number)) {
            $number = 0;
        }
        return number_format($number, $dec);
    }

    public static function discountPercent($price, $disc_price, $dec = 2)
    {
        $disPercent = 0;
        if ($price != "" && $price != 0)
            $disPercent = ($price - $disc_price) / $price * 100;

        return round($disPercent, $dec);
    }

    public static function storageProcess($type, $pathThumb, $filename = null, $requestName = null, $request = null)
    {
        switch ($type) {
            case 'save':
                if ($requestName !== null && $filename !== null && $filename != "") {
                    $request->file($requestName)->storeAs($pathThumb, $filename);
                    return true;
                }
                return false;
                break;

            case 'saveMulti':
                if ($requestName !== null && $filename !== null && $filename != "") {
                    $request->storeAs($pathThumb, $filename);
                    return true;
                }
                return false;
                break;

            case 'delete':
                if ($filename !== null && $filename != "") {
                    if (Storage::exists($pathThumb . "/" . $filename)) {
                        Storage::delete($pathThumb . "/" . $filename);
                        return true;
                    }
                }
                return false;
                break;

            case 'exists':
                if ($filename !== null && $filename != "") {
                    if (Storage::exists($pathThumb . "/" . $filename))
                        return true;
                }
                return false;
                break;

            case 'get':

                $url_file = "";
                if ($filename !== null && $filename != "") {
                    $url_file = $pathThumb . "/" . $filename;
                    if (Storage::exists($pathThumb . "/" . $filename)) {
                        $url_file = Storage::url($pathThumb . "/" . $filename);
                    }
                }
                return $url_file;
                break;
        }


    }

}