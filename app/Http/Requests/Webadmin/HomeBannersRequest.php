<?php

namespace App\Http\Requests\Webadmin;
use Illuminate\Foundation\Http\FormRequest;

class HomeBannersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        // dd("test");
        return [
            'title_th' => 'required'
        ];
    }



    public function messages()
    {
        return [
            'title_th.required' => 'Please enter your title th.',
        ];
    }
}