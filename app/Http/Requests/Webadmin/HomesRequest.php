<?php

namespace App\Http\Requests\Webadmin;
use Illuminate\Foundation\Http\FormRequest;

class HomesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'title' => 'required',
            'thumb' => 'mimes:jpeg,png,gif',
            'order_by' => 'required|numeric',
        ];
    }



    public function messages()
    {
        return [
            'title.required' => 'Please enter your title.',
            'order_by.required' => 'Please enter your order',
            'order_by.numeric' => 'Invalid order format! Please re-enter the order.',
        ];
    }
}