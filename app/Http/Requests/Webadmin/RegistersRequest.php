<?php

namespace App\Http\Requests\Webadmin;
use Illuminate\Foundation\Http\FormRequest;


class RegistersRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            'fullname' => 'required'
            //'phone' => 'required|numeric',
            // 'email' => 'required|email',
            // 'lineid' => 'required',
            // 'refer' => 'required',
            // 'sale_price' => 'required'
        ];
    }



    public function messages()
    {
        return [
            'fullname.required' => 'Please enter your fullname.'
        ];
    }
}