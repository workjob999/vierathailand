<?php

namespace App\;

use Illuminate\Database\Eloquent\Model;

/**
 *
 */
class RolesUser extends Model
{
    protected $table = 'role_user';
    protected $primaryKey = 'user_id , role_id';

    public function get_role(){
        return $this->hasOne('App\Http\Models\Role' , 'role_id' , 'id');
    }

    public function permission(){
        return $this->hasMany('App\Http\Models\PermissionRole' , 'role_id' , 'role_id');
    }
    
}