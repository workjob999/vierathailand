<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


/**
 *
 */
class PermissionRole extends Model
{
    protected $table = 'permission_role';
    protected $primaryKey = 'permission_id , role_id';


    public function get_permission(){
        return $this->hasOne('Laratrust\LaratrustPermission' , 'id' , 'permission_id');
    }

    public function check_permission($permission,$user_id){
        $result_permission = DB::table("permissions")->select('id')->where("name",$permission)->first();
        if (empty($result_permission))
            return false;
        $result_role_user = DB::table("role_user")->select('role_id')->where("user_id",$user_id)->first();
        if (empty($result_role_user))
            return false;
        $result = DB::table("permission_role")->where([
            ["permission_id",$result_permission->id],
            ["role_id",$result_role_user->role_id],
        ])->first();
        if (empty($result))
            return false;
        return true;
    }

    public function all_check_permission($user_id){
        $arr_data = array();
        $result_role_user = DB::table("role_user")->select('role_id')->where("user_id",$user_id)->first();
        $results_permission = DB::table("permissions")->select('id','name')->get();
        if (sizeof($results_permission)>0)
        {
            foreach ($results_permission as $permission)
            {
                $result = DB::table("permission_role")->where([
                    ["permission_id",$permission->id],
                    ["role_id",$result_role_user->role_id],
                ])->first();
                
                if (empty($result))
                    $arr_data[$permission->name] = false;
                else
                    $arr_data[$permission->name] = true;
            }
        }
        return $arr_data;
    }

}