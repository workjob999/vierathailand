<?php



return [
    'menu_item_list' => [
        [
            'label' => 'Home',
            'active' => 'home',
            'icon' => 'fas fa-home',
            'link' => 'home',
            'permission' => 'view-home-banners-list',
            'items'=>[
                [
                    'label' => 'Banners',
                    'active' => 'banners',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'home/banners',
                    'permission' => 'view-home-banners-list'
                ],
                [
                    'label' => 'Promotions',
                    'active' => 'promotions',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'home/promotions',
                    'permission' => 'view-home-promotions-list'
                ],
                [
                    'label' => 'TVC',
                    'active' => 'tvc',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'home/tvc',
                    'permission' => 'view-home-tvc-list'
                ]
            ]
        ],
        [
            'label' => 'About',
            'active' => 'about',
            'icon' => 'fas fa-home',
            'link' => 'about',
            'permission' => 'edit-about'
        ],
        [
            'label' => 'Promotions',
            'active' => 'promotions',
            'icon' => 'fas fa-tags',
            'link' => 'promotions',
            'permission' => 'view-promotions-list'
        ],
        [
            'label' => 'Reviews',
            'active' => 'reviews',
            'icon' => 'fas fa-comment',
            'link' => 'reviews',
            'permission' => 'view-reviews-list'
        ],
        [
            'label' => 'Dealer',
            'active' => 'dealer',
            'icon' => 'fa fa-users',
            'link' => 'dealer',
            'permission' => 'view-registers-list',
            'items'=>[
                [
                    'label' => 'Registers',
                    'active' => 'registers',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'dealer/registers',
                    'permission' => 'view-registers-list'
                ]
            ]
        ],
        [
            'label' => 'Users',
            'active' => 'users',
            'icon' => 'fa fa-users',
            'link' => 'users',
            'permission' => 'view-user-list',
            'items'=>[
                [
                    'label' => 'User',
                    'active' => 'user',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'users/user',
                    'permission' => 'view-user-list'
                ],
                [
                    'label' => 'Role',
                    'active' => 'role',
                    'icon' => 'fa fa-circle-o',
                    'link' => 'users/role',
                    'permission' => 'view-role-list'
                ]
            ]
        ]
    ]
];