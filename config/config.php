<?php



return [



    /*

    |--------------------------------------------------------------------------

    | Application Name

    |--------------------------------------------------------------------------

    |

    | This value is the name of your application. This value is used when the

    | framework needs to place the application's name in a notification or

    | any other location as required by the application or its packages.

    */



    /*Config Other*/
    'company_name' => "Viera Thailand",
    'config_meta_title' => 'Viera Thailand',
    'config_meta_keyword' => 'Viera Thailand',
    'config_pathUpload' => "upload",
    'config_pathWeb' => "",
    'config_pathCms' => "cms",
    'config_statusAdmin' => true,
    'config_statusMaintenance' => true,
    'config_arr_status'=>array(
          'Y'=>'Active',
          'N'=>'Inactive'
      ),
    'config_arr_statusCodes'=>array(
          'Y'=>'Used',
          'N'=>'Unused'
      ),
    
    'config_arr_statusPaid'=>array(
          'Y'=>'Completed',
          'N'=>'Paid'
      ),

    'config_arr_mailadmin'=>array(
          'email'=>'aonjung999@gmail.com',
          'name'=>'aonjung999'
      ),

    'config_arr_limit_page' => array(
        '1'=>'1',
        '5'=>'5',
        '10'=>'10',
        '15'=>'15',
        '20'=>'20',
        '25'=>'25',
        '30'=>'30',
        '50'=>'50',
        '100'=>'100',
        'All'=>'All'
      ),

      'config_arr_sex'=>array(
          'M'=>'ชาย',
          'F'=>'หญิง'
      ),

      'config_arr_title'=>array(
          '1'=>'นาย',
          '2'=>'นาง',
          '3'=>'นางสาว'
      ),

      'config_arr_type'=>array(
        '1'=>'DEALER',
        '2'=>'AGENT',
        '3'=>'DROP SHIP'
    ),


      /*Config Group Role*/
      'config_arr_roleGroup' => array(
        '4' => 'Home',
        '5' => 'About',
        '6' => 'Promotions',
        '7' => 'Reviews',
        '3' => 'Dealer',
        '1' => 'System Users'
      )
      /*Config Group Role*/

];

