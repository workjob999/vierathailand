<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function(){
    return redirect(url("th/"));
});

Route::group(['prefix' => '/{lang}','where'=>['lang'=>'en|th|ch']], function () {
    Route::get('/', 'Front\HomeController@index_viera')->name('home');
    Route::get('/products', 'Front\ProductsController@index_viera');
    //Route::get('/promotion', 'Front\PromotionController@index_viera');
    Route::group(['prefix' => '/promotion'], function () {
        Route::get('/', 'Front\PromotionController@index_viera')->name("promotions");
        Route::get('/details/{id}', 'Front\PromotionController@viera_details')->name("promotion-detail");
    });
    Route::group(['prefix' => '/review'], function () {
        Route::get('/', 'Front\ReviewController@index_viera')->name("reviews");
        Route::get('/details/{id}', 'Front\ReviewController@viera_details')->name("review-detail");
    });

    //Route::get('/review', 'Front\ReviewController@index_viera');
    Route::group(['prefix' => '/contact'], function () {
        Route::get('/', 'Front\ContactController@index_viera')->name("contact");
        Route::post('/sendmail', 'Front\ContactController@sendmail')->name("contact-sendmail");
    });
    

    Route::group(['prefix' => '/register'], function () {
        Route::get('/', 'Front\DealerController@index_viera')->name('registerPage');
        Route::post('/store', 'Front\DealerController@register')->name("registerStore");
        Route::post('/checkDuplicate', 'Front\DealerController@checkDuplicate')->name("checkDuplicate");
    });

    Route::group(['prefix' => '/dealer'], function () {
        Route::get('/', 'Front\DealerController@dealer');
    });
});

// Route::group(['prefix' => '/register'], function () {
//     Route::post('/store', 'Front\DealerController@register')->name("registerStore");
// });

Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');

/*Back end*/
//Auth::routes();
// Auth::routes(['register' => false]);
// Auth::routes();
Route::get('cms-login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('cms-login', 'Auth\LoginController@login');
Route::post('cms-logout', 'Auth\LoginController@logout')->name('logout');

Route::get(config('config.config_pathCms'), 'Webadmin\DashboardController@index')->name("cms-dashboard");

Route::group(['prefix' => config('config.config_pathCms'), 'middleware' => ['web', 'auth:web']], function () {
    /*Home*/
    Route::group(['prefix' => '/home'], function () {
        //banners
        Route::group(['prefix' => '/banners'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-home-banners']], function () {
                Route::get('/create', 'Webadmin\HomeBannersController@create');
                Route::post('/', 'Webadmin\HomeBannersController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-home-banners']], function () {
                Route::post('/deleteSubContent/{id}', 'Webadmin\HomeBannersController@deleteSubContent');
                Route::post('/deleteAllSubContent', 'Webadmin\HomeBannersController@deleteAllSubContent');
                Route::get('/{id}/edit', 'Webadmin\HomeBannersController@edit');
                Route::put('/{id}', 'Webadmin\HomeBannersController@update');
                Route::delete('/{id}', 'Webadmin\HomeBannersController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-home-banners-list']], function () {
                Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\HomeBannersController@index')->where('orderBy', 'title|modify|order_by')->name("cms-home-banners-index");
                Route::get('/{id}', 'Webadmin\HomeBannersController@show')->name("cms-home-banners-show");
            });
        });
        //banner

        //Promotion
        Route::group(['prefix' => '/promotions'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-home-promotions']], function () {
                Route::get('/create', 'Webadmin\HomePromotionsController@create');
                Route::post('/', 'Webadmin\HomePromotionsController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-home-promotions']], function () {
                Route::post('/deleteSubContent/{id}', 'Webadmin\HomePromotionsController@deleteSubContent');
                Route::post('/deleteAllSubContent', 'Webadmin\HomePromotionsController@deleteAllSubContent');
                Route::get('/{id}/edit', 'Webadmin\HomePromotionsController@edit');
                Route::put('/{id}', 'Webadmin\HomePromotionsController@update');
                Route::delete('/{id}', 'Webadmin\HomePromotionsController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-home-promotions-list']], function () {
                Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\HomePromotionsController@index')->where('orderBy', 'title|modify|order_by')->name("cms-home-promotions-index");
                Route::get('/{id}', 'Webadmin\HomePromotionsController@show')->name("cms-home-promotions-show");
            });
        });
        //Promotion

        //Tvc
        Route::group(['prefix' => '/tvc'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-home-tvc']], function () {
                Route::get('/create', 'Webadmin\HomeTvcsController@create');
                Route::post('/', 'Webadmin\HomeTvcsController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-home-tvc']], function () {
                Route::post('/deleteSubContent/{id}', 'Webadmin\HomeTvcsController@deleteSubContent');
                Route::post('/deleteAllSubContent', 'Webadmin\HomeTvcsController@deleteAllSubContent');
                Route::get('/{id}/edit', 'Webadmin\HomeTvcsController@edit');
                Route::put('/{id}', 'Webadmin\HomeTvcsController@update');
                Route::delete('/{id}', 'Webadmin\HomeTvcsController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-home-tvc-list']], function () {
                Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\HomeTvcsController@index')->where('orderBy', 'title|modify|order_by')->name("cms-home-tvcs-index");
                Route::get('/{id}', 'Webadmin\HomeTvcsController@show')->name("cms-home-tvcs-show");
            });
        });
        //Tvc
    });
    /*Home*/

    Route::group(['prefix' => '/about'], function () {
        Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-about']], function () {
            Route::put('/{id}', 'Webadmin\AboutController@update')->name("cms-about-update");
            Route::get('/', 'Webadmin\AboutController@edit')->name("cms-about-edit");
        });
    });


    //Promotions
    Route::group(['prefix' => '/promotions'], function () {
        Route::group(['middleware' => ['web', 'auth:web', 'permission:create-promotions']], function () {
            Route::get('/create', 'Webadmin\PromotionsController@create');
            Route::post('/', 'Webadmin\PromotionsController@store');
        });

        Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-promotions']], function () {
            Route::post('/deleteSubContent/{id}', 'Webadmin\PromotionsController@deleteSubContent');
            Route::post('/deleteAllSubContent', 'Webadmin\PromotionsController@deleteAllSubContent');
            Route::get('/{id}/edit', 'Webadmin\PromotionsController@edit');
            Route::put('/{id}', 'Webadmin\PromotionsController@update');
            Route::delete('/{id}', 'Webadmin\PromotionsController@destroy');
        });

        Route::group(['middleware' => ['web', 'auth:web', 'permission:view-promotions-list']], function () {
            Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\PromotionsController@index')->where('orderBy', 'title|modify|order_by')->name("cms-home-banners-index");
            Route::get('/{id}', 'Webadmin\PromotionsController@show')->name("cms-home-banners-show");
        });
    });
    //Promotions


    //Reviews
    Route::group(['prefix' => '/reviews'], function () {
        Route::group(['middleware' => ['web', 'auth:web', 'permission:create-reviews']], function () {
            Route::get('/create', 'Webadmin\ReviewsController@create');
            Route::post('/', 'Webadmin\ReviewsController@store');
        });

        Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-reviews']], function () {
            Route::post('/deleteSubContent/{id}', 'Webadmin\ReviewsController@deleteSubContent');
            Route::post('/deleteAllSubContent', 'Webadmin\ReviewsController@deleteAllSubContent');
            Route::get('/{id}/edit', 'Webadmin\ReviewsController@edit');
            Route::put('/{id}', 'Webadmin\ReviewsController@update');
            Route::delete('/{id}', 'Webadmin\ReviewsController@destroy');
        });

        Route::group(['middleware' => ['web', 'auth:web', 'permission:view-reviews-list']], function () {
            Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\ReviewsController@index')->where('orderBy', 'title|modify|order_by')->name("cms-home-banners-index");
            Route::get('/{id}', 'Webadmin\ReviewsController@show')->name("cms-home-banners-show");
        });
    });
    //Reviews

    /*Dealer*/
    Route::group(['prefix' => '/dealer'], function () {
        //register
        Route::group(['prefix' => '/registers'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-registers']], function () {
                Route::get('/create', 'Webadmin\RegistersController@create');
                Route::post('/', 'Webadmin\RegistersController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-registers']], function () {
                Route::post('/deleteSubContent/{id}', 'Webadmin\RegistersController@deleteSubContent');
                Route::post('/deleteAllSubContent', 'Webadmin\RegistersController@deleteAllSubContent');
                Route::get('/{id}/edit', 'Webadmin\RegistersController@edit');
                Route::put('/{id}', 'Webadmin\RegistersController@update');
                Route::delete('/{id}', 'Webadmin\RegistersController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-registers-list']], function () {
                Route::get('/exportAllSubContent', 'Webadmin\RegistersController@exportAllSubContent')->name('cms-register-export');
                Route::get('/{orderBy?}/{ascdesc?}/{search?}', 'Webadmin\RegistersController@index')->where('orderBy', 'fullname|email|lineid|order_by');
                Route::get('/{id}', 'Webadmin\RegistersController@show');
            });
        });
        //register
    });
    /*Dealer*/

    /*Users*/
    Route::group(['prefix' => '/users', 'middleware' => ['web', 'auth:web', 'permission:view-user-list']], function () {
        Route::group(['prefix' => '/user'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-user']], function () {
                Route::get('/create', 'Webadmin\UserController@create');
                Route::post('/', 'Webadmin\UserController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-user']], function () {
                Route::get('/{id}/edit', 'Webadmin\UserController@edit');
                Route::put('/{id}', 'Webadmin\UserController@update');
                Route::delete('/{id}', 'Webadmin\UserController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-user-list']], function () {
                Route::get('/', 'Webadmin\UserController@index');
                Route::get('/{id}', 'Webadmin\UserController@show');
            });
        });

        Route::group(['prefix' => '/role'], function () {
            Route::group(['middleware' => ['web', 'auth:web', 'permission:create-role']], function () {
                Route::get('/create', 'Webadmin\RoleController@create');
                Route::post('/', 'Webadmin\RoleController@store');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:edit-role']], function () {
                Route::get('/{id}/edit', 'Webadmin\RoleController@edit');
                Route::put('/{id}', 'Webadmin\RoleController@update');
                Route::delete('/{id}', 'Webadmin\RoleController@destroy');
            });

            Route::group(['middleware' => ['web', 'auth:web', 'permission:view-role-list']], function () {
                Route::get('/', 'Webadmin\RoleController@index');
                Route::get('/{id}', 'Webadmin\RoleController@show');
            });
        });
    });
    /*Users*/

    /*-----processdata-------*/
    Route::post('processdata/changeLimitPage', 'Webadmin\ProcessdataController@changeLimitPage');
    Route::post('processdata/changeStatus', 'Webadmin\ProcessdataController@changeStatus');
    Route::post('processdata/sortableData', 'Webadmin\ProcessdataController@sortableData');
    Route::post('processdata/sortableSlideData', 'Webadmin\ProcessdataController@sortableSlideData');
    Route::post('processdata/deleteFileData', 'Webadmin\ProcessdataController@deleteFileData');
    Route::post('processdata/deleteThumb', 'Webadmin\ProcessdataController@deleteThumb');
    Route::post('processdata/deleteBanner', 'Webadmin\ProcessdataController@deleteBanner');
    Route::post('processdata/deleteBannerMulti', 'Webadmin\ProcessdataController@deleteBannerMulti');
    Route::post('processdata/changeOrderAlbum', 'Webadmin\ProcessdataController@changeOrderAlbum');
    Route::post('processdata/changeOrderDetail', 'Webadmin\ProcessdataController@changeOrderDetail');
    Route::post('processdata/uploadAlbum/{page}', 'Webadmin\ProcessdataController@uploadFileAlbum');
    Route::post('processdata/deleteAlbum/{page}', 'Webadmin\ProcessdataController@deleteFileAlbum');
    Route::post('processdata/deleteAllData', 'Webadmin\ProcessdataController@deleteAllData');
    Route::post('processdata/deleteAlbumAllData', 'Webadmin\ProcessdataController@deleteAlbumAllData');
    /*-----processdata-------*/
});
