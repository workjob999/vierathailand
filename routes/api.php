<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['prefix'=>'/homeBanners', 'middleware' => ['cors']],function(){
    Route::get('/', 'Api\HomeBannersApiController@index');
    Route::get('/{id}', 'Api\HomeBannersApiController@show');
    Route::post('/', 'Api\HomeBannersApiController@store');
    Route::patch('/{id}', 'Api\HomeBannersApiController@update');
    Route::delete('/{id}', 'Api\HomeBannersApiController@delete');
});