#/var/www/html
php artisan down
git pull origin master
composer install --no-dev --prefer-dist
php artisan cache:clear
php artisan config:cache
php artisan route:cache
php artisan migrate
php artisan up
chown -Rf vierathailand:psaserv *
chmod -Rf 775 *
echo 'Deploy finished.'