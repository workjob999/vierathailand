/*-----------------------[Start] Banner full height-----------------------*/

function fullHeight(){

    var windowHeight = $(window).height();  

    var bannerHeight = windowHeight - $(".header").outerHeight();  

    $(".fullscreen-js").css('height',windowHeight);



    var bannerCaptionHeight = windowHeight - $(".photo-hidden").height(); 

    $(".banner-caption").css('height',bannerCaptionHeight); 



    var mCustomRatesHeight = windowHeight - 300; 

    $(".mCustomRates").css('height',mCustomRatesHeight);    

}

 

$(document).ready(function(){  fullHeight();}); 

$(window).bind("resize",function(e){fullHeight();});



$(window).load(function(){ 

    $(".preloader").delay(300).fadeOut(); 

});







$(window).scroll(function() {    

    var scroll = $(window).scrollTop();



    if (scroll >= 1) {

        $("body").addClass("scrolling");

    } else {

        $("body").removeClass("scrolling");

    }

});



 

$(document).ready(function(){



  /*-----------------------[Start] Back to top-----------------------*/

 

  $('.totop-item').click(function () {

      $("html, body").animate({

          scrollTop: 0

      }, 600);

      return false;

  });



  /*------------[Start] Menu Toggle ------------*/

 

  $( "<div class='menuToggle-overlay'></div>" ).appendTo( ".page" );

  $('.nav-toggle').click(function(){

    $("body").toggleClass("menu-opened");    

  });



  $('.menuToggle-overlay').on('click', function () {

    $("body").toggleClass("menu-opened");   

  });
    
  $('#policy').click(function(){

    $("body").toggleClass("menu-opened");    

  });



  /*------------[Start] Dropdown filter------------*/

 

  $('.dropdown.option ul li').on('click', function(){

      var textSelect = $(this).find("span").text();



      $(this).parent().parent().find(".textfilter").html(textSelect + '<span class="icon"></span>')

      $(this).addClass('active').siblings().removeClass('active');

  });



  /*------------[Start] change color of SVG image using CSS ------------*/



  $('img.svg-js').each(function() {

      var $img = jQuery(this);

      var imgURL = $img.attr('src');

      var attributes = $img.prop("attributes");



      $.get(imgURL, function(data) {

          // Get the SVG tag, ignore the rest

          var $svg = jQuery(data).find('svg');



          // Remove any invalid XML tags

          $svg = $svg.removeAttr('xmlns:a');



          // Loop through IMG attributes and apply on SVG

          $.each(attributes, function() {

              $svg.attr(this.name, this.value);

          });



          // Replace IMG with SVG

          $img.replaceWith($svg);

      }, 'xml');

  });

   

});



$(window).bind('resize load',function(){ 



    /*------------[Start] Manage Box -> equalize.heights.js------------*/



    equalheight('.col-height [class^="col"]');



});







$(window).load(function(){



  $(".flex-gallery").flexslider({

      animation: "fade",

      slideshowSpeed: 4500,

      animationSpeed: 1000,

      slideshow: true, 

      controlNav: true,

      directionNav:true,

      pauseOnHover: true

  }); 



  $(".flex-banner").flexslider({

      animation: "fade",

      slideshowSpeed: 4500,

      animationSpeed: 1000,

      slideshow: true, 

      controlNav: false,

      directionNav:false,

      pauseOnHover: true

  }); 







  var windowWidrth = $(window).width();

  var flex_animation = "slide";



 /* if (windowWidrth < 767){

    // Mobile

    flex_animation = "fade";

  }*/



  $(".flex-promotion").flexslider({

      animation: flex_animation,

      slideshowSpeed: 4500,

      animationSpeed: 1000,

      slideshow: true, 

      controlNav: false,

      directionNav:true,

      pauseOnHover: true,

      controlsContainer: ".flex-promotion",

        start: function(slider) {

          $('.total-slides').text(slider.count);

        },

        after: function(slider) {

          $('.current-slide').text(slider.currentSlide + 1); 

        } 

  }); 

  

}); 



 





 