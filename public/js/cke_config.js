    // JavaScript Document  
    var cke_config={  
        skin:'kama', // kama | office2003 | v2  
        language:'th', // th / en and more.....  
        enterMode: 2,// กดปุ่ม Enter -- 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>  
        shiftEnterMode  :1,// กดปุ่ม Shift กับ Enter พร้อมกัน 1=แทรกแท็ก <p> 2=แทรก <br> 3=แทรก <div>  
		width :700, // กำหนดความกว้าง * การกำหนดความกว้างต้องให้เหมาะสมกับจำนวนของ Toolbar  
        height :200, // กำหนดความสูง
		toolbar :
		[
			{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },
			{ name: 'styles', items : [ 'Styles','Format','Font','FontSize' ] },
			{ name: 'colors', items : [ 'TextColor','BGColor' ] },
			'/',
			/*{ name: 'clipboard', items : [ 'Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },*/
			{ name: 'paragraph', items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote',
			'-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
			/*{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },*/
			{ name: 'insert', items : [ 'Image','Table','HorizontalRule','Smiley','SpecialChar' ] },
			//{ name: 'document', items : [ 'Templates','-','Source' ] },
			{ name: 'document', items : [ 'Templates','-','Source' ] }
		],
		keystrokes: 
		[
			[ CKEDITOR.ALT + 121 /*F10*/, 'toolbarFocus' ],
			[ CKEDITOR.ALT + 122 /*F11*/, 'elementsPathFocus' ],
			[ CKEDITOR.SHIFT + 121 /*F10*/, 'contextMenu' ],
			[ CKEDITOR.CTRL + 90 /*Z*/, 'undo' ],
			[ CKEDITOR.CTRL + 89 /*Y*/, 'redo' ],
			[ CKEDITOR.CTRL + CKEDITOR.SHIFT + 90 /*Z*/, 'redo' ],
			[ CKEDITOR.CTRL + 76 /*L*/, 'link' ],
			[ CKEDITOR.CTRL + 66 /*B*/, 'bold' ],
			[ CKEDITOR.CTRL + 73 /*I*/, 'italic' ],
			[ CKEDITOR.CTRL + 85 /*U*/, 'underline' ],	
			[ CKEDITOR.CTRL + 86 /*V*/, 'pastefromword' ],
			[ CKEDITOR.SHIFT + 45 /*INS*/, 'pastefromword' ],
			[ CKEDITOR.ALT + 109 /*-*/, 'toolbarCollapse' ]
		],
		filebrowserBrowseUrl : '/js/ckfinder/ckfinder.html',
		filebrowserImageBrowseUrl : '/js/ckfinder/ckfinder.html?type=Images',
		filebrowserFlashBrowseUrl : '/js/ckfinder/ckfinder.html?type=Flash',
		filebrowserUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Files',
		filebrowserImageUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Images',
		filebrowserFlashUploadUrl : '/js/ckfinder/core/connector/php/connector.php?command=QuickUpload&type=Flash',
		filebrowserWindowWidth : '1000',
		filebrowserWindowHeight : '700'
    }   
      
    function InsertHTML(htmlValue,editorObj){// ฟังก์ขันสำหรับไว้แทรก HTML Code  
        if(editorObj.mode=='wysiwyg'){  
            editorObj.insertHtml(htmlValue);  
        }else{  
            alert( 'You must be on WYSIWYG mode!');  
        }     
    }  
    function SetContents(htmlValue,editorObj){ // ฟังก์ชันสำหรับแทนที่ข้อความทั้งหมด  
        editorObj.setData(htmlValue);  
    }  
    function GetContents(editorObj){// ฟังก์ชันสำหรับดึงค่ามาใช้งาน  
        return editorObj.getData();  
    }  
    function ExecuteCommand(commandName,editorObj){// ฟังก์ชันสำหรับเรียกใช้ คำสั่งใน CKEditor  
        if(editorObj.mode=='wysiwyg'){  
            editorObj.execCommand(commandName);  
        }else{  
            alert( 'You must be on WYSIWYG mode!');  
        }  
    }  