// JavaScript Document

function js_replaceCkeditor() {
  $(".ckeditor").each(function(e) {
    CKEDITOR.replace(this.id, cke_config);
  });
}

//-----------------------gen url-----------------------------

function js_genSlug(wording) {
  var slug = wording
    .toString()
    .toLowerCase()
    .trim()

    .replace(/\s+/g, "-") // Replace spaces with -

    .replace(/&/g, "-and-") // Replace & with 'and'

    .replace(/[^a-z0-9ก-๙\s\'\:\/\[\]-]/g, "")

    .replace(/\-\-+/g, "-") // Replace multiple - with single -

    .substring(0, 70);

  //.replace(/[^\w\-]+/g, '')       // Remove all non-word chars

  return slug;
}

//-----------------------gen url-----------------------------

//-----------------------gen meta keyword-----------------------------

function js_genMetaTitle(wording) {
  wording = wording;

  var keyword = wording.substring(0, 50);

  //จริง ๆๆ 255

  return keyword;
}

//-----------------------gen meta keyword-----------------------------

//-----------------------gen meta keyword-----------------------------

function js_genMetaKeyword(wording) {
  wording = wording;

  var keyword = wording.substring(0, 180);

  //จริง ๆๆ 255

  return keyword;
}

//-----------------------gen meta keyword-----------------------------

//-----------------------gen meta description-----------------------------

function js_genMetaDescription(wording) {
  var description = wording.substring(0, 200);

  return description;
}

//-----------------------gen meta description-----------------------------

//-----------------------gen format money-------------------------------

function formatAmountNoDecimals(number) {
  var rgx = /(\d+)(\d{3})/;

  while (rgx.test(number)) {
    number = number.replace(rgx, "$1" + "," + "$2");
  }

  return number;
}

function formatAmount(number) {
  // remove all the characters except the numeric values

  number = number.replace(/[^0-9]/g, "");

  // set the default value

  if (number.length == 0) number = "0.00";
  else if (number.length == 1) number = "0.0" + number;
  else if (number.length == 2) number = "0." + number;
  else
    number =
      number.substring(0, number.length - 2) +
      "." +
      number.substring(number.length - 2, number.length);

  // set the precision

  number = new Number(number);

  number = number.toFixed(2); // only works with the "."

  // change the splitter to ","

  number = number.replace(/\./g, ",");

  // format the amount

  x = number.split(",");

  x1 = x[0];

  x2 = x.length > 1 ? "." + x[1] : "";

  return formatAmountNoDecimals(x1) + x2;
}

//-----------------------gen format money-------------------------------

//----------------------check email-------------------------------------

function js_checkemail(str) {
  var emailFilter = /^.+@.+\..{2,3}$/;

  if (!emailFilter.test(str)) {
    //alert ("ท่านใส่อีเมล์ไม่ถูกต้อง");

    return false;
  }

  return true;
}

//----------------------check email-------------------------------------

//----------------------check id card-------------------------------------

function js_checkID(id) {
  if (id.length != 13) return false;

  for (i = 0, sum = 0; i < 12; i++) sum += parseFloat(id.charAt(i)) * (13 - i);

  if ((11 - (sum % 11)) % 10 != parseFloat(id.charAt(12))) return false;

  return true;
}

//----------------------check id card-------------------------------------

//----------------------check url ----------------------------------------
function js_checkURL(str) {
  //var expression = /(https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|www\.[a-zA-Z0-9][a-zA-Z0-9-]+[a-zA-Z0-9]\.[^\s]{2,}|https?:\/\/(?:www\.|(?!www))[a-zA-Z0-9]+\.[^\s]{2,}|www\.[a-zA-Z0-9]+\.[^\s]{2,})/gi;
  var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
  var pattern = new RegExp(expression);
  return pattern.test(str);
}
//----------------------check url ----------------------------------------

//----------------------check currency----------------------------------
function js_checkCurrency(num) {
  var pattern = /^\d+(?:\.\d{0,2})$/;
  return pattern.test(num);
}

//------------valid input---------------------------
function jsValidInput() {
  var isError = false;
  var msg = [];
  $("input").removeClass("is-error");
  $("input").each(function(index, value) {
    if ($(this).attr("isRequired") !== undefined && $(this).val() === "") {
      isError = true;
      msg.push($(this).attr("isRequired"));
      $(this).addClass("is-error");
    } else if (
      $(this).attr("isUrl") !== undefined &&
      $(this).val() !== "" &&
      !js_checkURL($(this).val())
    ) {
      isError = true;
      msg.push($(this).attr("isUrl"));
      $(this).addClass("is-error");
    } else if (
      $(this).attr("isNumber") !== undefined &&
      $(this).val() !== "" &&
      isNaN($(this).val())
    ) {
      isError = true;
      msg.push($(this).attr("isNumber"));
      $(this).addClass("is-error");
    } else if (
      $(this).attr("isCurrency") !== undefined &&
      $(this).val() !== "" &&
      !js_checkCurrency($(this).val())
    ) {
      isError = true;
      msg.push($(this).attr("isCurrency"));
      $(this).addClass("is-error");
    } else if (
      $(this).attr("isEmail") !== undefined &&
      $(this).val() !== "" &&
      !js_checkemail($(this).val())
    ) {
      isError = true;
      msg.push($(this).attr("isEmail"));
      $(this).addClass("is-error");
    }
  });

  var result = {
    isError: isError,
    msg: msg
  };

  console.log(result);

  return result;
}
//------------valid input---------------------------

$(document).ready(function() {
  //บังคับกรอกตัวเลข
  $(".class_keyNumber").keydown(function(e) {
    // Allow: backspace, delete, tab, escape, enter and .
    if (
      $.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
      // Allow: home, end, left, right, down, up
      (e.keyCode >= 35 && e.keyCode <= 40)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress

    var number = /^[0-9]*$/;
    var char = e.key;
    if (number.test(char)) {
      return true;
    }
    return false;
  });
  //บังคับกรอกตัวเลข

  $(".class_keyNumber:not('.class_zeroNumber')").keyup(function(e) {
    if ($(this).val() == "" || $(this).val() === null) $(this).val(1);
  });

  $(".class_keyEmail").keydown(function(event) {
    /* Act on the event */
    /* Act on the event */
    if (
      $.inArray(event.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
      // Allow: Ctrl+A, Command+A
      (event.keyCode === 65 &&
        (event.ctrlKey === true || event.metaKey === true)) ||
      // Allow: home, end, left, right, down, up
      (event.keyCode >= 35 && event.keyCode <= 40)
    ) {
      // let it happen, don't do anything
      return;
    }
    // Ensure that it is a number and stop the keypress

    var english = /^[A-Za-z0-9@_-]*$/;
    var char = event.key;
    if (english.test(char)) {
      return true;
    }
    return false;
  });
});
