$(document).ready(function(){ 
         
    google.maps.event.addDomListener(window, 'load', init);
        var myLatlng = new google.maps.LatLng(13.787357, 100.576828);
        
        function init() {
            var mapOptions = {
                zoom: 17,
                center: myLatlng,
                scrollwheel: true,
                mapTypeControl: true,
                streetViewControl: false,
                gestureHandling: 'greedy', 
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.LEFT_TOP
                }
                    
         
        };
        var mapElement = document.getElementById('map');
        var map = new google.maps.Map(mapElement, mapOptions);
        /**/
     
        var url = 'img/pin.svg';
        var size = new google.maps.Size(60, 70);
        
        var image = {
            url: url,
            size: size,
            scaledSize: new google.maps.Size(60, 70),
            origin: new google.maps.Point(0,0),
            anchor: new google.maps.Point(30.5,70)
        };
            
        var marker = new google.maps.Marker({
              position: myLatlng,
              map: map,
              icon: image
        });

        google.maps.event.addListener(marker, 'click', function() {
            window.open('https://goo.gl/Wxec9q', '_blank');
        });
                
    } 
    
    init(); 
     
});
  