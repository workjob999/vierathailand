<?
	function php_listFolder($path)
	{
		$file_names = scandir($path);

		foreach($file_names as $value)
		{
			if ($value != ".." && $value != ".")
			{
				$array_dir[$value] = strtr($value,"_"," ");
			}
		}
		
		return $array_dir;
	}
	
	function php_initialvariable(&$var)
	{
		return (!isset($var))?"":$var;
	}
?>