<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class InsertDataAboutTable extends Migration
{

    public $set_table = array('about');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable) {
            $now = Carbon::now();
            $current_datetime = $now->format("Y-m-d H:i:s");
            if (Schema::hasTable($setTable)) {
                $count = DB::table($setTable)->count();
                if ($count == 0) {
                    DB::table($setTable)->insert([
                        'created_at' => $current_datetime,
                        'updated_at' => $current_datetime,
                        'created_by' => 1,
                        'updated_by' => 1,
                    ]);
                }
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
