<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddPermissionPromotionReview extends Migration
{
    public $set_table = 'permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $result = DB::table($this->set_table)->orderBy("order_by","DESC")->first();
        $order_by = $result->order_by;

        //Promotions
        $result_count = DB::table($this->set_table)->where("name","view-promotions-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 6,
                'name' => "view-promotions-list",
                'display_name' => "View Promotions List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+1
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 6,
                'name' => "create-promotions",
                'display_name' => "Create Promotions",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+2
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 6,
                'name' => "edit-promotions",
                'display_name' => "Edit Promotions",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+3
            ]);
        }
        //Promotions


        //Reviews
        $result_count = DB::table($this->set_table)->where("name","view-reviews-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 7,
                'name' => "view-reviews-list",
                'display_name' => "View Reviews List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+4
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-home-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 7,
                'name' => "create-reviews",
                'display_name' => "Create Reviews",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+5
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-home-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 7,
                'name' => "edit-reviews",
                'display_name' => "Edit Reviews",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+6
            ]);
        }
        //Reviews
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        

    }
}
