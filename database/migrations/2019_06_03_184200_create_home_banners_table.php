<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHomeBannersTable extends Migration
{

    public $set_table = array('home_banners','home_promotions','home_tvc');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasTable($setTable)) {
                Schema::create($setTable, function (Blueprint $table) {
                    $table->increments('id')->unsigned();
                    $table->string('banner_th', 255)->nullable()->default(null);
                    $table->string('title_th', 150)->nullable()->default(null);
                    $table->string('detail_th', 255)->nullable()->default(null);
                    $table->enum('status_th', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน,N=ไม่ใช้งาน');
                    $table->string('banner_en', 255)->nullable()->default(null);
                    $table->string('title_en', 150)->nullable()->default(null);
                    $table->string('detail_en', 255)->nullable()->default(null);
                    $table->enum('status_en', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน,N=ไม่ใช้งาน');
                    $table->string('banner_ch', 255)->nullable()->default(null);
                    $table->string('title_ch', 150)->nullable()->default(null);
                    $table->string('detail_ch', 255)->nullable()->default(null);
                    $table->enum('status_ch', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน,N=ไม่ใช้งาน');
                    $table->string('url', 255)->nullable()->default(null);
                    $table->string('visitor', 45)->default('000.000.000.000')->comment('Ip Address');
                    $table->enum('status', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน,N=ไม่ใช้งาน');
                    $table->timestamps();
                    $table->integer('created_by')->default('0')->comment('สร้างโดย');
                    $table->integer('updated_by')->default('0')->comment('แก้ไขโดย');
                    $table->integer('order_by')->default('0')->comment('ลำดับ');
                });
            }
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
