<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFacebooksTable extends Migration
{

    public $set_table = 'facebooks';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->set_table)) {
            Schema::create($this->set_table, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('reg_id')->nullable()->default(0);
                $table->string('url', 255)->nullable()->default(null);
                $table->string('visitor', 45)->default('000.000.000.000')->comment('Ip Address');
                $table->enum('status', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน');
                $table->timestamps();
                $table->integer('created_by')->default('0')->comment('สร้างโดย');
                $table->integer('updated_by')->default('0')->comment('แก้ไขโดย');
                $table->integer('order_by')->default('0')->comment('ลำดับ');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
