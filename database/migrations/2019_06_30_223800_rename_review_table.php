<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameReviewTable extends Migration
{

    public $set_table = array('review');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (Schema::hasTable($setTable)) {
                Schema::rename($setTable, $setTable.'s');
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
