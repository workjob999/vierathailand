<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddPermissionRegisters extends Migration
{
    public $set_table = 'permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $result = DB::table($this->set_table)->orderBy("order_by","DESC")->first();
        $order_by = $result->order_by;

        //Registers
        $result_count = DB::table($this->set_table)->where("name","view-registers-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 3,
                'name' => "view-registers-list",
                'display_name' => "View Registers List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+1
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-registers")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 3,
                'name' => "create-registers",
                'display_name' => "Create Registers",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+2
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-registers")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 3,
                'name' => "edit-registers",
                'display_name' => "Edit Registers",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+3
            ]);
        }
        //Registers
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        

    }
}
