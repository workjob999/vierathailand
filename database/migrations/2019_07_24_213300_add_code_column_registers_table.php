<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCodeColumnRegistersTable extends Migration
{

    public $set_table = array('registers');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasColumn($setTable, "code")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('code',150)->nullable()->default(NULL)->after("id");
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
