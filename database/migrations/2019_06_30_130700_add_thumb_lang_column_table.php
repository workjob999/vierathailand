<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddThumbLangColumnTable extends Migration
{

    public $set_table = array('home_banners','home_promotions','home_tvc');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasColumn($setTable, "thumb_th")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('thumb_th', 255)->nullable()->default(NULL)->after("thumb");
                });
            }

            if (!Schema::hasColumn($setTable, "thumb_en")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('thumb_en', 255)->nullable()->default(NULL)->after("status_th");
                });
            }

            if (!Schema::hasColumn($setTable, "thumb_ch")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('thumb_ch', 255)->nullable()->default(NULL)->after("status_en");
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
