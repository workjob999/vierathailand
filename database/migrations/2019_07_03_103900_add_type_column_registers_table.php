<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTypeColumnRegistersTable extends Migration
{

    public $set_table = array('registers');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (Schema::hasColumn($setTable, "type")) {
                DB::statement("ALTER TABLE ".$setTable." CHANGE type type enum('1','2','3') DEFAULT '1' NULL");
                //Schema::table($setTable, function (Blueprint $table) {
                    //$table->enum('type',[1,2,3])->nullable()->default(1)->change();
                //});
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
