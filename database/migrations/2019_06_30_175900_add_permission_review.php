<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddPermissionReview extends Migration
{
    public $set_table = 'permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $result = DB::table($this->set_table)->orderBy("order_by","DESC")->first();
        $order_by = $result->order_by;

        //reviews
        $result_count = DB::table($this->set_table)->where("name","create-reviews")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 7,
                'name' => "create-reviews",
                'display_name' => "Create Reviews",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+1
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-reviews")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 7,
                'name' => "edit-reviews",
                'display_name' => "Edit Reviews",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+2
            ]);
        }
        //reviews
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        

    }
}
