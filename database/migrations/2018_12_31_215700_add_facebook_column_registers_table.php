<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFacebookColumnRegistersTable extends Migration
{

    public $set_table = array('registers');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasColumn($setTable, "facebook")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->text('facebook')->nullable()->default(NULL)->after("sale_place");
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
