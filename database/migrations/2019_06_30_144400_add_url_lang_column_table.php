<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlLangColumnTable extends Migration
{

    public $set_table = array('home_banners','home_promotions','home_tvc');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasColumn($setTable, "url_th")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('url_th', 255)->nullable()->default(NULL)->after("thumb_th");
                });
            }

            if (!Schema::hasColumn($setTable, "url_en")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('url_en', 255)->nullable()->default(NULL)->after("thumb_en");
                });
            }

            if (!Schema::hasColumn($setTable, "url_ch")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->string('url_ch', 255)->nullable()->default(NULL)->after("thumb_ch");
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
