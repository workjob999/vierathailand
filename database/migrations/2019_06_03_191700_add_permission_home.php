<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class AddPermissionHome extends Migration
{
    public $set_table = 'permissions';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $result = DB::table($this->set_table)->orderBy("order_by","DESC")->first();
        $order_by = $result->order_by;

        //Home Banners
        $result_count = DB::table($this->set_table)->where("name","view-home-banners-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "view-home-banners-list",
                'display_name' => "View Home Banners List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+1
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-home-banners")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "create-home-banners",
                'display_name' => "Create Home Banners",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+2
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-home-banners")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "edit-home-banners",
                'display_name' => "Edit Home Banners",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+3
            ]);
        }
        //Home Banners


        //Home Promotions
        $result_count = DB::table($this->set_table)->where("name","view-home-promotions-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "view-home-promotions-list",
                'display_name' => "View Home Promotions List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+4
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-home-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "create-home-promotions",
                'display_name' => "Create Home Promotions",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+5
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-home-promotions")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "edit-home-promotions",
                'display_name' => "Edit Home Promotions",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+6
            ]);
        }
        //Home Promotions


        //Home Tvc
        $result_count = DB::table($this->set_table)->where("name","view-home-tvc-list")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "view-home-tvc-list",
                'display_name' => "View Home Tvc List",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+7
            ]);
        }

        $result_count = DB::table($this->set_table)->where("name","create-home-tvc")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "create-home-tvc",
                'display_name' => "Create Home Tvc",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+8
            ]);
        }


        $result_count = DB::table($this->set_table)->where("name","edit-home-tvc")->count();
        if ($result_count==0)
        {
            $insert_id = DB::table($this->set_table)->insertGetId([
                'groups' => 4,
                'name' => "edit-home-tvc",
                'display_name' => "Edit Home Tvc",
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'order_by' => $order_by+9
            ]);
        }
        //Home Tvc
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        

    }
}
