<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegistersTable extends Migration
{

    public $set_table = 'registers';

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable($this->set_table)) {
            Schema::create($this->set_table, function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->enum('type',[1,2])->nullable()->default('1')->comment('1=viera');
                $table->string('fullname', 255)->nullable()->default(null);
                $table->string('phone', 20)->nullable()->default(null);
                $table->string('email', 150)->nullable()->default(null);
                $table->string('lineid', 150)->nullable()->default(null);
                $table->string('refer', 200)->nullable()->default(null);
                $table->string('sale_place', 255)->nullable()->default(null);
                $table->string('visitor', 45)->default('000.000.000.000')->comment('Ip Address');
                $table->enum('status', ['Y', 'N'])->default('Y')->comment('Y = ใช้งาน');
                $table->timestamps();
                $table->integer('created_by')->default('0')->comment('สร้างโดย');
                $table->integer('updated_by')->default('0')->comment('แก้ไขโดย');
                $table->integer('order_by')->default('0')->comment('ลำดับ');
            });
        }

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
