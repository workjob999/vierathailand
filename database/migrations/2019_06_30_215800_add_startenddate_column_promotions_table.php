<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartenddateColumnPromotionsTable extends Migration
{

    public $set_table = array('promotions','review');

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        foreach ($this->set_table as $setTable)
        {
            if (!Schema::hasColumn($setTable, "endDate")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->text('endDate')->nullable()->default(NULL)->after("url");
                });
            }

            if (!Schema::hasColumn($setTable, "startDate")) {
                Schema::table($setTable, function (Blueprint $table) {
                    $table->text('startDate')->nullable()->default(NULL)->after("url");
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->set_table);
    }
}
