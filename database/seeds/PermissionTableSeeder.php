<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $current_time = \Carbon\Carbon::now()->toDateTimeString();

        $result = DB::table('permissions')->select('id')->orderBy('id','DESC')->first();

        if ($result->id<=6)
        {
        	DB::table('permissions')->insert([
	            [
	            	'id' => 7,
	                'name' => "view-reports-list",
	                'display_name' => "View reports list",
	                'created_at' => $current_time,
	                'updated_at' => $current_time,
	                'groups' => 3,
	                'order_by' => 7,
	            ],
	            [
	            	'id' => 8,
	                'name' => "edit-reports",
	                'display_name' => "Edit reports",
	                'created_at' => $current_time,
	                'updated_at' => $current_time,
	                'groups' => 3,
	                'order_by' => 8,
	            ],
	            [
	            	'id' => 9,
	                'name' => "send-email-reports",
	                'display_name' => "Send Email reports",
	                'created_at' => $current_time,
	                'updated_at' => $current_time,
	                'groups' => 3,
	                'order_by' => 9,
	            ]
	        ]);

	        DB::table('permission_role')->insert([
	            [
	                'permission_id' => "7",
	                'role_id' => "1",
	            ],
	            [
	                'permission_id' => "8",
	                'role_id' => "1",
	            ],
	            [
	                'permission_id' => "9",
	                'role_id' => "1",
	            ],
			]);
        }
        else if ($result->id<=9)
        {
        	DB::table('permissions')->insert([
	            [
	            	'id' => 10,
	                'name' => "view-codes-list",
	                'display_name' => "View codes list",
	                'created_at' => $current_time,
	                'updated_at' => $current_time,
	                'groups' => 4,
	                'order_by' => 10,
	            ]
	        ]);

	        DB::table('permission_role')->insert([
	            [
	                'permission_id' => "10",
	                'role_id' => "1",
	            ]
			]);
        }

    }

}

