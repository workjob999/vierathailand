<?php



use Illuminate\Database\Seeder;



class RolesTableSeeder extends Seeder

{

    /**

     * Run the database seeds.

     *

     * @return void

     */

    public function run()

    {

        $current_time = \Carbon\Carbon::now()->toDateTimeString();

        DB::table('roles')->insert([

            [

                'name' => "super-admin",

                'display_name' => "Super Admin",

                'created_at' => $current_time,

                'updated_at' => $current_time,

            ],

            [

                'name' => "admin",

                'display_name' => "Admin",

                'created_at' => $current_time,

                'updated_at' => $current_time,

            ]

        ]);


        DB::table('role_user')->insert([
            [
                'role_id' => 1,
                'user_id' => 1,
                'user_type' => "App\User"
            ],
            [
                'role_id' => 1,
                'user_id' => 2,
                'user_type' => "App\User"
            ],
            [
                'role_id' => 2,
                'user_id' => 3,
                'user_type' => "App\User"
            ]
        ]);


        DB::table('permissions')->insert([
            [
                'name' => "view-user-list",
                'display_name' => "View user list",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 1,
                'order_by' => 1,
            ],
            [
                'name' => "create-user",
                'display_name' => "Create user",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 1,
                'order_by' => 2,
            ],
            [
                'name' => "edit-user",
                'display_name' => "Edit user",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 1,
                'order_by' => 3,
            ],
            [
                'name' => "view-role-list",
                'display_name' => "View role list",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 2,
                'order_by' => 4,
            ],
            [
                'name' => "create-role",
                'display_name' => "Create role",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 2,
                'order_by' => 5,
            ],
            [
                'name' => "edit-role",
                'display_name' => "Edit role",
                'created_at' => $current_time,
                'updated_at' => $current_time,
                'groups' => 2,
                'order_by' => 6,
            ]
        ]);



        DB::table('permission_role')->insert([

            [

                'permission_id' => "1",

                'role_id' => "1",

            ],

            [

                'permission_id' => "2",

                'role_id' => "1",

            ],

            [

                'permission_id' => "3",

                'role_id' => "1",

            ],

            [

                'permission_id' => "4",

                'role_id' => "1",

            ],

            [

                'permission_id' => "5",

                'role_id' => "1",

            ],

            [

                'permission_id' => "6",

                'role_id' => "1",

            ],

            [

                'permission_id' => "1",

                'role_id' => "2",

            ],

            [

                'permission_id' => "2",

                'role_id' => "2",

            ],

            [

                'permission_id' => "3",

                'role_id' => "2",

            ],

            [

                'permission_id' => "4",

                'role_id' => "2",

            ]

        ]);

    }

}

