<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $current_time = \Carbon\Carbon::now()->toDateTimeString();
          DB::table('users')->insert([
                [
                    'username' => "bird",
                    'name' => "Bird",
                    'email' => 'workjob999@gmail.com',
                    'password' => bcrypt('Bird2013!'),
                    'created_at' => $current_time,
                    'updated_at' => $current_time,
                ],
                [
                    'username' => "nod",
                    'name' => "Nod",
                    'email' => 'nodkrab@gmail.com',
                    'password' => bcrypt('123456'),
                    'created_at' => $current_time,
                    'updated_at' => $current_time,
                ],
                [
                    'username' => "admin",
                    'name' => "Admin",
                    'email' => 'admin@allinspire.co.th',
                    'password' => bcrypt('12345'),
                    'created_at' => $current_time,
                    'updated_at' => $current_time,
                ]

          ]);

    }

}

