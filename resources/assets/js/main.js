try {
    window.$ = window.jQuery = require('jquery')
    require('jquery-dotdotdot/src/jquery.dotdotdot.min.js')
} catch (e) {}

import 'bootstrap'
import 'slick-carousel-no-fonts'
import 'sweetalert'
import '@fancyapps/fancybox'


var mobileHover = function () {
    "use strict";
    $('a').on('touchstart', function () {
        $(this).trigger('hover');
    }).on('touchend', function () {
        $(this).trigger('hover');
    });
};

mobileHover();

//SVG
$('img.svg').each(function () {
    var $img = jQuery(this)
    var imgID = $img.attr('id')
    var imgClass = $img.attr('class')
    var imgURL = $img.attr('src')

    jQuery.get(imgURL, function (data) {
        // Get the SVG tag, ignore the rest
        var $svg = jQuery(data).find('svg')

        // Add replaced image's ID to the new SVG
        if (typeof imgID !== 'undefined') {
            $svg = $svg.attr('id', imgID)
        }
        // Add replaced image's classes to the new SVG
        if (typeof imgClass !== 'undefined') {
            $svg = $svg.attr('class', imgClass + ' replaced-svg')
        }

        // Remove any invalid XML tags as per http://validator.w3.org
        $svg = $svg.removeAttr('xmlns:a')

        // Check if the viewport is set, if the viewport is not set the SVG wont't scale.
        if (!$svg.attr('viewBox') && $svg.attr('height') && $svg.attr('width')) {
            $svg.attr('viewBox', '0 0 ' + $svg.attr('height') + ' ' + $svg.attr('width'))
        }

        // Replace image with new SVG
        $img.replaceWith($svg)

    }, 'xml')
})

$('.banner-slide').slick({
    arrows: false,
    dots: false,
    autoplay: true,
    autoplaySpeed: 4000,
});

$('.promotion-slide').slick({
    arrows: false,
    dots: true,
    autoplay: true,
    slidesToShow: 2,
    slidesToScroll: 1,
});

$('.review-slide').slick({
    arrows: false,
    dots: true,
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 992,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
            }
        },
    ]
});

$('.product-slide').slick({
    arrows: false,
    dots: false,
});

$('.lifestyle-slide').slick({
    arrows: false,
    dots: true,
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
});


