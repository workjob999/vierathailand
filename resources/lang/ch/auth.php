<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'ChooseImage' => 'Choose your image',
    'Fullname' => 'First name & Last name',
    'Telephone' => 'Telephone No.',
    'Email' => 'Email',
    'Line' => 'Line Id',
    'ReferPreson' => 'Advisor',
    'DistributionLocation' => 'Distribution Location',
    'Chooseyourtype' => 'Choose your type',
    'DEALER' => 'DEALER',
    'AGENT' => 'AGENT',
    'DROPSHIP' => 'DROPSHIP',
    'Subject' => 'Subject',
    'Message' => 'Message',
    'Send' => 'Send',
];
