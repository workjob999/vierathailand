<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
     */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'ChooseImage' => 'เลือกรูปภาพของคุณ',
    'Fullname' => 'ชื่อ-นามสกุล',
    'Telephone' => 'เบอร์โทร',
    'Email' => 'อีเมล',
    'Line' => 'ไลน์ไอดี',
    'ReferPreson' => 'ผู้แนะนำ',
    'DistributionLocation' => 'สถานที่จัดจำหน่าย',
    'Chooseyourtype' => 'เลือกประเภทตัวแทน',
    'DEALER' => 'DEALER',
    'AGENT' => 'AGENT',
    'DROPSHIP' => 'DROPSHIP',
    'Subject' => 'หัวข้อ',
    'Message' => 'เรื่อง',
    'Send' => 'Send',
];
