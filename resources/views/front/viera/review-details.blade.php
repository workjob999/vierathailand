@extends('layouts.front.viera')

@section('page_title','Review')

@section('page_style')
@stop

@section('content')
	<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{Route('home',['lang'=>Request()->lang])}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a href="{{Route('reviews',['lang'=>Request()->lang])}}">Review</a></li>
        </ol>
    </nav>
    <div class="row pb-5">
        <div class="col-12 py-4 px-4 text-center">
            <h1 class="py-2 display-3"> <strong> REVIEW <span>VIERA</span> </strong></h1>            
            <div class="review-slide">
                <div>
                    @if (isset($review) && file_exists(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb))
                        <img src="{{ url(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb) }}" class="img-fluid w-100" alt="{{$review->title}}">
                    @endif
                    {{-- <img src="{{ url('dist/img/review-03.jpg') }}" class="img-fluid w-100" alt=""> --}}
                </div>
            </div>
        </div>
        <div class="col-12 col-sm-1">
			<a href="javascript:history.back();"><img src="{{ url('dist/img/btn-back.jpg') }}" alt=""></a>
		</div> 
        <div class="col-12 col-sm-11">
			<h1 class="orangeColor">{{$review->title}}</h1>
            {!! $review->content !!}
		</div>
    </div>
</div>

@stop

@section('page_script')
@stop