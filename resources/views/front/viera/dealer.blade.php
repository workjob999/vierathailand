@extends('layouts.front.viera')

@section('page_title','Dealer')

@section('page_style')
@stop

@section('content')
<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page"><a href="#">Dealer / Agent </a></li>
		</ol>
	</nav>
	<!-- <div class="row no-gutters">
		<div class="col-12 text-center">
			<h1>REGISTER <span>DEALER</span> </h1>
		</div>
	</div> -->
	<div class="container py-3">
		<div class="row">
			<div class="col-12 text-center">
				<h1>SEARCH DEALER / AGENT <span>VIERA</span> </h1>
			</div>
			<div class="col-12 col-sm-6 offset-sm-3">
				<div class="input-group">
					<select id="searchType" name="searchType" class="form-control form-control-lg mx-3">
						<option value="">--กรุณาเลือกกลุ่ม--</option>
						@if (config('config.config_arr_type'))
							@foreach (config('config.config_arr_type') as $key=>$value)
								@if ($key!=3)
									@if (!empty(Request()->t))
										@if (Request()->t==$key)
											<option value="{!! $key !!}" selected>{!! $value !!}</option>
										@else
											<option value="{!! $key !!}">{!! $value !!}</option>
										@endif
									@else
										<option value="{!! $key !!}">{!! $value !!}</option>
									@endif
								@endif
							@endforeach
						@endif
					</select>

					<input id="searchDealer" name="searchDealer" type="text" class="form-control form-control-lg"
						placeholder="ค้นหา dealer" value="{{ @Request()->s }}">
					<div id="btnSearch" class="input-group-append">
						<span class="input-group-text"><i class="fas fa-search"></i></span>
					</div>
				</div>
			</div>
			{{-- @if (!empty($contentSearch) && sizeof($contentSearch)>0) --}}
			@if (!empty(Request()->t))
				<div class="col-12 text-center pt-5">
					<h3>Result Search: {{ config('config.config_arr_type')[Request()->t] }}</h3>
				</div>				
				@foreach($contentSearch as $deal)
				<div class="col-12 col-sm-3 pt-5 text-center">
					@if ($deal->thumb!=NULL && $deal->thumb!="" &&
					file_exists(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb))
					<img src="{{ url(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb) }}"
						class="img-fluid" alt="">
					@else
					<img src="{{ url('dist/img/pic.jpg') }}" class="img-fluid" alt="">
					@endif
					<h4 class="pt-3">{!! $deal->fullname !!}</h4>					
					<div class="row">
						<div class="col-4 text-right">Mobile :</div>
						<div class="col-8 text-left">{!! $deal->phone !!}</div>
						<div class="col-4 text-right">line id :</div>
						<div class="col-8 text-left"><a href="#">{!! $deal->lineid !!}</a></div>
						<div class="col-4 text-right">Saleplace:</div>
						<div class="col-8 text-left">{!! $deal->sale_place !!}</div>
						<div class="col-4 text-right">FB :</div>
						<div class="col-8 text-left"><a href="#">@if ($deal->arr_facebook!==NULL && sizeof($deal->arr_facebook)>0)
								@foreach ($deal->arr_facebook as $facebook)
								<a href="#">{!! $facebook !!}</a>
								@endforeach
								@endif
						</div>					
						<div class="col-4 text-right">Refer : </div>
						<div class="col-8 text-left">{!! $deal->refer !!}</div>
					</div>					
				</div>
				@endforeach
			@else
				@if (!empty($dealer) && sizeof($dealer)>0)
				<div class="col-12 text-center pt-5">
					<h3>DEALER</h3>
					<hr>
				</div>				
				@foreach($dealer as $deal)
				<div class="col-12 col-sm-3 pt-3 text-center">
					@if ($deal->thumb!=NULL && $deal->thumb!="" &&
					file_exists(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb))
					<img src="{{ url(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb) }}"
						class="img-fluid" alt="">
					@else
					<img src="{{ url('dist/img/pic.jpg') }}" class="img-fluid" alt="">
					@endif
					<h5 class="pt-2">{!! $deal->fullname !!}</h5>

					<div class="row">
							<div class="col-4 text-right">Mobile :</div>
							<div class="col-8 text-left">{!! $deal->phone !!}</div>
							<div class="col-4 text-right">line id :</div>
							<div class="col-8 text-left"><a href="#">{!! $deal->lineid !!}</a></div>
							<div class="col-4 text-right">Saleplace:</div>
							<div class="col-8 text-left">{!! $deal->sale_place !!}</div>
							<div class="col-4 text-right">FB :</div>
							<div class="col-8 text-left"><a href="#">@if ($deal->arr_facebook!==NULL && sizeof($deal->arr_facebook)>0)
									@foreach ($deal->arr_facebook as $facebook)
									<a href="#">{!! $facebook !!}</a>
									@endforeach
									@endif
							</div>					
							<div class="col-4 text-right">Refer :</div>
							<div class="col-8 text-left">{!! $deal->refer !!}</div>
						</div>

					{{-- <h5>Mobile {!! $deal->phone !!}</h5>
					<p>line id : <a href="#">{!! $deal->lineid !!}</a><br>
						FB :
						@if ($deal->arr_facebook!==NULL && sizeof($deal->arr_facebook)>0)
						@foreach ($deal->arr_facebook as $facebook)
						<br><a href="#">{!! $facebook !!}</a>
						@endforeach
						@endif
						<br>
						Refer : {!! $deal->refer !!}
					</p> --}}
				</div>				
				@endforeach
				@endif

				@if (!empty($agent) && sizeof($agent)>0)
				<div class="col-12 text-center pt-5">
					<h3>AGENT</h3>
					<hr>
				</div>	
				@foreach($agent as $deal)
				<div class="col-12 col-sm-3 pt-5 text-center">
					@if ($deal->thumb!=NULL && $deal->thumb!="" &&
					file_exists(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb))
					<img src="{{ url(config('config.config_pathUpload').'/registers/'.$deal->pathFile.'/'.$deal->thumb) }}"
						class="img-fluid" alt="">
					@else
					<img src="{{ url('dist/img/pic.jpg') }}" class="img-fluid" alt="">
					@endif
					<h5 class="pt-2">{!! $deal->fullname !!}</h5>
					<div class="row">
							<div class="col-4 text-right">Mobile :</div>
							<div class="col-8 text-left">{!! $deal->phone !!}</div>
							<div class="col-4 text-right">line id :</div>
							<div class="col-8 text-left"><a href="#">{!! $deal->lineid !!}</a></div>
							<div class="col-4 text-right">FB :</div>
							<div class="col-8 text-left"><a href="#">@if ($deal->arr_facebook!==NULL && sizeof($deal->arr_facebook)>0)
									@foreach ($deal->arr_facebook as $facebook)
									<a href="#">{!! $facebook !!}</a>
									@endforeach
									@endif
							</div>					
							<div class="col-4 text-right">Refer :</div>
							<div class="col-8 text-left">{!! $deal->refer !!}</div>
					</div>

					{{-- <h5>Mobile {!! $deal->phone !!}</h5>
					<p>line id : <a href="#">{!! $deal->lineid !!}</a><br>
						FB :
						@if ($deal->arr_facebook!==NULL && sizeof($deal->arr_facebook)>0)
						@foreach ($deal->arr_facebook as $facebook)
						<br><a href="#">{!! $facebook !!}</a>
						@endforeach
						@endif
						<br>
						Refer : {!! $deal->refer !!}
					</p> --}}
				</div>
				@endforeach
				@endif
			@endif
			{{-- <div class="col-12 pt-5 text-center">
			<img src="{{ url('dist/img/pic.jpg') }}" class="img-fluid" alt="">
			<h5 class="pt-2">Bom Sulasuk</h5>
			<h5>Mobile 098 098 0999</h5>
			<p>Everyday it provides me <br>
				with exactly what I need</p>
		</div> --}}
	</div>
</div>

@stop

@section('page_script')
<script type="text/javascript">
	$(document).ready(function() {
		$("#btnSearch").click(function(event) {
			/* Act on the event */
			var url = "{{ url()->current() }}";
			location = url+"?t="+$("#searchType option:selected").val()+"&s="+$("#searchDealer").val();
		});
	});
</script>
@stop