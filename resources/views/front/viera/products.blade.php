@extends('layouts.front.viera')

@section('page_title','Products')

@section('page_style')
@stop

@section('content')
<div class="container">
	<div class="row">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item"><a href="#">Home</a></li>
				<li class="breadcrumb-item active" aria-current="page"><a href="#">Product</a></li>
			</ol>
		</nav>
	</div>
</div>
<div class="row no-gutters">
	<div class="col-12">
		<img src="{{ url('dist/img/banner-02.jpg') }}" class="img-fluid w-100" alt="">
	</div>
	<div class="col-12">
		<img src="{{ url('dist/img/banner-03.jpg') }}" class="img-fluid w-100" alt="">
	</div>
	<div class="col-12">
		<img src="{{ url('dist/img/banner-04.jpg') }}" class="img-fluid w-100" alt="">
	</div>
</div>



@stop

@section('page_script')
@stop