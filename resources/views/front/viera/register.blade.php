@extends('layouts.front.viera')

@section('page_title','Register')

@section('page_style')
@stop

@section('content')
<div class="container">
	<nav aria-label="breadcrumb">
		<ol class="breadcrumb">
			<li class="breadcrumb-item"><a href="#">Home</a></li>
			<li class="breadcrumb-item active" aria-current="page"><a href="#">Dealer</a></li>
		</ol>
	</nav>
	<div class="row no-gutters">
		<div class="col-12 text-center">
			<h1>REGISTER <span>DEALER</span> </h1>
		</div>
	</div>
	<div class="container py-3">
		<div class="row">

			<div class="col-12 col-sm-6 offset-sm-3">				
				{!! Form::open(['url'=>'','method'=>'POST', 'files' =>
				true,'class'=>"form-horizontal","id"=>"frm_register", 'files' => true]) !!}
				
				<div class="text-center my-3"><img id="image_profile" src="http://placehold.it/180" class="img-fluid" width="180" height="auto" alt="your image" /></div>
				<div class="form-group">
						{{trans("auth.ChooseImage")}}
						<label class="btn btn-primary col-12">
							<input class="col-12" type='file' id="thumb" name="thumb" onchange="js_changeFile(this)" />
						</label>
				</div>
				<div class="form-group">
					<input type="text" class="form-control" name="fullname" id="fullname" placeholder="{{trans("auth.Fullname")}} *">
				</div>
				<div class="row">
					<div class="form-group col-6">
						<input type="text" class="form-control class_keyNumber class_zeroNumber" maxlength="10" name="phone" id="phone"
						 placeholder="{{trans("auth.Telephone")}} *">
					</div>
					<div class="form-group col-6">
						<input type="text" class="form-control" name="email" id="email" placeholder="{{trans("auth.Email")}}">
					</div>
				</div>
				<div class="row">
					<div class="form-group col-6">
						<input type="text" class="form-control" name="lineid" id="lineid" placeholder="{{trans("auth.Line")}} *">
					</div>
					<div class="form-group col-6">
						<input type="text" class="form-control" name="refer" id="refer" placeholder="{{trans("auth.ReferPreson")}} *">
					</div>
					<div id="div_place" class="form-group col-12">
						<input type="text" class="form-control" name="sale_place" id="sale_place" placeholder="{{trans("auth.DistributionLocation")}} *">
					</div>
					<div id="div_type" class="form-group col-12">
							<select class="form-control " name="type" id="inputType">
							<option value="0" selected>{{trans("auth.Chooseyourtype")}} *</option>
							<option value="1">{{trans("auth.DEALER")}}</option>
							<option value="2">{{trans("auth.AGENT")}}</option>
							<option value="3">{{trans("auth.DROPSHIP")}}</option>						
							</select>
					</div>
					<div id="div_facebook_1" class="form-group col-12 d-flex class_div_facebook">
						<input type="text" class="form-control class_facebook" style="height:auto;" name="facebook_page[]" id="facebook_page_1" placeholder="facebook page *">
						<button type="button" id="btn_add_facebook_1" class="btn btn-primary text-center class_add_facebook" onclick="js_addFacebook()">ADD <i class="fas fa-plus-circle"></i></button>
						<button type="button" id="btn_delete_facebook_1" class="btn btn-danger text-center class_delete_facebook" style="display: none" onclick="js_removeFacebook('1')">DELETE <i class="fas fa-minus-circle"></i></button>
					</div>
				</div>
				{{-- <div class="form-check text-center">
					<input class="form-check-input" type="checkbox" name="chk_rule" value="" id="chk_rule">
					<label class="form-check-label" for="defaultCheck1">
						หมายเหตุ <a href="#" data-toggle="modal" data-target="#exampleModal">ข้าพเจ้าทำความเข้าใจ รับทราบและยอมรับข้อตกลงที่กำหนด</a>
					</label>
				</div> --}}
				<div id="msg_error" class="alert alert-danger" role="alert" style="display:none"></div>
				<div class="col-12 text-center py-3"><button type="button" onclick='js_register()' class="btn btn-lg btn-primary text-center">REGISTER</button></div>
				
				<input type="hidden" id="baseimage" name="baseimage" value="">
				{!! Form::close() !!}
			</div>
		</div>
		<!-- <hr>
		<div class="col-12 text-center">
			<h1>SEARCH DEALER <span>VIERA</span> </h1>
		</div>
		<div class="col-12 col-sm-6 offset-sm-3">
			<div class="input-group">
				<input type="text" class="form-control form-control-lg" placeholder="ค้นหา dealer">
				<div class="input-group-append">
					<span class="input-group-text"><i class="fas fa-search"></i></span>
				</div>
			</div>
		</div>
		<div class="col-12 pt-5 text-center">
			<img src="{{ url('dist/img/pic.jpg') }}" class="img-fluid" alt="">
			<h5 class="pt-2">Bom Sulasuk</h5>
			<h5>Mobile 098 098 0999</h5>
			<p>Everyday it provides me <br>
				with exactly what I need</p>
		</div> -->
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="remarkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">เงื่อนไข</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				ข้อตกลงการเป็นผู้แทนจำหน่าย วีร่า (ผลิตภัณฑ์เสริมอาหาร) VIERA (Dietary Supplement Product) และผลิตภัณฑ์อื่น ๆ ภายใต้ บริษัท เฮลท์ธอรี่ จำกัด
				<ol>
					<li>ผู้แทนจำหน่ายจะต้องทำสัญญา และลงทะเบียนกับทางบริษัทฯ โดยตรง ซึ่งผู้แทนจำหน่ายต้องระบุข้อมูลที่ถูกต้อง และสามารถตรวจสอบได้ให้ครบถ้วน และปฏิบัติตามสัญญาอย่างเคร่งครัด</li>
					<li>ผลิตภัณฑ์ VIERA แบบกล่อง 10 ซอง ที่จำหน่ายให้แก่ผู้แทนจำหน่ายถือเป็นการขายขาด โดยผู้แทนจำหน่ายไม่สามารถคืนสินค้าได้ไม่ว่ากรณีใด ๆ เว้นแต่สินค้าบกพร่อง หรือเสียหายจากการผลิต (ภายหลังซื้อสินค้าไปไม่เกิน 15 วัน)</li>
					<li>ผู้แทนจำหน่ายจะต้องจัดจำหน่ายสินค้าตามโปรโมชั่นที่บริษัทฯ กำหนดเท่านั้น และไม่อนุญาตให้จำหน่ายสินค้าต่ำกว่าราคาที่ทางบริษัทฯ กำหนด หากผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ นำไปจำหน่ายในราคาต่ำกว่าที่บริษัทฯ กำหนดไว้ ผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ ยินยอมให้ทางบริษัทฯ ปรับเป็นจำนวนเงิน 500,000.00 บาท และบริษัทฯ มีสิทธิ์ยกเลิกการเป็นผู้แทนจำหน่ายโดยทันที</li>
					<li>ผู้แทนจำหน่ายที่ได้รับการอนุมัติการเป็นผู้แทนจำหน่ายจากบริษัทฯ เท่านั้นจึงจะได้รับอนุญาตให้นำภาพ พรีเซ็นเตอร์ แผ่นพับ สื่อโฆษณา ซึ่งบริษัทฯ ได้จัดทำขึ้น เพื่อนำแสดงต่อผู้สนใจ หรือลูกค้าเพื่อส่งเสริมการขาย โดยไม่อนุญาตให้ทำการแก้ไข เปลี่ยนแปลง ดัดแปลง ตัดทอน เพิ่มเติม เช่น รูปภาพ ฉลาก ตัวหนังสือ เครื่องหมายการค้า สัญลักษณ์ หรือสิ่งใด ๆ บนผลิตภัณฑ์สินค้า หรือภาพพรีเซ็นเตอร์ แผ่นพับ สื่อโฆษณาทั้งสิ้น</li>
					<li>ห้ามผู้แทนจำหน่ายโฆษณาผลิตภัณฑ์ หรือแสดงอวดอ้างสรรพคุณของผลิตภัณฑ์เกินจริง อันจะเป็นการขัดต่อกฎหมาย</li>
					<li>ห้ามผู้แทนจำหน่ายนำผลิตภัณฑ์ของบริษัทฯ จัดทำรายการส่งเสริมการขาย หรือร่วมกับสินค้าอื่น ๆ  โดยการแจก แถมหรือให้ทดลอง อันมิใช่การจำหน่ายตามราคา และรายการส่งเสริมการขายที่บริษัทฯ กำหนดโดยเด็ดขาด</li>
					<li>ไม่อนุญาตให้ผู้แทนจำหน่ายนำผลิตภัณฑ์ของบริษัทฯ จำหน่ายสินค้าผ่านช่องทาง LAZADA, SHOPEE หรือเว็บไซด์ E-COMMERCE (การพาณิชย์อิเล็กทรอนิกส์) หรือแอพพลิเคชั่นอื่น ๆ ในราคาที่ต่ำกว่าบริษัทฯ กำหนด กรณีที่ผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ วีร่า (VIERA) และผลิตภัณฑ์อื่น ๆ ของทางบริษัทฯ นำสินค้าไปจำหน่ายต่ำกว่าราคาที่บริษัทฯ กำหนด ทำให้เกิดความเสียหายกับบริษัทฯ และทำให้เสียโครงสร้างราคาสินค้าของบริษัทฯ ทางผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ จะต้องยินยอมให้ทางบริษัทฯ ปรับเป็นจำนวนเงิน 1,000,000.00 บาท และขอสงวนสิทธิ์ในการดำเนินคดีตามกฎหมาย</li>
					<li>กรณีที่ผู้แทนจำหน่ายผิดสัญญา หรือผิดข้อตกลงใด ๆ ถือเป็นการยกเลิกสัญญาทันที และขอสงวนสิทธิ์ในการดำเนินคดีตามกฎหมาย หากผู้แทนจำหน่ายได้กระทำการอันก่อให้เกิดความเสียหายต่อ บริษัท เฮลท์ธอรี่ จำกัด และ/หรือผลิตภัณฑ์อื่น ๆ ภายใต้บริษัทฯ</li>
					<li>บริษัท เฮลท์ธอรี่ จำกัด และ/หรือผลิตภัณฑ์อื่น ๆ ภายใต้บริษัทฯ สามารถเปลี่ยนแปลง แก้ไข เพิ่มเติม สัญญา และ/หรือข้อตกลงได้ โดยจะแจ้งล่วงหน้าแก่ผู้แทนจำหน่ายให้รับทราบก่อนทุกครั้ง เพื่อมิให้กระทบถึงสิทธิ์การเป็นผู้แทนจำหน่าย หรือการจำหน่ายผลิตภัณฑ์ต่าง ๆ ของทางบริษัทฯ</li>
					<li>ผู้แทนจำหน่ายได้อ่าน และทำความเข้าใจข้อตกลงทุกข้อ และยอมรับที่จะปฏิบัติตามข้อตกลงของบริษัทฯ </li>
				</ol>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSubmit" onclick="jsSaveData()"  class="btn btn-primary" data-dismiss="modal">ยอมรับเงื่อนไข</button>
				{{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>				 --}}
			</div>
		</div>
	</div>
</div>

<div id="template_facebook" style="display:none">
	<div id="div_facebook_ITEM_INDEX" class="form-group col-12 d-flex class_div_facebook">
		<input type="text" class="form-control class_facebook" style="height:auto;" name="facebook_page[]" id="facebook_page_ITEM_INDEX" placeholder="facebook page">
		<button type="button" id="btn_add_facebook_ITEM_INDEX" class="btn btn-primary text-center class_add_facebook" onclick="js_addFacebook()">ADD <i class="fas fa-plus-circle"></i></button>
		<button type="button" id="btn_delete_facebook_ITEM_INDEX" class="btn btn-danger text-center class_delete_facebook" style="display: none" onclick="js_removeFacebook('ITEM_INDEX')">DELETE <i class="fas fa-minus-circle"></i></button>
	</div>
</div>

@stop

@section('page_script')
<script type="text/javascript">

	var boolImage = false;
	var len = 1;
	function js_addFacebook()
	{
		//var len = $("#frm_register .class_div_facebook").length+1;
		len = len+1;
		var html_content = $("#template_facebook").html();
    	html_content = html_content.replace(/ITEM_INDEX/g, len);

		//$("#frm_register .class_div_facebook:last").after(html_content);
		$("#frm_register .class_add_facebook").hide();
		$("#frm_register .class_delete_facebook").show();
		$("#div_type").after(html_content);
	}

	function js_removeFacebook(index)
	{
		$("#div_facebook_"+index).remove();
	}


	function js_changeFile(input){
      readURL(input);
    }


    function readURL(input) {
    //   console.log(input["files"]);
    //   console.log(input["files"][0]);
        var input_id = input["id"];
        var split_input_id = input_id.split("-");

        if (input["files"] && input["files"][0]) {
          var reader = new FileReader();

          reader.onload = function(e) {
            //console.log(e.target.result);
            $("#image_profile").attr('src', e.target.result);
			$("#baseimage").val(e.target.result);
          }

          reader.readAsDataURL(input["files"][0]);
        }
    }

	function js_register() {
		$("#msg_error").hide();

		var html = "<ul>";
		var bool = false;
		$("input").removeClass('has-error');
		$("select").removeClass('has-error');
		if ($("#fullname").val() == "" || $("#fullname").val() == "ชื่อ-นามสกุล") {
			html += "<li>กรุณาใส่ชื่อ-นามสกุล</li>";
			bool = true;
			$("#fullname").addClass('has-error');
		}

		if ($("#phone").val() == "" || $("#phone").val() == "เบอร์โทร") {
			html += "<li>กรุณาใส่เบอร์โทร</li>";
			bool = true;
			$("#phone").addClass('has-error');
		}
		else if (isNaN($("#phone").val())) {
			html += "<li>รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง กรูณาใส่ใหม่อีกครั้ง</li>";
			bool = true;
			$("#phone").addClass('has-error');
		}

		// if ($("#email").val() == "" || $("#email").val() == "อีเมล") {
		// 	html += "<li>กรุณาใส่อีเมล</li>";
		// 	bool = true;
		// }
		// else if (!js_checkemail($("#email").val())) {
		if (($("#email").val() != "" && $("#email").val() != "อีเมล") && !js_checkemail($("#email").val())) {
			html += "<li>รูปแบบอีเมลไม่ถูกต้อง กรูณาใส่ใหม่อีกครั้ง</li>";
			bool = true;
		}

		if ($("#lineid").val() == "" || $("#lineid").val() == "Line id") {
			html += "<li>กรุณาใส่ Line id</li>";
			bool = true;
			$("#lineid").addClass('has-error');
		}

		if ($("#refer").val() == "" || $("#refer").val() == "บุคคลแนะนำ") {
			html += "<li>กรุณาใส่บุคคลแนะนำ</li>";
			bool = true;
			$("#refer").addClass('has-error');
		}

		if ($("#sale_place").val() == "" || $("#sale_place").val() == "สถานที่จัดจำหน่าย") {
			html += "<li>กรุณาใส่สถานที่จัดจำหน่าย</li>";
			bool = true;
			$("#sale_place").addClass('has-error');
		}

		if ($("#inputType option:selected").val() == "0") {
			html += "<li>เลือกประเภทตัวแทน</li>";
			bool = true;
			$("#inputType").addClass('has-error');
		}

		if ($("#facebook_page_1").val() == "" || $("#facebook_page_1").val() == "facebook page") {
			html += "<li>กรุณาใส่ facebook page</li>";
			bool = true;
			$("#facebook_page_1").addClass('has-error');
		}
		
		

		// if (!$("#chk_rule").is(":checked")) {
		// 	html += "<li>กรุณายอมรับเงื่อนไข</li>";
		// 	bool = true;
		// }
		html += "</ul>";

		if (bool) {
			$("#msg_error").html(html);
			$("#msg_error").show();
		}
		else {
			if (!submitted)
            {
				var frm_register = $("#frm_register").serialize();
				console.log(frm_register);
				$.ajax({
					url: "{{ url(Session::get('locale').'/register/checkDuplicate') }}",
					type: 'post',
					data: frm_register,
					beforeSend: function() {
                            // setting a timeout
                        submitted=true;
                    },
					success: function (data) {
						if (data.status == "success") {
							var htmlDuplicate = "";
							if (data.duplicate == "fullname")
							{
								htmlDuplicate = "<li>ชื่อ-นามสกุลซ้ำ กรุณาใส่ใหม่อีกครั้ง</li>";
								$("#fullname").addClass('has-error');
							}
							else if (data.duplicate == "phone")
							{
								htmlDuplicate = "<li>เบอร์โทรซ้ำ กรุณาใส่ใหม่อีกครั้ง</li>";
								$("#phone").addClass('has-error');
							}

							if (htmlDuplicate!="")
							{
								$("#msg_error").html(htmlDuplicate);
								$("#msg_error").show();
							}
							else
							{
								$("#remarkModal").modal("show");
							}
						}
						
						submitted=false;
					},
					statusCode: {
						404: function () {
							//alert( "page not found" );
						},
						500: function () {
							//alert( "page not found 2222" );
						}
					}
				});
			}
		}
	}

	var submitted = false;
	function jsSaveData()
	{
		if (!submitted)
            {
				var frm_register = $("#frm_register").serialize();
				$.ajax({
					url: "{{ url(Session::get('locale').'/register/store') }}",
					type: 'post',
					data: frm_register,
					beforeSend: function() {
                            // setting a timeout
                        submitted=true;
                    },
					success: function (data) {
						if (data.status == "success") {
							$("#remarkModal").modal("hide");
							swal({
								title: 'Register',
								text: "Register complete",
								type: 'success',
							}).then((result) => {
								if (result.value) {
									location = "{{ url(Session::get('locale').'/register') }}";
								}
							})
						}
						else if (data.status == "fail") {
							swal({
								title: 'Register',
								text: "Register fail",
								type: 'warning',
							});
						}
						//if (data.status=="success")
						// var objData = $.parseJSON(data);
						// alert(objData.status+"==test");
						submitted=false;
					},
					statusCode: {
						404: function () {
							//alert( "page not found" );
						},
						500: function () {
							//alert( "page not found 2222" );
						}
					}
				});
			}
	}

	$(document).ready(function(){
		// $('#remarkModal').on('hidden.bs.modal', function () {
		// 	// do something…
		// 	location = "{{ url(Session::get('locale').'/register') }}";
		// });

		// $("#frm_register").on("submit",function(){
		// 	js_register();
		// })
	})
</script>
@stop