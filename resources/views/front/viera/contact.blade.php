@extends('layouts.front.viera')

@section('page_title','Contact')

@section('page_style')
@stop

@section('content')
<div class="container-fluid bgContent">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active" aria-current="page"><a href="#">Contact</a></li>
            </ol>
        </nav>
        <div class="row pb-5">
            <div class="col-12 py-4 px-4 text-center">
                <h1 class="py-2 display-2"> <strong> CONTACT <span>VIERA</span> </strong></h1>
            </div>
            <div class="col-12 col-sm-6 py-3 px-4">
                {{-- <h3 class="orangeColor"><strong><span>บริษัท เฮลท์ธอรี่ จำกัด (สำนักงานใหญ่)</span></strong></h3>
                <h6>52/20 ซอยกรุงเทพกรีฑา 15 แขวงสะพานสูง เขตสะพานสูง กรุงเทพมหานคร 10250</h6>
                <h6>โทร. 02 115 4639, แฟ็กซ์. 02 115 4739</h6> --}}
                <hr>
                <h1><strong>SOCIAL <span>MEDIA</span></strong></h1>
                <h4>
                <a href="https://www.facebook.com/vieracollagenofficial" target="_blank" class="px-1">
					<i class="fab fa-facebook-square fa-2x"></i>
				</a>
				<a href="https://www.instagram.com/vieracollagen" target="_blank" class="px-1">
					<i class="fab fa-instagram fa-2x"></i>
				</a>
				<a href="https://twitter.com/vieracollagen" target="_blank" class="px-1">
					<i class="fab fa-twitter-square fa-2x"></i>
				</a>
                <a href="#" class="orangeColor">VIERACOLLAGEN</a>
                <hr>
                <a href="#" class="px-1">
                        <i class="fab fa-line fa-2x"></i>
                    </a>
                    <a href="#" class="px-1"> <span class="fontEng">@</span> VIERACOLLAGEN </a>
                <hr>
                <a href="tel:0984455154"><i class="fas fa-phone-square fa-2x"></i> 098-445-5154</a>
                </h4>
            </div>
            <div class="col-12 col-sm-6 py-3 px-4">
                <h1 class="py-2"> <strong> CONTACT <span>FORM</span> </strong></h1>
                {!! Form::open(['method'=>'POST',"id"=>"frmContact", 'files' => true,'class'=>"form-horizontal"]) !!}
                    <div class="form-group">
                        <input type="text" class="form-control" id="fullname" name="fullname" placeholder="{{trans("auth.Fullname")}}">
                    </div>
                    <div class="row">
                        <div class="form-group col-6">
                            <input type="text" class="form-control" id="phone" name="phone" placeholder="{{trans("auth.Telephone")}}">
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control" id="email" name="email" placeholder="{{trans("auth.Email")}}">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <input type="text" class="form-control" id="subject" name="subject" placeholder="{{trans("auth.Subject")}}">
                        </div>
                        <div class="form-group col-12">
                            <textarea class="form-control" id="msg" name="msg" rows="3" placeholder="{{trans("auth.Message")}}"></textarea>
                        </div>
                    </div>
                    <div id="msg_error" style="display:none"></div>
                    <div class="col-12 text-center py-3"><button type="submit" class="btn btn-lg btn-primary text-center">{{trans("auth.Send")}}</button></div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="remarkModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">เงื่อนไข</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				ข้อตกลงการเป็นผู้แทนจำหน่าย วีร่า (ผลิตภัณฑ์เสริมอาหาร) VIERA (Dietary Supplement Product) และผลิตภัณฑ์อื่น ๆ ภายใต้ บริษัท เฮลท์ธอรี่ จำกัด
				<ol>
					<li>ผู้แทนจำหน่ายจะต้องทำสัญญา และลงทะเบียนกับทางบริษัทฯ โดยตรง ซึ่งผู้แทนจำหน่ายต้องระบุข้อมูลที่ถูกต้อง และสามารถตรวจสอบได้ให้ครบถ้วน และปฏิบัติตามสัญญาอย่างเคร่งครัด</li>
					<li>ผลิตภัณฑ์ VIERA แบบกล่อง 10 ซอง ที่จำหน่ายให้แก่ผู้แทนจำหน่ายถือเป็นการขายขาด โดยผู้แทนจำหน่ายไม่สามารถคืนสินค้าได้ไม่ว่ากรณีใด ๆ เว้นแต่สินค้าบกพร่อง หรือเสียหายจากการผลิต (ภายหลังซื้อสินค้าไปไม่เกิน 15 วัน)</li>
					<li>ผู้แทนจำหน่ายจะต้องจัดจำหน่ายสินค้าตามโปรโมชั่นที่บริษัทฯ กำหนดเท่านั้น และไม่อนุญาตให้จำหน่ายสินค้าต่ำกว่าราคาที่ทางบริษัทฯ กำหนด หากผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ นำไปจำหน่ายในราคาต่ำกว่าที่บริษัทฯ กำหนดไว้ ผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ ยินยอมให้ทางบริษัทฯ ปรับเป็นจำนวนเงิน 500,000.00 บาท และบริษัทฯ มีสิทธิ์ยกเลิกการเป็นผู้แทนจำหน่ายโดยทันที</li>
					<li>ผู้แทนจำหน่ายที่ได้รับการอนุมัติการเป็นผู้แทนจำหน่ายจากบริษัทฯ เท่านั้นจึงจะได้รับอนุญาตให้นำภาพ พรีเซ็นเตอร์ แผ่นพับ สื่อโฆษณา ซึ่งบริษัทฯ ได้จัดทำขึ้น เพื่อนำแสดงต่อผู้สนใจ หรือลูกค้าเพื่อส่งเสริมการขาย โดยไม่อนุญาตให้ทำการแก้ไข เปลี่ยนแปลง ดัดแปลง ตัดทอน เพิ่มเติม เช่น รูปภาพ ฉลาก ตัวหนังสือ เครื่องหมายการค้า สัญลักษณ์ หรือสิ่งใด ๆ บนผลิตภัณฑ์สินค้า หรือภาพพรีเซ็นเตอร์ แผ่นพับ สื่อโฆษณาทั้งสิ้น</li>
					<li>ห้ามผู้แทนจำหน่ายโฆษณาผลิตภัณฑ์ หรือแสดงอวดอ้างสรรพคุณของผลิตภัณฑ์เกินจริง อันจะเป็นการขัดต่อกฎหมาย</li>
					<li>ห้ามผู้แทนจำหน่ายนำผลิตภัณฑ์ของบริษัทฯ จัดทำรายการส่งเสริมการขาย หรือร่วมกับสินค้าอื่น ๆ  โดยการแจก แถมหรือให้ทดลอง อันมิใช่การจำหน่ายตามราคา และรายการส่งเสริมการขายที่บริษัทฯ กำหนดโดยเด็ดขาด</li>
					<li>ไม่อนุญาตให้ผู้แทนจำหน่ายนำผลิตภัณฑ์ของบริษัทฯ จำหน่ายสินค้าผ่านช่องทาง LAZADA, SHOPEE หรือเว็บไซด์ E-COMMERCE (การพาณิชย์อิเล็กทรอนิกส์) หรือแอพพลิเคชั่นอื่น ๆ ในราคาที่ต่ำกว่าบริษัทฯ กำหนด กรณีที่ผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ วีร่า (VIERA) และผลิตภัณฑ์อื่น ๆ ของทางบริษัทฯ นำสินค้าไปจำหน่ายต่ำกว่าราคาที่บริษัทฯ กำหนด ทำให้เกิดความเสียหายกับบริษัทฯ และทำให้เสียโครงสร้างราคาสินค้าของบริษัทฯ ทางผู้แทนจำหน่าย และ/หรือบุคคลอื่นซึ่งได้รับผลิตภัณฑ์ของทางบริษัทฯ จะต้องยินยอมให้ทางบริษัทฯ ปรับเป็นจำนวนเงิน 1,000,000.00 บาท และขอสงวนสิทธิ์ในการดำเนินคดีตามกฎหมาย</li>
					<li>กรณีที่ผู้แทนจำหน่ายผิดสัญญา หรือผิดข้อตกลงใด ๆ ถือเป็นการยกเลิกสัญญาทันที และขอสงวนสิทธิ์ในการดำเนินคดีตามกฎหมาย หากผู้แทนจำหน่ายได้กระทำการอันก่อให้เกิดความเสียหายต่อ บริษัท เฮลท์ธอรี่ จำกัด และ/หรือผลิตภัณฑ์อื่น ๆ ภายใต้บริษัทฯ</li>
					<li>บริษัท เฮลท์ธอรี่ จำกัด และ/หรือผลิตภัณฑ์อื่น ๆ ภายใต้บริษัทฯ สามารถเปลี่ยนแปลง แก้ไข เพิ่มเติม สัญญา และ/หรือข้อตกลงได้ โดยจะแจ้งล่วงหน้าแก่ผู้แทนจำหน่ายให้รับทราบก่อนทุกครั้ง เพื่อมิให้กระทบถึงสิทธิ์การเป็นผู้แทนจำหน่าย หรือการจำหน่ายผลิตภัณฑ์ต่าง ๆ ของทางบริษัทฯ</li>
					<li>ผู้แทนจำหน่ายได้อ่าน และทำความเข้าใจข้อตกลงทุกข้อ และยอมรับที่จะปฏิบัติตามข้อตกลงของบริษัทฯ </li>
				</ol>
			</div>
			<div class="modal-footer">
				<button type="button" id="btnSubmit" onclick="jsSaveData()"  class="btn btn-primary" data-dismiss="modal">ยอมรับเงื่อนไข</button>
				{{-- <button type="button" class="btn btn-secondary" data-dismiss="modal">ปิด</button>				 --}}
			</div>
		</div>
	</div>
</div>

@stop

@section('page_script')

{!! Html::script('js/functionjs.js') !!}
<script type="text/javascript">
    var submitted = false;
    $(document).ready(function(){

        $("#frmContact").on("submit",function(e)
        {
            $("#msg_error").hide();

            var html = "<ul>";
            var bool = false;
            $("input").removeClass('has-error');
            $("select").removeClass('has-error');
            if ($("#fullname").val() == "" || $("#fullname").val() == "ชื่อ-นามสกุล") {
                html += "<li>กรุณาใส่ชื่อ-นามสกุล</li>";
                bool = true;
                $("#fullname").addClass('has-error');
            }

            if ($("#phone").val() == "" || $("#phone").val() == "เบอร์โทร") {
                html += "<li>กรุณาใส่เบอร์โทร</li>";
                bool = true;
                $("#phone").addClass('has-error');
            }
            else if (isNaN($("#phone").val())) {
                html += "<li>รูปแบบเบอร์โทรศัพท์ไม่ถูกต้อง กรูณาใส่ใหม่อีกครั้ง</li>";
                bool = true;
                $("#phone").addClass('has-error');
            }

            if ($("#email").val()=="")
            {
                html += "<li>กรุณาใส่อีเมล</li>";
                bool = true;
            }
            else if (($("#email").val() != "" && $("#email").val() != "อีเมล") && !js_checkemail($("#email").val())) {
                html += "<li>รูปแบบอีเมลไม่ถูกต้อง กรูณาใส่ใหม่อีกครั้ง</li>";
                bool = true;
            }

            if ($("#subject").val()=="")
            {
                html += "<li>กรุณาใส่หัวข้อ</li>";
                bool = true;
            }

            if ($("#subject").val()=="")
            {
                html += "<li>กรุณาใส่หัวข้อ</li>";
                bool = true;
            }

            if ($("#msg").val()=="")
            {
                html += "<li>กรุณาใส่เนื้อหา</li>";
                bool = true;
            }
            
            html += "</ul>";

            if (bool) {
                $("#msg_error").html(html);
                $("#msg_error").show();
            }
            else {
                if (!submitted)
                {
                    var frmContact = $("#frmContact").serialize();
                    $.ajax({
                        url: "{{ url(Session::get('locale').'/contact/sendmail') }}",
                        type: 'post',
                        data: frmContact,
                        beforeSend: function() {
                                // setting a timeout
                            submitted=true;
                        },
                        success: function (data) {
                            if (data.success) {
                                swal({
                                    title: 'Contact',
                                    text: "Contact complete",
                                    type: 'success',
                                }).then((result) => {
                                    if (result.value) {
                                        location = "{{ url(Session::get('locale').'/contact') }}";
                                    }
                                });
                            }
                            
                            submitted=false;
                        },
                        statusCode: {
                            404: function () {
                                //alert( "page not found" );
                            },
                            502: function () {
                                //alert( "page not found 2222" );
                                swal({
                                    title: 'Contact',
                                    text: "Contact error",
                                    type: 'error',
                                });
                            }
                        }
                    });
                }
            }

            return false;
        })
    })
</script>
@stop