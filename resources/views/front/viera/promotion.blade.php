@extends('layouts.front.viera')

@section('page_title','Promotion')

@section('page_style')
@stop

@section('content')
<div class="container">
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="{{Route('home',['lang'=>Request()->lang])}}">Home</a></li>
            <li class="breadcrumb-item active" aria-current="page"><a
                    href="{{Route('promotions',['lang'=>Request()->lang])}}">Promotion</a></li>
        </ol>
    </nav>
    <div class="row pb-5">
        {{-- <div class="col-12 py-4 px-4 text-center">
            <h1 class="py-2 display-3"> <strong> PROMOTION <span>VIERA</span> </strong></h1>            
            <div class="promotion-slide">
                <div><img src="{{ url('dist/img/promotion-01.jpg') }}" class="img-fluid w-100" alt=""></div>
</div>
</div> --}}
@if (isset($promotions))
@foreach ($promotions as $promotion)
<div class="col-12 col-sm-6 py-3 px-4">
    <a href="{{ Route('promotion-detail',['lang'=>Request()->lang,'id'=>$promotion->id]) }}" class="blacklink">
        {{-- <img src="{{ url(config('config.config_pathUpload').'/homePromotions/'.$promotion->pathFile.'/'.$promotion->thumb) }}"
        class="img-fluid w-100 pb-3" alt=""> --}}
        @if (isset($promotion) &&
        file_exists(config('config.config_pathUpload').'/promotions/'.$promotion->pathFile.'/'.$promotion->thumb))
        <img src="{{ url(config('config.config_pathUpload').'/promotions/'.$promotion->pathFile.'/'.$promotion->thumb) }}"
            class="img-fluid w-100 pb-3" alt="">
        @endif
        <h4>{{$promotion->title}}</h4>
        <h6>{{$promotion->detail}}</h6>
    </a>
</div>
@endforeach
@endif
{{-- <div class="col-12 col-sm-6 py-3 px-4">
            <a href="/viera/promotion/details" class="blacklink">
                <img src="{{ url('dist/img/tmb-promotion-01.jpg') }}" class="img-fluid w-100 pb-3" alt="">
<h4>Content Discount</h4>
<h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h6>
</a>
</div>
<div class="col-12 col-sm-6 py-3 px-4">
    <a href="/viera/promotion/details" class="blacklink">
        <img src="{{ url('dist/img/tmb-promotion-02.jpg') }}" class="img-fluid w-100 pb-3" alt="">
        <h4>Content Discount</h4>
        <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h6>
    </a>
</div>
<div class="col-12 col-sm-6 py-3 px-4">
    <a href="/viera/promotion/details" class="blacklink">
        <img src="{{ url('dist/img/tmb-promotion-03.jpg') }}" class="img-fluid w-100 pb-3" alt="">
        <h4>Content Discount</h4>
        <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h6>
    </a>
</div>
<div class="col-12 col-sm-6 py-3 px-4">
    <a href="/viera/promotion/details" class="blacklink">
        <img src="{{ url('dist/img/tmb-promotion-04.jpg') }}" class="img-fluid w-100 pb-3" alt="">
        <h4>Content Discount</h4>
        <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h6>
    </a>
</div> --}}
</div>
</div>

@stop

@section('page_script')
@stop