@extends('layouts.front.viera')

@section('page_title','Home')

@section('page_style')
@stop

@section('content')

<div class="container-fluid" style="padding:0;">
	<div class="row no-gutters">
	<div class="col-12">
		{{-- @if (isset($banner) && file_exists(config('config.config_pathUpload').'/homeBanners/'.$banner->pathFile.'/'.$banner->thumb))
			<a href="{{$banner->url}}"><img src="{{ url(config('config.config_pathUpload').'/homeBanners/'.$banner->pathFile.'/'.$banner->thumb) }}" class="img-fluid" alt="{{$banner->title}}"></a>
		@else
			<img src="{{ url('dist/img/banner-01.jpg') }}" class="img-fluid" alt="">
		@endif --}}
		{{-- <img src="{{ url('dist/img/banner-01.jpg') }}" class="img-fluid w-100" alt=""> --}}
		@if (isset($banners) && sizeof($banners)>0)
			<div class="banner-slide">
			@foreach ($banners as $banner)
				@if (isset($banner) && file_exists(config('config.config_pathUpload').'/homeBanners/'.$banner->pathFile.'/'.$banner->thumb))
					<a href="{{$banner->url}}"><img src="{{ url(config('config.config_pathUpload').'/homeBanners/'.$banner->pathFile.'/'.$banner->thumb) }}" class="img-fluid" alt="{{$banner->title}}"></a>
				@endif
			@endforeach
			</div>
		@endif
		{{-- <div class="banner-slide">
			<div><a href="#"> <img src="{{ url('dist/img/banner-01.jpg') }}" class="img-fluid" alt=""></a></div>
			<div><a href="#"> <img src="{{ url('dist/img/banner-01.jpg') }}" class="img-fluid" alt=""></a></div>
		</div> --}}
	</div>
</div>
	<div class="container">
		<div class="row">
			<div class="col-12 py-5 px-4">
				<div class="row">
					<div class="col-12 col-sm-6">
						<h1 class="orangeColo"><strong>PROMOTION</strong></h1>
					</div>
					<div class="col-12 col-sm-6 text-sm-right">
						<button type="button" class="btn btn-lg btn-outline-primary" onclick="location.href='{{ url(Request()->lang.'/promotion') }}';">VIEW ALL ></button>
					</div>
				</div>
			</div>
			<div class="col-12 offset-0 col-sm-10 offset-sm-1 px-4 text-center">
				{{-- @if (isset($promotion) && sizeof($promotion)>0 && file_exists(config('config.config_pathUpload').'/homePromotions/'.$promotion->pathFile.'/'.$promotion->thumb))
					<img src="{{ url(config('config.config_pathUpload').'/homePromotions/'.$promotion->pathFile.'/'.$promotion->thumb) }}" class="img-fluid w-100" alt="{{$promotion->title_th}}">
				@else
					<img src="{{ url('dist/img/promotion-03.jpg') }}" class="img-fluid w-100" alt="">
				@endif --}}
				@if (isset($promotions))
					<div class="promotion-slide">
					@foreach ($promotions as $promotion)
						<div class="px-3"><a href="{{$promotion->url}}"><img src="{{ url(config('config.config_pathUpload').'/homePromotions/'.$promotion->pathFile.'/'.$promotion->thumb) }}" class="img-fluid" alt="{{$promotion->title}}"></a></div>
					@endforeach
					</div>
				@endif
				
					{{-- <div><a href="/promotion"><img src="{{ url('dist/img/promotion-03.jpg') }}" class="img-fluid" alt=""></a></div>
					<div><a href="/promotion"><img src="{{ url('dist/img/promotion-03.jpg') }}" class="img-fluid" alt=""></a></div> --}}
				
			</div>
		</div>
	</div>

	<div class="container">
		<div class="row">
			<div class="col-12 px-4 bgReview">
				<div class="row">
					<div class="col-12 col-sm-3 py-5 py-sm-0">
						<h1 class="orangeColor"><strong>REVIEW</strong></h1>
						<h4>Testimonials Content</h4>
						<!-- <h6 class="pb-2">Lorem Ipsum is simply dummy text of the printing and typesetting industry. </h6> -->
						{{-- <button type="button" class="btn btn-outline-primary" onclick="location.href='/review';">VIEW ALL ></button> --}}
					</div>

					
					<div class="review-slide col-9 text-center">
								@if (isset($reviews))
								@foreach ($reviews as $review)
									@if (isset($review) &&
									file_exists(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb))
									<div class="mx-2">
										<a href="{{ Route('review-detail',['lang'=>Request()->lang,'id'=>$review->id]) }}">
											<img src="{{ url(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb) }}"
												class="img-fluid pb-2" alt="">
										</a>
									</div>
									@endif
								@endforeach
							@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

@stop

@section('page_script')
@stop
