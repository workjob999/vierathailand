@extends('layouts.front.viera')

@section('page_title','Review')

@section('page_style')
@stop

@section('content')
<div class="container-fluid bgContent">
	<div class="container">
		<nav aria-label="breadcrumb">
			<ol class="breadcrumb">
				<li class="breadcrumb-item">
					<a href="#">Home</a>
				</li>
				<li class="breadcrumb-item active" aria-current="page">
					<a href="#">Review</a>
				</li>
			</ol>
		</nav>
		<div class="row pb-5">
			<div class="col-12 py-4 px-4 text-center">
				<h1 class="py-2 pb-sm-5 display-3">
					<strong> REVIEW <span>MEDIA</span></strong>
				</h1>
				<div class="review-slide py-3 py-sm-5">
					@if (isset($reviews))
						@foreach ($reviews as $review)
							@if (isset($review) &&
							file_exists(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb))
							<div class="px-5">
								<a href="{{ Route('review-detail',['lang'=>Request()->lang,'id'=>$review->id]) }}">
									<img src="{{ url(config('config.config_pathUpload').'/reviews/'.$review->pathFile.'/'.$review->thumb) }}"
										class="img-fluid pb-2" alt="">
								</a>
							</div>
							@endif
						@endforeach
					@endif
					{{-- <div class="px-5">
						<a href="{{ url('dist/img/review-05.jpg') }}" data-fancybox="gallery"><img
						src="{{ url('dist/img/review-05.jpg') }}" class="img-fluid pb-2" alt=""></a>
				</div>
				<div class="px-5">
					<a href="{{ url('dist/img/review-03.jpg') }}" data-fancybox="gallery"><img
							src="{{ url('dist/img/review-03.jpg') }}" class="img-fluid pb-2" alt=""></a>
				</div>
				<div class="px-5">
					<a href="{{ url('dist/img/review-02.jpg') }}" data-fancybox="gallery"><img
							src="{{ url('dist/img/review-02.jpg') }}" class="img-fluid pb-2" alt=""></a>
				</div> --}}
			</div>
		</div>

	</div>
</div>
</div>
<div class="container-fluid bgWhite py-3 ">
	<div class="container">
		<div class="row py-4 px-2 p-sm-5">
			<div class="col-12 col-sm-6">
				<h5>Viera Collagen สอบถามข้อมูลเพิ่มเติม ศูนย์สุขภาพ</h5>
				<h4><a href="#"><i class="fab fa-line fa-2x"></i> : <span class="fontEng">@</span> vieracollagen</a></h4>
				<h4><a href="tel:02115463"><i class="fas fa-phone-volume fa-2x"></i> : 02-1154639</a></h4>
				<!-- <h6>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's
					standard
					dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type
					specimen
					book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining
					essentially
					unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages,
					and
					more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</h6> -->
			</div>
			<div class="col-12 col-sm-6">
				<div class="embed-responsive embed-responsive-16by9">
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/uFCgtu_AZoo"
						frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
						allowfullscreen></iframe>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page_script')
@stop