<html>

<body>

    <table border="1">
        <tbody>
            <tr>
                <th>Date</th>
                <th>รูป</th>
                <th>Code</th>
                <th>ชื่อ - นามสกุล</th>
                <th>เบอร์โทร</th>
                <th>อีเมล์</th>
                <th>Line</th>
                <th>บุคคลแนะนำ</th>
                <th>สถานที่จัดจำหน่าย</th>
                <th>Type</th>
                <th>Facebook</th>
                <th>Status</th>
            </tr>
            @foreach($registers as $register)
            <tr>
                <td>{!! \Carbon\Carbon::parse($register->created_at)->format("d/m/Y H:i:s") !!}</td>
                <td>{{ url(config('config.config_pathUpload').'/registers/'.$register->pathFile.'/'.$register->thumb) }}</td>
                <td>{!! $register->code !!}</td>
                <td>{!! $register->fullname !!}</td>
                <td>{!! $register->phone !!}</td>
                <td>{!! $register->email !!}</td>
                <td>{!! $register->lineid !!}</td>
                <td>{!! $register->refer !!}</td>
                <td>{!! $register->sale_place !!}</td>
                <td>{!! @config("config.config_arr_type")[$register->type] !!}</td>
                <td>{!! $register->facebook !!}</td>
                <td>{!! config("config.config_arr_status")[$register->status] !!}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>