@extends('layouts.front.main')
@section('page_title', "403 Page")

@section('page_style')
@stop

@section('default_config_js')
  var config_url_page = "";
@stop


@section('content')
  <section class="section section-body">
        <div class="container">
    <div class="row">
      <div class="logo-wrap">
          <a href="01-1-home.html">
            <span class="glyphicon glyphicon-info-sign"></span>
          </a>
      </div>
    </div>
    <div class="row">
        <div class="col-md-16">
            <div class="error-template">

                
<h2>The Error 403 "Page not found"</h2>

                <div class="error-actions">
                    <a href="/" class="btn btn-homes btn-lg"><span class="glyphicon glyphicon-home"></span>
                        Home Page</a>
                    <a href="06-contact.html" class="btn btn-homes btn-lg"><span class="glyphicon glyphicon-envelope"></span> Contact Us </a>
                </div>
            </div>
        </div>
    </div>
</div>       
    </section>
@stop

@section('page_script')

@stop