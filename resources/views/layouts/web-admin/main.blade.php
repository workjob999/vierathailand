<!DOCTYPE html>

<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->

<!--[if !IE]><!-->

<html lang="en">

<!--<![endif]-->

<head>

  <meta charset="utf-8" />

  <title>@yield('page_title') | {{ config('config.company_name') }}</title>

  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />

  <meta content="" name="description" />

  <meta content="" name="author" />

  

  <!-- ================== BEGIN BASE CSS STYLE ================== -->

  <link href="http://fonts.googleapis.com/css?family=Nunito:400,300,700" rel="stylesheet" id="fontFamilySrc" />

  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet" />

  {!! Html::style('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') !!}

  {!! Html::style('assets/plugins/bootstrap/css/bootstrap.min.css') !!}

  {{-- {!! Html::style('assets/plugins/font-awesome/css/font-awesome.min.css') !!} --}}
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">

  {!! Html::style('assets/plugins/sweetalert2/dist/sweetalert2.min.css') !!}

  {!! Html::style('assets/css/animate.min.css') !!}

  {!! Html::style('assets/css/style.css') !!}

  <!-- ================== END BASE CSS STYLE ================== -->

  

  <!-- ================== BEGIN PAGE LEVEL CSS STYLE ================== -->

  {!! Html::style('assets/plugins/bootstrap-calendar/css/bootstrap_calendar.css') !!}

  {!! Html::style('assets/plugins/switchery/switchery.min.css') !!}

  {!! Html::style('css/style-web-admin.css') !!}

  <!-- ================== END PAGE LEVEL CSS STYLE ================== -->

    

  <!-- ================== BEGIN BASE JS ================== -->

  {!! Html::script('assets/plugins/jquery/jquery-1.9.1.min.js') !!}

  {!! Html::script('assets/plugins/pace/pace.min.js') !!}

  <!-- ================== END BASE JS ================== -->

  

  <!--[if lt IE 9]>

      {!! Html::script('assets/crossbrowserjs/excanvas.min.js') !!}

  <![endif]-->



  @yield('page_style')

</head>

<body>

  <!-- begin #page-loader -->

  <div id="page-loader" class="page-loader fade in"><span class="spinner">Loading...</span></div>

  <!-- end #page-loader -->



  <!-- begin #page-container -->

  <div id="page-container" class="fade page-container page-header-fixed page-sidebar-fixed page-with-two-sidebar page-with-footer">

    <!-- begin #header -->

    <div id="header" class="header navbar navbar-primary navbar-fixed-top">

      @include('layouts.web-admin.header')

    </div>

    <!-- end #header -->

    

    <!-- begin #sidebar -->

    <div id="sidebar" class="sidebar sidebar-light sidebar-highlight-light">

      @include('layouts.web-admin.leftmenu')

    </div>

    <div class="sidebar-bg"></div>

    <!-- end #sidebar -->

    

    <!-- begin #content -->

    <div id="content" class="content">

      <!-- begin breadcrumb -->

      <ol class="breadcrumb pull-right">

        <li><a href="{{url(config('config.config_pathCms'))}}">Home</a></li>

        @yield('page_nav')

      </ol>

      <!-- end breadcrumb -->



      <!-- begin page-header -->

      <h1 class="page-header">@yield('page_title') <small>@yield('page_action')</small></h1>

      <!-- end page-header -->

      

      <!-- Main content -->

      @yield('content')

      <!-- /.content -->

      

      <!-- begin #footer -->

      <div id="footer" class="footer">

        @include('layouts.web-admin.footer')

      </div>

      <!-- end #footer -->

    </div>

    <!-- end #content -->

    

    <!-- begin #sidebar-right -->

    <!-- end #sidebar-right -->

  </div>

  <!-- end page container -->





  @include('layouts.web-admin.modal')

  

    <!-- begin theme-panel -->

    <!-- end theme-panel -->

  

  <!-- ================== BEGIN BASE JS ================== -->

  {!! Html::script('assets/plugins/jquery/jquery-migrate-1.1.0.min.js') !!}

  {!! Html::script('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js') !!}

  {!! Html::script('assets/plugins/bootstrap/js/bootstrap.min.js') !!}  

  <!--[if lt IE 9]>

    {!! Html::script('assets/crossbrowserjs/html5shiv.js') !!}

    {!! Html::script('assets/crossbrowserjs/respond.min.js') !!}

  <![endif]-->

  {!! Html::script('assets/plugins/slimscroll/jquery.slimscroll.min.js') !!}

  {!! Html::script('assets/plugins/jquery-cookie/jquery.cookie.js') !!}

  {!! Html::script('assets/plugins/switchery/switchery.min.js') !!}

  {!! Html::script('assets/plugins/sweetalert2/dist/sweetalert2.all.min.js') !!}

  <!-- ================== END BASE JS ================== -->

  

  <!-- ================== BEGIN PAGE LEVEL JS ================== -->

  {!! Html::script('assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js') !!}

  {!! Html::script('assets/js/page-form-slider-switcher.demo.min.js') !!}

  {!! Html::script('assets/js/demo.min.js') !!}

  {!! Html::script('assets/js/apps.min.js') !!}

  {!! Html::script('js/functionjs.js') !!}

  <!-- ================== END PAGE LEVEL JS ================== -->





  @include('layouts.web-admin.functionjs')



  @yield('page_script')

  

  <script>

    $(document).ready(function() {

        App.init();

        Demo.init();

        PageDemo.init();

    });

  </script>



  <script>

    $(function(){

        @if (isset($_GET["rs"]))

          @if ($_GET["rs"]=="success")

              js_slideMSGSuccess();

          @elseif ($_GET["rs"]=="error")

              js_slideMSGError();

          @endif

        @endif

    })

  </script>

</body>

</html>

