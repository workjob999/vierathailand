<div class="alert alert-success alert-dismissible" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-right:15px;margin-top:10px;">×</button>
    <h4><i class="icon fa fa-check"></i>Everything has been saved succesfully</h4>
</div>

<div class="alert alert-danger alert-dismissible" style="display:none;">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true" style="margin-right:15px;margin-top:10px;">×</button>
    <h4><i class="icon fa fa-ban"></i>Error information ! Please re-enter or contact the administrator.</h4>
</div>
