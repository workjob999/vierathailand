<!-- begin sidebar scrollbar -->

      <div data-scrollbar="true" data-height="100%">

        <!-- begin sidebar nav -->

        <ul class="nav">

            <li class="nav-user">

                <div class="image">

                    <img src="{{url('assets/img/user_profile.jpg')}}" alt="" />

                </div>

                <div class="info">

                    <div class="name dropdown">

                      <a href="javascript:;" data-toggle="dropdown">{{ Auth::user()->name }} <b class="caret"></b></a>

                    </div>

                </div>

            </li>

          <li class="nav-header">Navigation</li>


          @if (sizeof(config('menu.menu_item_list'))>0)

            @foreach(config('menu.menu_item_list') as $menu)

                @if ($hasPermission[$menu['permission']])

                  @if (isset($menu['items']) && sizeof($menu['items'])>0)

                    <li class="has-sub @yield('menu_active_'.$menu['active'])">

                      <a href="javascript:;">

                        <b class="caret pull-right"></b>

                        <i class="{{$menu['icon']}}"></i>

                        <span>{{$menu['label']}}</span>

                      </a>

                      <ul class="sub-menu">

                        @foreach ($menu["items"] as $submenu)

                          @if ($hasPermission[$submenu['permission']])

                            <li class="@yield('submenu_active_'.$submenu['active'])">

                              <a href="{{url(config('config.config_pathCms').'/'.$submenu['link'])}}">

                                <span>{{$submenu['label']}}</span>

                              </a>

                            </li>

                          @endif

                        @endforeach

                      </ul>

                    </li>

                  @else

                    @if ($hasPermission[$menu['permission']])

                      <li class="@yield('menu_active_'.$menu['active'])">

                        <a href="{{url(config('config.config_pathCms').'/'.$menu['link'])}}">

                          <i class="{{$menu['icon']}}"></i>

                          <span>{{$menu['label']}}</span>

                        </a>

                      </li>

                    @endif

                  @endif

                @endif

            </li>

            @endforeach

          @endif

        </ul>

        <!-- end sidebar nav -->

      </div>

      <!-- end sidebar scrollbar -->