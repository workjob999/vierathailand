<script type="text/javascript">

  function js_slideMSGSuccess()

  {

    if ($("div").is(".alert-success"))

    {

        $(".alert-success").slideDown("slow");

        $(".alert-success").delay(3000).slideUp("slow");

    }

  }



  function js_slideMSGError()

  {

    if ($("div").is(".alert-danger"))

    {

        $(".alert-danger").slideDown("slow");

        $(".alert-danger").delay(3000).slideUp("slow");

    }

  }



  function js_chkAll()

  {

      if ($("#chk_all").is(":checked"))

      {

        $("#chk_all").prop({

          checked: true

        })



        $(".class_chk:not(.chk_disabled)").prop({

          checked: true

        })

      }

      else

      {

        $("#chk_all").prop({

          checked: false

        })



        $(".class_chk").prop({

          checked: false

        })

      }

  }



  function js_chkOther()

  {

      var bool = false;

      $(".class_chk:not(.chk_disabled)").each(function(index, el) {

        if (!$(".class_chk:not(.chk_disabled):eq("+index+")").is(":checked"))

          bool = true;

      });

      if (bool)

      {

        $("#chk_all").prop({

          checked: false

        });

      }

      else

      {

        $("#chk_all").prop({

          checked: true

        });

      }

  }





  /*limit page*/

  function js_changeLimitPage(Table,value)

  {

    $.ajax({

        type: "POST",

        url : "{{ url(config('config.config_pathCms').'/processdata/changeLimitPage') }}",

        data : "cmd=changeLimitPage&table="+Table+"&limit_page="+value+"&_token={!! csrf_token() !!}",

        success : function(data){

            location.reload();

        },

        error: function(data){

          js_slideMSGError();

        }

      });

  }

  /*limit page*/





  /*change status*/

    function js_changeStatus(Table,Id,Status)

    {

        $('#modal-changeStatus').modal('show');

        //$('#modal-changeStatus .modal-title').html("Warning !");

        $('#btn_changeStatusModal').attr("onclick","js_changeStatusSendData('"+Table+"','"+Id+"','"+Status+"')");

    }



    function js_changeStatusSendData(Table,Id,Status)

    {

      $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/changeStatus') }}",

          data : "cmd=changeStatus&table="+Table+"&id="+Id+"&status="+Status+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data!="F")

              {

                  $('#modal-changeStatus').modal('hide');



                  $("html, body").animate({ scrollTop: 0 }, "slow");



                  if (data=="Y")

                  {

                      $("#btn_statusDataActive-"+Id).removeClass("hide");

                      $("#btn_statusDataInactive-"+Id).addClass("hide");

                  }

                  else if (data=="N")

                  {

                      $("#btn_statusDataActive-"+Id).addClass("hide");

                      $("#btn_statusDataInactive-"+Id).removeClass("hide");

                  }



                  setTimeout(function(){

                      js_slideMSGSuccess();

                  },500);

              }

          },

          error: function(data){

            //alert("test2");

            js_slideMSGError();

          }

        });

    }

  /*change status*/





  /*remove thumb*/

    function js_removeThumb(Table,Id,Thumb,Lang)

    {

        $('#modal-deleteImage').modal('show');

        $('#btn_deleteImageModal').attr("onclick","js_deleteThumbSendData('"+Table+"','"+Id+"','"+Thumb+"','"+Lang+"')");

    }



    function js_deleteThumbSendData(Table,Id,Thumb,Lang)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteThumb') }}",

          data : "cmd=deleteThumb&table="+Table+"&id="+Id+"&thumb="+Thumb+"&lang="+Lang+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                  location.reload();

              }

              else

              {

                  $('#modal-deleteImage').modal('hide');

                  $('#modal-deleteDataErrorRefer').modal('show');

              }

          },

          error: function(data){

            //alert("test2");

          }

        });

    }

  /*remove thumb*/





  /*delete banner*/

    function js_removeBanner(Table,Id)

    {

        $('#modal-deleteImage').modal('show');

        $('#btn_deleteImageModal').attr("onclick","js_deleteBannerSendData('"+Table+"','"+Id+"')");

    }



    function js_deleteBannerSendData(Table,Id)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteBanner') }}",

          data : "cmd=deleteBanner&table="+Table+"&id="+Id+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                location.reload();

              }

              else

              {

                  $('#modal-deleteImage').modal('hide');

                  $('#modal-deleteDataErrorRefer').modal('show');

              }

          },

          error: function(data){

            //alert("test2");

          }

        });

    }

  /*delete banner*/





  /*delete banner multi*/

    function js_removeBanner_multi(Table,Id,Banner_field,Filename)

    {

        $('#modal-deleteImage').modal('show');

        $('#btn_deleteImageModal').attr("onclick","js_deleteBannerSendData_multi('"+Table+"','"+Id+"','"+Banner_field+"','"+Filename+"')");

    }



    function js_deleteBannerSendData_multi(Table,Id,Banner_field,Filename)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteBannerMulti') }}",

          data : "cmd=deleteBannerMulti&table="+Table+"&id="+Id+"&banner_field="+Banner_field+"&filename="+Filename+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                location.reload();

              }

              else

              {

                  $('#modal-deleteImage').modal('hide');

                  $('#modal-deleteDataErrorRefer').modal('show');

              }

          },

          error: function(data){

            //alert("test2");

          }

        });

    }

  /*delete banner*/





  /*delete all*/

    function js_deleteContentAll(Table)

    {

        var html_value = "";

        $(".class_chk:not(.chk_disabled)").each(function(index, el) {

            if ($(".class_chk:not(.chk_disabled):eq("+index+")").is(":checked"))

            {

                if (html_value!="")

                    html_value += "|$|";

                html_value += $(".class_chk:not(.chk_disabled):eq("+index+")").val();

            }

        });



        if (html_value!="")

        {

            $('#modal-deleteData').modal('show');

            $('#btn_deleteDataModal').attr("onclick","js_deleteContentAllSendData('"+Table+"','"+html_value+"')");

        }

    }



    function js_deleteContentAllSendData(Table,html_value)

    {

      $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteAllData') }}",

          data : "cmd=deleteAllData&table="+Table+"&html_value="+html_value+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                  var split_html = html_value.split("|$|");

                  for (i = 0; i < split_html.length; i++) {

                      $("tbody tr[rel='tr_"+split_html[i]+"']").remove();

                  }



                  $('#modal-deleteData').modal('hide');



                  $("html, body").animate({ scrollTop: 0 }, "slow");



                  $("#chk_all").prop({

                        "checked": false

                      });



                  setTimeout(function(){

                      js_slideMSGSuccess();

                  },500);

              }

          },

          error: function(data){

            //alert("test2");

            js_slideMSGError();

          }

        });

    }

  /*delete all*/







  /*delete album all*/

    function js_deleteContentAlbumAll(Table)

    {

        var html_value = "";

        $(".class_chk:not(.chk_disabled)").each(function(index, el) {

            if ($(".class_chk:not(.chk_disabled):eq("+index+")").is(":checked"))

            {

                if (html_value!="")

                    html_value += "|$|";

                html_value += $(".class_chk:not(.chk_disabled):eq("+index+")").val();

            }

        });



        if (html_value!="")

        {

            $('#modal-deleteData').modal('show');

            $('#btn_deleteDataModal').attr("onclick","js_deleteContentAlbumAllSendData('"+Table+"','"+html_value+"')");

        }

    }



    function js_deleteContentAlbumAllSendData(Table,html_value)

    {

      $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteAlbumAllData') }}",

          data : "cmd=deleteAlbumAllData&table="+Table+"&html_value="+html_value+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                  var split_html = html_value.split("|$|");

                  for (i = 0; i < split_html.length; i++) {

                      $("tbody tr[rel='tr_"+split_html[i]+"']").remove();

                  }



                  $('#modal-deleteData').modal('hide');



                  $("html, body").animate({ scrollTop: 0 }, "slow");



                  $("#chk_all").prop({

                        "checked": false

                      });



                  setTimeout(function(){

                      js_slideMSGSuccess();

                  },500);

              }

          },

          error: function(data){

            //alert("test2");

            js_slideMSGError();

          }

        });

    }

  /*delete all*/





  /*delete content*/

    function js_deleteContent(Table,Id)

    {

        $('#modal-deleteData').modal('show');

        $('#btn_deleteDataModal').attr("onclick","js_deleteContentSendData('"+Table+"','"+Id+"')");

    }



    function js_deleteContentSendData(Table,Id)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms')) }}/"+Table+"/"+Id,

          data : "_method=delete&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                location.reload();

              }

              else if (data=="F")

              {

                  $('#modal-deleteData').modal('hide');

                  $('#modal-deleteDataErrorRefer').modal('show');

              }

          },

          error: function(data){

            //alert("test2");

          }

        });

    }





    /*delete sub content*/

    function js_deleteContentSub(Table,Id)
    {
        $('#modal-deleteData').modal('show');
        $('#btn_deleteDataModal').attr("onclick","js_deleteContentSubSendData('"+Table+"','"+Id+"')");
    }



    function js_deleteContentSubSendData(Table,Id)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms')) }}/"+Table+"/deleteSubContent/"+Id,

          data : "cms=deleteSubContent&_token={!! csrf_token() !!}",

          success : function(data){
              if (data=="Y")
              {
                //location.reload();
                location = "{!!url(config('config.config_pathCms').'/"+Table+"?rs=success')!!}"
              }

              else if (data=="F")

              {

                  $('#modal-deleteData').modal('hide');

                  $('#modal-deleteDataHasActive').modal('show');

              }

          },

          error: function(data){

            //alert("test2");

            js_slideMSGError();

          }

        });

    }

    /*delete sub content*/





    /*delete allsub content*/

    function js_deleteAllSubContent(Table)

    {

        $('#modal-deleteData').modal('show');

        $('#btn_deleteDataModal').attr("onclick","js_deleteAllSubContentData('"+Table+"')");

    }



    function js_deleteAllSubContentData(Table)

    {

        var Refer_id = "";

        $(".class_chk:not(.chk_disabled)").each(function(index, el) {

          if ($(".class_chk:not(.chk_disabled):eq("+index+")").is(":checked"))

          {

            if (Refer_id!="")

              Refer_id += "|$|";

            Refer_id += $(".class_chk:not(.chk_disabled):eq("+index+")").val();

          }

        });



        $.ajax({

          type: "POST",

          url : "{{url(config('config.config_pathCms'))}}/"+Table+"/deleteAllSubContent",

          data : "cmd=deleteAllSubContent&refer_id="+Refer_id+"&_token={!! csrf_token() !!}",

          success : function(data){

            //alert(data);

            if (data=="Y")

            {

                location.reload();

            }

            else

            {

                $('#modal-deleteData').modal('hide');

                $('#modal-deleteDataHasActive').modal('show');

            }

          },

          error: function(data){

            //js_slideMSGError();

          }

        });

    }

    /*delete allsub content*/





  /*remove file*/

    function js_removeFile(Table,Id,Field)

    {

        $('#modal-deleteData').modal('show');

        $('#btn_deleteDataModal').attr("onclick","js_deleteFileSendData('"+Table+"','"+Id+"','"+Field+"')");

    }



    function js_deleteFileSendData(Table,Id,Field)

    {

        $.ajax({

          type: "POST",

          url : "{{ url(config('config.config_pathCms').'/processdata/deleteFileData') }}",

          data : "cmd=deleteFileData&table="+Table+"&id="+Id+"&field_data="+Field+"&_token={!! csrf_token() !!}",

          success : function(data){

              if (data=="Y")

              {

                  //location.reload();

                  $('#'+Field+'-'+Id).remove();

                  $('#modal-deleteData').modal('hide');

              }

              else

              {

                  $('#modal-deleteData').modal('hide');

                  $('#modal-deleteDataErrorRefer').modal('show');

              }

          },

          error: function(data){

            alert(data);

          }

        });

    }

  /*remove file*/





    /*search Data*/

    function js_searchData(Table)

    {

        @if (isset($arr_data['orderByUrl']))

          location = "{{ url(config('config.config_pathCms')) }}/"+Table+"/{{ $arr_data['orderByUrl'] }}/{{ $arr_data['ascdesc'] }}/"+encodeURIComponent($("#search").val());



          console.log(location);

        @endif



        if ($("#search").val().trim()=="")

          location = "{{ url(config('config.config_pathCms')) }}/"+Table;

    }

    /*search Data*/





    /*check has active*/

    function js_checkHasActiveData(Table)

    {

        $('#modal-deleteData').modal('show');

        $('#btn_deleteDataModal').attr("onclick","js_checkHasActiveAllData('"+Table+"')");

    }



    function js_checkHasActiveAllData(Table)

    {

        var Refer_id = "";

        $(".class_chk:not(.chk_disabled)").each(function(index, el) {

          if ($(".class_chk:not(.chk_disabled):eq("+index+")").is(":checked"))

          {

            if (Refer_id!="")

              Refer_id += "|$|";

            Refer_id += $(".class_chk:not(.chk_disabled):eq("+index+")").val();

          }

        });



        $.ajax({

          type: "POST",

          url : "{{url(config('config.config_pathCms'))}}/"+Table+"/checkHasActiveData",

          data : "cmd=checkHasActiveData&refer_id="+Refer_id+"&_token={!! csrf_token() !!}",

          success : function(data){

            //alert(data);

            if (data=="Y")

            {

                location.reload();

            }

            else

            {

                $('#modal-deleteData').modal('hide');

                $('#modal-deleteDataHasActive').modal('show');

            }

          },

          error: function(data){

            //js_slideMSGError();

          }

        });

    }

    /*check has active*/





    $(function(){



        //drag and drop order number

        @if (isset($arr_data["orderByUrl"]))

          @if ($arr_data["orderByUrl"] == "order_by" || $arr_data["orderByUrl"] == "order_slide")

            $('#tbl_dataList tbody').sortable({

                items: '> tr:not(.tr_nosorted)',

                handle : '.class_icon_sort',

                forcePlaceholderSize: true,

                start: function (event, ui) {

                  // Build a placeholder cell that spans all the cells in the row

                  var cellCount = 0;

                  $('td, th', ui.helper).each(function () {

                      // For each TD or TH try and get it's colspan attribute, and add that or 1 to the total

                      var colspan = 1;

                      var colspanAttr = $(this).attr('colspan');

                      if (colspanAttr > 1) {

                          colspan = colspanAttr;

                      }

                      cellCount += colspan;

                  });



                  // Add the placeholder UI - note that this is the item's content, so TD rather thanTR

                  ui.placeholder.html('<td colspan="' + cellCount + '">&nbsp;</td>');

                },

                update: function (event, ui) {

                    // Build a placeholder cell that spans all the cells in the row

                    var html_sort = "";

                    $('#tbl_dataList tbody tr').each(function(index, el) {

                        if (html_sort!="")

                          html_sort += "|$|";

                        html_sort += $('tbody tr:eq('+index+')').attr("rel");

                    });



                    var Table = $("#tbl_dataList").attr("rel");



                    @if ($arr_data["orderByUrl"] == "order_slide")

                      $.ajax({

                        type: "POST",

                        url : "{{ url(config('config.config_pathCms').'/processdata/sortableSlideData') }}",

                        data : "cmd=sortableSlideData&table="+Table+"&html_sort="+html_sort+"&start_sort={{ $arr_data['start_sort'] }}&_token={!! csrf_token() !!}",

                        success : function(data){

                            setTimeout(function(){

                                js_slideMSGSuccess();

                            },500);

                        },

                        error: function(data){

                          //alert("test2");

                          js_slideMSGError();

                        }

                      });

                    @else

                      $.ajax({

                        type: "POST",

                        url : "{{ url(config('config.config_pathCms').'/processdata/sortableData') }}",

                        data : "cmd=sortableData&table="+Table+"&html_sort="+html_sort+"&start_sort={{ $arr_data['start_sort'] }}&_token={!! csrf_token() !!}",

                        success : function(data){

                            setTimeout(function(){

                                js_slideMSGSuccess();

                            },500);

                        },

                        error: function(data){

                          //alert("test2");

                          js_slideMSGError();

                        }

                      });

                    @endif

                }

            }).disableSelection();

          @endif;

        @endif;

        //drag and drop order number





        //function change type money

        $('.keyup_money').keyup( function() {

          $(this).val(formatAmount($( this ).val()));

        });

        //function change type money

    });

    function js_excelAllSubContent(Table)
    {
        $('#modal-exportData').modal('show');
        $('#btn_exportDataModal').attr("onclick","js_excelAllSubContentData('"+Table+"')");
    }

    function js_excelAllSubContentData(Table)
    {
      location ="{{url(config('config.config_pathCms'))}}/"+Table+"/exportAllSubContent";
      $('#modal-exportData').modal('hide');
    }

</script>