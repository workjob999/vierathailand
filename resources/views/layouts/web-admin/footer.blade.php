	<span class="pull-right">

        <a href="javascript:;" class="btn-scroll-to-top" data-click="scroll-top">

            <i class="fa fa-arrow-up"></i> <span class="hidden-xs">Back to Top</span>

        </a>

    </span>

    &copy; 2018 <b>{{ config('config.company_name') }}</b> All Right Reserved