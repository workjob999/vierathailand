<!--popup alert change status-->

<div class="modal modal-warning fade" id="modal-changeStatus"  tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="gridSystemModalLabel">Warning !!!</h4>

      </div>

      <div class="modal-body">

          <div class="alert alert-warning m-b-0">

            <label for="recipient-name" class="control-label">Do you want to change status ? </label>

          </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        <button type="button" id="btn_changeStatusModal" class="btn btn-primary">Confirm</button>

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!--/ popup alert change status-->





<!--popup alert delete data warning-->

<div class="modal modal-warning fade" id="modal-deleteData"  tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="gridSystemModalLabel">Delete !!!</h4>

      </div>

      <div class="modal-body">

          <div class="alert alert-warning m-b-0">

            <label for="recipient-name" class="control-label">Do you want to delete data ? </label>

          </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        <button type="button" id="btn_deleteDataModal" class="btn btn-primary">Confirm</button>

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!--/ popup alert delete data-->





<!--popup alert delete Image warning-->

<div class="modal modal-warning fade" id="modal-deleteImage"  tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="gridSystemModalLabel">Delete !!!</h4>

      </div>

      <div class="modal-body">

          <div class="alert alert-warning m-b-0">

            <label for="recipient-name" class="control-label">Do you want to delete image ? </label>

          </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>

        <button type="button" id="btn_deleteImageModal" class="btn btn-primary">Confirm</button>

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!--/ popup alert delete Image-->





<!--popup alert cannot delete data-->

<div class="modal modal-danger fade" id="modal-deleteDataErrorRefer"  tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="gridSystemModalLabel">Delete !!!</h4>

      </div>

      <div class="modal-body">

          <div class="alert alert-danger m-b-0">

            <label for="recipient-name" class="control-label">Error information ! Please contact the administrator.</label>

          </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!--/ popup alert cannot delete data-->





<!--popup alert cannot delete data-->

<div class="modal modal-danger fade" id="modal-deleteDataHasActive"  tabindex="-1" role="dialog">

  <div class="modal-dialog" role="document">

    <div class="modal-content">

      <div class="modal-header">

        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

        <h4 class="modal-title" id="gridSystemModalLabel">Delete !!!</h4>

      </div>

      <div class="modal-body">

          <div class="alert alert-danger m-b-0">

            <label for="recipient-name" class="control-label">Error! Data can not be deleted. Because it is active.</label>

          </div>

      </div>

      <div class="modal-footer">

        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

      </div>

    </div><!-- /.modal-content -->

  </div><!-- /.modal-dialog -->

</div><!-- /.modal -->

<!--/ popup alert cannot delete data-->



<!--popup alert change status-->
<div class="modal modal-warning fade" id="modal-sendMail"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Warning !!!</h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-warning m-b-0">
            <label for="recipient-name" class="control-label">Do you want to send email ? </label>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_changeSendMail" class="btn btn-primary">Confirm</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/ popup alert change status-->


<!--popup alert export data warning-->
<div class="modal modal-warning fade" id="modal-exportData"  tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="gridSystemModalLabel">Export !!!</h4>
      </div>
      <div class="modal-body">
          <div class="alert alert-warning m-b-0">
            <label for="recipient-name" class="control-label">Do you want to export data ? </label>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <button type="button" id="btn_exportDataModal" class="btn btn-primary">Confirm</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!--/ popup alert delete data-->