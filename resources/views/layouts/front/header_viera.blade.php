	<header class="fixed-top">		
		<div class="row no-gutters d-flex flex-row justify-content-between align-items-center">
			<div class="d-none d-lg-block col-lg-1 col-xl-2"><img src="{{ url('dist/img/logo-viera.svg') }}" class="img-fluid pl-3" width="60" alt=""> </div>
			<nav class="col-lg-10 col-xl-10 navbar navbar-expand-lg navbar-light">
			<a class="navbar-brand d-lg-none" href="#"><img src="{{ url('dist/img/logo-viera.svg') }}" class="img-fluid pl-3" width="47" alt=""></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
				<div class="collapse navbar-collapse justify-content-end" id="navbarSupportedContent">
					<ul class="nav align-items-center">
						<li class="nav-item">
						<a class="nav-link" href="{{ url(Request()->lang) }}">HOME</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/products') }}">PRODUCT</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/register') }}">REGISTER</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/dealer') }}">DEALER / AGENT</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/review') }}">REVIEW/MEDIA</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/promotion') }}">PROMOTION</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="{{ url(Request()->lang.'/contact') }}">CONTACT</a>
						</li>
						<li class="nav-item language">

							{{-- เปลี่ยนภาษา --}}
							{{-- @if (Session::has('locale') && Session::get('locale')=="th")
								@if (!empty(Request::input("s")) && Request::input("s")!="")
									<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="dropdownLanguage">ไทย</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->full() }}&lang=en" data-toggle="dropdown" id="dropdownLanguage">Eng</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->full() }}&lang=ch" data-toggle="dropdown" id="dropdownLanguage">中文</a>
								@else
									<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="dropdownLanguage">ไทย</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->current() }}?lang=en" data-toggle="dropdown" id="dropdownLanguage">Eng</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->current() }}?lang=ch" data-toggle="dropdown" id="dropdownLanguage">中文</a>
								@endif
							@elseif (Session::has('locale') && Session::get('locale')=="en")
								@if (!empty(Request::input("s")) && Request::input("s")!="")
									<a class="nav-link dropdown-toggle" href="{{ url()->full() }}&lang=th" data-toggle="dropdown" id="dropdownLanguage">ไทย</a>
									<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="dropdownLanguage">Eng</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->full() }}&lang=ch" data-toggle="dropdown" id="dropdownLanguage">中文</a>
								@else
									<a class="nav-link dropdown-toggle" href="{{ url()->current() }}?lang=th" data-toggle="dropdown" id="dropdownLanguage">ไทย</a>
									<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="dropdownLanguage">Eng</a>
									<a class="nav-link dropdown-toggle" href="{{ url()->current() }}?lang=ch" data-toggle="dropdown" id="dropdownLanguage">中文</a>
								@endif
							@endif --}}

							<a class="nav-link dropdown-toggle" href="#" data-toggle="dropdown" id="dropdownLanguage">
								@if (Session::get('locale')=="en")
									ENG
								@elseif (Session::get('locale')=="ch")
									中文
								@else
									ไทย
								@endif
							</a>
								<div class="dropdown-menu" aria-labelledby="dropdownLanguage">
									<a class="dropdown-item" href="javascript:void(0)" onclick="jsChangeLanguage('th')">ไทย</a>
									<a class="dropdown-item" href="javascript:void(0)" onclick="jsChangeLanguage('en')">ENG</a>
									<a class="dropdown-item" href="javascript:void(0)" onclick="jsChangeLanguage('ch')">中文</a>
								</div>
							
						</li>			
					</ul>
				</div>	
			</nav>		
		</div>	
						
	</header>

<script type="text/javascript">
	function jsChangeLanguage(lang)
	{
		location = "/"+lang+"/{{ Session::get('urlCurrent') }}";
	}
</script>
