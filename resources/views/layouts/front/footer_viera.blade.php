<footer>
	<div class="container p-3">
		<div class="row align-items-center justify-content-center">
			<div class="col-12 col-md-3 pt-2 text-center">
				<a href="https://www.facebook.com/vieracollagenofficial" target="_blank" class="px-1">
					<i class="fab fa-facebook-square fa-2x"></i>
				</a>
				<a href="https://www.instagram.com/vieracollagen" target="_blank" class="px-1">
					<i class="fab fa-instagram fa-2x"></i>
				</a>
				<a href="https://twitter.com/vieracollagen" target="_blank" class="px-1">
					<i class="fab fa-twitter-square fa-2x"></i>
				</a>
				<p class="mb-auto">VIERACOLLAGEN</p>
			</div>
			<div class="col-12 col-md-4 pt-2 d-flex flex-row justify-content-center align-items-center text-center">
				<a href="#" class="px-1">
					<i class="fab fa-line fa-2x"></i>
				</a>
				<a href="#" class="px-1"> <span class="fontEng">@</span> VIERACOLLAGEN </a>
				<!-- <a class="px-2"><img src="img/qr-code.jpg" alt=""></a> -->
			</div>
			<div class="col-12 col-md-5 pt-2 text-center text-sm-left">
				<h6>สอบถามรายละเอียดเพิ่มเติมที่ศูนย์ข้อมูลสุขภาพ HEALTHORY
					<br> โทร. 098-445-5154
					<br> www.healthorythailand.com</h6>
			</div>
		</div>
	</div>
</footer>