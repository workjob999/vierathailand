<!doctype html>
<html lang="{{ app()->getLocale() }}">

<head>
	<meta charset="UTF-8">
	<title>
		@if(View::hasSection('page_title'))
	        @yield('page_title')
	    @else
	        {{ config('config.config_meta_title') }}
	    @endif
	</title>
	<meta http-equiv="Content-Language" content="">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="google" content="notranslate">

	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<meta property="fb:app_id" content="">
	<meta property="og:url" content="">
	<meta property="og:type" content="">
	<meta property="og:title" content="">
	<meta property="og:image" content="">
	<meta property="og:description" content="">

	<link rel="shortcut icon" type="image/x-icon" href="{{url('img/favicon/favicon.ico')}}">
	<link rel="apple-touch-icon" sizes="57x57" href="{{url('img/favicon/apple-icon-57x57.png')}}">
	<link rel="apple-touch-icon" sizes="60x60" href="{{url('img/favicon/apple-icon-60x60.png')}}">
	<link rel="apple-touch-icon" sizes="72x72" href="{{url('img/favicon/apple-icon-72x72.png')}}">
	<link rel="apple-touch-icon" sizes="76x76" href="{{url('img/favicon/apple-icon-76x76.png')}}">
	<link rel="apple-touch-icon" sizes="114x114" href="{{url('img/favicon/apple-icon-114x114.png')}}">
	<link rel="apple-touch-icon" sizes="120x120" href="{{url('img/favicon/apple-icon-120x120.png')}}">
	<link rel="apple-touch-icon" sizes="144x144" href="{{url('img/favicon/apple-icon-144x144.png')}}">
	<link rel="apple-touch-icon" sizes="152x152" href="{{url('img/favicon/apple-icon-152x152.png')}}">
	<link rel="apple-touch-icon" sizes="180x180" href="{{url('img/favicon/apple-icon-180x180.png')}}">
	<link rel="manifest" href="{{url('img/favicon/manifest.json')}}">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="{{url('img/favicon/ms-icon-144x144.png')}}">
	<meta name="theme-color" content="#ffffff">

	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,700" rel="stylesheet">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css">
	<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

	<!-- Styles -->
	{!! Html::style('dist/css/app.min.css') !!}

	@yield('page_style')

</head>

<body>
	<div class="container-fluid">
		<div class="row bgContent">
			@include('layouts.front.header_viera')
			@yield('content')			
		</div>
	</div>
	@include('layouts.front.footer_viera')

	<!-- <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script> -->
	<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script> -->
	<!-- <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script> -->
	{!! Html::script('dist/js/main.min.js') !!}	

	{!! Html::script('assets/plugins/sweetalert2/dist/sweetalert2.min.js') !!}
	{!! Html::style('assets/plugins/sweetalert2/dist/sweetalert2.min.css') !!}

	{!! Html::script('js/functionjs.js') !!}
	@yield('page_script')
</body>
</html>
