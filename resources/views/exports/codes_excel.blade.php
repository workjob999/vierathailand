<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="12" style="text-align: center"><h1>Codes - {{ $type }}</h1></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="1">
	<tr style="background-color:#0000FF;color:#FFFFFF">
		<th>No.</th>
		<th>Code</th>
		<th>Status</th>
		<th>Status Paid</th>
	</tr>
	@if (isset($codes) && sizeof($codes)>0)
		@foreach ($codes as $code)
			<tr>
				<td>{{ ($loop->index+1) }}</td>
				<td>{{ $code->code }}</td>
				<td>{{ config('config.config_arr_status')[$code->status] }}</td>
				<td>{{ config('config.config_arr_statusPaid')[$code->status_paid] }}</td>
			</tr>
		@endforeach
	@endif
</table>