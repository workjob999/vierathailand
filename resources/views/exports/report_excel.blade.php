<table cellpadding="0" cellspacing="0" border="0">
	<tr>
		<td colspan="12" style="text-align: center"><h1>Register - {{ $type }}</h1></td>
	</tr>
</table>
<table cellpadding="0" cellspacing="0" border="1">
	<tr style="background-color:#0000FF;color:#FFFFFF">
		<th>No.</th>
		<th>Code</th>
		<th>Title</th>
		<th>Firstname</th>
		<th>Lastname</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Ticket</th>
		<th>Register date</th>
		<th>Send email date</th>
		<th>Status</th>
		<th>Status Paid</th>
	</tr>
	@if (isset($reports) && sizeof($reports)>0)
		@foreach ($reports as $report)
			<tr>
				<td>{{ ($loop->index+1) }}</td>
				<td>{{ $report->code }}</td>
				<td>{{ config('config.config_arr_title')[$report->title] }}</td>
				<td>{{ $report->firstname }}</td>
				<td>{{ $report->lastname }}</td>
				<td>{{ $report->email }}</td>
				<td>{{ $report->phone }}</td>
				<td>{{ $report->ticket }}</td>
				<td>{{ $report->create_datetime }}</td>
				<td>{{ $report->update_datetime }}</td>
				<td>{{ config('config.config_arr_status')[$report->status] }}</td>
				<td>{{ config('config.config_arr_statusPaid')[$report->status_paid] }}</td>
			</tr>
		@endforeach
	@endif
</table>