<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  <tbody>
    <tr>
      <td><div style="text-align: center"><img src="{{url('img/banner-edm.jpg')}}" alt=""></div>
        <h2 style="text-align: center; color: #00a345;"> THANK YOU </h2>
        <h3 style="text-align: center; color: #00a345;"> ท่านจองสำเร็จเรียบร้อยแล้ว <br>
          ขอขอบคุณ สำหรับการจองบัตรราคาพิเศษ (Early Bird)</h3>
        <table width="920" border="0" align="center" cellpadding="10" cellspacing="0" class="responsive-table" style="background: #5fc3b7; border-radius: 10px; margin: 50px 0; color:#fff;">
          <thead>
            <tr>
              <th colspan="6" align="center" class="center-align"><h1>ยืนยันการจอง</h1></th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td width="25%" align="left" valign="middle">ชื่อผู้จอง</td>
              <td colspan="2" align="left" valign="middle" style="color: #7030a0;">{{ $title }} {{ $firstname }}</td>
              <td width="25%" align="left" valign="middle">นามสกุล</td>
              <td colspan="2" align="left" valign="middle" style="color: #7030a0;">{{ $lastname }}</td>
            </tr>
            <tr>
              <td width="25%" align="left" valign="middle">เบอร์มือถือ</td>
              <td colspan="2" align="left" valign="middle" style="color: #7030a0;">{{ $phone }}</td>
              <td width="25%" align="left" valign="middle">อีเมล</td>
              <td colspan="2" align="left" valign="middle" style="color: #7030a0;">{{ $email }}</td>
            </tr>
            <tr>
              <td width="25%" align="left" valign="middle">จำนวนบัตร</td>
              <td width="25%" align="left" valign="middle" style="color: #7030a0;">{{ $ticket }}</td>
              <td width="20" align="left" valign="middle">ใบ<span></td>
              <td width="25%" align="left" valign="middle">จำนวนเงินที่ต้องชำระ</td>
              <td width="25%" align="left" valign="middle" style="color: #7030a0;">{{ number_format($ticket*900,2) }}</td>
              <td width="40" align="left" valign="middle">บาท</td>
            </tr>
            <tr>
              <td height="60" colspan="6" align="center" valign="middle" style="color: #7030a0;">(ราคาบัตรใบละ 1,300 บาท ส่วนลดจาก SCB จำนวน 400 บาท คงเหลือใบละ 900 บาท)</td>
            </tr>
          </tbody>
        </table>
        <table width="920" border="0" align="center" cellpadding="10" cellspacing="0" style="background: #7030a0; border-radius: 10px; margin: 50px 0; color:#fff;">
          <tbody>
            <tr>
              <td colspan="2" align="center" class="center-align"><h1>ช่องทางการชำระเงิน</h1></td>
            </tr>
            <tr>
              <td width="90" valign="top"><div style="border-radius: 50%; border:1px solid white; width: 50px; height: 45px; font-size: 30px; text-align: center; padding-top: 5px">1</div></td>
              <td>โอนเงินเข้าบัญชี<br>
                ชื่อบัญชี : บริษัท ครีเอท อินเทลลิเจ้นซ์ จำกัด<br>
                เลขที่บัญชี : 011-257649-2<br>
                ธนาคาร : ไทยพาณิชย์ สาขาบางพลัด ประเภทออมทรัพย์<br>
                <div class="remark"><em>หมายเหตุ เพื่อความสะดวกในการตรวจสอบยอดเงินของท่าน
                  กรุณาโอนเงินเป็นเศษสตางค์ เช่น 99,000.09 บาท เป็นต้น </em></div></td>
            </tr>
            <tr>
              <td valign="top"><div style="border-radius: 50%; border:1px solid white; width: 50px; height: 45px; font-size: 30px; text-align: center; padding-top: 5px">2</div></td>
              <td>ชำระผ่าน Prompt Pay (QR Code)<br>
                <img src="{{url('img/qr-code.png')}}" alt=""></td>
            </tr>
          </tbody>
        </table>
        <table width="920" border="0" align="center" cellpadding="10" cellspacing="0">
          <tbody>
            <tr>
              <td align="center"><p style="color: #ff2e66; text-center;">**กำหนดชำระเงินและส่งหลักฐานการชำระเงินภายในวันที่ 10 มิถุนายน 61 เท่านั้น**</p>
                <h3 style="color: #093272; text-center;">หลังจากชำระเงินเรียบร้อยแล้ว กรุณาส่งสลิปการชำระเงินมาที่ </h3>
                <div style="text-align: center"></div></td>
            </tr>
            <tr>
              <td align="center" valign="middle"><a href="#" style="width: 300px; background: #4285f4; padding: 15px 30px; color: white; border-radius: 4px;">คลิกเพื่อส่งหลักฐาน</a></td>
            </tr>
            <tr>
              <td align="left" valign="middle"><h4 style="color: #093272;">หรือส่งผ่านทางอีเมล singing.ticket@gmail.com  โดยกรอกข้อมูลผู้จองให้ครบถ้วน ดังนี้</h4>
                <ul style="list-style: none; color: #093272;">
                  <li>- ชื่อและนามสกุลผู้จอง</li>
                  <li>- อีเมลที่ใช้จอง</li>
                  <li>- เบอร์โทรศัพท์ที่ติดต่อได้</li>
                  <li>- วันที่ชำระเงิน</li>
                  <li>- เวลาที่ชำระเงิน</li>
                  <li>- สาขาที่ชำระเงิน (กรณีชำระที่ธนาคารโดยตรง)</li>
                  <li>- จำนวนเงินที่ชำระทั้งสิ้น</li>
                </ul></td>
            </tr>
            <tr>
              <td align="left" valign="middle"><p style="color: #093272;">หมายเหตุ: <br>
                  - การส่งสลิปการชำระเงิน กรุณาถ่ายให้เห็นรายละเอียด วัน, เวลา, และจำนวนเงินที่ชำระให้ชัดเจน สามารถใช้<br>
                  ไฟล์ประเภท .JPG, . PNG, หรือ PDF ได้<br>
                  - กรณีที่ท่านชำระเงินหลายยอด ขอความกรุณาจัดส่งหลักฐานการชำระเงินมาพร้อมกันในอีเมลเดียวกัน <br>
                  ไม่แนะนำให้ส่งสลิปแยกกัน </p>
                <p style="color: #093272; text-align: left;">หลังจากทีมงานได้รับหลักฐานการจองหรือสลิปการชำระเงินจากท่านเรียบร้อยแล้ว จะดำเนินการตรวจสอบและส่ง <br>
                  &quot;ใบยืนยันการจอง&quot; กลับไปให้ท่านทางอีเมลที่ท่านจองมาภายใน 3-5 วันทำการ</p></td>
            </tr>
            <tr>
              <td align="center" valign="middle"><p style="text-align: center; color: #093272;">Singing in the Rain Music Festival : Good Old Days ไม่อยากเป็นเด็ก แต่ก็ไม่อยากโตเลย 25 สิงหาคม 2561 <br>
                  ณ แรนโช ชาญวีร์ รีสอร์ต แอนด์ คันทรีคลับ เขาใหญ่, นครราชสีมา</p></td>
            </tr>
            <tr>
              <td align="center" valign="middle"><img src="{{url('img/logo-ci.png')}}" alt=""></td>
            </tr>
            <tr>
              <td><hr></td>
            </tr>
            <tr>
              <td><p style="text-align: center; color: #f82e6a;">**กรุณาอย่าตอบกลับอีเมลฉบับนี้ เนื่องจากเป็นระบบการส่งอีเมลอัตโนมัติ <br>
                  ดังนั้นผู้จัดงานจะไม่ได้รับอีเมลของท่านและไม่สามารถตอบกลับได้**</p></td>
            </tr>
          </tbody>
        </table></td>
    </tr>
  </tbody>
</table>
