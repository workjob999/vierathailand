@extends('layouts.web-admin.main')

@section('page_title', 'Promotions')
@section('page_action', 'Edit Data')
@section('menu_active_promotions',' active ')


@section('page_style')
@stop

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/promotions')}}">Promotions</a></li>
  <li class="active">Edit Data</li>
@stop

@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        {{-- @if($errors->any()) --}}
        <ul id="msgError" class="alert alert-danger hidden">
          {{-- @foreach($errors->all() as $error)
            <li>{{ $error }}</li>
          @endforeach --}}
        </ul>
      {{-- @endif --}}

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::model($promotions, ['method' => 'put','class'=>"form-horizontal",'files' => true,"id"=>"frmPromotions",'action'=>['Webadmin\PromotionsController@update', $promotions->id]]) !!}
                  @include('web-admin.promotions._form',['submitButtonText' => 'Save'])
              {!! Form::close() !!}


          </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
@stop
