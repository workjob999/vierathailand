@extends('layouts.web-admin.main')

@section('page_title', 'Home Tvc')
@section('page_action', 'Edit Data')
@section('menu_active_home',' active ')
@section('submenu_active_tvc',' active ')

@section('page_style')
@stop

@section('page_nav')
<li><a href="{{url(config('config.config_pathCms').'/home/tvc')}}">Home Tvc</a></li>
  <li class="active">Edit Data</li>
@stop

@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        @if($errors->any())
          <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::model($homeTvcs, ['method' => 'post','class'=>"form-horizontal",'files' => true,'action'=>['Webadmin\HomeTvcsController@index', $homeTvcs->id]]) !!}
                  @include('web-admin.homeTvcs._form',['action_page' => 'view'])
              {!! Form::close() !!}


          </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
@stop
