{!! Html::style('js/fileinput/fileinput.css') !!}
{!! Html::style('js/fileinput/themes/explorer/theme.css') !!}
{!! Html::script('js/fileinput/plugins/sortable.min.js') !!}
{!! Html::script('js/fileinput/fileinput.js') !!}
{!! Html::script('js/fileinput/themes/explorer/theme.js') !!}

{!! Html::script('js/ckeditor/ckeditor.js') !!}
{!! Html::script('js/cke_config.js') !!}

<!--.box-body Other-->
<div class="box-body">

  <div class="form-group">
    {!! HTML::decode(Form::label("youtube", "Youtube", ['class' => 'col-sm-2
    control-label','for'=>'youtube'])) !!}
    <div class="col-sm-5">
      @if (isset($action_page) && $action_page=="view")
      {!! Form::text("youtube", null, ['class' =>
      'form-control','placeholder'=>"","disabled"=>@$arr_data['disabled_data']]) !!}
      @else
      {!! Form::text("youtube", null, ['class' =>
      'form-control','placeholder'=>"","isUrl"=>"รูปแบบลิงค์ Youtube ไม่ถูกต้อง! กรุณาใส่ลิงค์ใหม่อีกครั้ง"])
      !!}
      @endif
    </div>
  </div>


  <div class="form-group">
    {!! Form::label("startDate", "Start Date", ['class' => 'col-sm-2 control-label','for'=>'startDate']) !!}
    <div class="col-sm-5">
      <div class="input-group date">
        <input type="text" id="startDate" value="{{@$startDate}}" name="startDate" class="form-control"
          {{ @$arr_data['disabled_data']?'disabled':"" }}>
      </div>
    </div>
  </div>

  <div class="form-group">
    {!! Form::label("endDate", "End Date", ['class' => 'col-sm-2 control-label','for'=>'endDate']) !!}
    <div class="col-sm-5">
      <div class="input-group date">
        <input type="text" id="endDate" name="endDate" value="{{@$endDate}}" class="form-control"
          {{ @$arr_data['disabled_data']?'disabled':"" }}>
      </div>
    </div>
  </div>

  <div class="form-group">
    {!! Form::label("status", "Status", ['class' => 'col-sm-2 control-label','for'=>'status']) !!}
    <div class="col-sm-5">
      @if (isset($action_page) && $action_page=="view")
      @if (isset($homeTvcs->status))
      @if ($homeTvcs->status=="Y")
      {!! Form::checkbox('status', 'Y', true, ['data-render' =>
      'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
      @else
      {!! Form::checkbox('status', 'Y', false, ['data-render' =>
      'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
      @endif
      @else
      {!! Form::checkbox('status', 'Y', true, ['data-render' =>
      'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
      @endif
      @else
      @if (isset($homeTvcs->status))
      @if ($homeTvcs->status=="Y")
      {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
      @else
      {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
      @endif
      @else
      {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
      @endif
      @endif
    </div>
  </div>




  <div class="form-group">
    {!! Form::label("order_by", "Order", ['class' => 'col-sm-2 control-label','for'=>'order_by']) !!}
    <div class="col-sm-5">
      @if (isset($action_page) && $action_page=="view")
      {!! Form::text("order_by", null, ['class' => 'form-control','placeholder'=>"0",'readonly'=>true]) !!}
      @else
      {!! Form::text("order_by", $arr_data["order_by"], ['class' =>
      'form-control','placeholder'=>"0",'readonly'=>true])
      !!}
      @endif
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-12">
      <div class="panel">
        <div class="col-sm-5">
          <ul class="nav nav-tabs nav-tabs-primary nav-justified">
            <li class="active">
              <input type="checkbox" name="status_th" id="status_th" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" checked disabled />
              <a href="#justified-tab-1" class="text-left" data-toggle="tab" aria-expanded="true">Thai</a>
            </li>
            <li class="col-sm-3">
              <input type="checkbox" name="status_en" id="status_en" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" @if(!isset($homeTvcs) ||
                @$homeTvcs->status_en==="Y") checked @endif />
              <a href="#justified-tab-2" class="text-left" data-toggle="tab" aria-expanded="true">Eng</a>
            </li>
            <li class="col-sm-3">
              <input type="checkbox" name="status_ch" id="status_ch" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" @if(!isset($homeTvcs) ||
                @$homeTvcs->status_ch==="Y") checked @endif />
              <a href="#justified-tab-3" class="text-left" data-toggle="tab" aria-expanded="true">Ch</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content m-b-0" style="clear: both;">
        <div class="tab-pane fade active in" id="justified-tab-1">
          <div class="row">
              @if (isset($homeTvcs->thumb_th))
              @if ($homeTvcs->thumb_th!="")
              @if (file_exists(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_th))
              <div id="thumb_th-{{ $homeTvcs->id }}" class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                  <img
                    src="{{ url(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_th) }}"
                    height="200" class="img-responsive"><br>
                  @if (isset($action_page) && $action_page!="view")
                  <a href="javascript:void(0)"
                    onclick="js_removeFile('homeTvcs','{{ $homeTvcs->id }}','thumb_th')"><i
                      class="fa fa-fw fa-remove"></i>Remove</a>
                  @endif
                </div>
              </div>
              @endif
              @endif
              @endif
  
  
              <div class="form-group">
                {!! Form::label("thumb_th", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb_th']) !!}
                <div class="col-sm-5">
                  {!! Form::file('thumb_th', ['class' =>
                  'form-control','accept'=>"image/jpeg,image/png,image/gif","disabled"=>@$arr_data['disabled_data']]) !!}
                </div>
              </div>


              <div class="form-group">
                  {!! HTML::decode(Form::label("url_th", "Url", ['class' => 'col-sm-2
                  control-label','for'=>'order_by'])) !!}
                  <div class="col-sm-5">
                    @if (isset($action_page) && $action_page=="view")
                    {!! Form::text("url_th", null, ['class' =>
                    'form-control','placeholder'=>"","disabled"=>@$arr_data['disabled_data']]) !!}
                    @else
                    {!! Form::text("url_th", null, ['class' =>
                    'form-control','placeholder'=>"","isUrl"=>"รูปแบบลิงค์ไม่ถูกต้อง! กรุณาใส่ลิงค์ใหม่อีกครั้ง"])
                    !!}
                    @endif
                  </div>
                </div>


            <div class="form-group">
              {!! HTML::decode(Form::label("title_th", "ชื่อ <em style='color:red'>*</em>", ['class' => 'col-sm-2
              control-label','for'=>'title_th'])) !!}
              <div class="col-sm-5">
                {!! Form::text("title_th", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_th',"isRequired"=>"กรุณาใส่ชื่อภาษาไทย","disabled"=>@$arr_data['disabled_data']])
                !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_th", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_th']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_th", null, ['class' =>'form-control','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_th',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="justified-tab-2">
          <div class="row">
              @if (isset($homeTvcs->thumb_en))
              @if ($homeTvcs->thumb_en!="")
              @if (file_exists(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_en))
              <div id="thumb_en-{{ $homeTvcs->id }}" class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                  <img
                    src="{{ url(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_en) }}"
                    height="200" class="img-responsive"><br>
                  @if (isset($action_page) && $action_page!="view")
                  <a href="javascript:void(0)"
                    onclick="js_removeFile('homeTvcs','{{ $homeTvcs->id }}','thumb_en')"><i
                      class="fa fa-fw fa-remove"></i>Remove</a>
                  @endif
                </div>
              </div>
              @endif
              @endif
              @endif
  
  
              <div class="form-group">
                {!! Form::label("thumb_en", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb_en']) !!}
                <div class="col-sm-5">
                  {!! Form::file('thumb_en', ['class' =>
                  'form-control','accept'=>"image/jpeg,image/png,image/gif","disabled"=>@$arr_data['disabled_data']]) !!}
                </div>
              </div>

              <div class="form-group">
                  {!! HTML::decode(Form::label("url_en", "Url", ['class' => 'col-sm-2
                  control-label','for'=>'order_by'])) !!}
                  <div class="col-sm-5">
                    @if (isset($action_page) && $action_page=="view")
                    {!! Form::text("url_en", null, ['class' =>
                    'form-control','placeholder'=>"","disabled"=>@$arr_data['disabled_data']]) !!}
                    @else
                    {!! Form::text("url_en", null, ['class' =>
                    'form-control','placeholder'=>"","isUrl"=>"รูปแบบลิงค์ไม่ถูกต้อง! กรุณาใส่ลิงค์ใหม่อีกครั้ง"])
                    !!}
                    @endif
                  </div>
                </div>

            <div class="form-group">
              {!! Form::label("title_en", "ชื่อ", ['class' => 'col-sm-2 control-label','for'=>'title_en']) !!}
              <div class="col-sm-5">
                {!! Form::text("title_en", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_en',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_en", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_en']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_en", null, ['class' =>'form-control','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_en',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="justified-tab-3">
          <div class="row">
              @if (isset($homeTvcs->thumb_ch))
              @if ($homeTvcs->thumb_ch!="")
              @if (file_exists(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_ch))
              <div id="thumb_ch-{{ $homeTvcs->id }}" class="form-group">
                <div class="col-sm-2"></div>
                <div class="col-sm-5">
                  <img
                    src="{{ url(config('config.config_pathUpload').'/homeTvcs/'.$pathDate.'/'.$homeTvcs->thumb_ch) }}"
                    height="200" class="img-responsive"><br>
                  @if (isset($action_page) && $action_page!="view")
                  <a href="javascript:void(0)"
                      onclick="js_removeFile('homeTvcs','{{ $homeTvcs->id }}','thumb_ch')"><i
                      class="fa fa-fw fa-remove"></i>Remove</a>
                  @endif
                </div>
              </div>
              @endif
              @endif
              @endif
  
  
              <div class="form-group">
                {!! Form::label("thumb_ch", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb_ch']) !!}
                <div class="col-sm-5">
                  {!! Form::file('thumb_ch', ['class' =>
                  'form-control','accept'=>"image/jpeg,image/png,image/gif","disabled"=>@$arr_data['disabled_data']]) !!}
                </div>
              </div>

              <div class="form-group">
                  {!! HTML::decode(Form::label("url_ch", "Url", ['class' => 'col-sm-2
                  control-label','for'=>'order_by'])) !!}
                  <div class="col-sm-5">
                    @if (isset($action_page) && $action_page=="view")
                    {!! Form::text("url_ch", null, ['class' =>
                    'form-control','placeholder'=>"","disabled"=>@$arr_data['disabled_data']]) !!}
                    @else
                    {!! Form::text("url_ch", null, ['class' =>
                    'form-control','placeholder'=>"","isUrl"=>"รูปแบบลิงค์ไม่ถูกต้อง! กรุณาใส่ลิงค์ใหม่อีกครั้ง"])
                    !!}
                    @endif
                  </div>
                </div>


            <div class="form-group">
              {!! Form::label("title_ch", "ชื่อ", ['class' => 'col-sm-2 control-label','for'=>'title_ch']) !!}
              <div class="col-sm-5">
                {!! Form::text("title_ch", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_ch',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_ch", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_ch']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_ch", null, ['class' =>'form-control','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_ch',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





</div>
<!-- /.box-body Other-->

<div class="box-footer">
  <div class="form-group">
    <div class="col-sm-2">&nbsp;</div>
    <div class="col-sm-7">
      @if (isset($action_page) && $action_page=="view")
      <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/home/tvc') }}'"
        class="btn btn-primary width-100 m-r-5">
        << Back</button> @else {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}
          <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/home/tvc') }}'"
            class="btn btn-default width-100 m-r-5">Cancel</button>
          @endif
    </div>
  </div>
</div>
<!-- /.box-footer -->

<script>
  $( function() {
        var config_dateFormat = "dd/mm/yy",
        from = $( "#startDate" )
          .datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:2028",
            dateFormat: config_dateFormat,
            numberOfMonths: 1,
            showOn: "both",//button
            buttonText: '<i class="fa fa-calendar"></i>'
          })
          .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
          }),
        to = $( "#endDate" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1920:2028",
          dateFormat: config_dateFormat,
          numberOfMonths: 1,
          showOn: "both",//button
          buttonText: '<i class="fa fa-calendar"></i>'
        })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });
   
      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( config_dateFormat, element.value );
        } catch( error ) {
          date = null;
        }
   
        return date;
      }
  });
</script>

<script type="text/javascript">
  var submitted = false;
  $(document).ready(function(){
      $("#frmHomeTvcs").on("submit",function()
      {
        var hasError = false;
        if (!$("#msgError").hasClass("hidden"))
        {
          $("#msgError").addClass("hidden");
        }

        var response = jsValidInput();
        var html = "";
        console.log("test555",response);
        if (response.isError)
        {
          var msg = response.msg;
          $.each(msg,function(index,data){
            html += "<li>"+data+"</li>";
          })

          $("#msgError").html(html);
          $("#msgError").removeClass("hidden");

          $('html,body').animate({
            scrollTop: 0
          }, 700);

          hasError = true;
        }

        return !hasError;
      })
  })
</script>