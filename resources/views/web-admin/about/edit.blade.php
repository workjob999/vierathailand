@extends('layouts.web-admin.main')

@section('page_title', 'About')
@section('page_action', 'Edit Data')
@section('menu_active_about',' active ')


@section('page_style')
@stop

@section('page_nav')
  <li><a href="{{Route("cms-about-edit")}}">About</a></li>
  <li class="active">Edit Data</li>
@stop

@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        <ul id="msgError" class="alert alert-danger hidden">
        </ul>

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::model($about, ['method' => 'put','class'=>"form-horizontal",'files' => true,"id"=>"frmAbout",'action'=>['Webadmin\AboutController@update', $about->id]]) !!}
                  @include('web-admin.about._form',['submitButtonText' => 'Save'])
              {!! Form::close() !!}


          </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
@stop
