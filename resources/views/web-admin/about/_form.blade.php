{!! Html::style('js/fileinput/fileinput.css') !!}
{!! Html::style('js/fileinput/themes/explorer/theme.css') !!}
{!! Html::script('js/fileinput/plugins/sortable.min.js') !!}
{!! Html::script('js/fileinput/fileinput.js') !!}
{!! Html::script('js/fileinput/themes/explorer/theme.js') !!}

{!! Html::script('js/ckeditor/ckeditor.js') !!}
{!! Html::script('js/cke_config.js') !!}

<!--.box-body Other-->
<div class="box-body">

  @if (isset($about->thumb))
  @if ($about->thumb!="")
  @if (file_exists(config('config.config_pathUpload').'/about/'.$pathDate.'/'.$about->thumb))
  <div id="thumb-{{ $about->id }}" class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-5">
      <img src="{{ url(config('config.config_pathUpload').'/about/'.$pathDate.'/'.$about->thumb) }}" height="200"
        class="img-responsive"><br>
      @if (isset($action_page) && $action_page!="view")
      <a href="javascript:void(0)" onclick="js_removeFile('about','{{ $about->id }}','thumb')"><i
          class="fa fa-fw fa-remove"></i>Remove</a>
      @endif
    </div>
  </div>
  @endif
  @endif
  @endif


  <div class="form-group">
    {!! Form::label("thumb", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb']) !!}
    <div class="col-sm-5">
      {!! Form::file('thumb', ['class' =>'form-control','accept'=>"image/jpeg,image/png,image/gif"]) !!}
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-12">
      <div class="panel">
        <div class="col-sm-5">
          <ul class="nav nav-tabs nav-tabs-primary nav-justified">
            <li class="active">
              <input type="checkbox" name="status_th" id="status_th" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" checked disabled />
              <a href="#justified-tab-1" class="text-left" data-toggle="tab" aria-expanded="true">Thai</a>
            </li>
            <li class="col-sm-3">
              <input type="checkbox" name="status_en" id="status_en" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" @if(!isset($about) || @$about->status_en==="Y") checked
              @endif />
              <a href="#justified-tab-2" class="text-left" data-toggle="tab" aria-expanded="true">Eng</a>
            </li>
            <li class="col-sm-3">
              <input type="checkbox" name="status_ch" id="status_ch" value="Y"
                style="position: absolute;top: 35%;z-index: 2;" @if(!isset($about) || @$about->status_ch==="Y") checked
              @endif />
              <a href="#justified-tab-3" class="text-left" data-toggle="tab" aria-expanded="true">Ch</a>
            </li>
          </ul>
        </div>
      </div>
      <div class="tab-content m-b-0" style="clear: both;">
        <div class="tab-pane fade active in" id="justified-tab-1">
          <div class="row">
            <div class="form-group">
              {!! HTML::decode(Form::label("title_th", "ชื่อ <em style='color:red'>*</em>", ['class' => 'col-sm-2
              control-label','for'=>'title_th'])) !!}
              <div class="col-sm-5">
                {!! Form::text("title_th", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_th',"isRequired"=>"กรุณาใส่ชื่อภาษาไทย","disabled"=>@$arr_data['disabled_data']])
                !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_th", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_th']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_th", null, ['class' =>'form-control class_ckeditor','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_th',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="justified-tab-2">
          <div class="row">
            <div class="form-group">
              {!! Form::label("title_en", "ชื่อ", ['class' => 'col-sm-2 control-label','for'=>'title_en']) !!}
              <div class="col-sm-5">
                {!! Form::text("title_en", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_en',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_en", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_en']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_en", null, ['class' =>'form-control class_ckeditor','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_en',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
        <div class="tab-pane fade" id="justified-tab-3">
          <div class="row">
            <div class="form-group">
              {!! Form::label("title_ch", "ชื่อ", ['class' => 'col-sm-2 control-label','for'=>'title_ch']) !!}
              <div class="col-sm-5">
                {!! Form::text("title_ch", null, ['class'
                =>'form-control','placeholder'=>"",'id'=>'title_ch',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>

            <div class="form-group">
              {!! Form::label("detail_ch", "รายละเอียด", ['class' => 'col-sm-2 control-label','for'=>'detail_ch']) !!}
              <div class="col-sm-5">
                {!! Form::textarea("detail_ch", null, ['class' =>'form-control class_ckeditor','size' =>
                '50x3','placeholder'=>"",'id'=>'detail_ch',"disabled"=>@$arr_data['disabled_data']]) !!}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>





</div>
<!-- /.box-body Other-->

<div class="box-footer">
  <div class="form-group">
    <div class="col-sm-2">&nbsp;</div>
    <div class="col-sm-7">
      {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}
      <button type="button" onclick="location='{{ Route('cms-about-edit') }}'"
        class="btn btn-default width-100 m-r-5">Cancel</button>
    </div>
  </div>
</div>
<!-- /.box-footer -->

<script type="text/javascript">
  var submitted = false;
  $(document).ready(function(){
      $("#frmAbout").on("submit",function()
      {
        var hasError = false;
        if (!$("#msgError").hasClass("hidden"))
        {
          $("#msgError").addClass("hidden");
        }

        var response = jsValidInput();
        var html = "";
        if (response.isError)
        {
          var msg = response.msg;
          $.each(msg,function(index,data){
            html += "<li>"+data+"</li>";
          })

          $("#msgError").html(html);
          $("#msgError").removeClass("hidden");

          $('html,body').animate({
            scrollTop: 0
          }, 700);

          hasError = true;
        }

        return !hasError;
      })


      $(window).on("load resize",(function() {
        /* Act on the event */
          setTimeout(function(){
                $('.class_ckeditor').each(function(index, el) {
                  CKEDITOR.replace($('.class_ckeditor:eq('+index+')').attr('id'),cke_config);
                });
            },200);
        })
      );
  })
</script>