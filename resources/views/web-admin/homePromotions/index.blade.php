@extends('layouts.web-admin.main')

@section('page_title', 'Home Promotions')
@section('page_action', 'List Data')
@section('menu_active_home',' active ')
@section('submenu_active_promotions',' active ')

@section('page_nav')
<li><a href="{{url(config('config.config_pathCms').'/home/promotions')}}">Home Promotions</a></li>
<li class="active">List Data</li>
@stop

@section('page_style')
@stop


@section('content')
<!-- Content Header (Page header) -->

<section class="section-container section-with-top-border p-b-5">

  @include('layouts.web-admin.massage_block')

  <div class="row">
    <div class="col-md-12">


      <div class="panel">
        <div class="panel-heading">

          <div class="panel-heading-btn">
            <div class="pull-right">
              <button class="btn btn-default-sm" type="button" onclick="js_searchData('home/promotions')">
                <i class="fa fa-search"></i>
              </button>
            </div>
            <div class="pull-right">
              @if (isset($arr_data["search"]))
              <input type="text" class="form-control" name="search" id="search" value="{{ $arr_data['search'] }}"
                placeholder="Search...">
              @else
              <input type="text" class="form-control" name="search" id="search" placeholder="Search...">
              @endif
            </div>
          </div>

          <h4 class="panel-title">
            Total number of Promotions : {{ $homePromotions->total() }}
            @if ($hasPermission["create-home-promotions"])
            &nbsp;&nbsp;
            <a href="javascript:void(0)"
              onclick="location='{{url(config('config.config_pathCms').'/home/promotions/create')}}'"
              class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse" title="Add Data"><i
                class="fa fa-plus"></i></a>
            @endif
            @if ($hasPermission["edit-home-promotions"])
            &nbsp;&nbsp;
            <a href="javascript:void(0)" onclick="js_deleteAllSubContent('home/promotions')"
              class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-collapse" title="Delete Data"><i
                class="fas fa-minus"></i></a>
            @endif
          </h4>
        </div>

        <!-- /.box-header -->
        <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

          <table id="tbl_dataList" rel="home_promotions"
            class="table table-bordered table-hover dataTable no-footer dtr-inline table-responsive">
            <thead>
              <tr>
                <th style="width: 4%">
                  @if ($arr_data["orderByUrl"] == "order_by")
                  @if ($arr_data["ascdesc"] == "ASC")
                  <a href="javascript:void(0)"><i class="fas fa-sort-alpha-down pull-right"></i></a>
                  @endif
                  @else
                  <a
                    href="{{ url(config('config.config_pathCms').'/home/promotions') }}/order_by/ASC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"><i
                      class="fas fa-sort-alpha-up pull-right text-muted"></i></a>
                  @endif
                </th>
                <th class="text-center" style="width: 4%">
                  <p class=""><input type="checkbox" id="chk_all" onclick="js_chkAll()" class="chk_all" value="Y"></p>
                </th>
                <th class="text-center" style="width: 15%">
                  <p class="pull-left">Thumb</p>
                </th>
                <th style="width: 30%">
                  <p class="pull-left">ชื่อ promotion</p>
                  <p class="pull-right">
                    @if ($arr_data["orderByUrl"] == "title")
                    @if ($arr_data["ascdesc"] == "ASC")
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/title/DESC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-down"></i></a>
                    @else
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/title/ASC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-up"></i></a>
                    @endif
                    @else
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/title/DESC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-down text-muted"></i></a>
                    @endif
                  </p>
                </th>
                <th style="width: 6%">
                  <p class="pull-left">Status</p>
                </th>
                <th style="width: 20%">
                  <p class="pull-left">Modified</p>
                  <p class="pull-right">
                    @if ($arr_data["orderByUrl"] == "modify")
                    @if ($arr_data["ascdesc"] == "ASC")
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/modify/DESC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-down"></i></a>
                    @else
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/modify/ASC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-up"></i></a>
                    @endif
                    @else
                    <a href="{{ url(config('config.config_pathCms').'/home/promotions') }}/modify/DESC{{ $arr_data['urlSearch'] }}?page={{ $homePromotions->currentPage() }}"
                      class="pull-right"><i class="fas fa-sort-alpha-down text-muted"></i></a>
                    @endif
                  </p>
                </th>
                <th class="text-center" style="width: 21%">
                  <p class="">Process</p>
                </th>
              </tr>
            </thead>
            <tbody>
              @if ($homePromotions->count()>0)
              @foreach($homePromotions as $homePromotion)
              <tr class="tr_sorted" rel="tr_{{ $homePromotion->id }}">
                <td class="text-center"><i class="fa fa-bars {{ $arr_data['status_icon_sort'] }} class_icon_sort"
                    aria-hidden="true"></i></td>
                <td class="text-center">
                  <input type="checkbox" id="chk_{{ $homePromotion->id }}" name="chk_{{ $homePromotion->id }}"
                    class="class_chk" value="{{ $homePromotion->id }}"></input>
                </td>
                <td class="text-center">
                    @if (isset($homePromotion->thumb_th) && $homePromotion->thumb_th!="" && file_exists(config('config.config_pathUpload').'/homePromotions/'.$homePromotion->pathFile.'/'.$homePromotion->thumb_th))
                      <img src="{{ url(config('config.config_pathUpload').'/homePromotions/'.$homePromotion->pathFile.'/'.$homePromotion->thumb_th) }}"
                      height="150" class="img-responsive">
                    @endif
                </td>
                <td>
                  {{ $homePromotion->title_th }} / {{$homePromotion->title_en}} / {{$homePromotion->title_ch}}<Br /><Br />
                  <span style="font-weight:bold">Start Date</span> : @if($homePromotion->startDate!==null)
                  {{\Carbon\Carbon::parse($homePromotion->startDate)->format("d/m/Y")}} @endif <br />
                  <span style="font-weight:bold">End Date</span> : @if($homePromotion->endDate!==null)
                  {{\Carbon\Carbon::parse($homePromotion->endDate)->format("d/m/Y")}} @endif
                </td>
                <td>
                  @if (!$hasPermission["edit-home-promotions"])
                  <a id="btn_statusDataActive-{{$homePromotion->id}}" href="javascript:void(0)"
                    class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10">
                    {{ config('config.config_arr_status')["Y"] }}</a>
                  @else
                  @if ($homePromotion->status==="Y")
                  <a id="btn_statusDataActive-{{$homePromotion->id}}" href="javascript:void(0)"
                    onclick="js_changeStatus('home_promotions',{{ $homePromotion->id }},'Y')"
                    class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10">
                    {{ config('config.config_arr_status')["Y"] }}</a>

                  <a id="btn_statusDataInactive-{{$homePromotion->id}}" href="javascript:void(0)"
                    onclick="js_changeStatus('home_promotions',{{ $homePromotion->id }},'N')"
                    class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 hide">
                    {{ config('config.config_arr_status')["N"] }}</a>

                  @else
                  <a id="btn_statusDataActive-{{$homePromotion->id}}" href="javascript:void(0)"
                    onclick="js_changeStatus('home_promotions',{{ $homePromotion->id }},'Y')"
                    class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10 hide">
                    {{ config('config.config_arr_status')["Y"] }}</a>

                  <a id="btn_statusDataInactive-{{$homePromotion->id}}" href="javascript:void(0)"
                    onclick="js_changeStatus('home_promotions',{{ $homePromotion->id }},'N')"
                    class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 ">
                    {{ config('config.config_arr_status')["N"] }}</a>
                  @endif
                  @endif
                </td>
                <td>
                  {{ \Carbon\Carbon::parse($homePromotion->updated_at)->diffForHumans() }}<br>
                  @if (isset($getUsersData[$homePromotion->created_by]["name"]) &&
                  $getUsersData[$homePromotion->created_by]["name"]!="")
                  Edit By : {{$getUsersData[$homePromotion->created_by]["name"]}}
                  @endif
                </td>
                <td>
                  <a href="{{ url(config('config.config_pathCms').'/home/promotions/'.$homePromotion->id) }}"
                    class="btn btn-default btn-block btn-xs btn-rounded p-l-10 p-r-10"><i
                      class="fa fa-fw fa-play"></i>View</a>
                  @if ($hasPermission["edit-home-promotions"])
                  <a href="{{ url(config('config.config_pathCms').'/home/promotions/'.$homePromotion->id.'/edit') }}"
                    class="btn btn-default btn-block btn-xs btn-rounded p-l-10 p-r-10"><i
                      class="fa fa-fw fa-edit"></i>Edit</a>

                  <a href="javascript:void(0)" onclick="js_deleteContentSub('home/promotions','{{ $homePromotion->id }}')"
                    class="btn btn-danger btn-block btn-xs btn-rounded p-l-10 p-r-10"><i
                      class="fa fa-fw fa-trash"></i>Delete</a>
                  @endif
                </td>
              </tr>
              @endforeach
              @else
              <tr>
                <td colspan="7" class="text-danger text-center">--No Data--</td>
              </tr>
              @endif
            </tbody>
          </table>

          <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-5 col-sm-7">
              <div class="row">
                <div class="col-lg-2 col-md-4 col-sm-4 text-right">
                  {!! Form::select('option_limit_page', config('config.config_arr_limit_page'),
                  Session::get('limit_page.homePromotions'),
                  ["class"=>"form-control","id"=>"option_limit_page","onchange"=>"js_changeLimitPage('homePromotions',this.value)"]);
                  !!}
                </div>
                <div class="col-lg-10 col-md-8 col-sm-8">
                  Showing {{ $homePromotions->firstItem() }} to {{ $homePromotions->lastItem() }} of
                  {{ $homePromotions->total() }} entries
                </div>
              </div>
            </div>
            <!-- /.col-sm-5 -->
            <div class="col-sm-7 text-right">
              {{ $homePromotions->render() }}
            </div>
            <!-- /.col-sm-7 -->

          </div>
          <!-- /.row -->

        </div>
        <!-- /.box-header -->

      </div>
      <!-- /.box -->


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@stop


@section('page_script')
{!! Html::script('js/functionjs.js') !!}
@stop