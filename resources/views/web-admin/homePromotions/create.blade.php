@extends('layouts.web-admin.main')

@section('page_title', 'Home Promotions')
@section('page_action', 'Add Data')
@section('menu_active_home',' active ')
@section('submenu_active_promotions',' active ')


@section('page_style')
@stop

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/home/promotions')}}">Home Promotions</a></li>
  <li class="active">Add Data</li>
@stop


@section('content')

<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        {{-- @if($errors->any()) --}}
          <ul id="msgError" class="alert alert-danger hidden">
            {{-- @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach --}}
          </ul>
        {{-- @endif --}}

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::open(['url'=>config('config.config_pathCms').'/home/promotions','method'=>'POST', 'files' => true,'class'=>"form-horizontal","id"=>"frmHomePromotions"]) !!}
                @include('web-admin.homePromotions._form',['submitButtonText' => 'Save'])
              {!! Form::close() !!}


          </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
@stop