@extends('layouts.web-admin.main')

@section('page_title', 'Roles')
@section('page_action', 'Add Data')
@section('menu_active_users',' active ')
@section('submenu_active_role',' active ')

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/users/role')}}">Roles</a></li>
  <li class="active">Add Data</li>
@stop

@section('page_style')
@stop


@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        @if($errors->any())
          <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::open(['url'=>config('config.config_pathCms').'/users/role','method'=>'POST', 'files' => true,'class'=>"form-horizontal"]) !!}
                @include('web-admin.roles._form',['submitButtonText' => 'Save'])
              {!! Form::close() !!}


          </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
<!-- date-range-picker -->
{!! Html::script('plugins/daterangepicker/daterangepicker.js') !!}
<!-- bootstrap datepicker -->
{!! Html::script('plugins/datepicker/bootstrap-datepicker.js') !!}
<!-- bootstrap time picker -->
{!! Html::script('plugins/timepicker/bootstrap-timepicker.min.js') !!}

<script type="text/javascript">
$(function () {
    $('.datepicker').datepicker({
        format: 'dd/mm/yy'
    });
});
</script>
@stop
