@extends('layouts.web-admin.main')

@section('page_title', 'Roles')
@section('page_action', 'View Data')
@section('menu_active_users',' active ')
@section('submenu_active_role',' active ')

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/users/role')}}">Roles</a></li>
  <li class="active">View Data</li>
@stop

@section('page_style')
@stop


@section('content')
<!-- Content Header (Page header) -->

<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">

  <div class="row">
    <div class="col-md-12">

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

            {!! Form::model($roles, ['class'=>"form-horizontal"]) !!}

                <!--box-body other-->
                <div class="box-body">
                  <div class="form-group">
                    {!! Form::label("display_name", "Display name", ['class' => 'col-sm-2 control-label','for'=>'display_name']) !!}
                    <div class="col-sm-5">
                      {!! Form::text("display_name", null, ['class' => 'form-control','placeholder'=>"","disabled"=>true]) !!}
                    </div>
                  </div>

                  <div class="form-group">
                    {!! Form::label("name", "Name", ['class' => 'col-sm-2 control-label','for'=>'name']) !!}
                    <div class="col-sm-5">
                      {!! Form::text("name", null, ['class' => 'form-control','placeholder'=>"","disabled"=>true]) !!}
                    </div>
                  </div>

                   @foreach (config('config.config_arr_roleGroup') as $keyGroup=>$valueRole)
                    <div class="form-group">
                      <div class="col-sm-2">
                        <label class="checkbox-inline pull-right">
                          <input type="checkbox" class="chk_group" id="chk_group_{{$keyGroup}}" name="chk_group_{{$keyGroup}}" value="{{$keyGroup}}" disabled> {{$valueRole}}
                        </label>
                      </div>
                      <div class="col-sm-5">&nbsp;</div>
                    </div>

                    <div class="form-group">
                      <div class="col-sm-2">
                        &nbsp;
                      </div>
                      <div class="col-sm-10">
                        <div class="row">
                          
                            @if (isset($permissionName) && sizeof($permissionName)>0)
                              @foreach ($permissionName[$keyGroup] as $key=>$value)
                                <div class="col-sm-3">
                                  <label class="checkbox-inline">
                                    @if (isset($permissionRole[$key]) && $permissionRole[$key])
                                      <input type="checkbox" class="chk_sub chk_group_{{$keyGroup}}" rel="chk_group_{{$keyGroup}}" id="chk_sub_{{$key}}" checked name="chk_permission[{{$key}}]" value="{{$key}}" disabled> {{$value}}
                                    @else
                                      <input type="checkbox" class="chk_sub chk_group_{{$keyGroup}}" rel="chk_group_{{$keyGroup}}" id="chk_sub_{{$key}}" name="chk_permission[{{$key}}]" value="{{$key}}" disabled> {{$value}}
                                    @endif
                                  </label>
                                </div>
                              @endforeach
                            @endif
                          </div>
                        </div>
                      </div>
                  @endforeach

                </div>
                <!-- /.box-body Other -->



                <div class="box-footer">
                    <div class="form-group">
                      <div class="col-sm-2">&nbsp;</div>
                      <div class="col-sm-5">
                        <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/users/role') }}'" class="btn btn-default width-100"> Back</button>
                      </div>
                    </div>
                  </div>
                  <!-- /.box-footer -->

              {!! Form::close() !!}

          </div>

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop

@section('page_script')
<script type="text/javascript">
$(function () {
    //Flat red color scheme for iCheck
    /*$('input[type="radio"].flat-red').iCheck({
      radioClass: 'iradio_square-blue'
    });*/

});
</script>
@stop
