

  <!--.box-body Other-->

  <div class="box-body">



    <div class="form-group">

      {!! Form::label("display_name", "Display name", ['class' => 'col-sm-2 control-label','for'=>'display_name']) !!}

      <div class="col-sm-5">

        {!! Form::text("display_name", null, ['class' => 'form-control','placeholder'=>""]) !!}

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("name", "Name", ['class' => 'col-sm-2 control-label','for'=>'name']) !!}

      <div class="col-sm-5">

        {!! Form::text("name", null, ['class' => 'form-control','placeholder'=>""]) !!}

      </div>

    </div>





    

      @foreach (config('config.config_arr_roleGroup') as $keyGroup=>$valueRole)

        <div class="form-group">

          <div class="col-sm-offset-1 col-sm-3">

            <label class="checkbox-inline">

              <input type="checkbox" class="chk_group" id="chk_group_{{$keyGroup}}" name="chk_group_{{$keyGroup}}" value="{{$keyGroup}}"> {{$valueRole}}

            </label>

          </div>

          <div class="col-sm-8">&nbsp;</div>

        </div>



        <div class="form-group">

          <div class="col-sm-2">

            &nbsp;

          </div>

          <div class="col-sm-10">

            <div class="row">

              

                @if (isset($permissionName) && sizeof($permissionName)>0)

                  @foreach ($permissionName[$keyGroup] as $key=>$value)

                    <div class="col-sm-4">

                      <label class="checkbox-inline">

                        @if (isset($permissionRole[$key]) && $permissionRole[$key])

                          <input type="checkbox" class="chk_sub chk_group_{{$keyGroup}}" rel="chk_group_{{$keyGroup}}" id="chk_sub_{{$key}}" checked name="chk_permission[{{$key}}]" value="{{$key}}"> {{$value}}

                        @else

                          <input type="checkbox" class="chk_sub chk_group_{{$keyGroup}}" rel="chk_group_{{$keyGroup}}" id="chk_sub_{{$key}}" name="chk_permission[{{$key}}]" value="{{$key}}"> {{$value}}

                        @endif

                      </label>

                    </div>

                  @endforeach

                @endif

              </div>

            </div>

          </div>

      @endforeach



    </div>



    <div class="box-footer">

      <div class="form-group">

        <div class="col-sm-2">&nbsp;</div>

        <div class="col-sm-5">

          {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}

          <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/users/role') }}'" class="btn btn-default width-100">Cancel</button>

        </div>

      </div>

    </div>



  <!-- /.box-footer -->



<script type="text/javascript">

  $(document).ready(function() {

    $('.chk_group').each(function(index, el) {

      var rel_group = $(this).attr('id');

      js_chkGroup(rel_group);

    });





    $('.chk_sub').click(function(event) {

      /* Act on the event */

      var rel_group = $(this).attr('rel');

      js_chkGroup(rel_group);

    });



    $('.chk_group').click(function(event) {

      /* Act on the event */

      var rel_group = $(this).attr('id');

      if ($(this).is(":checked"))

      {

        $("input[rel="+rel_group+"]").prop('checked', true);

      }

      else

        $("input[rel="+rel_group+"]").prop('checked', false);

    });

  });



  function js_chkGroup(rel_group)

  {

      var bool = true;

      $('input[rel='+rel_group+']').each(function(index, el) {

       if (!$('.'+rel_group+':eq('+index+')').is(':checked'))

        bool = false; 

      });



      if (bool)

        $("#"+rel_group).prop('checked', true);

      else

        $("#"+rel_group).prop('checked', false);

  }

</script>