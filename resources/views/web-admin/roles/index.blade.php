@extends('layouts.web-admin.main')

@section('page_title', 'Roles')
@section('page_action', 'List Data')
@section('menu_active_users',' active ')
@section('submenu_active_role',' active ')

@section('page_style')
@stop


@section('content')
<!-- Content Header (Page header) -->

<section class="section-container section-with-top-border p-b-5">

  @include('layouts.web-admin.massage_block')

  <div class="row">
    <div class="col-md-12">

      <!-- begin panel -->
      <div class="panel">
        <div class="panel-heading">
          <div class="panel-heading-btn">
            &nbsp;
          </div>
          <h4 class="panel-title">Total number of Roles : {{ $roles->total() }}
                @if ($hasPermission["create-role"])
                  &nbsp;&nbsp;
                  <a href="javascript:void(0)" onclick="location='{{url(config('config.config_pathCms').'/users/role/create')}}'" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse" title="Add Data"><i class="fa fa-plus"></i></a>
                @endif</h4>
        </div>

        <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
          <table id="tbl_dataList" rel="roles" class="table table-bordered table-hover dataTable no-footer dtr-inline table-responsive">
          <thead>
            <tr>
              <th style="width: 58%">Roles</th>
              <th style="width: 15%">Modified</th>
              <th class="text-center" style="width: 27%">Process</th>
            </tr>
          </thead>
          <tbody>
            @if ($roles->count()>0)
              @foreach($roles as $role)
                <tr>
                    <td>{{$role->display_name}}</td>
                    <td>{{ \Carbon\Carbon::parse($role->updated_at)->diffForHumans() }}</td>
                    <td>
                        <a href="{{ url(config('config.config_pathCms').'/users/role/'.$role->id) }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-play"></i> View</a>
                        @if ($hasPermission["edit-role"])
                          <a href="{{ url(config('config.config_pathCms').'/users/role/'.$role->id.'/edit') }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-edit"></i> Edit</a>

                          <a href="#" onclick="js_deleteContent('users/role','{{ $role->id }}')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-trash"></i> Delete</a>

                        @endif
                    </td>
                </tr>
              @endforeach
            @else
              <tr><td colspan="5" class="text-red text-center">--No Data--</td></tr>
            @endif
          </tbody>
          </table>
        </div>
      </div>
      <!-- end panel -->

    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@stop


@section('page_script')
{!! Html::script('js/functionjs.js') !!}
@stop
