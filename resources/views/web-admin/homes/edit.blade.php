@extends('layouts.web-admin.main')

@section('page_title', 'Homes')
@section('page_action', 'Edit Data')
@section('menu_active_homes',' active ')

@section('page_style')
{!! Html::script('js/ckeditor/ckeditor.js') !!}
{!! Html::script('js/cke_config.js') !!}
@stop

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/homes')}}">Homes</a></li>
  <li class="active">Edit Data</li>
@stop

@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">
  @include('layouts.web-admin.massage_block')
  <div class="row">
    <div class="col-md-12">
        @if($errors->any())
          <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->

              {!! Form::model($homes, ['method' => 'put','class'=>"form-horizontal",'files' => true,'action'=>['Webadmin\HomesController@update', $homes->id]]) !!}
                  @include('web-admin.homes._form',['submitButtonText' => 'Save'])
              {!! Form::close() !!}

          </div>
    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop





@section('page_script')

<script type="text/javascript">

  $(function(){

      $(window).on("load",(function() {

        /* Act on the event */

          setTimeout(function(){

              $('.class_ckeditor').each(function(index, el) {

                CKEDITOR.replace($('.class_ckeditor:eq('+index+')').attr('id'), cke_config);

              });

          },200);

        })

      );

      

  });

</script>

@stop

