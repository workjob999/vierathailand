{!! Html::style('js/fileinput/fileinput.css') !!}
{!! Html::style('js/fileinput/themes/explorer/theme.css') !!}
{!! Html::script('js/fileinput/plugins/sortable.min.js') !!}
{!! Html::script('js/fileinput/fileinput.js') !!}
{!! Html::script('js/fileinput/themes/explorer/theme.js') !!}

{!! Html::style('css/theme.css') !!}
{!! Html::style('js/selectize/selectize.default.css') !!}

<!--.box-body Other-->
<div class="box-body">
  @if (isset($homes->thumb))
  @if ($homes->thumb!="")
  @if (file_exists(config('config.config_pathUpload').'/homes/'.$pathDate.'/'.$homes->thumb))
  <div id="thumb-{{ $homes->id }}" class="form-group">
    <div class="col-sm-2"></div>
    <div class="col-sm-5">
      <img src="{{ url(config('config.config_pathUpload').'/homes/'.$pathDate.'/'.$homes->thumb) }}" height="200"
        class="img-responsive"><br>
      <a href="javascript:void(0)" onclick="js_removeFile('homes','{{ $homes->id }}','thumb')"><i
          class="fa fa-fw fa-remove"></i>Remove</a>
    </div>
  </div>
  @endif
  @endif
  @endif

  <div class="form-group">
    {!! Form::label("thumb", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb']) !!}
    <div class="col-sm-5">
      {!! Form::file('thumb', ['class' => 'form-control']) !!}
    </div>
  </div>


  <div class="box-body">
    @if (isset($homes->banner))
    @if ($homes->banner!="")
    @if (file_exists(config('config.config_pathUpload').'/homes/'.$pathDate.'/'.$homes->banner))
    <div id="banner-{{ $homes->id }}" class="form-group hidden">
      <div class="col-sm-2"></div>
      <div class="col-sm-5">
        <img src="{{ url(config('config.config_pathUpload').'/homes/'.$pathDate.'/'.$homes->banner) }}" height="200"
          class="img-responsive"><br>
        <a href="javascript:void(0)" onclick="js_removeFile('homes','{{ $homes->id }}','banner')"><i
            class="fa fa-fw fa-remove"></i>Remove</a>
      </div>
    </div>
    @endif
    @endif
    @endif


    <div class="form-group hidden">
      {!! Form::label("banner", "Banner", ['class' => 'col-sm-2 control-label','for'=>'banner']) !!}
      <div class="col-sm-5">
        {!! Form::file('banner', ['class' => 'form-control']) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("title", "Title", ['class' => 'col-sm-2 control-label','for'=>'title']) !!}
      <div class="col-sm-5">
        {!! Form::text("title", null, ['class' => 'form-control','placeholder'=>""]) !!}
      </div>
    </div>


    <div class="form-group">
      {!! Form::label("status", "Status", ['class' => 'col-sm-2 control-label','for'=>'status']) !!}
      <div class="col-sm-5">
        @if (isset($homes->status))
        @if ($homes->status=="Y")
        {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
        @else
        {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
        @endif
        @else
        {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
        @endif
      </div>
    </div>

    <div class="form-group hidden">
      {!! Form::label("order_by", "Order", ['class' => 'col-sm-2 control-label','for'=>'order_by']) !!}
      <div class="col-sm-5">
        {!! Form::text("order_by", $arr_data["order_by"], ['class' =>
        'form-control','placeholder'=>"0",'readonly'=>true]) !!}
      </div>
    </div>

    <div class="form-group hidden">
      {!! Form::label("startDate", "Start Date", ['class' => 'col-sm-2 control-label','for'=>'startDate']) !!}
      <div class="col-sm-5">
        <div class="input-group date">
          <input type="text" id="startDate" value="{{$startDate}}" name="startDate" class="form-control">
        </div>
      </div>
    </div>

    <div class="form-group hidden">
      {!! Form::label("endDate", "End Date", ['class' => 'col-sm-2 control-label','for'=>'endDate']) !!}
      <div class="col-sm-5">
        <div class="input-group date">
          <input type="text" id="endDate" name="endDate" value="{{$endDate}}" class="form-control">
        </div>
      </div>
    </div>
    <!--end box-body Other-->


    <div class="box-header box box-success">
      <h3 class="box-title">Images</h3>
    </div>

    <div class="form-group">
      <div class="col-sm-12">
        <input id="images" name="images[]" type="file" multiple class="file-loading" accept="image/*">
      </div>
    </div>


    <div class="box-footer">
      <div class="form-group">
        <div class="col-sm-2">&nbsp;</div>
        <div class="col-sm-5">
          {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}
          <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/rooms/homes') }}'"
            class="btn btn-default width-100">Cancel</button>
        </div>
      </div>
    </div>
    <!-- /.box-footer -->


    <input type="hidden" id="hid_item_id" name="hid_item_id" value="
    @if (sizeof($home_items)>0)
        @foreach ($home_items as $home_item)
            @if ($loop->index>0)
                |$|
            @endif
            {{ ($home_item->order_by-1) }}
        @endforeach
      @endif">


    <div id="hid_item_thumb" class="hidden">
      @if (isset($arr_data["home_items_thumb"]))
      {{ $arr_data["home_items_thumb"] }}
      @endif
    </div>

    @if (isset($homes->id))
    {{ Form::hidden('hid_home_id', $homes->id, array('id' => 'hid_home_id')) }}
    @else
    {{ Form::hidden('hid_home_id', "", array('id' => 'hid_home_id')) }}
    @endif


    @if (isset($homes->thumb))
    {{ Form::hidden('thumbOld', $homes->thumb, array('id' => 'thumbOld')) }}
    @endif


    @if (isset($homes->banner))
    {{ Form::hidden('bannerOld', $homes->banner, array('id' => 'bannerOld')) }}
    @endif



    <script>
      $( function() {
        var config_dateFormat = "dd/mm/yy",
        from = $( "#startDate" )
          .datepicker({
            changeMonth: true,
            changeYear: true,
            yearRange: "1920:2028",
            dateFormat: config_dateFormat,
            numberOfMonths: 1,
            showOn: "both",//button
            buttonText: '<i class="fa fa-calendar"></i>'
          })
          .on( "change", function() {
            to.datepicker( "option", "minDate", getDate( this ) );
          }),
        to = $( "#endDate" ).datepicker({
          changeMonth: true,
          changeYear: true,
          yearRange: "1920:2028",
          dateFormat: config_dateFormat,
          numberOfMonths: 1,
          showOn: "both",//button
          buttonText: '<i class="fa fa-calendar"></i>'
        })
        .on( "change", function() {
          from.datepicker( "option", "maxDate", getDate( this ) );
        });

      function getDate( element ) {
        var date;
        try {
          date = $.datepicker.parseDate( config_dateFormat, element.value );
        } catch( error ) {
          date = null;
        }
        return date;
      }


      @if (isset($homes->id))
          var rand_item_id = {{ $homes->id }};
        @else
          var rand_item_id = parseInt((Math.random()*1000000)*-1);
        @endif


      /*Gallery*/
      var $el1 = $("#images");
      $el1.fileinput({
        initialPreview: [
            @if (sizeof($home_items)>0)
              @foreach($home_items as $item)
                @if (isset($item->thumb))
                  @if ($item->thumb!="")
                    @if (file_exists(config('config.config_pathUpload').'/homes/'.$item->pathDate."/".$item->thumb))
                        '{{ url(config("config.config_pathUpload")."/homes/".$item->pathDate."/".$item->thumb) }}',
                    @endif
                  @endif
                @endif
              @endforeach
            @endif
        ],
        initialPreviewAsData: true,
        initialPreviewConfig: [
          @if (sizeof($home_items)>0)
            @foreach($home_items as $item)
              @if (isset($item->thumb))
                @if ($item->thumb!="")
                  @if (file_exists(config('config.config_pathUpload').'/homes/'.$item->pathDate.'/'.$item->thumb))
                      {caption: "{{ $item->thumb }}", size: "{{ $item->size }}", width: "80px", key: "{{ $item->order_by }}"},
                  @endif
                @endif
              @endif
            @endforeach
          @endif
        ],
        uploadUrl: '{{url(config("config.config_pathCms")."/processdata/uploadAlbum/homes")}}',
        uploadExtraData: {_token:"{{csrf_token()}}",table:"home_items",item_id:rand_item_id,thumb:"homes",field_refer:"home_id"},
        deleteUrl: '{{url(config("config.config_pathCms")."/processdata/deleteAlbum/homes")}}',
        deleteExtraData : {_token:"{{csrf_token()}}",table:"home_items",item_id:rand_item_id,thumb:"homes",field_refer:"home_id"},
        uploadAsync : false,
        showUpload: false, // hide upload button
        showRemove: false, // hide remove button
        allowedFileTypes: ['image'],
        overwriteInitial: false,
        maxFileCount: 10,
    });

    //upload batch error
    $el1.on('filebatchuploaderror', function(event, data, previewId, index) {
      var form = data.form, files = data.files, extra = data.extra,
      response = data.response, reader = data.reader;
    });
    //upload batch error

    //upload batch success
    $el1.on('filebatchuploadsuccess', function(event, data, previewId, index) {
      var form = data.form, files = data.files, extra = data.extra,
      response = data.response, reader = data.reader;

      var html_thumb = "";
      $(".kv-preview-thumb").each(function(indexEach, el) {
        html_thumb += '<input type="hidden" name="hid_images[]" id="hid_'+$(".kv-preview-thumb:eq("+indexEach+")").attr("id")+'" value="'+indexEach+'" >';
      });

      $("#hid_item_thumb").html(html_thumb);
      $('#hid_home_id').val(rand_item_id);

      var split_html = response.html_result.split("|$|");
      $('#hid_item_id').html($('#hid_item_id').html()+"|$|"+split_html[0]);
    });
    //upload batch success


    //upload uploaded
    $el1.on('fileuploaded', function(event, data, previewId, index) {
      var response = data.response;
      var html_thumb = "";

      $(".kv-preview-thumb").each(function(indexEach, el) {
        html_thumb += '<input type="hidden" name="hid_images[]" id="hid_'+$(".kv-preview-thumb:eq("+indexEach+")").attr("id")+'" value="'+indexEach+'" >';
      });

      $("#hid_item_thumb").html(html_thumb);
      $('#hid_home_id').val(rand_item_id);

      var split_html = response.html_result.split("|$|");
      $('#hid_item_id').html($('#hid_item_id').html()+"|$|"+split_html[0]);
    });
    //upload uploaded

    //remove success
    $el1.on('filesuccessremove', function(event, id) {
      var index_delete = 0;
      $(".kv-preview-thumb").each(function(indexEach, el) {
        if (id==$(".kv-preview-thumb:eq("+indexEach+")").attr("id"))
          index_delete = indexEach;
      });

      $.ajax({
        url: '{{ config("config.config_pathWeb") }}/{{ config("config.config_pathCms") }}/processdata/deleteAlbum/homes',
        type: 'POST',
        data: '_token={{csrf_token()}}&table=home_items&item_id='+rand_item_id+'&field_refer=home_id&thumb=homes&key='+index_delete,
        success: function(data){
          $(".kv-preview-thumb:eq("+index_delete+")").remove();

          var html_thumb = "";
          $(".kv-preview-thumb").each(function(indexEach, el) {
            html_thumb += '<input type="hidden" name="hid_images[]" id="hid_'+$(".kv-preview-thumb:eq("+indexEach+")").attr("id")+'" value="'+indexEach+'" >';
          });
          $("#hid_item_thumb").html(html_thumb);
        }
      });
      //end ajax
    });
    //remove success


    $el1.on("filepredelete", function(jqXHR) {
        var abort = true;
        if (confirm("Are you sure you want to delete this image?")) {
            abort = false;
        }
        return abort; // you can also send any data/object that you can receive on `filecustomerror` event
    });

    $el1.on('fileclear', function(event) {
      //alert("test7");
    });

    $el1.on('fileremoved', function(event, id, index) {
      //alert("test8");
    });

    $el1.on("filebatchselected", function(event, files) {
      $el1.fileinput("upload");
    });

    $('.file-preview-thumbnails').sortable({
      items: '.file-preview-frame',
      tolerance: 'pointer',
      placeholder: 'ui-state-highlight',
      update: function(event, ui) {
        var html = "";
        var arr_split_data = "";

        $('.file-preview-thumbnails .file-preview-frame').each(function( index, value ){
          var file_preview = $('.file-preview-thumbnails .file-preview-frame:eq('+index+')');
          arr_split_data = file_preview.attr("id").split("-");
          if (arr_split_data[0].trim()=="preview")
          {
            arr_split_data = file_preview.attr("id").split("_");
            if (html!="")
              html += "|$|";
            html += arr_split_data[1];
          }
        });

        $("#hid_item_id").val(html);
      }
    });
    /*end Gallery*/

  });

function sortableEnable(htmlselector) {
    $( htmlselector ).sortable();
    $( htmlselector).sortable( "option", "disabled", false );
    // ^^^ this is required otherwise re-enabling sortable will not work!
    $( htmlselector ).disableSelection();
    return false;
  }
  function sortableDisable(htmlselector) {
    $( htmlselector ).sortable("disable");
    return false;
  }

    </script>