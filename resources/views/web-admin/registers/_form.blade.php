
<!--.box-body Other-->
  <div class="box-body">

    @if (isset($registers->thumb))
      @if ($registers->thumb!="")
        @if (file_exists(config('config.config_pathUpload').'/registers/'.$pathDate.'/'.$registers->thumb))
          <div id="thumb-{{ $registers->id }}" class="form-group">
            <div class="col-sm-2"></div>
            <div class="col-sm-5">
              <img src="{{ url(config('config.config_pathUpload').'/registers/'.$pathDate.'/'.$registers->thumb) }}" height="200" class="img-responsive"><br>
              @if (isset($action_page) && $action_page!="view")
                <a href="javascript:void(0)" onclick="js_removeFile('registers','{{ $registers->id }}','thumb')"><i class="fa fa-fw fa-remove"></i>Remove</a>
              @endif
            </div>
          </div>
        @endif
      @endif
    @endif


    <div class="form-group">
      {!! Form::label("thumb", "Thumb", ['class' => 'col-sm-2 control-label','for'=>'thumb']) !!}
      <div class="col-sm-5">
        {!! Form::file('thumb', ['class' => 'form-control','accept'=>"image/jpeg,image/png,image/gif","disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("type", "Type", ['class' => 'col-sm-2 control-label','for'=>'type']) !!}
      <div class="col-sm-5">
        {!! Form::select('type',  config('config.config_arr_type'), null, ['class' => 'form-control',"id"=>"inputType","disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div id="divCode"class="form-group {{(isset($registers->type) && @$registers->type!=1)?'hidden':''}}">
      {!! Form::label("code", "Code", ['class' => 'col-sm-2 control-label','for'=>'code']) !!}
      <div class="col-sm-5">
        {!! Form::text("code", null, ['class' => 'form-control','placeholder'=>"",'id'=>'code',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("fullname", "Fullname", ['class' => 'col-sm-2 control-label','for'=>'fullname']) !!}
      <div class="col-sm-5">
        {!! Form::text("fullname", null, ['class' => 'form-control','placeholder'=>"",'id'=>'fullname',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("phone", "phone", ['class' => 'col-sm-2 control-label','for'=>'phone']) !!}
      <div class="col-sm-5">
        {!! Form::text("phone", null, ['class' => 'form-control class_keyNumber class_phone','placeholder'=>"",'id'=>'phone',"maxlength"=>10,"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("email", "email", ['class' => 'col-sm-2 control-label','for'=>'email']) !!}
      <div class="col-sm-5">
        {!! Form::text("email", null, ['class' => 'form-control','placeholder'=>"",'id'=>'email',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("lineid", "lineid", ['class' => 'col-sm-2 control-label','for'=>'lineid']) !!}
      <div class="col-sm-5">
        {!! Form::text("lineid", null, ['class' => 'form-control','placeholder'=>"",'id'=>'lineid',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("refer", "refer", ['class' => 'col-sm-2 control-label','for'=>'refer']) !!}
      <div class="col-sm-5">
        {!! Form::text("refer", null, ['class' => 'form-control','placeholder'=>"",'id'=>'refer',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>

    <div class="form-group">
      {!! Form::label("sale_place", "Sale place", ['class' => 'col-sm-2 control-label','for'=>'sale_place']) !!}
      <div class="col-sm-5">
        {!! Form::text("sale_place", null, ['class' => 'form-control','placeholder'=>"",'id'=>'sale_place',"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>


    <div class="form-group">
      {!! Form::label("status", "Status", ['class' => 'col-sm-2 control-label','for'=>'status']) !!}
      <div class="col-sm-5">
        @if (isset($action_page) && $action_page=="view")
            @if (isset($registers->status))
                @if ($registers->status=="Y")
                  {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
                @else
                  {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
                @endif
              @else
                {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}
              @endif
        @else
          @if (isset($registers->status))
                @if ($registers->status=="Y")
                  {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
                @else
                  {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
                @endif
              @else
                {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}
              @endif
        @endif
      </div>
    </div>


    <div class="form-group hidden">
      {!! Form::label("order_by", "Order", ['class' => 'col-sm-2 control-label','for'=>'order_by']) !!}
      <div class="col-sm-5">
        @if (isset($action_page) && $action_page=="view")
          {!! Form::text("order_by", null, ['class' => 'form-control','placeholder'=>"0",'readonly'=>true]) !!}
        @else
          {!! Form::text("order_by", $arr_data["order_by"], ['class' => 'form-control','placeholder'=>"0",'readonly'=>true]) !!}
        @endif
      </div>
    </div>


    <div class="form-group">
      {!! Form::label("facebook", "Facebook", ['class' => 'col-sm-2 control-label','for'=>'meta_description']) !!}
      <div class="col-sm-5">
        {!! Form::textarea('facebook', null, ['class' => 'form-control','size' => '50x3','placeholder'=>"",'id'=>'facebook','maxlength'=>200,"disabled"=>@$arr_data['disabled_data']]) !!}
      </div>
    </div>


  </div>
  <!-- /.box-body Other-->

  <div class="box-footer">
    <div class="form-group">
      <div class="col-sm-2">&nbsp;</div>
      <div class="col-sm-7">
        @if (isset($action_page) && $action_page=="view")
          <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/dealer/registers') }}'" class="btn btn-primary width-100 m-r-5"><< Back</button>
        @else
          {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}
          <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/dealer/registers') }}'" class="btn btn-default width-100 m-r-5">Cancel</button>
        @endif
      </div>
    </div>
  </div>
  <!-- /.box-footer -->

  @if (isset($registers->thumb))
    {{ Form::hidden('old_thumb', $registers->thumb, array('id' => 'old_thumb')) }}
  @endif

  <script type="text/javascript">
    $(document).ready(function(){
      $("#inputType").change(function(data){
        // alert("Test1=="+$(this).val());
        $("#divCode").removeClass("hidden");
        if ($(this).val()!=1)
          $("#divCode").addClass("hidden");
      })
    })
  </script>