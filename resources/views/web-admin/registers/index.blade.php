@extends('layouts.web-admin.main')

@section('page_title', 'Registers')
@section('page_action', 'List Data')
@section('menu_active_dealer',' active ')
@section('submenu_active_registers',' active ')

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/dealer/registers')}}">Registers</a></li>
  <li class="active">List Data</li>
@stop

@section('page_style')
@stop


@section('content')
<!-- Content Header (Page header) -->

<section class="section-container section-with-top-border p-b-5">

  @include('layouts.web-admin.massage_block')

  <div class="row">
    <div class="col-md-12">


      <div class="panel">
        <div class="panel-heading">

          <div class="panel-heading-btn">
            <div class="pull-right">
              <button class="btn btn-default-sm" type="button" onclick="js_searchData('dealer/registers')">
                  <i class="fa fa-search"></i>
              </button>
            </div>
            <div class="pull-right">
              @if (isset($arr_data["search"]))
                <input type="text" class="form-control" name="search" id="search" value="{{ $arr_data['search'] }}"  placeholder="Search...">
              @else
                <input type="text" class="form-control" name="search" id="search"  placeholder="Search...">
              @endif
            </div>
          </div>
          
          <h4 class="panel-title">
              Total number of Registers : {{ $registers->total() }}
                @if ($hasPermission["create-registers"])
                  &nbsp;&nbsp;
                  <a href="javascript:void(0)" onclick="location='{{url(config('config.config_pathCms').'/dealer/registers/create')}}'" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse" title="Add Data"><i class="fa fa-plus"></i></a>
                @endif
                @if ($hasPermission["edit-registers"])
                  &nbsp;&nbsp;
                  <a href="javascript:void(0)" onclick="js_deleteAllSubContent('dealer/registers')" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-collapse" title="Delete Data"><i class="fas fa-minus"></i></a>
                  
                @endif
                &nbsp;&nbsp;
                <a href="javascript:void(0)" onclick="js_excelAllSubContent('dealer/registers')" class="btn btn-icon btn-info" data-click="panel-collapse" title="export Data"><i class="fas fa-download"></i> Export Data</a>
          </h4>
        </div>

        <!-- /.box-header -->
        <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

          <table id="tbl_dataList" rel="registers" class="table table-bordered table-hover dataTable no-footer dtr-inline table-responsive">
            <thead>
              <tr>
                <th class="text-center" style="width: 4%">
                  <p class=""><input type="checkbox" id="chk_all" onclick="js_chkAll()" class="chk_all" value="Y"></p>
                </th>
                <th style="width: 40%">
                    <p class="pull-left">ชื่อ-นามสกุล</p>
                    <p class="pull-right">
                    @if ($arr_data["orderByUrl"] == "fullname")
                        @if ($arr_data["ascdesc"] == "ASC")
                            <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/fullname/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc"></i></a>
                        @else
                            <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/fullname/ASC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-desc"></i></a>
                        @endif
                    @else
                        <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/fullname/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc text-muted"></i></a>
                    @endif
                    </p>
                </th>
                <th style="width: 7%">
                  <p class="pull-left">Code</p>
                  <p class="pull-right">
                  @if ($arr_data["orderByUrl"] == "code")
                      @if ($arr_data["ascdesc"] == "ASC")
                          <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/code/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc"></i></a>
                      @else
                          <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/code/ASC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-desc"></i></a>
                      @endif
                  @else
                      <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/code/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc text-muted"></i></a>
                  @endif
                  </p>
              </th>
                <th style="width: 6%"><p class="pull-left">Status</p></th>
                <th style="width: 14%">
                    <p class="pull-left">Modified</p>
                    <p class="pull-right">
                    @if ($arr_data["orderByUrl"] == "modify")
                        @if ($arr_data["ascdesc"] == "ASC")
                            <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/modify/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc"></i></a>
                        @else
                            <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/modify/ASC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-desc"></i></a>
                        @endif
                    @else
                        <a href="{{ url(config('config.config_pathCms').'/dealer/registers') }}/modify/DESC{{ $arr_data['urlSearch'] }}?page={{ $registers->currentPage() }}" class="pull-right"><i class="fa fa-fw fa-sort-amount-asc text-muted"></i></a>
                    @endif
                    </p>
                </th>
                <th class="text-center" style="width: 29%"><p class="">Process</p></th>
              </tr>
            </thead>
            <tbody>
            @if ($registers->count()>0)
                @foreach($registers as $register)
                  <tr class="tr_sorted" rel="tr_{{ $register->id }}">
                    <td class="text-center">
                        <input type="checkbox" id="chk_{{ $register->id }}" name="chk_{{ $register->id }}" class="class_chk" value="{{ $register->id }}"></input>
                    </td>
                    <td>
                      {{ $register->fullname }}
                    </td>
                    <td>
                      {{ $register->code }}
                    </td>
                    <td>
                        @if (!$hasPermission["edit-registers"])
                          <a id="btn_statusDataActive-{{$register->id}}" href="javascript:void(0)" class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10"> {{ config('config.config_arr_status')["Y"] }}</a>
                        @else
                          @if ($register->status==="Y")
                              <a id="btn_statusDataActive-{{$register->id}}" href="javascript:void(0)" onclick="js_changeStatus('registers',{{ $register->id }},'Y')" class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10"> {{ config('config.config_arr_status')["Y"] }}</a>

                              <a id="btn_statusDataInactive-{{$register->id}}" href="javascript:void(0)" onclick="js_changeStatus('registers',{{ $register->id }},'N')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 hide"> {{ config('config.config_arr_status')["N"] }}</a>

                          @else
                              <a id="btn_statusDataActive-{{$register->id}}" href="javascript:void(0)" onclick="js_changeStatus('registers',{{ $register->id }},'Y')" class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10 hide"> {{ config('config.config_arr_status')["Y"] }}</a>

                              <a id="btn_statusDataInactive-{{$register->id}}" href="javascript:void(0)" onclick="js_changeStatus('registers',{{ $register->id }},'N')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 "> {{ config('config.config_arr_status')["N"] }}</a>
                          @endif
                        @endif
                    </td>
                    <td>
                      {{ \Carbon\Carbon::parse($register->updated_at)->diffForHumans() }}<br>
                      @if (isset($getUsersData[$register->updated_by]["name"]) && $getUsersData[$register->updated_by]["name"]!="")
                          Edit By : {{$getUsersData[$register->updated_by]["name"]}}
                      @endif
                    </td>
                    <td>
                      <a href="{{ url(config('config.config_pathCms').'/dealer/registers/'.$register->id) }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-play"></i>View</a>
                        @if ($hasPermission["edit-registers"])
                          <a href="{{ url(config('config.config_pathCms').'/dealer/registers/'.$register->id.'/edit') }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-edit"></i>Edit</a>

                          <a href="javascript:void(0)" onclick="js_deleteContentSub('dealer/registers','{{ $register->id }}')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-trash"></i>Delete</a>
                        @endif
                    </td>
                  </tr>
                @endforeach
            @else
                <tr><td colspan="6" class="text-danger text-center">--No Data--</td></tr>
            @endif
          </tbody>
          </table>

          <div class="row" style="margin-bottom: 10px;">
            <div class="col-md-5 col-sm-7">
                <div class="row">
                  <div class="col-lg-2 col-md-4 col-sm-4 text-right">
                      {!! Form::select('option_limit_page', config('config.config_arr_limit_page'), Session::get('limit_page.registers'), ["class"=>"form-control","id"=>"option_limit_page","onchange"=>"js_changeLimitPage('registers',this.value)"]); !!}
                  </div>
                  <div class="col-lg-10 col-md-8 col-sm-8">
                    Showing {{ $registers->firstItem() }} to {{ $registers->lastItem() }} of {{ $registers->total() }} entries
                </div>
              </div>
            </div>
            <!-- /.col-sm-5 -->
            <div class="col-sm-7 text-right">
                  {{ $registers->render() }}
            </div>
              <!-- /.col-sm-7 -->

          </div>
          <!-- /.row -->

        </div>
        <!-- /.box-header -->

      </div>
      <!-- /.box -->


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->

@stop


@section('page_script')
{!! Html::script('js/functionjs.js') !!}
@stop
