

  {!! Html::script('js/password-strength-meter/password.js') !!}

  {!! Html::style('js/password-strength-meter/password.css') !!}





  <!--.box-body Other-->

  <div class="box-body">



    <div class="form-group">

      {!! Form::label("group", "Group", ['class' => 'col-sm-2 control-label','for'=>'username']) !!}

      <div class="col-sm-5">

        <select name="group" id="group" class="form-control">

          @if (isset($groups) && sizeof($groups))

            @foreach ($groups as $group)

              @if(isset($users))

                @if ($group->id==$role_user[$users->id])

                  <option value="{{$group->id}}" selected>{{$group->display_name}}</option>

                @else

                  <option value="{{$group->id}}">{{$group->display_name}}</option>

                @endif

              @else

                <option value="{{$group->id}}">{{$group->display_name}}</option>

              @endif

            @endforeach

          @endif

        </select>

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("username", "Username", ['class' => 'col-sm-2 control-label','for'=>'username']) !!}

      <div class="col-sm-5">

        {!! Form::text("username", null, ['class' => 'form-control','placeholder'=>""]) !!}

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("name", "Name", ['class' => 'col-sm-2 control-label','for'=>'name']) !!}

      <div class="col-sm-5">

        {!! Form::text("name", null, ['class' => 'form-control','placeholder'=>""]) !!}

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("email", "Email", ['class' => 'col-sm-2 control-label','for'=>'email']) !!}

      <div class="col-sm-5">

        {!! Form::text("email", null, ['class' => 'form-control','placeholder'=>""]) !!}

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("password", "Password", ['class' => 'col-sm-2 control-label','for'=>'password']) !!}

      <div class="col-sm-5">

        {!! Form::password("password", ['class' => 'form-control']) !!}

      </div>

    </div>



    <div class="form-group">

      {!! Form::label("status", "Status", ['class' => 'col-sm-2 control-label','for'=>'status']) !!}

      <div class="col-sm-5">

            @if (isset($users->status))

                @if ($users->status=="Y")

                  {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}

                @else

                  {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary']); !!}

                @endif

              @else

                {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary']); !!}

              @endif

      </div>

    </div>

</div>





  <div class="box-footer">

    <div class="form-group">

      <div class="col-sm-2">&nbsp;</div>

      <div class="col-sm-5">

        {!! Form::submit($submitButtonText, ['class'=>'btn btn-success width-100 m-r-5']) !!}

        <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/users/user') }}'" class="btn btn-default width-100">Cancel</button>

      </div>

    </div>

  </div>

  <!-- /.box-footer -->



  @if (isset($users->user))

    {{ Form::hidden('userOld', $users->user, array('id' => 'userOld')) }}

  @endif



  <script type="text/javascript">

    $(document).ready(function() {

      $('#password').password({ 

        minimumLength: 4

      });

    });

  </script>