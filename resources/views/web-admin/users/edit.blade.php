@extends('layouts.web-admin.main')

@section('page_title', 'Users')
@section('page_action', 'Add Data')
@section('menu_active_users',' active ')
@section('submenu_active_user',' active ')

@section('page_style')
@stop


@section('content')
<!-- Main content -->
<section class="section-container section-with-top-border p-b-5">
  <div class="row">
    <div class="col-xs-12">

        @if($errors->any())
          <ul class="alert alert-danger">
            @foreach($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
          </ul>
        @endif

        <div class="panel p-20">
            <!-- /.box-header -->
            <!-- form start -->
            <!--{!! Form::model($users, ['method' => 'post','class'=>"form-horizontal",'files' => true]) !!}

            {!! Form::close() !!}-->

            {!! Form::model($users, ['method' => 'put','class'=>"form-horizontal",'files' => true,'action'=>['Webadmin\UserController@update', $users->id]]) !!}
              @include('web-admin.users._form',['submitButtonText' => 'Save'])
            {!! Form::close() !!}


        </div>


    </div>
    <!-- /.col -->
  </div>
  <!-- /.row -->
</section>
<!-- /.content -->
@stop


@section('page_script')
<!-- date-range-picker -->
{!! Html::script('plugins/daterangepicker/daterangepicker.js') !!}
<!-- bootstrap datepicker -->
{!! Html::script('plugins/datepicker/bootstrap-datepicker.js') !!}
<!-- bootstrap time picker -->
{!! Html::script('plugins/timepicker/bootstrap-timepicker.min.js') !!}
@stop
