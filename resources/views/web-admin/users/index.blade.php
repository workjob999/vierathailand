@extends('layouts.web-admin.main')

@section('page_title', 'Users')
@section('page_action', 'List Data')
@section('menu_active_users',' active ')
@section('submenu_active_user',' active ')

@section('page_nav')
  <li><a href="{{url(config('config.config_pathCms').'/users/user')}}">Users</a></li>
  <li class="active">List Data</li>
@stop

@section('page_style')
@stop


@section('content')

<!-- Content Header (Page header) -->



<section class="section-container section-with-top-border p-b-5">



  @include('layouts.web-admin.massage_block')



  <div class="row">

    <div class="col-md-12">



      <!-- begin panel -->

      <div class="panel">

        <div class="panel-heading">

          <h4 class="panel-title">

              Total number of Users : {{ $users_db->total() }}

                @if ($hasPermission["create-user"])

                  &nbsp;&nbsp;

                  <a href="javascript:void(0)" onclick="location='{{url(config('config.config_pathCms').'/users/user/create')}}'" class="btn btn-xs btn-icon btn-circle btn-grey" data-click="panel-collapse" title="Add Data"><i class="fa fa-plus"></i></a>

                @endif

          </h4>



        </div>



        <div class="clear"></div>



        <div id="data-table_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">

          <table id="tbl_dataList" rel="users" class="table table-bordered table-hover dataTable no-footer dtr-inline table-responsive">

          <thead>

            <tr>

              <th style="width: 29%">Username</th>

              <th style="width: 24%">Email</th>

              <th style="width: 6%">Status</th>

              <th style="width: 14%">Modified</th>

              <th class="text-center" style="width: 27%">Process</th>

            </tr>

          </thead>

          <tbody>

            @if ($users_db->count()>0)

              @foreach($users_db as $user)

                <tr>

                    <td>{{$user->username}}</td>

                    <td>{{$user->email}}</td>

                    <td>

                      @if ($user->status==="Y")

                          <a id="btn_statusDataActive-{{$user->id}}" href="javascript:void(0)" onclick="js_changeStatus('users',{{ $user->id }},'Y')" class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10"> {{ config('config.config_arr_status')["Y"] }}</a>



                          <a id="btn_statusDataInactive-{{$user->id}}" href="javascript:void(0)" onclick="js_changeStatus('users',{{ $user->id }},'N')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 hide"> {{ config('config.config_arr_status')["N"] }}</a>



                      @else

                          <a id="btn_statusDataActive-{{$user->id}}" href="javascript:void(0)" onclick="js_changeStatus('users',{{ $user->id }},'Y')" class="btn btn-success btn-xs btn-rounded p-l-10 p-r-10 hide"> {{ config('config.config_arr_status')["Y"] }}</a>



                          <a id="btn_statusDataInactive-{{$user->id}}" href="javascript:void(0)" onclick="js_changeStatus('users',{{ $user->id }},'N')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10 "> {{ config('config.config_arr_status')["N"] }}</a>

                      @endif

                    </td>

                    <td>{{ \Carbon\Carbon::parse($user->updated_at)->diffForHumans() }}</td>

                    <td>

                        <a href="{{ url(config('config.config_pathCms').'/users/user/'.$user->id) }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-play"></i> View</a>

                        @if ($hasPermission["edit-user"])

                          <a href="{{ url(config('config.config_pathCms').'/users/user/'.$user->id.'/edit') }}" class="btn btn-default btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-edit"></i> Edit</a>



                          <a href="javascript:void(0)" onclick="js_deleteContent('users/user','{{ $user->id }}')" class="btn btn-danger btn-xs btn-rounded p-l-10 p-r-10"><i class="fa fa-fw fa-trash"></i> Delete</a>

                        @endif

                    </td>

                </tr>

              @endforeach

            @else

              <tr><td colspan="5" class="text-red text-center">--No Data--</td></tr>

            @endif

          </tbody>

          </table>

        </div>

      </div>

      <!-- end panel -->



    </div>

    <!-- /.col -->

  </div>

  <!-- /.row -->

</section>

<!-- /.content -->



@stop





@section('page_script')

{!! Html::script('js/functionjs.js') !!}

@stop

