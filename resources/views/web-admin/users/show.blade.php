@extends('layouts.web-admin.main')



@section('page_title', 'Users')

@section('page_action', 'View Data')

@section('menu_active_users',' active ')

@section('submenu_active_user',' active ')



@section('page_nav')

  <li><a href="{{url(config('config.config_pathCms').'/users/user')}}">Users</a></li>

  <li class="active">Show Data</li>

@stop



@section('page_style')

@stop



@section('content')

<!-- Content Header (Page header) -->



<!-- Main content -->

<section class="section-container section-with-top-border p-b-5">



  <div class="row">

    <div class="col-md-12">

          <div class="panel p-20">

            <!-- /.box-header -->

            <!-- form start -->



            {!! Form::model($users, ['class'=>"form-horizontal"]) !!}



                <!--box-body other-->

                <div class="box-body">

                  <div class="form-group">

                    {!! Form::label("group", "Group", ['class' => 'col-sm-2 control-label','for'=>'username']) !!}

                    <div class="col-sm-5">

                      <select name="group" id="group" class="form-control" disabled>

                        @if (isset($groups) && sizeof($groups))

                          @foreach ($groups as $group)

                            @if(isset($users))

                              @if ($group->id==$role_user[$users->id])

                                <option value="{{$group->id}}" selected>{{$group->display_name}}</option>

                              @else

                                <option value="{{$group->id}}">{{$group->display_name}}</option>

                              @endif

                            @else

                              <option value="{{$group->id}}">{{$group->display_name}}</option>

                            @endif

                          @endforeach

                        @endif

                      </select>

                    </div>

                  </div>



                  <div class="form-group">

                    {!! Form::label("username", "Username", ['class' => 'col-sm-2 control-label','for'=>'title']) !!}

                    <div class="col-sm-5">

                      {!! Form::text("username", null, ['class' => 'form-control','placeholder'=>"","disabled"=>true]) !!}

                    </div>

                  </div>



                  <div class="form-group">

                    {!! Form::label("name", "Name", ['class' => 'col-sm-2 control-label','for'=>'detail']) !!}

                    <div class="col-sm-5">

                      {!! Form::text("name", null, ['class' => 'form-control','placeholder'=>"","disabled"=>true]) !!}

                    </div>

                  </div>



                  <div class="form-group">

                    {!! Form::label("email", "Email", ['class' => 'col-sm-2 control-label','for'=>'detail']) !!}

                    <div class="col-sm-5">

                      {!! Form::text("email", null, ['class' => 'form-control','placeholder'=>"","disabled"=>true]) !!}

                    </div>

                  </div>





                  <div class="form-group">

                    {!! Form::label("status", "Status", ['class' => 'col-sm-2 control-label','for'=>'status']) !!}

                    <div class="col-sm-5">

                          @if (isset($users->status))

                            @if ($users->status=="Y")

                              {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}

                            @else

                              {!! Form::checkbox('status', 'Y', false, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}

                            @endif

                          @else

                            {!! Form::checkbox('status', 'Y', true, ['data-render' => 'switchery','data-theme'=>'primary','data-disabled'=>'true']); !!}

                          @endif

                        </div>

                    </div>



                </div>

                <!-- /.box-body Other -->







                  <div class="box-footer">

                    <div class="form-group">

                      <div class="col-sm-2">&nbsp;</div>

                      <div class="col-sm-5">

                        <button type="button" onclick="location='{{ url(config('config.config_pathCms').'/users/user') }}'" class="btn btn-default width-100"> Back</button>

                      </div>

                    </div>

                  </div>

                  <!-- /.box-footer -->



              {!! Form::close() !!}



          </div>



    </div>

    <!-- /.col -->

  </div>

  <!-- /.row -->

</section>

<!-- /.content -->

@stop



@section('page_script')

<script type="text/javascript">

$(function () {

    //Flat red color scheme for iCheck

    /*$('input[type="radio"].flat-red').iCheck({

      radioClass: 'iradio_square-blue'

    });*/



});

</script>

@stop

