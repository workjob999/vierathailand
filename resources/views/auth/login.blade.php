<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
  <meta charset="utf-8" />
  <title>Login | {{ config('config.company_name') }}</title>
  <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
  <meta content="" name="description" />
  <meta content="" name="author" />
  
  <!-- ================== BEGIN BASE CSS STYLE ================== -->
  <link href="http://fonts.googleapis.com/css?family=Nunito:400,300,700" rel="stylesheet" id="fontFamilySrc" />

  {!! Html::style('assets/plugins/jquery-ui/themes/base/minified/jquery-ui.min.css') !!}
    {!! Html::style('assets/plugins/bootstrap/css/bootstrap.min.css') !!}
    {!! Html::style('assets/plugins/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('assets/css/animate.min.css') !!}
    {!! Html::style('assets/css/style.min.css') !!}
  <!-- ================== END BASE CSS STYLE ================== -->
    
  <!-- ================== BEGIN BASE JS ================== -->
  {!! Html::script('assets/plugins/jquery/jquery-1.9.1.min.js') !!}
  {!! Html::script('assets/plugins/pace/pace.min.js') !!}
  <!-- ================== END BASE JS ================== -->
  
  <!--[if lt IE 9]>
      {!! Html::script('assets/crossbrowserjs/excanvas.min.js') !!}
    <![endif]-->
</head>

<body class="pace-top">
  <!-- begin #page-loader -->
  <div id="page-loader" class="page-loader fade in"><span class="spinner">Loading...</span></div>
  <!-- end #page-loader -->

  <!-- begin #page-container -->
  <div id="page-container" class="fade page-container">
      <!-- begin login -->
    <div class="login">
        <!-- begin login-brand -->
            <div class="login-brand bg-inverse text-white">
                <img src="assets/img/logo-white.png" height="36" class="pull-right" alt="" /> Login Panel
            </div>
        <!-- end login-brand -->
        <!-- begin login-content -->
        <form action="{{ route('login') }}" role="form" method="POST">
          {{ csrf_field() }}

            <div class="login-content">
                <h4 class="text-center m-t-0 m-b-20">Great to have you back!</h4>
                <form action="index.html" method="POST" name="login_form" class="form-input-flat">
                    @if ($errors->has('username'))
                        <span class="help-block">
                            <strong>{{ $errors->first('username') }}</strong>
                        </span>
                    @endif
                    @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif

                    <div class="form-group">
                        <input type="text" id="username" name="username" class="form-control input-lg" placeholder="Username" value="{{ old('username') }}" required autofocus>
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control input-lg" placeholder="Password" required>
                    </div>
                    <div class="row m-b-20">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-lime btn-lg btn-block">Sign in to your account</button>
                        </div>
                    </div>
                    <div class="text-center">
                        New here? <a href="extra_register.html" class="text-muted">Create a new account</a>
                    </div>
                </form>
            </div>
        </form>
        <!-- end login-content -->
    </div>
    <!-- end login -->
  </div>
  <!-- end page container -->


  <!-- ================== BEGIN BASE JS ================== -->
  {!! Html::script('assets/plugins/jquery/jquery-migrate-1.1.0.min.js') !!}
  {!! Html::script('assets/plugins/jquery-ui/ui/minified/jquery-ui.min.js') !!}
  {!! Html::script('assets/plugins/bootstrap/js/bootstrap.min.js') !!}  
  <!--[if lt IE 9]>
    {!! Html::script('assets/crossbrowserjs/html5shiv.js') !!}
    {!! Html::script('assets/crossbrowserjs/respond.min.js') !!}
  <![endif]-->
  {!! Html::script('assets/plugins/slimscroll/jquery.slimscroll.min.js') !!}
  {!! Html::script('assets/plugins/jquery-cookie/jquery.cookie.js') !!}
  <!-- ================== END BASE JS ================== -->
  
  <!-- ================== BEGIN PAGE LEVEL JS ================== -->
  {!! Html::script('assets/js/demo.min.js') !!}
  {!! Html::script('assets/js/apps.min.js') !!}
  <!-- ================== END PAGE LEVEL JS ================== -->
  
  <script>
    $(document).ready(function() {
        App.init();
        Demo.initThemePanel();
    });
  </script>
</body>
</html>
