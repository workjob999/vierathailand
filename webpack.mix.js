let mix = require('laravel-mix');
var readDir = require('readdir');

try {
    window.$ = window.jQuery = require('jquery');
    window.Popper = Popper;
    require('bootstrap');
} catch (e) {}

var folderSource = "resources/assets";
var folderDestination = "public/dist";
var pathWeb = "http://viera.local";

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */


	// mix.js(folderSource+'/js/main.js', folderDestination+'/js')
	// 	.combine([folderSource+'/js/main/*'], folderDestination+'/js/main.js')
	// 	.minify(folderDestination+'/js/main.js')
 //        .copyDirectory(folderSource+'/img', folderDestination+'/img');

 mix.copyDirectory(folderSource+'/sass/fonts', folderDestination+'/css/fonts');

 mix.autoload({
     jquery: ['$', 'window.jQuery', 'jQuery'],
     'popper.js/dist/umd/popper.js': ['Popper']
 })

const pathFolders = folderSource+"/sass";
var filename = "";
var split_filetype = "";
var fileArray = readDir.readSync(pathFolders,['**.sass']);
var pathFolderOther = "";
    for (var i in fileArray){
        var split_file = fileArray[i].split("/");
        pathFolderOther = "";
        filename = "";
		split_filetype = "";

        if (split_file.length>1)
        {
            for (var j=0;j<split_file.length-1;j++)
            {
                pathFolderOther += "/"+split_file[j];
            }
        }

        split_filetype = split_file[split_file.length-1].split(".");
        filename = split_filetype[0];

        var name = pathFolders+'/'+fileArray[i];
        //console.log(name+"=="+filename+"=="+"public/dist/css"+pathFolderOther+"/"+filename+".css");
        mix.sass(name, folderDestination+'/css'+pathFolderOther)
            .options({
                processCssUrls: false
            })
         	.minify(folderDestination+"/css"+pathFolderOther+"/"+filename+".css");
    }


pathFolder = folderSource+"/js";
filename = "";
split_filetype = "";
fileArray = readDir.readSync(pathFolder,['**.js']);
pathFolderOther = "";
    for (var i in fileArray){
        split_file = fileArray[i].split("/");
        pathFolderOther = "";
        filename = "";
		split_filetype = "";

        if (split_file.length>1)
        {
            for (var j=0;j<split_file.length-1;j++)
            {
                pathFolderOther += "/"+split_file[j];
            }
        }

        split_filetype = split_file[split_file.length-1].split(".");
        filename = split_filetype[0];

        name = pathFolder+'/'+fileArray[i];
        //console.log(name+"=="+filename+"=="+"public/dist/css"+pathFolderOther+"/"+filename+".css");
        mix.js(name, folderDestination+'/js'+pathFolderOther)
            .minify(folderDestination+"/js"+pathFolderOther+"/"+filename+".js");
    }

mix.browserSync({
    proxy: pathWeb,
    port:'3002'
});


